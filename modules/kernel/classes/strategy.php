<?php defined('SYSPATH') or die('No direct script access.');
/** 
 * Cloud平台的策略模式
 * @ClassName: Strategy
 *
 * @author bzhao
 *
 * @version $Id: strategy.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
abstract class Strategy
{
   /**
     * 默认异常对象名称
     *
     * @var string
     */
    const DEFAULT_EXCEPTION = 'Kohana_Exception';//Cloud_Exception
    
    const NAME_PREFIX = 'Strategy_';
    
	protected $_config;

	final public static function factory($model, $config = NULL)
	{
	    if (Kohana::auto_load($clsname = self::NAME_PREFIX.$model))
        {
            return new $clsname($config);
        } 
        else 
        {
           	echo $clsname;
            $exception = self::DEFAULT_EXCEPTION;
            throw new $exception(sprintf('未找到名称为 "%s" 的 Strategy', $clsname));
        }
		/*		
	 	if ( ! in_array($model,array_map('ucfirst',Kohana::config("cloud.strategy_available"))))
		{
			$exception = self::DEFAULT_EXCEPTION;
			throw new $exception(sprintf('未找到名称为 "%s" 的 Strategy', $model));
		}
		*/

	}
   
	protected function __construct($config = NULL)
	{
		$this->_config = $config; 
	}
}
