<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @ClassName: Model_Role
 *
 * @author bzhao
 *
 * @version $Id: role.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Role extends ORM
{	
	protected $_belong_to = array(
		'user' => array('model' => 'user'),
	);
	
	protected $_filters = array(
		TRUE => array('trim' => NULL)
	);
	
	protected $_rules = array(
    	'resource_id'	=> 	array('not_empty' => NULL,'max_length' => array(255)),//name
    	'resource_type'	=>  array('not_empty' => NULL),
    	'role_description'	=>	array('not_empty' => NULL),
		'role_schema'	=>	array('not_empty' => NULL),
		'role_link'		=>	array('not_empty' => NULL),
    );

}
