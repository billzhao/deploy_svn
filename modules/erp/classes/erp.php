<?php

class ERP
{
    const ORDER_CREATE = 0x01;
    const ORDER_UPDATE = 0x02;
    const ORDER_DELETE = 0x04;

    const ORDER_LINE_CREATE = 0x08;
    const ORDER_LINE_UPDATE = 0x10;
    const ORDER_LINE_DELETE = 0x20;

    const ORDER_GL = 0x40;

    const STATUS_NEW = 0x01;
    const STATUS_RUN = 0x02;
    const STATUS_DONE = 0x03;
    const STATUS_FAIL = 0x04;

    public static $error = '';

    public static function create_order($order)
    {
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'CREATE';
        $params['details'] = array();
        $products = $order->products();
        foreach ($products as $product)
        {
            // skip cancelled line
            if ($product['status'] == 'cancel')
                continue;

            $line = self::_create_order_item_params($product);
            $line['operation'] = 'CREATE';

            $params['details'][] = $line;
        }

        return self::enqueue_task(array(
            'type' => self::ORDER_CREATE | self::ORDER_LINE_CREATE, 
            'option' => 'processB2COrder', 
            'params' => json_encode($params), 
        ));
    }

    public static function sync_order($order)
    {
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'CREATE';
        $params['details'] = array();
        $products = $order->products();
        foreach ($products as $product)
        {
            // skip cancelled line
            if ($product['status'] == 'cancel')
                continue;

            $line = self::_create_order_item_params($product);
            $line['operation'] = 'CREATE';

            $params['details'][] = $line;
        }

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        $ret = self::_is_success($result);
        if (!$ret)
            return $ret;

        // record erp line id
        foreach ($result['details'] as $line)
        {
            $order->update_item($line['originalLineId'], array(
                'erp_line_id' => $line['lineId'], 
            ));
        }

        return $order->set(array(
            'erp_header_id' => $result['headerId'], 
            'erp_customer_id' => $result['customer_id'], 
            'erp_fee_line_id' => $result['feeLineId'], 
            'erp_otherfee_line_id' => $result['otherFeeLineId'], 
            'erp_ship_line_id' => $result['shipLineId'], 
        ));
    }

    public static function order_gl($order)
    {
        $order_data = $order->get();
        $params = self::_create_order_params($order_data);
        $params['headerId'] = $order->get('erp_header_id');
        $old_timezone = date_default_timezone_get();
        date_default_timezone_set('Asia/Shanghai');
        $params['orderedDate'] = date('Y-m-d H:i:s', $order_data['payment_date'] ? $order_data['payment_date'] : $order_data['created']);
        date_default_timezone_set($old_timezone);
        $products = $order->products();
        foreach ($products as $product)
        {
            // skip cancelled line
            if ($product['status'] == 'cancel')
                continue;

            $line = self::_create_order_item_params($product);
            $line['lineId'] = $product['erp_line_id'];
            $params['details'][] = $line;
        }

        return self::enqueue_task(array(
            'type' => self::ORDER_GL, 
            'option' => 'orderGL', 
            'params' => json_encode($params), 
        ));
    }

    public static function update_order($order_data)
    {
        $params = self::_create_order_params($order_data);
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order_data['erp_header_id'];
        $params['details'] = array();

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function verify_order($order)
    {
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order->get( 'erp_header_id' );
        $params['details'] = array();
        $products = $order->products();
        foreach ($products as $product)
        {
            // skip cancelled line
            if ($product['status'] == 'cancel')
                continue;

            $line = self::_create_order_item_params($product);
            $line['operation'] = 'UPDATE';
            $line['lineId'] = $product['erp_line_id'];

            $params['details'][] = $line;
        }

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);
        return self::_is_success($result);
    }

    public static function cancel_order($order_data)
    {
        $params = self::_create_order_params($order_data);
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order_data['erp_header_id'];
        $params['cancelledFlag'] = 'Y';
        $params['details'] = array();

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function update_order_task($order_data)
    {
        $params = self::_create_order_params($order_data);
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order_data['erp_header_id'];
        $params['details'] = array();

        return self::enqueue_task(array(
            'type' => self::ORDER_UPDATE, 
            'option' => 'processB2COrder', 
            'params' => json_encode($params), 
        ));
    }

    public static function create_order_item($item)
    {
        $order = Order::instance($item['order_id']);
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order->get('erp_header_id');

        $line = self::_create_order_item_params($item);
        $line['operation'] = 'CREATE';
        $params['details'][] = $line;

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function update_order_item($item)
    {
        $order = Order::instance($item['order_id']);
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order->get('erp_header_id');

        $line = self::_create_order_item_params($item);
        $line['operation'] = 'UPDATE';
        $line['lineId'] = $item['erp_line_id'];
        $params['details'] = array($line);

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function delete_order_item($item)
    {
        $order = Order::instance($item['order_id']);
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order->get('erp_header_id');

        $line = self::_create_order_item_params($item);
        $line['operation'] = 'DELETE';
        $line['lineId'] = $item['erp_line_id'];
        $params['details'] = array($line);

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function cancel_order_item($item)
    {
        $order = Order::instance($item['order_id']);
        $params = self::_create_order_params($order->get());
        $params['operation'] = 'UPDATE';
        $params['headerId'] = $order->get('erp_header_id');
        if (isset($item['customize']) && isset($item['customize']['lens_price'])) 
        {
            $params['otherFee'] -= 
                $item['customize']['lens_price'] * $item['quantity'];
        }

        $line = self::_create_order_item_params($item);
        $line['operation'] = 'UPDATE';
        $line['lineId'] = $item['erp_line_id'];
        $line['cancelledFlag'] = 'Y';
        $line['cancelledQuantity'] = $item['quantity'];
        $params['details'] = array($line);

        $response = self::call('processB2COrder', 
            Site::instance()->erp_domain(), json_encode($params));
        $result = json_decode($response, TRUE);

        return self::_is_success($result);
    }

    public static function call($option, $domain, $params)
    {
        $qs = array(
            'option' => $option, 
            'domain' => $domain, 
            'params' => $params, 
        );

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => Kohana::config('erp.server').'?'.http_build_query($qs), 
            CURLOPT_RETURNTRANSFER => TRUE, 
            CURLOPT_TIMEOUT => 60, 
        ));

        $fp = fopen($_SERVER['COFREE_LOG_DIR'].'/erp_call.log', 'a');
        fwrite($fp, date('Y-m-d H:i:s')." Request: $params\n");
        $response = curl_exec($ch);
        if (curl_errno($ch))
            return curl_errno($ch);

        fwrite($fp, date('Y-m-d H:i:s')." Response: $response\n");
        fclose($fp);
        return $response;
    }

    public static function enqueue_task($task)
    {
        $task += array(
            'site_id' => Site::instance()->get('id'), 
            'retry' => 0, 
            'status' => self::STATUS_NEW, 
            'server' => Kohana::config('erp.server'), 
            'domain' => Kohana::config('erp.domain'), 
            'result' => '', 
            'created' => time(), 
            'updated' => time(), 
        );

        return DB::insert('erp_task_queue', array_keys($task))
            ->values(array_values($task))
            ->execute();
    }

    public static function do_task()
    {
        $tasks = DB::select()
            ->from('erp_task_queue')
            ->where('status', '=', self::STATUS_NEW)
            ->where('retry', '<', 5)
            ->order_by('created', 'asc')
            ->order_by('retry', 'asc')
            ->execute();
        foreach ($tasks as $task)
        {
            $response = self::call($task['option'], $task['domain'], $task['params']);
            DB::update('erp_task_queue')
                ->set(array('result' => $response, 'updated' => time()))
                ->where('id', '=', $task['id'])
                ->execute();

            $result = json_decode($response, TRUE);
            if (!$result || !isset($result['retCode']) || $result['retCode'] != 'S')
            {
                self::task_retry_inc($task['id']);
                if ($task['retry'] >= 3)
                    self::task_update_status($task['id'], self::STATUS_FAIL);

                continue;
            }

            if ($task['type'] & self::ORDER_CREATE)
            {
                $order = Order::get_from_ordernum($result['orderNumber']);
                $order = Order::instance($order);
                if ($order)
                {
                    $order->set(array(
                        'erp_header_id' => $result['headerId'], 
                        'erp_customer_id' => $result['customer_id'], 
                        'erp_fee_line_id' => $result['feeLineId'], 
                        'erp_otherfee_line_id' => $result['otherFeeLineId'], 
                        'erp_ship_line_id' => $result['shipLineId'], 
                    ));
                }
            }

            if ($task['type'] & self::ORDER_LINE_CREATE)
            {
                $order = Order::get_from_ordernum($result['orderNumber']);
                $order = Order::instance($order);
                if ($order)
                {
                    foreach ($result['details'] as $line)
                    {
                        $order->update_item($line['originalLineId'], array(
                            'erp_line_id' => $line['lineId'], 
                        ));
                    }
                }
            }

            self::task_update_status($task['id'], self::STATUS_DONE);
        }

        return TRUE;
    }

    public static function task_update($id, $data)
    {
        return DB::update('erp_task_queue')
            ->set($data)
            ->where('id', '=', $id)
            ->execute();
    }

    public static function task_update_status($id, $status)
    {
        return self::task_update($id, array('status' => $status));
    }

    public static function task_retry_inc($id)
    {
        return DB::query(Database::UPDATE, 
            "UPDATE erp_task_queue SET retry = retry + 1 WHERE id = $id")
            ->execute();
    }

    protected static function _create_order_params($order_data)
    {
        $old_timezone = date_default_timezone_get();
        date_default_timezone_set('Asia/Shanghai');
        $params = array(
            'customer_email' => $order_data[ 'email' ], 
            'customer_id' => $order_data['erp_customer_id'], 
            'orderNumber' => $order_data[ 'ordernum' ], 
            'ship_fname' => $order_data[ 'shipping_firstname' ], 
            'ship_lname' => $order_data[ 'shipping_lastname' ], 
            'ship_phone' => $order_data[ 'shipping_phone' ], 
            'ship_mobile' => $order_data[ 'shipping_mobile' ], 
            'ship_country' => $order_data[ 'shipping_country' ], 
            'ship_city' => $order_data[ 'shipping_city' ], 
            'ship_state' => $order_data[ 'shipping_state' ], 
            'ship_zip' => $order_data[ 'shipping_zip' ], 
            'ship_address' => $order_data[ 'shipping_address' ], 
            'bill_fname' => $order_data[ 'billing_firstname' ], 
            'bill_lname' => $order_data[ 'billing_lastname' ], 
            'bill_phone' => $order_data[ 'billing_phone' ], 
            'bill_mobile' => $order_data[ 'billing_mobile' ], 
            'bill_country' => $order_data[ 'billing_country' ], 
            'bill_city' => $order_data[ 'billing_city' ], 
            'bill_state' => $order_data[ 'billing_state' ], 
            'bill_zip' => $order_data[ 'billing_zip' ], 
            'bill_address' => $order_data[ 'billing_address' ], 
            'shippingFee' => $order_data['amount_shipping'], 
            'currencyCode' => $order_data['currency'], 
            'feeLineId' => $order_data['erp_fee_line_id'], 
            'otherFeeLineId' => $order_data['erp_otherfee_line_id'], 
            'shipLineId' => $order_data['erp_ship_line_id'], 
            'createdDate' => date('Y-m-d H:i:s', $order_data['created']), 
            'orderedDate' => date('Y-m-d H:i:s', $order_data['payment_date'] ? $order_data['payment_date'] : $order_data['created']),
            'attr1' => ($order_data['payment_status'] == 'verify_pass' || $order_data['amount'] == 0)
                ? 'Y'
                : 'N',
        );
        date_default_timezone_set($old_timezone);
        $products = Order::instance($order_data['id'])->products();
        $other_fee = 0;
        foreach ($products as $product)
        {
            if ($product['status'] == 'cancel')
                continue;
            if (isset($product['customize']['lens_price']))
                $other_fee += $product['customize']['lens_price'] * $product['quantity'];
        }

        $params['otherFee'] = $other_fee;
        return $params;
    }

    protected static function _create_order_item_params($item_data)
    {
        $order = Order::instance($item_data['order_id']);
        $line = array(
            'inventoryItemId' => Product::instance($item_data['item_id'])
                ->get('erp_item_id'), 
            'orderedQuantity' => $item_data['quantity'], 
            'unitSellingPrice' => $item_data['price'], 
            'originalLineId' => $item_data['id'], 
        );

        if ($item_data['customize'])
        {
            $lens_config = Kohana::config('glasseslens');
            $customize = $item_data['customize'];
            $line['otherFee'] = $customize['lens_price'];
            $line['unitSellingPrice'] -= $line['otherFee'];

            // prescription
            $line['attr1'] = isset($customize['od_sph']) && isset($customize['os_sph'])
                ? "R:{$customize['od_sph']};L:{$customize['os_sph']}"
                : "";
            $line['attr2'] = isset($customize['od_cyl']) && isset($customize['os_cyl'])
                ? "R:{$customize['od_cyl']};L:{$customize['os_cyl']}"
                : "";
            $line['attr3'] = isset($customize['od_axis']) && isset($customize['os_axis'])
                ? "R:{$customize['od_axis']};L:{$customize['os_axis']}"
                : "";
            $line['attr4'] = isset($customize['od_add']) && isset($customize['os_add'])
                ? "R:{$customize['od_add']};L:{$customize['os_add']}"
                : "";

            if (isset($customize['pd_near']) && $customize['pd_near'])
            {
                $pd_near_r = $pd_near_l = ($customize['pd_near'] / 2);
                $pd_far_r = $pd_far_l = '';
            }
            else if (isset($customize['pd_far']) && $customize['pd_far'])
            {
                $pd_far_r = $pd_far_l = ($customize['pd_far'] / 2);
                $pd_near_r = $pd_near_l = '';
            }
            else
            {
                if ($customize['lensType'] == 2)
                {
                    // Single vision reading only
                    $pd_near_r = $customize['pd_r'];
                    $pd_near_l = $customize['pd_l'];
                    $pd_far_r = $pd_far_l = '';
                }
                else
                {
                    $pd_far_r = $customize['pd_r'];
                    $pd_far_l = $customize['pd_l'];
                    $pd_near_r = $pd_near_l = '';
                }
            }

            $line['attr5'] = "R:$pd_far_r;L:$pd_far_l";
            $line['attr6'] = "R:$pd_near_r;L:$pd_near_l";
            $line['attr7'] = isset($lens_config['lens'][$customize['lensType']])
                ? $lens_config['lens'][$customize['lensType']]['name']
                : "";
            $line['attr8'] = isset($customize['specialRequirement'])
                ? $customize['specialRequirement']
                : "";
            $line['attr9'] = isset($lens_config['lens_index'][$customize['lens_index']])
                ? $lens_config['lens_index'][$customize['lens_index']]['name']
                : "";
            $line['attr10'] = isset($customize['lens_index_color'])
                ? $customize['lens_index_color']
                : "";
            $line['attr11'] = isset($customize['color_tint'])
                ? $customize['color_tint']
                : "";
            $line['attr12'] = isset($customize['photochromic_color'])
                ? $customize['photochromic_color']
                : "";
            $line['attr13'] = isset($customize['transition_color'])
                ? $customize['transition_color']
                : "";
            $line['attr14'] = isset($customize['anti']) && $customize['anti']
                ? "Anti-reflective"
                : "";
            $line['attr15'] = "R:1;L:1";
            $line['attr16'] = isset($customize['frame']) && $customize['frame']
                ? $customize['frame']
                : $item_data['sku'];
            $line['attr17'] = isset($customize['supplier']) 
                ? $customize['supplier']
                : "";
            /*
            if ($item_data['sku'] == 'GSFVF001' || (isset($customize['frame']) && $customize['frame'] == 'GSFVF001'))
            {
                $line['attr17'] = '只割不装';
            }
             */
            $line['attr18'] = date('Y-m-d H:i:s', $order->get('verified_at'));
        }
        else
        {
            $line['attr17'] = 'Frames Only';
            $line['attr18'] = date('Y-m-d H:i:s', $order->get('created'));
        }

        return $line;
    }

    protected static function _is_success($result)
    {
        if (!$result 
            || !isset($result['retCode']) 
            || $result['retCode'] != 'S')
        {
            if (isset($result['retMsg']))
                self::$error = $result['retMsg'];
            else
                self::$error = 'ERP通信失败';

            return FALSE;
        }

        return $result;
    }
}
