<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 配件
 *
 * @ClassName: Controller_Part
 *
 * @author bzhao
 *
 * @version $Id: part.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_Part extends Controller_Webpage
{
	public function action_detail($id = 0)
	{
	 	$id = $this->request->param('id');
	
      	$product = Service_Channel_Product::instance($id);
       
       	if ( ! $product->get('id') || $product->get('type') != self::PART)
       	{
       		$this->request->redirect('/Parts-order');
       	}
       	
       	
       	
       	$catalog = Service_Channel_Catalog::instance(16);
       
      	$this->request->response = View::factory('part_detail')
      		 ->set('product', $product)
      		 ->set('catalog', $catalog)
     		 ->render();
	}

	/*
	 * 通过 Catalog解决
	 */
	
//	public function action_list()
//	{
//		
//		Service_Channel_Catalog::instance($id)->products();
//		
//		$this->request->response = View::factory('part_list')
//      		 ->set('data', $data)
//     		 ->render();
//	}

}
