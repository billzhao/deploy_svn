<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Project extends Controller_Base
{
    public function action_list()
    {
        $this->template->content = View::factory('project_list')
            ->set('projects', $this->user->projects->find_all()->as_array());
    }

    public function action_add()
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        $post = array(
            'name' => '', 
            'svn_path' => '', 
            'rsync_module' => 'www', 
            'description' => '', 
        );

        if ($_POST) {
            $project = ORM::factory('project');
            $project->values($_POST + array(
                'created' => time(), 
                'updated' => time(), 
            ));

            if ($project->check()) {
                $project->save();
                $this->user->add('projects', $project);

                if (isset($_POST['servers'])) {
                    foreach ($_POST['servers'] as $sid)
                        $project->add('servers', ORM::factory('server', $sid));
                }

                $this->request->redirect('/project/list');
            }

            $post = Arr::overwrite($post, $_POST);
        }

        $this->template->content = View::factory('project_add')
            ->set('servers', ORM::factory('server')->find_all()->as_array())
            ->set('post', $post);
    }

    public function action_edit($id)
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        if (isset($_POST['submit'])) {
            $project->name = trim($_POST['name']);
            $project->svn_path = trim($_POST['svn_path']);
            $project->rsync_module = trim($_POST['rsync_module']);
            $project->description = trim($_POST['description']);
            $project->updated = time();
            if ($project->check()) {
                $project->save();
            } else {
                die("invalid form data.");
            }

            foreach ($project->get_checked() as $sid) 
                $project->remove('servers', ORM::factory('server', $sid));

            if (isset($_POST['servers'])) {
                foreach ($_POST['servers'] as $sid)
                    $project->add('servers', ORM::factory('server', $sid));
            }

            $this->request->redirect('/project/list');
        }

        $this->template->content = View::factory('project_edit')
            ->set('project', $project)
            ->set('servers', ORM::factory('server')->find_all()->as_array())
            ->set('checked', $project->get_checked());
    }

    public function action_delete($id)
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }
    
        $project->delete_user_rel();
        $project->delete_server_rel();
        $project->delete();

        $this->request->redirect('/project/list');
    }

    public function action_target($id)
    {
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        if ($_POST) {
            if (!isset($_POST['target']) || !is_numeric($_POST['target'])) {
                die("invalid target");
            }

            $target = ORM::factory('server', $_POST['target']);
            if (!$target->loaded()) {
                die(header('HTTP/1.1 404 Not Found'));
            }

            Session::instance()->set('rev', $_POST['rev']);
            Session::instance()->set('branch', $_POST['branch']);
            Session::instance()->set('target', $target->as_array());
            $this->request->redirect("/project/checkout/$id");
        }

        $this->template->content = View::factory('project_target')
            ->set('id', $id)
            ->set('servers', $project->servers->find_all()->as_array());
    }

    public function action_checkout($id)
    {
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        $svn_command = Kohana::config('deploy.svn_path');
        $svn_options = Kohana::config('deploy.svn_option');
        $svn_username = Kohana::config('deploy.svn_username');
        $svn_password = Kohana::config('deploy.svn_password');

        $path = $project->checkout_path();
        $rev = Session::instance()->get('rev', 'HEAD');
        $branch = Session::instance()->get('branch', 'trunk');
        $svn_path = str_replace('{branch}', $branch, $project->svn_path);
        exec("env - LC_ALL=zh_CN.utf8 $svn_command $svn_options --username $svn_username --password $svn_password -r $rev co $svn_path $path 2>&1", $output, $ret);
        if ($ret != 0) {
            die(implode('<br>', $output));
        }

        $this->request->redirect("/project/explore/$id");
    }

    public function action_explore($id)
    {
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        $base = isset($_GET['base']) ? $_GET['base'] : '';
        $path = $project->checkout_path() . $base;
        if (!is_dir($path)) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        $entries = array();
        foreach (scandir($path) as $entry) {
            if ($entry == '.' || $entry == '..' || $entry == '.svn') {
                continue;
            }

            $entries[] = array(
                'name' => $entry, 
                'type' => is_dir("$path/$entry") ? 'dir' : 'file', 
                'path' => "$path/$entry", 
            );
        }

        $this->template->content = View::factory('project_explore')
            ->set('id', $id)
            ->set('base', $base)
            ->set('target', Session::instance()->get('target'))
            ->set('entries', $entries);
    }

    public function action_deploy($id)
    {
        $project = ORM::factory('project', $id);
        if (!$project->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        if (!$this->user->has('projects', $project)) {
            die(header('HTTP/1.1 403 Forbidden'));
        }

        $base = isset($_POST['base']) ? $_POST['base'] : '';
        $path = $project->checkout_path() . $base;
        if (!is_dir($path)) {
            die(header('HTTP/1.1 404 Not Found'));
        }

        $src = array();
        foreach ($_POST['files'] as $file) {
            $src[] = escapeshellarg("$path/$file");
        }

        $target = Session::instance()->get('target');
        $src = implode(' ', $src);
        $dst = long2ip($target['ip']) . "::{$project->rsync_module}";

        $rsync_command = Kohana::config('deploy.rsync_path');
        $rsync_options = Kohana::config('deploy.rsync_option');

        exec("env - RSYNC_PASSWORD=tuniu520 $rsync_command $rsync_options $src ftwbzhao@$dst 2>&1", $output, $ret);
        if ($ret != 0) {
            die(implode('<br>', $output));
        }

        $log = ORM::factory('log');
        $log->action = 'deploy';
        $log->message = $this->user->username.'('.$this->user->id.')'." delpoy project $project->name($project->id) to {$target['name']}";
        $log->created = time();
        $log->save();

        $this->template->content = implode('<br>', $output)
            . '<br><span style="color:green">Deploy Successfully</span>';
    }
}
