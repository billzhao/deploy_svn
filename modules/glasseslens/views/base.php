<?php echo View::factory(Site::instance()->get('id').'/view/header')->render(); ?>
<link href="/css/lens.css" type="text/css" rel="stylesheet"/>
<div id="mainbody">
    <div class="wrapperLens">
        <div class="shopDetails sectionLens">
            <h3 class="cT">Your Options &amp; Prescription</h3>
            <div class="cC">
                <div class="inner">
                    <div class="pic"><img style="padding-top:17px;" src="<?php echo Image::link(Product::instance($product['id'])->cover_image(), 0);?>" alt="<?php echo $product['name'];?>" /></div>
                    <div class="details">
                        <h2><?php echo $product['name'];?></h2>
                        <div>
                            <span >SKU:<strong><?php echo $product['sku'];?></strong></span>
                            <span>Material:<strong><?php echo $attributes[kohana::config('glasseslens_attributes.material_id')]['value'];?></strong></span>
                        </div>
                        <div>
                            <span >Frame Styles:<strong><?php echo $attributes[kohana::config('glasseslens_attributes.circle_id')]['value'];?></strong></span>
                            <span>Color:<strong ><?php echo $attributes[kohana::config('glasseslens_attributes.color_id')]['value'];?></strong></span>
                        </div>
                        <div class="bgf4">
                            <span>Retail Price: <strong><?php echo Site::instance()->price($product['market_price'], 'code_view');?></strong></span>
                            <span class="ourPrice">Our Price:
                                <?php if ($product['discount_price']<$product['price']){?>
                                <s><?php echo Site::instance()->price($product['price'], 'code_view');?></s>
                                <strong><?php echo Site::instance()->price($product['discount_price'], 'code_view');?></strong>
                                <?php }else{?>
                                <strong><?php echo Site::instance()->price($product['price'], 'code_view');?></strong>
                                <?php }?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="lens_error" style="display:none;">
            <strong style="padding:6px 0 6px 6px;" id="liErrorTitle">Attention:</strong>
            <ul style="padding:6px 6px 6px 0;"><li id="liErrorMsg"></li></ul>
            <span><a href="javascript:void(0)" onclick="closeMsg();"><img src="/images/close.gif" width="9" height="9" /></a></span>
        </div>
        <form name="glasseslens" method="post">
            <input type="hidden" name="id" value="<?php echo $gproduct['id']; ?>"/>
            <input type="hidden" name="items[]" value="<?php echo $product['id']; ?>"/>
            <input type="hidden" name="quantity" value="1"/>
            <div class="enterPres sectionLens">
                <h3 class="cT" title="Enter Prescription">
                    <?php echo View::factory(Site::instance()->get('id').'/view/livechat_lens')->render(); ?>
                    Enter Prescription<ins id="Tip_enterPre" class="r"></ins>
                </h3>
                <div class="cC">
                    <div class="inner">
                        <div class="formTip">
                            <a class="directlyUpload" href="javascript:void(0);" onclick="showUpload()">Directly Upload Your Prescription</a>To use a previous prescription, please <a href="javascript:" onclick="prescription()"><img src="/images/clickhare.gif" alt="click hare!" /></a>
                            <input type='hidden' id='upload_file' name="upload_file" value='' />
                        </div>
                        <div class="itemEnterPres">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td4">Select your lens type:</td>
                                    <td class="td5">
                                        <select onchange="lens_change(this)" name="lens" id="lens">
                                            <?php foreach($lens as $k => $v){?>
                                            <option value="<?php echo $k;?>"><?php echo $v['name']; echo $v['price'] ? ' ('.Site::instance()->price($v['price'], 'code_view').')' : __(' (Free)');?></option>
                                            <?php }?>
                                        </select>
                                        <ins class="r" id="Tip_lensType"></ins>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="itemEnterPres">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td4">&nbsp;</td>
                                    <td class="td6">Sphere (SPH)<ins id="Tip_sph" class="r"></ins></td>
                                    <td class="td6">Cylinder (CYL)<ins id="Tip_cyl" class="r"></ins></td>
                                    <td class="td6">Axis<ins id="Tip_axis" class="r"></ins></td>
                                    <td class="td6">Addition (near) ADD<ins id="Tip_add" class="help"></ins></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="td4">Right Eye (OD):</td>
                                    <td class="td6">
                                        <select name="od_sph" id="od_sph" class="selectEyeOD" title="Please pay attention to the sign(-/+)">
                                            <?php foreach($sph as $v){?>
                                            <option value="<?php echo $v;?>" <?php echo $v == 0 ? 'selected':'';?>><?php echo $v==0 ? 'Plano' : (($v>0 ? '+' : '').number_format($v, 2, '.', ''));?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="td6">
                                        <select name="od_cyl" id="od_cyl" class="selectEyeOD" onchange="linkChange(this,'od')" title="Please pay attention to the sign(-/+)">
                                            <option value="0" selected>Cylinder</option>
                                            <?php foreach($cyl as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo ($v>0 ? '+' : '').number_format($v, 2, '.', '');?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="td6">
                                        <select name="od_axis" id="od_axis" disabled onchange="check_form()">
                                            <option value="0" selected>Axis</option>
                                            <?php foreach($axis as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="td6">
                                        <select name="od_add" id="od_add" onchange="check_form()" disabled>
                                            <option value="0" selected>(ADD) Addition</option>
                                            <?php foreach($add as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo ($v>0 ? '+' : '').number_format($v, 2, '.', '');?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="td4">Left Eye (OS):</td>
                                    <td class="td6">
                                        <select name="os_sph" id="os_sph" class="selectEyeOD" title="Please pay attention to the sign(-/+)">
                                            <?php foreach($sph as $v){?>
                                            <option value="<?php echo $v;?>" <?php echo $v == 0 ? 'selected':'';?>><?php echo $v==0 ? 'Plano' : (($v>0 ? '+' : '').number_format($v, 2, '.', ''));?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="td6">
                                        <select name="os_cyl" id="os_cyl" class="selectEyeOD" onchange="linkChange(this,'os')" title="Please pay attention to the sign(-/+)">
                                            <option value="0" selected>Cylinder</option>
                                            <?php foreach($cyl as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo ($v>0 ? '+' : '').number_format($v, 2, '.', '');?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="os_axis" id="os_axis" disabled onchange="check_form()" >
                                            <option value="0" selected>Axis</option>
                                            <?php foreach($axis as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo $v;?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="td6">
                                        <select class="selectEyeOD"  name="os_add" id="os_add" onchange="check_form()" disabled>
                                            <option value="0" selected>(ADD) Addition</option>
                                            <?php foreach($add as $v){?>
                                            <option value="<?php echo $v;?>"><?php echo ($v>0 ? '+' : '').number_format($v, 2, '.', '');?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <div class="itemEnterPres">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="td4">Pupillary Distance (PD):<ins class="r" id="Tip_pd" name="Tip_pd"></ins></td>
                                    <td class="td5">
                                        <span id="onePd">
                                            <select name="pd" id="pd" onchange="check_form()">
                                                <option value="" selected>Far</option>
                                                <?php foreach($pd as $v){?>
                                                <option value="<?php echo $v;?>"><?php echo number_format($v, 1, '.', '');?></option>
                                                <?php }?>
                                            </select>
                                            <select name="near_pd" id="near_pd"  style="display:none" onchange="check_form()">
                                                <option value="" selected>Near PD</option>
                                                <?php foreach($near_pd as $v){?>
                                                <option value="<?php echo $v;?>"><?php echo number_format($v, 1, '.', '');?></option>
                                                <?php }?>
                                            </select>
                                        </span>
                                        <span id="twoPd" style="display:none">
                                            <select name="mono_l" id="mono_l" onchange="check_form()">
                                                <option value="" selected>Left PD</option>
                                                <?php foreach($mono as $v){?>
                                                <option value="<?php echo $v;?>"><?php echo number_format($v, 1, '.', '');?></option>
                                                <?php }?>
                                            </select>
                                            <select name="mono_r" id="mono_r" onchange="check_form()">
                                                <option value="" selected>Right PD</option>
                                                <?php foreach($mono as $v){?>
                                                <option value="<?php echo $v;?>"><?php echo number_format($v, 1, '.', '');?></option>
                                                <?php }?>
                                            </select>
                                        </span>
                                        <label>
                                            <input type="checkbox" id="pdButton" onclick="changePd(this)"/>
                                            Click here if you have two PD numbers</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td4">Special Requirements:</td>
                                    <td class="td5"><textarea class="specialRequirements" cols="100" rows="3" name="message" id="message"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lensThi sectionLens">
                <h3 class="cT" title="Lens Thickness">Lens Thickness<ins id="Tip_lensInd" class="r"></ins></h3>
                <div class="cC">
                    <div class="inner">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <?php foreach($lens_index as $k => $v){?>
                            <tr class="mytr">
                                <td class="td0"><input type="radio" name="lens_index" id="lens_<?php echo $k;?>" value="<?php echo $k;?>" onclick="lens_index_change(this)" /></td>
                                <td class="td1"><?php echo $v['name'];?></td>
                                <td class="td2">
                                <?php if ($v['color']) {?>
                                Select Color:
                                <ins id="Tip_plarized" class="help"><a target="_blank" href="/lens_and_coat#plarized_lens"><img border="0" src="/images/helpl.gif" style=""></a></ins>
                                <select onchange="changeBGColor(this)" id="lens_color" name="lens_color" disabled="disabled">
                                    <option selected="selected" value="0">Please Select</option>
                                    <?php foreach($v['color'] as $k => $c){?>
                                    <option value="<?php echo $k;?>" style="background:<?php echo $c['colorhex'];?>"><?php echo $c['name'];?></option>
                                    <?php }?>
                                </select>
                                <?php }?>
                                </td>
                                <td class="td3"><?php echo $v['price']?Site::instance()->price($v['price'], 'code_view'):'FREE';?></td>
                            </tr>
                            <?php }?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="eyePro sectionLens">
                <h3 class="cT" title="Eye Protection Add-on（s）">Eye Protection Add-on(s)
                    <ins id="Tip_specialAdd" class="r"></ins></h3>
                <div class="cC">
                    <div class="inner">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr class="mytr">
                                <td class="td0"><input type="checkbox" name="chb_color_tint" id="chb_color_tint" onclick="color_tint_click(this)" disabled /></td>
                                <td class="td1">Color Tint<ins id="Tip_colorTint" class="r"></ins></td>
                                <td class="td2">
                                    <label class="selectColor">Select Color:
                                        <select id="color_tint" name="color_tint" onchange="changeBGColor(this)" disabled>
                                            <option value="0" selected>Please Select</option>
                                            <?php foreach($color_tint['color'] as $k => $v){?>
                                            <option value="<?php echo $k;?>" style="background:<?php echo $v['colorhex'];?>"><?php echo $v['name'];?></option>
                                            <?php }?>
                                        </select>
                                    </label>
                                </td>
                                <td class="td3"><?php echo Site::instance()->price($color_tint['price'], 'code_view');?></td>
                            </tr>
                            <tr class="mytr">
                                <td class="td0"><input type="checkbox" value="275" name="chb_photochromic" id="chb_photochromic" onclick="photochromic_click(this)"/></td>
                                <td class="td1">Photochromic(Light-adjusting)<ins id="Tip_photochromic" class="r"></ins></td>
                                <td class="td2">
                                    <label class="selectColor">Select Color:
                                        <select name="photochromic_color" id="photochromic_color" onchange="changeBGColor(this)" disabled>
                                            <option value="0" selected>Please Select</option>
                                            <?php foreach($ptp['color'] as $k => $v){?>
                                            <option value="<?php echo $k;?>" style="background:<?php echo $v['colorhex'];?>"><?php echo $v['name'];?></option>
                                            <?php }?>
                                        </select>
                                    </label>
                                </td>
                                <td class="td3"><?php echo Site::instance()->price($ptp['price'], 'code_view');?></td>
                            </tr>
                            <tr class="mytr">
                                <td class="td0"><input type="checkbox" value="275" name="chb_transition" id="chb_transition" onclick="transition_click(this)" /></td>
                                <td class="td1"><img src="/images/transition.gif" alt="Transition" /><ins class="r" id="Tip_transitions"></ins></td>
                                <td class="td2">
                                    <label class="selectColor">Select Color:
                                        <select name="transition_color" id="transition_color" onchange="changeBGColor(this)" disabled>
                                            <option value="0" selected>Please Select</option>
                                            <?php foreach($ptv['color'] as $k => $v){?>
                                            <option value="<?php echo $k;?>" style="background:<?php echo $v['colorhex'];?>"><?php echo $v['name'];?></option>
                                            <?php }?>
                                        </select>
                                    </label>
                                </td>
                                <td class="td3"><?php echo Site::instance()->price($ptv['price'], 'code_view');?></td>
                            </tr>
                            <tr>
                                <td class="td0"><input class="checkbox" type="checkbox" name="anti" id="anti" onclick="javascript:calc_price();"/></td>
                                <td class="td1">Anti-Reflective<ins id="Tip_antiRef" class="r"></ins></td>
                                <td class="td2"></td>
                                <td class="td3"><?php echo Site::instance()->price($anti['price'], 'code_view');?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="actionBar">
                <div class="row1">
                    <div class="totalPrice">TOTAL:<strong><span id="fntTotal"><?php echo Site::instance()->price(Product::instance($product['id'])->get('price'), 'code_view');?></span></strong></div>
                    <div class="addToCartBtn"><a href="#lens_error" onclick="submit_form()"><img src="/images/addtocartbtn.gif"/></a></div>
                </div>
                <div class="row2"><a href="#" onclick="goonShopping()">Save and countinue shopping &gt;</a></div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
<?php
foreach(kohana::config('glasseslens.lens') as $k => $v)
    $lens_price[$k] = $v['price'];
echo 'lens_price = '.json_encode($lens_price).';';
foreach(kohana::config('glasseslens.lens_index') as $k => $v)
    $lens_index_price[$k] = $v['price'];
echo 'lens_index_price = '. json_encode($lens_index_price).';';
echo 'color_tint_price = '.kohana::config('glasseslens.color_tint.price').';';
echo 'photochromic_price = '.kohana::config('glasseslens.ptp.price').';';
echo 'transition_price = '.kohana::config('glasseslens.ptv.price').';';
echo 'anti_price = '.kohana::config('glasseslens.anti.price').';';
echo 'currency = '.json_encode(Site::instance()->currency()).';';
?>
frame_price='<?php echo ($product['price']>$product['discount_price'])?Site::instance()->price($product['discount_price']):Site::instance()->price($product['price']);?>';
var frame_bifocal = <?php echo $attributes[kohana::config('glasseslens_attributes.bifocal_id')]['value'];?>;
var frame_circle = '<?php echo $attributes[kohana::config('glasseslens_attributes.circle_id')]['value'];?>';
</script>
<script type="text/javascript" src="/glasseslens/media/js/lens.js"></script>
<script type="text/javascript" src="/glasseslens/media/js/tip.js"></script>
<?php echo View::factory(Site::instance()->get('id').'/view/footer')->render(); ?>