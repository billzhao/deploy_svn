<?php
defined('SYSPATH') or die('No direct script access.');

class Service_Merchant_Context extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'merchant_context';
    
    private $_data = NULL;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
        $this->_load($id);
	}

	public function tree($query_struct=array(),$select = array(),$key='',$value='')
	{
		return $this->dao($this->_dao_object)
					->db_find_all(
						$this->dao($this->_dao_object)->dbselect($select),
						$query_struct,
						$key,
						$value
					);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	public function add($data = array())
	{	
		$attributes=$this->tree(array('where'=>array('name'=>$data['name'],'merchant_id'=>$data['merchant_id'])),array('attribute'));
		$existence=array();
		foreach ($attributes as $attribute)
			$existence[]=$attribute['attribute'];
		$columns=Service_Merchant_Product::instance()->showColumnsArray();
		$i=1;
		while(true)
		{
			$attribute='attribute'.str_pad($i,2,'0',STR_PAD_LEFT);
			if(!in_array($attribute, $existence))
				break;
			if(!in_array($attribute, $columns))
			{
				Message::set("Need More Attribute Columns","error");
				return false;
			}
			$i++;
		}
		$data+=array('attribute'=>$attribute);
		return $this->dao($this->_dao_object)->add($data);
	}
	
	/**
	 * associate context to ERP 
	 * @param
	 */
	public static function associateErpContext($contextName, $merchantid, $erpId = null)
	{
		if (is_null($contextName) || is_null($merchantid))
			return FALSE;
		$ErpService = new ErpService(FALSE,'icebearatv.com');

		return $ErpService->syncContext($contextName, $merchantid, $erpId);
	}

	public function edit($data = array())
	{
		if(!$this->_data['id'])
		{
			Message::set('No context id');
			return false;
		}
		$data['id']=$this->_data['id'];
		if($this->dao($this->_dao_object)->edit($data))
		{
			Message::set('Edit context success');
			return true;
		}
		else
		{
			Message::set('Edit context fail');
			return false;
		}
	}
	
	public function delete($data = array())
	{
		if(!$this->_data['id'])
		{
			Message::set('No context id');
			return false;
		}
		if($this->dao($this->_dao_object)->delete($this->_data['id']))
			return true;
		else
			return false;
	}
	
	public function deleteName($query_struct)
	{
		return $this->dao($this->_dao_object)->delete($query_struct,false);
	}
	
	public function getContextNameArray($merchant_id=0)
	{
		if($merchant_id)
			$where=array('where'=>array('merchant_id'=>$merchant_id), 'orderby'=>'attribute');
		else 
			$where=array('orderby'=>'attribute');
		$attributes=$this->tree($where);
		$context_array=array();
		foreach($attributes as $attribute)
		{	
			$context_array[$attribute['name']][]=$attribute;
		}
		return $context_array;
	}
	
	public function getDistinctContaxtName($merchant_id=0)
	{
		if($merchant_id)
			$where=array('where'=>array('merchant_id'=>$merchant_id),'orderby'=>'attribute');
		else 
			$where=array('orderby'=>'attribute');
		$names=$this->tree($where,array('name'),'name');
		return $names?array_keys($names):array();
		
	}
	
    public function erpDomain()
    {
        return substr($this->get('domain'), 4);
    }

    public function erpEnabled()
    {
        return (bool)$this->get('erp_enabled');
    }
	
}
