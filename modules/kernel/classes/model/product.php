<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Product
 *
 * @author bzhao
 *
 * @version $Id: product.php 6317 2012-02-22 09:53:05Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Product extends ORM {

	protected $_belong_to = array(
		'merchant' => array('model' => 'merchant'),
		'catalog'  => array('model' => 'catalog'),
	);
	
	protected $_filters = array(
        TRUE => array('trim' => NULL)
    );

    protected $_rules = array(
        'merchant_id' => array(
            'not_empty' => NULL,
        ),
        'name' => array(
            'not_empty' => NULL,
            'max_length' => array(255),
        ),
        'sku' => array(
            'not_empty' => NULL,
            'max_length' => array(255),
        ),
        'link' => array(
            'max_length' => array(255),
        ),
        'sellable' => array(
            'not_empty' => NULL,
        ),
//        'price' => array(
//            'not_empty' => NULL,
//        ),
        'market_price' => array(
            'max_length' => array(65535),
        ),
        'cost' => array(
            'max_length' => array(65535),
        ),
        'total_cost' => array(
            'max_length' => array(65535),
        ),
        'stock' => array(
            'numeric' => NULL,
        ),
        'weight' => array(
            'numeric' => NULL,
        ),
        'brief' => array(
            'max_length' => array(65535),
        ),
        'description' => array(
            'max_length' => array(65535),
        ),
        'meta_title' => array(
            'max_length' => array(65535),
        ),
        'meta_keywords' => array(
            'max_length' => array(65535),
        ),
        'meta_description' => array(
            'max_length' => array(65535),
        ),
        'keywords' => array(
            'max_length' => array(65535)
        )
    );

}

