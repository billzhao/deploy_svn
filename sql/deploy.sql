-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2012 at 10:42 AM
-- Server version: 5.5.24
-- PHP Version: 5.4.9-4~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `deploy`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` enum('deploy') NOT NULL,
  `message` mediumtext NOT NULL,
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `action`, `message`, `created`) VALUES
(1, 'deploy', 'admin(1) delpoy project deploy_test(1) to COFREE_21', 1344938023),
(2, 'deploy', 'admin(1) delpoy project deploy_test(1) to COFREE_23', 1344938272),
(3, 'deploy', 'admin(1) delpoy project deploy_test(1) to COFREE_23', 1344938628),
(4, 'deploy', 'admin(1) delpoy project test_deploy(2) to COFREE_21', 1344942781),
(5, 'deploy', 'admin(1) delpoy project test_deploy(2) to Ay_OneMM', 1345018467),
(6, 'deploy', 'ay_saas(3) delpoy project test_deploy(2) to Ay_OneMM', 1345019059);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `svn_path` varchar(255) NOT NULL DEFAULT '',
  `rsync_module` varchar(32) NOT NULL DEFAULT 'www',
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  `updated` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `svn_path`, `rsync_module`, `created`, `updated`) VALUES
(1, 'deploy_test', '测试整个流程', 'http://svn.cofreeonline.com/code/sa/sites/deploy.cofreeonline.com', 'deploy.cofreeonline.com', 1344936695, 1344938012),
(2, 'test_deploy', 'Test Deploy', 'http://svn.a-y.com/myproject', 'test_deploy', 1344942691, 1345018519);

-- --------------------------------------------------------

--
-- Table structure for table `project_server`
--

CREATE TABLE IF NOT EXISTS `project_server` (
  `project_id` int(10) unsigned NOT NULL,
  `server_id` int(10) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`,`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_server`
--

INSERT INTO `project_server` (`project_id`, `server_id`, `created`) VALUES
(1, 1, 0),
(1, 2, 0),
(2, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE IF NOT EXISTS `servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `servers`
--

INSERT INTO `servers` (`id`, `ip`, `name`, `created`) VALUES
(1, 2886729749, 'COFREE_21', 1344936719),
(2, 2886729751, 'COFREE_23', 1344936729),
(3, 3232235709, 'Ay_OneMM', 1345017482);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created`) VALUES
(1, 'admin', 'hello1234', 0),
(2, 'test1', '5a105e8b9d40e1329780d62ea2265d8a', 1344936849),
(3, 'ay_saas', 'ay_saas', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_project`
--

CREATE TABLE IF NOT EXISTS `user_project` (
  `user_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`project_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_project`
--

INSERT INTO `user_project` (`user_id`, `project_id`, `created`) VALUES
(1, 1, 0),
(2, 1, 0),
(1, 2, 0),
(3, 2, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
