<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Cloud Service层
 * @package Service
 * @category Service
 * @version $Id: service.php 6285 2012-02-16 10:20:30Z zhao.yang $ 
 * @copyright 2012 Cofree Development Term
 */
class Service 
{
	/**
	 * 默认异常类型
	 * @var Kohana_Exception 
	 */
    protected $exception = 'Kohana_Exception';
    
	/**
	 * 组合策略 
	 * @var Strategy $strategy
	 */
    protected $strategy = NULl;
    
    const DEFAULT_EXCEPTION = 'Kohana_Exception';
    
    /**
     * ID_ROUTE 默认路由
     * @var int
     */
    const ID_ROUTE = 0x01;
    
    /**
     * LINK_ROUTE 自定义路由
     * @var int
     */
    const LINK_ROUTE = 0x02;

    protected function  __construct()
    {	
        if (is_null($this->exception))
            $this->exception = self::DEFAULT_EXCEPTION;
    }

    /**
     * 加载 DAO实例 
     * @param string $name
     */
    final protected function dao($name)
    {
        return DAO::instance($name);
    }
    
    /**
     * Service层策略
     * @param string $Strategy
     */
    public function setServiceStrategy($Strategy)
    {
    	$this->strategy = Strategy::factory("service_".$Strategy);
    }
}