<?php defined('SYSPATH') or die('No direct script access.');
class Subject_Channel_Customer implements SplSubject
{
	private $observers;
	
	public function __construct()
	{
		$this->observers = new SplObjectStorage();
	}
	
	public function attach(SplObserver $observer)
	{
		$this->observers->attach($observer);
	}
	
	public function detach(SplObserver $observer)
	{
		$this->observers->detach($observer);
	}
	
	/**
	 * 编辑商户 
	 */
	public function editMerchant()
	{
		$this->notify();
	}
	
	public function notify()
	{
		foreach ($this->observers as $observer)
		{
			$observer->update($this);
		}

	}
}

class ChannelCustomerOberver implements SplObserver
{
	public function update(SplSubject $subject)
	{
		//邮件通知
		Message::setc('Edit Channel Customer','info');
	}
}