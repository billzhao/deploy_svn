<?php if ($user->id == 1): ?>
<?php print html::anchor('/server/add', 'Add Server'); ?>
<?php endif ?>

<?php if ($servers): ?>
<ul id="server-list">
    <?php foreach ($servers as $server): ?>
    <li>
    <?php print $server->name . '(' . long2ip($server->ip) . ')'; ?>
    <?php if ($user->id == 1) { print html::anchor("/server/delete/$server->id", 'Delete', array('onclick' => 'return window.confirm("delete?");')); } ?>
    </li>
    <?php endforeach ?>
</ul>
<?php endif ?>
