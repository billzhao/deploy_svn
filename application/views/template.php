<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Code Deploy System</title>
        <style type="text/css">
            body {
                color: #333;
                background: #FFF;
                font-size:14px;
                font-family: Helvetica, Arial, Verdana;
                line-height: 1.5em;
            }

            #nav {
                background: #333;
            }

            #menu li {
                list-style-type: none;
                color: #000;
                float: left;
                height: 30px;
                display: block;
            }

            #menu li:hover {
                background: #EEE;
            }

            #menu a {
                color:#FFF; 
                display: block;
                padding: 0 5px;
                line-height: 30px;
                text-decoration: none;
            }

            #menu a:hover {
                color: #000;
            }
            
input[type="checkbox"]
{
	width:20px; height:20px; padding:0; overflow:hidden; vertical-align:middle;            
}
input[type="radio"]
{
	width:20px; height:20px; padding:0; overflow:hidden; vertical-align:middle;            
}
.filter_condition_checkboxs {

}
.filter_condition_checkboxs label {
	padding-left:3px;
	cursor: pointer;
	line-height:16px;
}
.filter_condition_checkboxs_label_hover,
.filter_condition_checkboxs_label_selected {


}            
        </style>
        <script type="text/javascript" src="/media/jquery-1.4.2.min.js"></script>
    </head>
    <body>
<script type="text/javascript">
$('.filter_condition_checkboxs').live('click',function(event){

	if (event.target.nodeName != 'INPUT')
	{
        $checkbox = $('input[type=checkbox]',this);
        $checkbox.attr('checked',$checkbox.is(':checked')?'':'checked');
	}
    
});

$('.filter_condition_checkboxs input[type=checkbox]').live('click',function(){
});


</script>    
        <?php if ($user->loaded()): ?>
        <div id="nav">
            <ul id="menu">
                <?php if ($user->id == 1): ?>
                <li><?php print html::anchor('/user/list', 'User'); ?></li>
                <li><?php print html::anchor('/server/list', 'Server'); ?></li>
                <?php endif ?>
                <li><?php print html::anchor('/project/list', 'Project'); ?></li>
                <li><?php print html::anchor('/user/logout', 'Logout'); ?></li>
            </ul>
            <div style="clear:both"></div>
        </div>
        <?php endif ?>
        <?php if (isset($content) && !empty($content)): ?>
        <div id="content">
            <?php print $content; ?>
        </div>
        <?php endif ?>
    </body>
</html>
