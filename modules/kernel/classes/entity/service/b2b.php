<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Entity_Service_B2B
 *
 * @author bzhao
 *
 * @version $Id: b2b.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
Interface Entity_Service_B2B
{
	public function serviceB2BManage();

}
