<?php if ($user->id == 1): ?>
<?php print html::anchor('/user/add', 'Add User'); ?>
<?php endif ?>

<?php if ($users): ?>
<ul id="user-list">
    <?php foreach ($users as $user): ?>
    <li><?php print html::anchor("/user/edit/$user->id", $user->username); ?>
	<?php if ($user->id != 1) { print html::anchor("/user/delete/$user->id", 'Delete', array('onclick' => 'return window.confirm("delete?");')); } ?>
    </li>
	<?php endforeach ?>
</ul>
<?php endif ?>
