<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Image
 *
 * @author bzhao
 *
 * @version $Id: image.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Image extends ORM {

	protected $_belongs_to = array('product' => array());
	
    protected $_filters = array(
        TRUE => array('trim' => NULL)
    );

    protected $_rules = array(
        'img_url' => array(
            'not_empty' => NULL,
            'max_length' => array(255),
        ),
    );
}