<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Service_Channel_Catalog
 *
 * @author bzhao
 *
 * @version $Id: catalog.php 7564 2012-09-18 03:25:35Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Channel_Catalog extends Service
{
	const CATALOG_VISIBAL = '1';
	const CATALOG_INVISIBAL = '0';
	const CATALOG_ON_MENU = '1';
	const CATALOG_NOTON_MENU = '0';
	const CATALOG_TYPE_MAINPAGE = 2;
	const CATALOG_TYPE_NORMAL = 1;
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel_catalog';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{      
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct,$select = array() , $key = NULL ,$value = NULL)
	{
		
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache

		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	/**
	 * <code>        
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code> 
	 * @param array $data
	 */
	public function set($data = array())
	{
		return $this->_dao->add($data);
	}

	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }
        
        return $this->_dao->edit($data);
	}

	public function delete($id)
	{
		return $this->_dao->delete($id);
	}
	

	public function count($query_struct)
	{
		return $this->_dao->count($query_struct);
	}
	public function add($data = array())
	{
		var_dump($data);
		return $this->_dao->add($data);
	}

    public function products($query_struct = array())
    {
		return Service_Channel_Product::instance()->tree(
				$query_struct,
				array('id'),
				'id',
				'id');	
    }
    
    public function getCatalogsNameArray($channel_id)
    {
    	$catalogs=$this->tree(array('where'=>array('channel_id'=>$channel_id)));
    	foreach ($catalogs as $catalog)
    	{
    		$cata[$catalog['id']]=$catalog['name'];
    	}
    	return isset($cata)?$cata:array();
    }
    
    public function getCatalogsIdTree($root_id=0, $channel_id=0)
    {
    	$catalogs_id = array();
    	$children = $this->getCatalogsByParentId($root_id, $channel_id);
    	if(empty($children))
    	{
    		return array();
    	}
    	foreach ($children as $key=>$v)
    	{
    		$catalogs_id[$v['id']] = $this->getCatalogsIdTree($v['id'], $channel_id);
    	}
    	return $catalogs_id;
    }
    
    public function getCatalogsByParentId($id, $channel_id=0)
    {
    	return $catalogs=$this->tree(array('where'=>array('parent_id'=>$id,'channel_id'=>$channel_id,'visible'=>self::CATALOG_VISIBAL,'type'=>self::CATALOG_TYPE_NORMAL),'orderby'=>array('sequence','ASC')));
    }
    public function getMainPageCatalog($channel_id=0)
    {
    	$catalogs = $this->tree(array('where'=>array('channel_id'=>$channel_id,'visible'=>self::CATALOG_VISIBAL,'type'=>self::CATALOG_TYPE_MAINPAGE)));
    	if (!$catalogs)
    	{
    		$data['type'] = self::CATALOG_TYPE_MAINPAGE;
    		$data['visible'] = self::CATALOG_VISIBAL;
    		$data['on_menu'] = self::CATALOG_ON_MENU;
    		
    		$data['name'] = 'MainPage';
    		$data['channel_id'] = $channel_id;
    		$data['link'] = 'mainpage';
    		$ret = DB::insert('channel_catalogs', array_keys($data))
				->values($data)
				->execute();
    		if ($ret)
    		{
    			return Service_Channel_Catalog::instance($ret[0])->get();
    		}
    		else
    		{
    			return array();
    		}
    	}
    	
    	return $catalogs[0];
    	
    }
    
    public function getMainPageProductIds($channel_id=0)
    {
    	$main_cat = $this->getMainPageCatalog($channel_id);
    	if ($main_cat)
    		return $this->getChannelCatalogProductIds($main_cat['id']);
    	else
    		return array();
    }
    
    public function getChannelCatalogProductIds($catalog_id = NULL)
    {
        if ( ! $catalog_id)
		{
			$catalog_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
    	
		return $this->_dao->getChannelCatalogProductIds($catalog_id);
    }
    
    
    /**
     * 返回Catalog的完整链接地址，包含当前协议('http://'或'https://')
     * @return string
     */
    public function permalink($channel_id = NULL)
    {
		$channel = Service_Channel::instance($channel_id);
		
        $route_type = $channel->get('route_type');
		
        $link = '';

        switch( $route_type )
        {
            case self::ID_ROUTE:
           		$link = URL::base(FALSE, TRUE).$channel->get('catalog').'/'.$this->_data['id'];
            	break;
            case self::LINK_ROUTE:
            	//hack
                $link = URL::base(FALSE, TRUE).$channel->get('catalog').$this->_data['link'];
                break;
            default:
           		$link = URL::base(FALSE, TRUE).$channel->get('catalog').'/'.$this->_data['id'];
            	break;          
        }
        
        return $link;
    }
}
