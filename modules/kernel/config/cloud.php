<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Cloud平台配置信息
 */


/**
 * Merchant流程设计
 * 
 * admin->Merchant  => 
 	merchant++    
 	user++ => 添加user记录 merchant为insert_id 
 	user_role++  
 * Merchant->Channel
 * 
 */
/**
 * 登录流程设计
 * 
 * 
 * 
 * 
 * 
 */
return array(

	/**
	 * 资源类型
	 */
	'resource'	=>	array(
		'merchant'	=>	1,//商户
		'channel'	=>	2,//站点
	),
	
	/**
	 * ERP LOG
	 */
	
	'erp_log_dir' => $_SERVER['COFREE_LOG_DIR'].'/erp',
	
	
	/**
	 * Channel类型               
	 * 			<ul style="padding-left:0">
                <?php foreach (kohana::config("cloud.channel" as $key => $channel): ?>
                <li style="list-style-type:none">
                <input type="checkbox" name="channels[]" value="<?php $key; ?>" />
                <?php echo $channel;?>
                </li>
                <?php endforeach ?>
                </ul>
                if (isset($_POST['servers'])) 
                {
                    foreach ($_POST['servers'] as $sid)
                        $project->add('servers', ORM::factory('server', $sid));
                }
                edit
                <?php print in_array($key, $checked) ? 'checked="checked"' : ''; ?> 
	 */
	'channel'	=>	array(
		'b2b'		=>	'B2B Manage',  //B2B管理中心
		'b2c'		=>	'B2C Manage',  //B2C管理中心
		'product' 	=>	'Product Manage', //产品中心
		'multi' 	=>  'Multichannel',	  //多渠道
	),

	/*
	 * 策略根据Service,Controller分配
	 */
		'strategy_available'	=>	array(			
			'controller_b2b',
			'controller_b2b_product',			
			'service_b2b',
			'service_b2b_product',	
		),
	
		'notify_available'	=>	array(
			'log','email'
		),
	
	
	'salesRepType' => array(
		''	=> '--Select SalesRep Type--',
		'Sale_Director'	=> 'Sales Director',
		'Sale_Manager' =>	'Sales Manager',
		'Sale_Repo'	=> 'Sales Rep',
		'Parts_Manager' => 'Parts Manager',
		),
		
	'RMA' => array(
		'901RMA_CA'	=> 'Returning CA',
		'901REP_CA' =>	'Repairing CA',
		'901FG_CA'	=> 'Instock CA',
		'911RMA_CA'	=> 'Returning GA',
		'911REP_CA' =>	'Repairing GA',
		'911FG_CA'	=> 'Instock GA',
		),		
		
//	'assistantType' => array(
//		''	=> '--Select Assistant Type--',
//		'Product Assistant' =>	'Product Assistant',
//		'Order Assistant'	=> 'Order Assistant'
//		),		
	
	'assistantType' => array(
		''	=> '--Select Assistant Type--',
		'Product Assistant' =>	'CN Office Assistant',
		'Order Assistant'	=> 'US Office Assisant'
		),			
		
	/**
	 * Role类型
	 * Role ->Role Brief -> Role Schema ->Role Link
	 */
	'role'	=>	array(
		'admin'		=>	'Cloud平台管理员',//010000	->cloud/
		'merchant'	=>	'商户管理员',//020000	->cloud/merchant/
		'b2b'		=>	'B2B站点管理员',//030000	->cloud/merchant/b2b/
		'b2c'		=>	'B2C站点管理员',//04
		'product'	=>	'产品中心管理员',//05
		'multi'		=>	'多渠道管理员',//06
		'b2b-product'	=>	'B2B站点->产品中心管理员',//030100 -> cloud/merchant/b2b/product/
		'b2b-order'		=>	'B2B站点->订单中心管理员',//0302
		'b2b-prodcut-basic'	=>	'B2B站点->产品中心->产品基本信息',//030101 -> cloud/merchant/b2b/product/basic
		'b2b-product-brief'	=>	'B2B站点->产品中心->产品描述',//030102
		'b2b-product-seo'	=>	'B2B站点->产品中心->产品SEO',//030103
	),
	
	
	/**
	 * Role Schema设计
	 * 插入Role表的 kohana::config("cloud.schema.{$user_role['schema_id']}");->加载当前角色的Schema
	 */
	
	
	/**
	 * 菜单header设计,根据URL目录设计
	 */
	'header' => array(
		'cloud' => array(
			'System Config'	=> array(
				'账号与权限' => array(
								array('name' => '角色管理','url' => '/cloud/schema/list','hidden' => true),
								array('name' => '角色管理','url' => '/cloud/schema/list'),
								array('name' => '用户管理','url' => '/cloud/uesr/list'),
								array('name' => '角色管理','url' => '/cloud/schema/list'),
								array('name' => '用户管理','url' => '/cloud/uesr/list'),
								),
				'参数设置' => array(
								array('name' => 'Dashboard','url' => '/cloud/dashboard/list'),
								array('name' => '用户管理','url' => '/cloud/uesr/list'),
								),			
				),
				
			'Merchant Config'	=> array(
				'商户管理' => array(
								array('name' => '商户列表','url' => '/cloud/merchant/list'),
								array('name' => '用户管理','url' => '/merchant/customer/list'),
								array('name' => '权限分配','url' => '/merchant/schema/list'),
								),
				'参数设置' => array(
								array('name' => 'ERP设置','url' => '/cloud/channel/list'),
								),			
				),
				
			'Product Centre'	=> array(
				'产品管理' => array(
								array('name' => '产品列表','url' => '/merchant/product/list'),
								),
				'图片管理' => array(
								array('name' => '图片中心','url' => '/cloud/channel/list'),
								),	
				'目录管理' => array(
								array('name' => '目录列表','url' => '/cloud/channel/list'),
								),	
				),
					
			'B2B Centre'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=b2b'),
								),				
				),
				
			'B2C Centre'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=b2c'),
								),				
				),
				
			'MutliChannel'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=multi'),
								),				
				),
												
			),
		'merchant' => array(
			'商户配置'	=> array(
				'商户管理' => array(
								array('name' => '用户管理','url' => '/merchant/customer/list'),
								array('name' => '权限分配','url' => '/merchant/schema/list'),
								),
				'参数设置' => array(
								array('name' => 'ERP设置','url' => '/cloud/channel/list'),
								),			
				),
				
			'产品中心'	=> array(
				'产品管理' => array(
								array('name' => '产品列表','url' => '/merchant/product/list'),
								),
				'图片管理' => array(
								array('name' => '图片中心','url' => '/cloud/channel/list'),
								),	
				'目录管理' => array(
								array('name' => '目录列表','url' => '/cloud/channel/list'),
								),	
				),
					
			'B2B运营中心'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=b2b'),
								),				
				),
				
			'B2C运营中心'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=b2c'),
								),				
				),
				
			'多渠道运营中心'	=> array(
				'站点管理' => array(
								array('name' => '站点列表','url' => '/merchant/channel/list?type=mutli'),
								),				
				),
												
			),
	),
	
	/**
	 * 左侧导航设计,根据URL目录设计
	 */
	'menu'	=> array(
		/*Cloud导航 */
		'cloud' => array(
			'Cloud Config' => array(
				array('name' => 'CloudDashboard' ,'url' => '/cloud/dashboard/list'),
				),
			'Merchant Manage'  => array(
				array('name' => 'Merchant List' ,'url' => '/cloud/merchant/list'),
				array('name' => 'Merchant Add' ,'url' => '/cloud/merchant/add'),
				),
			'User Manage'  => array(
				array('name' => 'User Profile' ,'url' => '/cloud/user/profile'),
				array('name' => 'User List' ,'url' => '/cloud/user/list'),
				array('name' => 'User Add' ,'url' => '/cloud/user/add'),
				),
			'Role Manage'  => array(
				array('name' => 'Role List' ,'url' => '/cloud/role/list'),
				array('name' => 'Schema List' ,'url' => '/cloud/schema/add'),
				),				
			),
		/*Merchant导航 */	
		'merchant' => array(
			'Merchant Config'	=> array(
				array('name' => 'MerchantDashboard' ,'url' => '/merchant/dashboard/list'),
				),
			'Channel Manage'  => array(
				array('name' => 'B2B Manage' ,'url' => '/merchant/channel/list?type=b2b'),
				array('name' => 'B2C Manage' ,'url' => '/merchant/channel/list?type=b2c'),
				array('name' => 'MultiChannel' ,'url' => '/merchant/channel/list?type=multi'),
				),
			'Product Manage'  => array(
				array('name' => '产品列表' ,'url' => '/merchant/product/list'),
				array('name' => '产品添加' ,'url' => '/merchant/product/add'),
				),
			'Context管理'  => array(
				array('name' => 'Context列表' ,'url' => '/merchant/context/list'),
				array('name' => 'Context添加' ,'url' => '/merchant/context/add'),
				),
			'User Manage'  => array(
				array('name' => 'User Profile' ,'url' => '/merchant/user/profile'),
				array('name' => 'User List' ,'url' => '/merchant/user/list'),
				array('name' => 'User Add' ,'url' => '/merchant/user/add'),
				),			
			'物流管理'	=>	array(
				array('name' => '系统参数' ,'url' => '/merchant/channel/list'),
				),	
			'支付管理'	=>	array(
				array('name' => '系统参数' ,'url' => '/merchant/channel/list'),
				),									
			),
		/*Channel导航 */
		'channelb2b' => array(
			'B2B Config'	=> array(
				array('name' => 'ChannelDashboard' ,'url' => '/channel/dashboard/list'),
				array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
				array('name' => 'SEO' ,'url' => '/channel/dashboard/seo'),
				array('name' => 'Currency' ,'url' => '/channel/currency/list'),
				array('name' => 'Country' ,'url' => '/channel/country/list'),
				array('name' => 'Carrier' ,'url' => '/channel/carrier/list'),
				array('name' => '邮件模板' ,'url' => '/channel/mail/list'),				
				),
			'Product Manage'	=> array(
				array('name' => 'Product List' ,'url' => '/channel/product/list'),
				array('name' => 'Product Import' ,'url' => '/channel/product/import'),				
				),
			'Catalog'	=> array(
				array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
				array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add'),				
				),
			'订单中心'	=> array(
				array('name' => '标准订单' ,'url' => '/channel/news/list'),
				array('name' => '寄售订单' ,'url' => '/channel/news/list'),
				array('name' => '样品单' ,'url' => '/channel/news/list'),
				array('name' => '处方单' ,'url' => '/channel/news/list'),
			),
			'报表中心'	=> array(
				array('name' => '订单列表' ,'url' => '/channel/news/list'),
				array('name' => '销售利润表' ,'url' => '/channel/news/list'),
				array('name' => '异常报表' ,'url' => '/channel/news/list'),
				array('name' => 'Outstanding Balance' ,'url' => '/channel/news/list'),				
			),	
			'应用中心'	=> array(
				array('name' => '价目表维护' ,'url' => '/channel/news/list'),
				array('name' => '在线支付' ,'url' => '/channel/news/list'),
				array('name' => '库存查询' ,'url' => '/channel/news/list'),	
				array('name' => '日志分析' ,'url' => '/channel/news/list'),								
			),
			'CRM'	=> array(
				array('name' => 'Customer Manage' ,'url' => '/channel/customer/list'),	
				array('name' => 'Customer Add' ,'url' => '/channel/customer/add'),				
				array('name' => 'SalesRep Manage' ,'url' => '/channel/sale/list'),	
				array('name' => 'SalesRep Add' ,'url' => '/channel/sale/add'),		
				array('name' => 'User Manage' ,'url' => '/channel/admin/list'),	
				array('name' => 'User Add' ,'url' => '/channel/admin/list'),				
			),		
			'News Manage'	=> array(
				array('name' => 'News List' ,'url' => '/channel/news/list'),
				array('name' => 'News Add' ,'url' => '/channel/news/add'),
			),			
		),

		/*Channel导航 */
		'channelb2c' => array(
			'B2C设置'	=> array(
				array('name' => 'ChannelDashboard' ,'url' => '/channel/dashboard/list'),
				),
			'Product'	=> array(
				array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add'),
				),			
			'新闻管理'	=> array(
				array('name' => '新闻列表' ,'url' => '/channel/news/list'),
				array('name' => '新闻添加' ,'url' => '/channel/news/add'),
				),
			'邮件订阅'	=> array(
				array('name' => '邮件订阅列表' ,'url' => '/channel/news/list'),
				),
			'会员管理'	=> array(
				array('name' => '会员列表' ,'url' => '/channel/news/list'),
			),
		
			'友情链接'	=> array(
				array('name' => '友情链接列表' ,'url' => '/channel/news/list'),
			),
			'推广设置'	=> array(
				array('name' => 'SEO推广信息设置' ,'url' => '/channel/news/list'),
				array('name' => '产品SEO信息设置' ,'url' => '/channel/news/list'),
				array('name' => 'SEO推广信息设置' ,'url' => '/channel/news/list'),
			),	
			'工具箱'	=> array(
				array('name' => 'Sitemap' ,'url' => '/channel/news/list'),
			),		
		),	
		
		/*Channel导航 */
		'channelmulti' => array(
			'多渠道设置'	=> array(
				array('name' => 'ChannelDashboard' ,'url' => '/channel/dashboard/list'),
				),
			'Ebay'	=> array(
				array('name' => 'Ebay账号' ,'url' => '/channel/news/list'),
				),
			'Amazon'	=> array(
				array('name' => 'Amazon账号' ,'url' => '/channel/news/list'),
				),
			'模板管理'	=> array(
				array('name' => '刊登' ,'url' => '/channel/news/list'),
			),
	
			'日志管理'	=> array(
				array('name' => 'Log' ,'url' => '/channel/news/list'),
			),		
		),	
	),
	
	
	/*SchemeCode*/
	/*注意路由设置关系,通过隐藏栏位设置*/
	'10000'	=>	array(	
			'System Config'	=> array(
				'Accounts & Permissions' => array(
								array('name' => 'User Profile','url' => '/cloud/user/profile','hidden'=>true),
								array('name' => 'User Manage','url' => '/cloud/user/list','hidden' => true),
								array('name' => 'Role Manage','url' => '/cloud/role/list'),
								),
				'Settings' => array(
								array('name' => 'Dashboard','url' => '/cloud/dashboard/list'),
								array('name' => 'Schema Manage','url' => '/cloud/schema/list'),
								),			
				),
			'Merchant Config'	=> array(
				'Merchant Manage' => array(
								array('name' => 'Merchant List','url' => '/cloud/merchant/list'),
								array('name' => 'Merchant Add' ,'url' => '/cloud/merchant/add'),								
								),
				'Settings' => array(
								array('name' => 'Dashboard','url' => '/merchant/dashboard/list'),
								),									
				),
				
			'Product Centre'	=> array(
					'Product Manage'  => array(
						array('name' => 'Product List' ,'url' => '/merchant/product/list'),
						array('name' => 'Product Add' ,'url' => '/merchant/product/add'),
					),
					
					'Context Manage'  => array(
						array('name' => 'Context List' ,'url' => '/merchant/context/list'),
						array('name' => 'Context Add' ,'url' => '/merchant/context/add'),
					),
				),
					
			'B2B Centre'	=> array(	
				'B2B Manage' => array(
						array('name' => 'B2B Channel List' ,'url' => '/merchant/channel/list?type=b2b'),
						array('name' => 'B2B Channel Add' ,'url' => '/merchant/channel/add?type=b2b'),
						),
				'B2B Settings' => array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo'),
						array('name' => 'Currency' ,'url' => '/channel/currency/list'),
						array('name' => 'Country' ,'url' => '/channel/country/list'),
						array('name' => 'Carrier' ,'url' => '/channel/carrier/list'),
						array('name' => 'Email' ,'url' => '/channel/mail/list')
						),
				'Order Manage'	=> array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
					),							
				'Product Manage'	=> array(
						array('name' => 'Product List' ,'url' => '/channel/product/list'),
						array('name' => 'Product Import' ,'url' => '/channel/product/import'),				
					),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add'),				
					),
				'CRM'	=> array(
					array('name' => 'Customer Manage' ,'url' => '/channel/customer/list'),	
					array('name' => 'Customer Add' ,'url' => '/channel/customer/add'),				
					array('name' => 'SalesRep Manage' ,'url' => '/channel/sale/list'),	
					array('name' => 'SalesRep Add' ,'url' => '/channel/sale/add'),		
					array('name' => 'User Manage' ,'url' => '/channel/admin/list'),	
					array('name' => 'User Add' ,'url' => '/channel/admin/list'),				
					),		
				'News Manage'	=> array(
					array('name' => 'News List' ,'url' => '/channel/news/list'),
					array('name' => 'News Add' ,'url' => '/channel/news/add'),
					),			
				),
				
			'B2C Centre'	=> array(
				'B2C Manage' => array(
						array('name' => 'B2C Channel List' ,'url' => '/merchant/channel/list?type=b2c'),
						array('name' => 'B2C Channel Add' ,'url' => '/merchant/channel/add?type=b2c'),
						),
				'B2C Settings' => array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo'),
						array('name' => 'Currency' ,'url' => '/channel/currency/list'),
						array('name' => 'Country' ,'url' => '/channel/country/list'),
						array('name' => 'Carrier' ,'url' => '/channel/carrier/list'),
						array('name' => 'Email' ,'url' => '/channel/mail/list'),
						),
				'News Manage'	=> array(
					array('name' => 'News List' ,'url' => '/channel/news/list'),
					array('name' => 'News Add' ,'url' => '/channel/news/add'),
					),							
					),
				
			'MutliChannel'	=> array(
				'MutliChannel Manage' => array(
						array('name' => 'MutliChannel List' ,'url' => '/merchant/channel/list?type=multi'),
						array('name' => 'MutliChannel Add' ,'url' => '/merchant/channel/add?type=multi'),
						),
				'Ebay'	=> array(
					array('name' => 'Ebay Account' ,'url' => '/channel/news/list'),
					),
				'Amazon'	=> array(
					array('name' => 'Amazon Account' ,'url' => '/channel/news/list'),
					),
				'Template Manage'	=> array(
					array('name' => 'Published' ,'url' => '/channel/news/list'),
				),
	
				'Log Manage'	=> array(
					array('name' => 'Log' ,'url' => '/channel/news/list'),
				),			
				)
		),
		
		
		/*Merchant*/
	'20000'	=>	array(	
			'Merchant Config'	=> array(
//				'Merchant Manage' => array(
//								array('name' => 'User Profile','url' => '/cloud/user/profile','hidden'=>true),
//								array('name' => 'Merchant List','url' => '/cloud/merchant/list'),
//								array('name' => 'Merchant Add' ,'url' => '/cloud/merchant/add'),
//								),
				'Settings' => array(
								array('name' => 'Dashboard','url' => '/merchant/dashboard/list'),
								array('name' => 'Navigation','url' => '/merchant/navigation/list'),
								),
				),
				
			'Product Centre'	=> array(
					'Product Manage'  => array(
						array('name' => 'Product List' ,'url' => '/merchant/product/list'),
						array('name' => 'Product Add' ,'url' => '/merchant/product/add'),
						array('name' => 'Product Import' ,'url' => '/merchant/product/import'),
					),
					
					'Context Manage'  => array(
						array('name' => 'Context List' ,'url' => '/merchant/context/list'),
						array('name' => 'Context Add' ,'url' => '/merchant/context/add'),
						array('name' => 'Context Import' ,'url' => '/merchant/context/import'),
					),
					
				),
					
			'B2B Centre'	=> array(	
				'B2B Manage' => array(
						array('name' => 'B2B Channel List' ,'url' => '/merchant/channel/list?type=b2b'),
						array('name' => 'B2B Channel Add' ,'url' => '/merchant/channel/add?type=b2b'),
						),
				'B2B Settings' => array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo'),
//						array('name' => 'Currency' ,'url' => '/channel/currency/list'),
//						array('name' => 'Country' ,'url' => '/channel/country/list'),
//						array('name' => 'Carrier' ,'url' => '/channel/carrier/list'),
						array('name' => 'Email' ,'url' => '/channel/mail/list')
						),
				'Reports'	=>	array(
					array('name' => 'Sales Report by Customer/Item/SalesRep' ,'url' => '/channel/report/sales'),
//					array('name' => 'Profit Analysis by Item', 'url' => '/channel/report/profitbyitem'),
					array('name' => 'Profit Analysis by Salesperon', 'url' => '/channel/report/profitbysales'),
					),	
				'Order Manage'	=> array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
					),						
				'Product Manage'	=> array(
						array('name' => 'Product List' ,'url' => '/channel/product/list'),
						array('name' => 'New Product' ,'url' => '/channel/product/add'),
						array('name' => 'Product Import' ,'url' => '/channel/product/import'),
						array('name' => 'Context Manage' ,'url' => '/channel/context/list'),
						array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config'),
					),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add'),				
					),
				'CRM'	=> array(
					array('name' => 'Customer Manage' ,'url' => '/channel/customer/list'),	
					array('name' => 'Customer Add' ,'url' => '/channel/customer/add'),				
					array('name' => 'SalesRep Manage' ,'url' => '/channel/sale/list'),	
					array('name' => 'SalesRep Add' ,'url' => '/channel/sale/add'),		
					array('name' => 'User Manage' ,'url' => '/channel/admin/list'),	
					array('name' => 'User Add' ,'url' => '/channel/admin/list'),				
					),		
				'News Manage'	=> array(
					array('name' => 'News List' ,'url' => '/channel/news/list'),
					array('name' => 'News Add' ,'url' => '/channel/news/add'),
					),						
				),
				
			'B2C Centre'	=> array(
				'B2C Manage' => array(
						array('name' => 'B2C Channel List' ,'url' => '/merchant/channel/list?type=b2c'),
						array('name' => 'B2C Channel Add' ,'url' => '/merchant/channel/add?type=b2c'),
						),
				'B2C Settings' => array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo'),
						array('name' => 'Currency' ,'url' => '/channel/currency/list'),
						array('name' => 'Country' ,'url' => '/channel/country/list'),
						array('name' => 'Carrier' ,'url' => '/channel/carrier/list'),
						array('name' => 'Email' ,'url' => '/channel/mail/list'),
						),
				'News Manage'	=> array(
					array('name' => 'News List' ,'url' => '/channel/news/list'),
					array('name' => 'News Add' ,'url' => '/channel/news/add'),
					),
				'Product Manage'	=> array(
				array('name' => 'Product List' ,'url' => '/channel/product/list'),
				array('name' => 'Product Import' ,'url' => '/channel/product/import'),				
				),
					),
				
			'MutliChannel'	=> array(
				'MutliChannel Manage' => array(
						array('name' => 'MutliChannel List' ,'url' => '/merchant/channel/list?type=multi'),
						array('name' => 'MutliChannel Add' ,'url' => '/merchant/channel/add?type=multi'),
						),
				'Ebay'	=> array(
					array('name' => 'Ebay Account' ,'url' => '/channel/news/list'),
					),
				'Amazon'	=> array(
					array('name' => 'Amazon Account' ,'url' => '/channel/news/list'),
					),
				'Template Manage'	=> array(
					array('name' => 'Published' ,'url' => '/channel/news/list'),
				),
	
				'Log Manage'	=> array(
					array('name' => 'Log' ,'url' => '/channel/news/list'),
				),			
				)
		),	

		/*Channel B2B*/
		'30000'	=>	array(	
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
					),
					'Basic Information' => array(
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic'),
						array('name' => 'Carrier List' ,'url' => '/channel/carrier/list')						
						),
					'SEO' => array(	
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo')),
					'NewsLetter Manage' => array(
						array('name' => 'NewsLetter List' ,'url' => '/channel/newsletter/list')),
					'News Manage' => array(
								array('name' => 'News List' ,'url' => '/channel/news/list'),
								array('name' => 'News Add' ,'url' => '/channel/news/add')),
					'Product Review Manage' => array(
								array('name' => 'Product Review List' ,'url' => '/channel/review/list'),
								array('name' => 'Product Review Add' ,'url' => '/channel/review/add')),						
				
					'Product Q&A Manage' => array(
								array('name' => 'Product Q&A List' ,'url' => '/channel/qa/list'),
					),						
								
								
			),
				
			
				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
							array('name' => 'Product Add' ,'url' => '/channel/product/add')),
						'Product Import' => array(
							array('name' => 'Product Import' ,'url' => '/channel/product/import'),
							array('name' => 'Product Export' ,'url' => '/channel/product/export')
							
							),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add')),
										
						'Context Manage' => array(
							array('name' => 'Context List' ,'url' => '/channel/context/list')),
						'Config Product' => array(
							array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config')),
					),
				'Order Manage'	=> array(
						'Order List' => array(
							array('name' => 'Order List' ,'url' => '/channel/order/list'),
							array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
							array('name' => 'Quick Order', 'url' => '/channel/order/add')
						),
					),
								
					
				'Customer Manage' => array(
					'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')))
				,
				
				'Sales Rep Manage' => array(
					'Sales Rep Manage' => array(
							array('name' => 'Sales Rep List' ,'url' => '/channel/sale/list'),	
							array('name' => 'Sales Rep  Add' ,'url' => '/channel/sale/add')))
				,

				'User Manage' => array(
					'User Manage' => array(
							array('name' => 'User List' ,'url' => '/channel/admin/list'),	
							array('name' => 'User Add' ,'url' => '/channel/admin/add'))),			

				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),
						'Profit Analysis' => array(
//								array('name' => 'Profit Analysis by Item' ,'url' => '/channel/report/profitbyitem'),
								array('name' => 'Profit Analysis by Salesperon' ,'url' => '/channel/report/profitbysales'),								
						),	
						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),			
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),			
					
				),
				
		/*Channel B2B Sales*/
		'70000'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				)),

				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
						array('name' => 'Quick Order', 'url' => '/channel/order/add')
					)),	

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
							array('name' => 'Product Add' ,'url' => '/channel/product/add')),
						'Product Import' => array(
							array('name' => 'Product Import' ,'url' => '/channel/product/import')),
						'Context Manage' => array(
							array('name' => 'Context List' ,'url' => '/channel/context/list')),
						'Config Product' => array(
							array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config')),
					),
										
				'Customer Manage' => array(
						'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')),
					),	
				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),
						'Profit Analysis' => array(
//								array('name' => 'Profit Analysis by Item' ,'url' => '/channel/report/profitbyitem'),
								array('name' => 'Profit Analysis by Salesperon' ,'url' => '/channel/report/profitbysales'),								
						),	
						'Vehicle Report' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								),	
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),													
				),

		/*Channel B2B Salesrep*/
		'30400'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				)),

				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
						array('name' => 'Quick Order', 'url' => '/channel/order/add')
					)					
					),	

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list')),
									),
										
				'Customer Manage' => array(
						'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')),
					),
				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),
						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),	
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),															
				),
				
		'30300'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				),
					'NewsLetter Manage' => array(
						array('name' => 'NewsLetter List' ,'url' => '/channel/newsletter/list')),
					'News Manage' => array(
								array('name' => 'News List' ,'url' => '/channel/news/list'),
								array('name' => 'News Add' ,'url' => '/channel/news/add')),
					'Product Review Manage' => array(
								array('name' => 'Product Review List' ,'url' => '/channel/review/list'),
								array('name' => 'Product Review Add' ,'url' => '/channel/review/add')),						
				
					'Product Q&A Manage' => array(
								array('name' => 'Product Q&A List' ,'url' => '/channel/qa/list'),
					),					
				),

				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
						array('name' => 'Quick Order', 'url' => '/channel/order/add')
						
					)),	

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list')),
				),

				
				'Customer Manage' => array(
						'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')),
					),	
					
				'Sales Rep Manage' => array(
					'Sales Rep Manage' => array(
							array('name' => 'Sales Rep List' ,'url' => '/channel/sale/list'),	
							array('name' => 'Sales Rep  Add' ,'url' => '/channel/sale/add')))
				,
									
				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),

						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),	
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),													
				),				
	'30600'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				)),

				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
						array('name' => 'Quick Order', 'url' => '/channel/order/add')
						
					)),	

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
									)),
										
				'Customer Manage' => array(
					'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')))
				,
				
				'Report Center'	=> array(

						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),													
				),	
				
		'30100'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				),
						'News Manage' => array(
								array('name' => 'News List' ,'url' => '/channel/news/list'),
								array('name' => 'News Add' ,'url' => '/channel/news/add')),
					'Product Review Manage' => array(
								array('name' => 'Product Review List' ,'url' => '/channel/review/list'),
								array('name' => 'Product Review Add' ,'url' => '/channel/review/add')),						
				
					'Product Q&A Manage' => array(
								array('name' => 'Product Q&A List' ,'url' => '/channel/qa/list'),
					),					
				),

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
							array('name' => 'Product Add' ,'url' => '/channel/product/add')),
						'Product Import' => array(
							array('name' => 'Product Import' ,'url' => '/channel/product/import')),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add')),
										
						'Context Manage' => array(
							array('name' => 'Context List' ,'url' => '/channel/context/list')),
						'Config Product' => array(
							array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config')),
					),
				
				
			
				

				'Report Center'	=> array(
					
						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),	
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

						
					),													
				),	

		'30200'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				),
						'News Manage' => array(
								array('name' => 'News List' ,'url' => '/channel/news/list'),
								array('name' => 'News Add' ,'url' => '/channel/news/add')),
					'Product Review Manage' => array(
								array('name' => 'Product Review List' ,'url' => '/channel/review/list'),
								array('name' => 'Product Review Add' ,'url' => '/channel/review/add')),						
				
					'Product Q&A Manage' => array(
								array('name' => 'Product Q&A List' ,'url' => '/channel/qa/list'),
					),					
				),

				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
							array('name' => 'Product Add' ,'url' => '/channel/product/add')),
						'Product Import' => array(
							array('name' => 'Product Import' ,'url' => '/channel/product/import')),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add')),
										
						'Context Manage' => array(
							array('name' => 'Context List' ,'url' => '/channel/context/list')),
						'Config Product' => array(
							array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config')),
					),
				

				
				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'Order List' ,'url' => '/channel/order/list'),
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
						array('name' => 'Quick Order', 'url' => '/channel/order/add')
					)),	
				
//					
				'Customer Manage' => array(
					'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')))
				,					
//					
			'Sales Rep Manage' => array(
					'Sales Rep Manage' => array(
							array('name' => 'Sales Rep List' ,'url' => '/channel/sale/list'),	
							array('name' => 'Sales Rep  Add' ,'url' => '/channel/sale/add')))
				,
				

				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),

						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),	
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),													
				),		

		'30500'	=>	array(
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
				)),

				'Order Manage'	=> array(
						'Order List' => array(
						array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
					)),	
				),	
								
				
			'30900'	=>	array(	
				'Dashboard'	=> array(
					'Dashboard'	=>	array(
						array('name' => 'Dashboard' ,'url' => '/channel/dashboard/list'),
					),
					'Basic Information' => array(
						array('name' => 'Basic Information' ,'url' => '/channel/dashboard/basic')),
					'SEO' => array(	
						array('name' => 'SEO' ,'url' => '/channel/dashboard/seo')),
					'NewsLetter Manage' => array(
						array('name' => 'NewsLetter List' ,'url' => '/channel/newsletter/list')),
					'News Manage' => array(
								array('name' => 'News List' ,'url' => '/channel/news/list'),
								array('name' => 'News Add' ,'url' => '/channel/news/add')),
					'Product Review Manage' => array(
								array('name' => 'Product Review List' ,'url' => '/channel/review/list'),
								array('name' => 'Product Review Add' ,'url' => '/channel/review/add')),						
				
					'Product Q&A Manage' => array(
								array('name' => 'Product Q&A List' ,'url' => '/channel/qa/list'),
					),						
								
								
			),
				
			
				'Product Manage'	=> array(
						'Product List'	=>	array(
							array('name' => 'Product List' ,'url' => '/channel/product/list'),
							array('name' => 'Product Add' ,'url' => '/channel/product/add')),
						'Product Import' => array(
							array('name' => 'Product Import' ,'url' => '/channel/product/import')),
				'Catalog Manage'	=> array(
						array('name' => 'Catalog List' ,'url' => '/channel/catalog/list'),
						array('name' => 'Catalog Add' ,'url' => '/channel/catalog/add')),
										
						'Context Manage' => array(
							array('name' => 'Context List' ,'url' => '/channel/context/list')),
						'Config Product' => array(
							array('name' => 'Auto Config Product' ,'url' => '/channel/product/generate_config')),
					),
				'Order Manage'	=> array(
						'Order List' => array(
							array('name' => 'Order List' ,'url' => '/channel/order/list'),
							array('name' => 'RMA List' ,'url' => '/channel/order/refund_list'),
							array('name' => 'Quick Order', 'url' => '/channel/order/add')
						),
					),
								
					
				'Customer Manage' => array(
					'Customer Manage' => array(
							array('name' => 'Customer List' ,'url' => '/channel/customer/list'),	
							array('name' => 'Customer Add' ,'url' => '/channel/customer/add')))
				,
				
				'Sales Rep Manage' => array(
					'Sales Rep Manage' => array(
							array('name' => 'Sales Rep List' ,'url' => '/channel/sale/list'),	
							array('name' => 'Sales Rep  Add' ,'url' => '/channel/sale/add')))
				,

				'User Manage' => array(
					'User Manage' => array(
							array('name' => 'User List' ,'url' => '/channel/admin/list'),	
							array('name' => 'User Add' ,'url' => '/channel/admin/add'))),			

				'Report Center'	=> array(
						'Sales Report' => array(
								array('name' => 'Sales Report' ,'url' => '/channel/report/sales'),),
						'Profit Analysis' => array(
//								array('name' => 'Profit Analysis by Item' ,'url' => '/channel/report/profitbyitem'),
								array('name' => 'Profit Analysis by Salesperon' ,'url' => '/channel/report/profitbysales'),								
						),	
						'Price List' => array(
								array('name' => 'General Price List' ,'url' => '/channel/report/general_price_list'),
								array('name' => 'Special Price Lists' ,'url' => '/channel/report/list_price_list'),
								),			
						'Inventory List' => array(
//								array('name' => 'Inventory List for Customer' ,'url' => '/channel/report/inventory_list_for_customer'),
								array('name' => 'Incoming Inventory for Internal User' ,'url' => '/channel/report/incoming_inventory_list'),
								array('name' => 'Current Inventory List' ,'url' => '/channel/report/inventory_list'),
								
								),									

								
						'Log' => array(
								array('name' => 'Shipment Log' ,'url' => '/channel/report/shipment_log'),
								array('name' => 'Return Log' ,'url' => '/channel/report/return_log'),								
						),	
						'Freight Record' => array(
								array('name' => 'Freight Record' ,'url' => '/channel/report/freight_record'),
						),
					),			
					
				),		
		'inventory' => array(
				'901FG_CA' => 'CA',
				'901INTR_CA' => 'CA INTR',
				'911FG_GA' 	=> 'GA',
				'911INTR_GA' => 'GA INTR',
				),
	
);

