<?php defined('SYSPATH') or die('No direct script access.');
/**
 * User Detail
 *
 * @package Model
 * @author ding.wang
 * @copyright © Cofree Development
 */
class Model_User_Detail extends ORM {
			
	// Relationships
	protected $_has_one = array(
		'user'	=>	array('model'	=>	'user','foreign_key' => 'id')
	);
	
	protected $_filters = array(
		TRUE => array('trim' => NULL)
	);
	
	/**
     * disabled字段
     * @var string $disabled;
     */
    protected $disabled = 'active';
}

