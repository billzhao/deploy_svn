<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Service_Channel_Product
 *
 * @author bzhao
 *
 * @version $Id: product.php 7578 2012-09-27 03:57:47Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Channel_Product extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel_product';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }
    
	protected  function __construct($id = 0)
	{      
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache

		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			if (isset($this->_data[$field]))
				return $this->_data[$field];
			else
			{
				if (isset($this->_data['product_id']) && $this->_data['product_id'] > 0)
				{
					$m_product = Service_Merchant_Product::instance($this->_data['product_id']);
					if ($m_product)
					{
						return $m_product->get($field);
					}
				}
			}
		}
		return NULL;
	}
	
	/**
	 * <code>        
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code> 
	 * @param array $data
	 */
	public function set($data = array())
	{
		return $this->_dao->add($data);
	}

	public function edit($data)
	{
		if(!$this->_data['id'])
		{
			Message::set('No product id');
			return false;
		}
		$data['id']=$this->_data['id'];
		
		if ($this->dao($this->_dao_object)->edit($data))
		{
			Message::set('Edit product success');
			return true;
		}
		else
		{
			Message::set('Edit product fail');
			return false;
		}
	}

	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }
        
        return $this->_dao->edit($data);
	}

	public function delete($id)
	{
		return $this->_dao->delete($id);
	}
	
	
    public function count($query_struct)
    {
        return $this->_dao->count($query_struct);
    }

    
    //TODO 根据Attribute  getProduct
    
    /**
     * 返回产品的完整链接地址，包含当前协议('http://'或'https://')
     * @return string
     */
    public function permalink($channel_id = NULL)
    {
		$channel = Service_Channel::instance($channel_id);
        
		$route_type = $channel->get('route_type');
		
        $link = '';

        switch( $route_type )
        {
            case self::ID_ROUTE:
           		$link = URL::base(FALSE, TRUE).$channel->get('product').'/'.$this->_data['id'];
            	break;
            case self::LINK_ROUTE:
                $link = URL::base(FALSE, TRUE).$channel->get('product').'/'.$this->_data['link'];
                break;
            default:
           		$link = URL::base(FALSE, TRUE).$channel->get('product').'/'.$this->_data['id'];
            	break;          
        }
        
        return $link;
    }
    
    public static function get_productId_by_sku($sku)
    {
    	$result=ORM::factory("channel_product")
    			->where("sku","=",$sku)
    			->find();
    			
    	if($result->loaded())
    		return $result->id;
    }    
    
    /**
     * 产品库存
     * @param string $product_erp_item_no
     * @param string $subinventory_code
     */
    public function inventory($subinventory_code = '204FG_CA',$product_erp_item_no = NULL)
    {
    	
    	return TRUE;
		/*
		 * 查询库存信息
		 */
    	if ( ! $product_erp_item_no)
		{
			$product_erp_item_no = isset($this->_data['erp_item_id']) && $this->_data['erp_item_id'] ? $this->_data['erp_item_id'] : NULL;
		}
		
    	$query_struct['where'] = array('item_no' => $product_erp_item_no);
        $query_struct['where'] += array('subinventory_code' => $subinventory_code);
        $query_struct['limit'] = 1;
    	
        if ( $result = Service_Erp_Inventory::instance()->tree(
        	$query_struct,
        	array('available_stock')))
        {
        	return floor($result[0]['available_stock']);
        }
	
        return FALSE;
    	 
    }
    
    /**
     * 读取ERP price
     * @param int $customer_erp_id
     * @param string $product_erp_item_no
     */
    public function price($customer_erp_id = NULL,$product_erp_item_no = NULL)
    {
    	return TRUE;
    	
       	if ( ! $product_erp_item_no)
		{
			$product_erp_item_no = isset($this->_data['erp_item_id']) && $this->_data['erp_item_id'] ? $this->_data['erp_item_id'] : NULL;
		}
        
        if ( ! $customer_erp_id)
        {
            return $this->_data['market_price'];
        }

        $query_struct['where'] = array('customer_erp_id' => $customer_erp_id);
        $query_struct['where'] += array('item_id' => $product_erp_item_no);
        $query_struct['limit'] = 1;
        
        if ( $result = Service_Erp_Price::instance()->tree(
        	$query_struct,
        	array('selling_price')))
        {
        	return round($result[0]['selling_price'],2);
        }
        
        return FALSE;
        
        //number_format(this->price(), 2, '.', '');

    }
    
    /**
     * 得到产品的Catalog
     * @return int
     */
    public function catalog($product_id = NULL)
    {
    	/*
    	 * TODO
    	 * 判断产品类型
    	 * 配置产品读取方法不同
    	 */
    	
        if ( ! $product_id)
		{
			$product_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
		
		/*
		 * TODO
		 * 存在一个产品对应多个目录的关系,
		 * 目录表中设置需要设置visible
		 */
		
    	$result = DB::select('channel_catalogs.id')->from('channel_catalogs') 
			->join('channel_catalog_products') 
			->on('channel_catalogs.id', '=', 'channel_catalog_products.channel_catalog_id') 
			->where('channel_catalog_products.channel_product_id', '=', $product_id)
			->where('channel_catalogs.visible','=','1')
			->where('channel_catalogs.type','=',Service_Channel_Catalog::CATALOG_TYPE_NORMAL)
			->execute()
			->current();
    	
    	return isset($result['id']) 
    				? $result['id'] 
    				: FALSE;	
    }

	public function add($data = array())
	{
		return $this->_dao->add($data);
	}
	
	public function get_id_by_erpid($erp_id = null, $channel_id = null)
	{
		if ( ! $erp_id)
		{
			return false;
		}
		if ( ! $channel_id)
		{
			return false;
		}
		$result = DB::select('channel_products.id')->from('channel_products')
		->join('products')
		->on('channel_products.product_id', '=', 'products.id')
		->where('channel_products.channel_id', '=', $channel_id)
		->where('products.erp_id','=',$erp_id)
		->execute()
		->current();
		 
		return isset($result['id'])
		? $result['id']
		: FALSE;
	}
	
}
