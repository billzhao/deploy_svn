<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * @ClassName: Service_Role
 *
 * @author bzhao
 *
 * @version $Id: role.php 6568 2012-03-26 08:19:12Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Role extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'role';
    
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_load($id);
	}

	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
//		if ( ! is_null($this->strategy))
//		{
//			if (method_exists($this->strategy , 'serviceB2BNewsList'))
//			{
//				return $this->strategy->serviceB2BNewsList($this->_dao,$query_struct,$select,$key,$value);
//			}	
//		}
		
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	public function set($data = array())
	{
		//插入Schema信息
		if ( ! isset($data['schema_id']) OR ! $schema = Service_Schema::instance($data['schema_id'])->get())
		{
			return FALSE;
		}

		//unset($data['schema_id']);
		
		$data['role_description'] = $schema['schema_description'];
		$data['role_schema'] = $schema['schema_code'];
		$data['role_link'] = $schema['schema_link'];
		$data['role_level'] = $schema['schema_level'];
		$data['parent_id'] = $schema['parent_id'];
		
		//TODO Kohana_Log::instance()->add('Role Array', print_r($data,TRUE))->write();
		
		if ( $role_id = $this->isHasRole($data['resource_id'],$data['resource_type'],$data['role_schema']))
			return $role_id;
		
		//TODO log
		return $this->dao($this->_dao_object)->add($data);
	}

	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }
        
        $result = $this->dao($this->_dao_object)->edit($data);
        
        if (empty($result)) 
        {
            return FALSE;
        }
        return TRUE;
	}

	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}

	/**
	 * 判断当前角色是否存在 
	 * @param int $resource_id
	 * @param int $resource_type
	 * @param char $role_schema
	 * @return boolen
	 */
	protected function isHasRole($resource_id = NUlL,$resource_type = NUlL,$role_schema = NUlL)
	{
		$query_struct = array(
		'where'	=>	array(
			'resource_id' => $resource_id,
			'resource_type' => $resource_type,
			'role_schema'	=> $role_schema,
			)
		);
		
		if ($role = $this->dao($this->_dao_object)->find($query_struct))
			return $role['id'];
			
		return FALSE;
	}
	
	/**
	 * 判断用户是否可以访问当前URL 
	 * @param string $request_url
	 * @param array $user_schema
	 * @return boolen 
	 */
     public function checkUserRole($request_url = NULL,$user_schema = array())
     {
			if ( ! $request_url)
				return FALSE;
				
			if ( ! $user_schema 
				 OR ! is_array($user_schema))
				return FALSE;
				
			/*处理返回的URL*/
								
			if ( ! in_array($request_url ,self::_filterRoleUrl($user_schema)))
				return FALSE;

			return TRUE;
				
     }
     
     /**
      * 根据Schema过滤URl
      * @param array  
      */
     protected function _filterRoleUrl($user_schema)
     {
     		$roleUrls = array();
     	
     		//TODO 时间有了，改成递归
     		
     		foreach ($user_schema as $key => $value)
     		{
				if ( is_array($value))
				{
					foreach ($value as $value_key => $value_value)
					{
						if ( is_array($value_value))
						{
							foreach ($value_value as $value_value_key => $value_value_value)
							{
								if ($value_value_key == 'url')
								{
									$roleUrls[] = $value_value_value['url'];
 								}
							}
						}
						
					    if ($key == 'url')
						{
							$roleUrls[] = $value_value['url'];
 						}
					}
				}
     			
     			if ($key == 'url')
				{
					$roleUrls[] = $value['url'];
 				}
 				
     		}
     		
     		return $roleUrls;
     }
     
     
     /**
      * 得到具备当前条件的User列表
      * @param int $resource_id
	  * @param int $resource_type
	  * @param char $role_schema
	  * @return array()
      */
     public function getRoleUserList($resource_id = NUlL,$resource_type = NUlL,$role_schema = NUlL)
     {
     	$query_struct = array();
     	$query_struct['where'] = array(
     		'resource_id'	=> 	$resource_id,
     		'resource_type'	=>	$resource_type);
     	
     	//一期不判断 Schema的对应关系
     	//处理数据
     	$dataGrid = self::tree($query_struct,array('id','role_description'));
     	//加上Cloud平台用户记录
     	
     	//Channel级别读取Merchant的信息
     	if ( $resource_type == 2)
     	{
     		$merchant_id = Service_Channel::instance($resource_id)->get('merchant_id');
     		$query_struct['where'] = array(
     			'resource_id'	=> 	$merchant_id,
     			'resource_type'	=>	1);
     		
     		$extentDataGrid = self::tree($query_struct,array('id','role_description'));
     	}
     	
     	return isset($extentDataGrid) 
     				? self::_associateRoleToUser(array_merge($dataGrid, $extentDataGrid))
     				: self::_associateRoleToUser($dataGrid);
     }
     
     /**
      * 得到Role对应的Uid 
      * @param array $role
      */
     protected function _associateRoleToUser($roleList)
     {
     	$roleUser = array();
     	
        if (is_array($roleList))
		{
			for ($i = 0; $i < count($roleList); $i++)
			{
				$result = DB::select('user_id')->from('user_roles')
					->where('role_id','=',$roleList[$i]['id'])
					->execute()->as_array();
			
				if (count($result))
				{
					foreach ($result as $r)					
					$roleUser[] = 
						array(
							'id' 	=> 	$r['user_id'],
							//'email'	=> 	$result['email'],
							//'name'	=>	$result['name'],
							'desc' 	=> 	$roleList[$i]['role_description']);
				}
				else 
					continue;
			}
			
		}
     	
		return $roleUser;
		
     }
     
}
