<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * News Controller
 *
 * @ClassName: Controller_News
 *
 * @author bzhao
 *
 * @version $Id: news.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_News extends Controller_Webpage
{
	/**
	 * News detail
	 * @param int $id
	 */
	public function action_detail($id = 0)
	{
	 	//$id = $this->request->param('id');
	
      	$data = Service_Channel_New::instance($id)->get();
       
       	if ( ! $data)
       	{
       		$this->request->redirect('/news/list');
       	}
       
      	$this->request->response = View::factory('news_detail')
      		 ->set('data', $data)
     		 ->render();
	}
	
	public function action_list()
	{
		$query_struct = array(
    	'where' => array(
        	'channel_id' => $this->channel_id,
		));
		
        $count = Service_Channel_New::instance()->count($query_struct);

        $pagination = Pagination::factory(
                array(
                'current_page' => array( 'source' => 'query_string', 'key' => 'page' ),
                'total_items'  => $count,
                'items_per_page' => Service_Channel::instance()->get('per_page'),
                'view' => 'pagination',
                )
        );
        

		$query_struct['orderby'] = array('created','DESC');
		$query_struct['limit'] = array(
        					'per_page' => $pagination->items_per_page,
        					'offset'   => $pagination->offset);
		
		$news = Service_Channel_New::instance()->tree($query_struct);
        
        //推荐$news = array_slice($news, $pagination->offset, $pagination->items_per_page);		

        $this->request->response = View::factory('news_list')
        	->set('news', $news)
        	->set('page_view', $pagination->render())
        	->render();
	}

}
