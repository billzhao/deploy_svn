<style>
.middle {width:200px;}
</style>
<form id="frm-project-add" action="" method="post">
    <table class="layout">
        <tr>
            <td>项目名：</td>
            <td><input type="text" name="name" class="middle" value="<?php print isset($post['name']) ? $post['name'] : ''; ?>" /></td>
        </tr>
        <tr>
            <td>SVN路径：</td>
            <td><input type="text" name="svn_path" class="middle" value="<?php print isset($post['svn_path']) ? $post['svn_path'] : ''; ?>" /></td>
        </tr>
        <tr>
            <td>Rsync模块名：</td>
            <td><input type="text" name="rsync_module" class="middle" value="<?php print isset($post['rsync_module']) ? $post['rsync_module'] : ''; ?>" /></td>
        </tr>
        <tr>
            <td>备注：</td>
            <td><textarea name="description"><?php print isset($post['description']) ? $post['description'] : ''; ?></textarea></td>
        </tr>
        <?php if ($servers): ?>
        <tr>
            <td>部署目的主机：</td>
            <td>
                <ul style="padding-left:0">
                <?php foreach ($servers as $server): ?>
                <li style="list-style-type:none">
                    <input type="checkbox" name="servers[]" value="<?php print $server->id; ?>" />
                    <?php print $server->name . '(' . long2ip($server->ip) . ')'; ?>
                </li>
                <?php endforeach ?>
                </ul>
            </td>
        </tr>
        <?php endif ?>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="添加" />
                <?php print html::anchor('/project/list', '取消'); ?>
            </td>
        </tr>
    </table>
</form>
