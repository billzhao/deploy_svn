<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel
 *
 * @author bzhao
 *
 * @version $Id: channel.php 6283 2012-02-16 09:27:48Z zhao.yang $
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel extends ORM
{
	// Relationships
	protected $_has_many = array(
		'customers' => array('model' => 'channel_customer'),
		'products' => array('model' => 'channel_product'),
		'docs'	=> array('model' => 'channel_doc'),
		'countries' => array('model' => 'channel_countries'),
		'carriers' => array('model' => 'channel_carriers'),
		'mails'	=>	array('model' => 'channel_mails'),
		'catalogs' => array('model' => 'channel_catalog'),
		'images' => array('model' => 'channel_image'),
		'contexts' => array('model' => 'channel_context'),
		'news' => array('model' => 'channel_new'),
		'orders' => array('model' => 'channel_order'),

	);

	protected $_belong_to = array(
		'merchant' => array('model' => 'merchant')
	);


	protected $_filters = array(
		TRUE => array( 'trim' => NULL ),

	);
	protected $_rules = array
		(
		'merchant_id' => array('not_empty' => NULL),

		'domain' => array(
			'not_empty' => NULL,
			'max_length' => array( 255 ),
		),
		'email' => array(
			'not_empty' => NULL,
			'max_length' => array( 255 ),
			'validate::email' => NULL,
		),

		'config'  => array('not_empty' => NULL),

		'meta_title' => array(
			'max_length' => array( 255 ),
		),
		'meta_keywords' => array(
			'max_length' => array( 355 ),
		),
		'meta_description' => array(
			'max_length' => array( 65535 ),
		),
		'route_type' => array(
			'not_empty' => NULL,
		),

		'product' => array(
			'max_length' => array( 32 ),
		),
		'catalog' => array(
			'max_length' => array( 32 ),
		),

		'stat_code' => array(
			'max_length' => array( 65535 ),
		),
		'robots' => array(
			'max_length' => array( 255 ),
		),
		'per_page' => array(
			'not_empty' => NULL,
			'min_length' => array( 1 ),
			'max_length' => array( 32 ),
		),



	);


	publi
}


