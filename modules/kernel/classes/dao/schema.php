<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @ClassName: DAO_schema
 *
 * @author bzhao
 *
 * @version $Id: schema.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_schema extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'schema';
}