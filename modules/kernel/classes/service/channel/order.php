<?php
defined('SYSPATH') or die('No direct script access.');

class Service_Channel_Order extends Service
{

    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel_order';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }
    
	protected  function __construct($id = 0)
	{      
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
//		if ( ! is_null($this->strategy))
//		{
//			if (method_exists($this->strategy , 'serviceB2BNewsList'))
//			{
//				return $this->strategy->serviceB2BNewsList($this->_dao,$query_struct,$select,$key,$value);
//			}	
//		}
		
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache

		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}

	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
    public function update($data)
    {
     	 return $this->dao($this->_dao_object)->edit($data);
    }
    
    
	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}
	
	
    public function add($data)
    {
    	 return $this->dao($this->_dao_object)->add($data);
    }

	
	
	// Used
	public static function create_ordernum()
	{
		$order_num = (time() - 1278000000).str_pad(mt_rand(1, 9999), 4, '0');
		
		return $order_num;
	}

	// get a order by ordernum
	// return order_id
	public static function get_from_ordernum($ordernum)
	{
		if( ! $ordernum)
		{
			return FALSE;
		}

		return DB::select('id')
				->from('orders')
				->where('site_id', '=', Service_Channel::instance()->get('id'))
				->where('ordernum', '=', $ordernum)
				->execute()
				->get('id');
	}

	public static function init()
	{
		$order = ORM::factory('order');
		$order->site_id = Site::instance()->get('id');
		if(Site::instance()->get('id')=='99') //hvac usa time
		{
			$order->created = time()+Date::offset('America/New_York','PRC');
		}
		else
		{
			$order->created = time();
		}
		$order->ordernum = self::create_ordernum();
		// initial payment information
		$order->amount_payment = 0;
		$order->payment_status = 'new';
		$order->transaction_id = 0;
		// set shipping information
		$order->shipping_status = 'new_s';
		$order->save();
		$order->ordernum = str_pad($order->id, 7, '0')
			.Site::instance()->get('cc_payment_id');
		$order->save();
		return $order->id;
	}


	public function set_products($products = array( ))
	{
		if( ! is_array($products))
		{
			return FALSE;
		}

		$this->clean_products();

		foreach( $products as $key => $product )
		{
			if( ! $this->add_product($product, $key))
			{
				// FIXME roll back
				return FALSE;
			}
		}

		return TRUE;
	}

	

	public function set_item($item_id, $product_id, $key, $quantity, $price = NULL, $is_gift = 0)
	{
		$item = Product::instance($item_id);
		if($item->get('id'))
		{
			$item_data = array( );
			$item_data['product_id'] = $product_id;
			$item_data['order_id'] = $this->data['id'];
			$item_data['key'] = $key;
			$item_data['site_id'] = $this->site_id;
			$item_data['item_id'] = $item_id;
			$item_data['name'] = $item->get('name');
			$item_data['sku'] = $item->get('sku');
			$item_data['link'] = $item->get('link');
			$item_data['price'] = isset($price) ? $price : $item->price($quantity);
			$item_data['cost'] = $item->get('cost');
			$item_data['weight'] = $item->get('weight');
			$item_data['quantity'] = $quantity;
			$item_data['created'] = time();
			$item_data['is_gift'] = $is_gift;
			DB::insert('order_items', array_keys($item_data))->values($item_data)->execute();

			//add items hits
			$product = ORM::factory('product', $item_id);
			$product->hits += $quantity;
			$product->save();
			return TRUE;
		}
		return FALSE;
	}
	
	// get order basic information
	public function basic()
	{
		return DB::select()
				->from('orders')
				->where('site_id', '=', $this->site_id)
				->where('id', '=', $this->id)
				->execute()
				->current();
	}

	// set order basic information
	public function update_basic($update)
	{
		$orig = $this->basic();

		$update['updated'] = time();
		$ret = DB::update('orders')
			->set($update)
			->where('site_id', '=', $this->site_id)
			->where('id', '=', $this->id)
			->execute();
		if($ret)
		{
			// record change to order history
			$message = array( );
			$updated = array_diff_assoc($update, $orig);
			foreach( $updated as $k => $u )
			{
				if($k == 'updated' || $k == 'products') continue;

				$message[] = "update $k from $orig[$k] to $u";
			}

			$message = implode('<br />', $message);
			if( ! empty($message))
			{
				$this->add_history(array(
					'order_status' => 'update basic',
					'message' => $message,
				));
			}
		}

		return $ret;
	}

	// get all payment records
	public function payments()
	{
		return DB::select()
				->from('channel_orderpayments')
				->where('order_id', '=', $this->_data['id'])
				->order_by('created', 'DESC')
				->execute()
				->as_array();
	}

	// add a payment record
	public function add_payment($payment)
	{
		if ( ! $this->_data['id'])
		{
			return FALSE;
		}
		return DB::insert('channel_orderpayments', array_keys($payment))
				->values(array_values($payment))
				->execute();
	}

	
	// get all history records
	public function histories()
	{
		return DB::select()
				->from('channel_orderhistories')
				->where('order_id', '=', $this->_data['id'])
				->order_by('created', 'DESC')
				->execute()
				->as_array();
	}

	// insert a history
	public function add_history($history)
	{
		if ( ! $this->_data['id'])
		{
			return FALSE;
		}
		return DB::insert('channel_orderhistories', array_keys($history))
				->values(array_values($history))
				->execute();
	}

	// get all products
	/**
	 * 内关联ERp进行读取信息
	 */
	public function products()
	{
		/*读取流程设计*/
		$products = array();
		
		$products = $this->get_erp_order_products();
		
		/*hack,结果集必须相等*/
		$tmp_products = DB::select()
				->from('channel_orderitems')
				->where('order_id', '=', $this->_data['id'])
				->execute()
				->as_array();
				
		if (count($products) == count($tmp_products))
		{
			return $products;
		}
		else 
		{
			return $tmp_products;
		}
	}

	
	/**
	 * Erp Order Products
	 */
	public function get_erp_order_products()
	{
	    $products = array();
	    
        if ($this->_data['erp_head_id'])
        {
        	/**
        	 * Product 
        	 * product_id
        	 * name
        	 * quantity
        	 * price
        	 * total
        	 * status
        	 */
          //  $sql = "SELECT oos.*,oi.`id`,oi.`product_id`,oi.`name`,oi.`lot_no`,oi.`subinventory`"
          /*产品读取逻辑需要修改.等逻辑通了在修改*/
        	$sql ="SELECT DISTINCT oi.* ,oos.line_status,oos.tracking_no,oos.line_status ,oos.subinventory as `sb` FROM `channel_orderitems` oi
left JOIN `channel_orders` o ON o.id = oi.order_id 
left JOIN `orc_orderline_status` oos ON oos.`head_id` = o.`erp_head_id` and  oos.line_id=oi.erp_line_id 
left JOIN `channel_products` p ON p.`id` = oi.`product_id` 
WHERE o.`erp_head_id` =  ".$this->_data['erp_head_id'];
// 			 $sql = "SELECT oi.*,oos.`line_status`"/*oos.`unit_price`,oos.`quantity`" */         
//            . " FROM `channel_orders` o"
//            . " JOIN `orc_order_status` oos ON oos.`head_id` = o.`erp_head_id`"
//            . " JOIN `channel_orderitems` oi ON oos.line_id = oi.erp_line_id"
//            . " JOIN `channel_products` p ON p.`id` = oi.`product_id`"
//            . " WHERE o.`erp_head_id` = ".$this->_data['erp_head_id'] 
//            . " and oos.`attribute5` != 'D'";
            $result = DB::query(Database::SELECT, $sql)
            ->execute();
            
            $i = 0;
            
            foreach( $result as $orderitem )
            {
            	$products[$i]['id'] = $orderitem['id'];
           		$products[$i]['product_id'] = $orderitem['product_id'];
           		$products[$i]['name'] = $orderitem['name'];
           		$products[$i]['sku'] = $orderitem['sku'];
           		$products[$i]['quantity'] = $orderitem['quantity'];
           		/*默认去大写的状态*/
           		$products[$i]['line_status'] = strtoupper($orderitem['line_status']);
           		$products[$i]['status'] = strtoupper($orderitem['status']);
           		$products[$i]['price'] = $orderitem['price'];
           		$products[$i]['item_id'] = $orderitem['item_id'];
           		$products[$i]['lot_no'] = $orderitem['lot_no'];
           		/*读取仓库时，更新order_item*/
           		if (isset($orderitem['sb']) 
           			&& $orderitem['sb']
           			&& $orderitem['subinventory'] != $orderitem['sb'])
           			{
           				$this->update_item($orderitem['id'], array('subinventory' => $orderitem['sb']));
           				$products[$i]['subinventory'] = $orderitem['sb'];
           			}
           		else
           			$products[$i]['subinventory'] = $orderitem['subinventory'];
           		$products[$i]['erp_line_id'] = $orderitem['erp_line_id'];
           		$products[$i]['cost'] = $orderitem['cost'];
           		$products[$i]['referenceHeadId'] = $orderitem['referenceHeadId'];
           		$products[$i]['referenceLineId'] = $orderitem['referenceLineId'];
           		$products[$i]['track_no'] = $orderitem['tracking_no'];
           		$products[$i]['memo'] = $orderitem['memo'];/*配件行的memo*/
           		$products[$i]['bol_weight'] = $orderitem['bol_weight'];
           		$i++;
            }

        }

        return $products;
	}
	
	
	// get all order items.
	public function getitems()
	{
		$orderitems = DB::select()
			->from('order_items')
			->where('order_id', '=', $this->id)
			->execute();
		return $orderitems;
	}

	// get all order items.
	public function orderitems()
	{
		$orderitems = DB::select()
			->from('order_items')
			->where('order_id', '=', $this->id)
			->execute();

		$ois = array( );
		foreach( $orderitems as $v )
		{
			if(isset($ois[$v['item_id']]))
			{
				$ois[$v['item_id']] += $v['quantity'];
			}
			else
			{
				$ois[$v['item_id']] = $v['quantity'];
			}
		}

		return $ois;
	}

	// get an order item by product key and item_id
	public function orderitem($product_key, $item_id)
	{
		return DB::select()
				->from('order_items')
				->where('order_id', '=', $this->id)
				->where('key', '=', $product_key)
				->where('item_id', '=', $item_id)
				->execute()
				->current();
	}

	/**
	 * 得到当前订单的费用 
	 */
	public function orderOtherFee()
	{
		$fee = 0.00;
		
		/*退货单*/
		if ($this->_data['parent_id'])
		{
			return $this->rmaOrderOtherFee();
		}
		
		$rst = DB::select()
				->from('channel_orders')
				->where('id', '=', $this->_data['id'])
				->execute()
				->current();
				
		$fee = $rst['assembly_fee'] 
				+ $rst['pdi_fee']
				+ $rst['license_fee']
				+ $rst['credit_handling_fee']
				+ $rst['wire_fee']
				+ $rst['labor_fee'];

		return floatval($fee);
	}
	
	public function rmaOrderOtherFee()
	{
		$fee = 0.00;
		
		$rst = DB::select()
				->from('channel_orders')
				->where('id', '=', $this->_data['id'])
				->execute()
				->current();
				
		$fee = $rst['re_stocking_fee'] 
				+ $rst['re_damage_fee']
				+ $rst['re_freight_fee'];

		return floatval($fee);
	}	
	
	// delete a product by key
	public function delete_product($key)
	{
		$products = unserialize($this->get('products'));
		if( ! array_key_exists($key, $products))
		{
			return FALSE;
		}

		unset($products[$key]);
		if($this->update_basic(array( 'products' => serialize($products) )))
		{
			$this->add_history(array(
				'order_status' => 'delete product',
				'message' => "key = $key",
			));
			// delete order_items
			return DB::delete('order_items')
					->where('site_id', '=', $this->site_id)
					->where('order_id', '=', $this->id)
					->where('key', '=', $key)
					->execute();
		}

		return FALSE;
	}

	// get all shipment records
	public function shipments()
	{
		$shipments = DB::select()
			->from('order_shipments')
			->where('site_id', '=', $this->site_id)
			->where('order_id', '=', $this->id)
			->order_by('created', 'DESC')
			->execute()
			->as_array();
		if( ! $shipments)
		{
			return $shipments;
		}

		// get items information for each shipment record
		foreach( $shipments as &$shipment )
		{
			$shipment['items'] = DB::select()
				->from('order_shipmentitems')
				->where('shipment_id', '=', $shipment['id'])
				->execute()
				->as_array();
		}

		return $shipments;
	}

	// add a shipment record
	public function add_shipment($shipment, $items)
	{
		if( ! $this->id || ! $items)
		{
			return FALSE;
		}

		$shipment += array(
			'admin_id' => 0,
			'site_id' => $this->site_id,
			'order_id' => $this->id,
			'ordernum' => $this->get('ordernum'),
			'created' => time(),
		);

		// insert shipment information to order_shipments
		$shipment_id = DB::insert('order_shipments', array_keys($shipment))
			->values(array_values($shipment))
			->execute();
		if( ! $shipment_id)
		{
			return FALSE;
		}

		$shipment_id = $shipment_id[0];

		// insert shipment items
		foreach( $items as $item )
		{
			$item += array(
				'order_id' => $this->id,
				'shipment_id' => $shipment_id,
			);

			$success = DB::insert('order_shipmentitems', array_keys($item))
				->values(array_values($item))
				->execute();
			if( ! $success)
			{
				// roll back
				DB::delete('order_shipmentitems')
					->where('shipment_id', '=', $shipment_id)
					->execute();

				DB::delete('order_shipment')
					->where('id', '=', $shipment_id)
					->execute();

				return FALSE;
			}
		}

		return TRUE;
	}

	public function add_product($product, $key=null)
	{
		if( ! $key) $key = "${product['id']}_${product['type']}_".md5(serialize($product['items'])).(isset($product['attributes']) ? '_'.md5(serialize($product['attributes'])) : '');
		$products = empty($this->data['products']) ? array( ) : unserialize($this->data['products']);

		if(array_key_exists($key, $products))
		{
			$products[$key]['quantity'] += $product['quantity'];
		}
		else
		{
			$products[$key] = $product;
		}

		// save updated products data
		if( ! $this->set(array( 'products' => serialize($products) )))
		{
			return FALSE;
		}

		// set product items
		$this->clean_items($key);
		switch( $product['type'] )
		{
			case 0: // simple
			default:
				$this->add_item(array(
					'key' => $key,
					'product_id' => $product['id'],
					'item_id' => $product['id'],
					'price' => $product['price'],
					'quantity' => $product['quantity'],
					'customize' => isset($product['customize']) ? serialize($product['customize']) : NULL,
					'customize_type' => isset($product['customize_type']) ? $product['customize_type'] : 'none',
				));
				break;
			case 1: // config
				$this->add_item(array(
					'key' => $key,
					'product_id' => $product['id'],
					'item_id' => $product['items'][0],
					'price' => $product['price'],
					'quantity' => $product['quantity'],
					'customize' => isset($product['customize']) ? serialize($product['customize']) : NULL,
					'customize_type' => isset($product['customize_type']) ? $product['customize_type'] : 'none',
				));
				break;
			case 2: // package
				foreach( $product[$key]['items'] as $item )
				{
					$this->add_item(array(
						'key' => $key,
						'product_id' => $product['id'],
						'item_id' => $item['id'],
						'price' => $product['price'] / $item['quantity'],
						'quantity' => $item['quantity'] * $product['quantity'],
					));
				}
				break;
			case 3: // simple-config
				$attribute = '';
				foreach( Arr::get($product, 'attributes', array( )) as $attr_key => $attr_value ) $attribute.=$attr_key.":".$attr_value.";";
				$this->add_item(array(
					'key' => $key,
					'product_id' => $product['id'],
					'item_id' => $product['id'],
					'price' => $product['price'],
					'quantity' => $product['quantity'],
					'customize' => isset($product['customize']) ? serialize($product['customize']) : NULL,
					'customize_type' => isset($product['customize_type']) ? $product['customize_type'] : 'none',
					'attributes' => $attribute,
				));
				break;
			case 4: // coupon item
                $this->add_item(array(
                    'key' => $key,
                    'product_id' => $product['id'],
                    'item_id' => $product['id'],
                    'price' => $product['price'],
                    'quantity' => $product['quantity'],
                    'customize' => isset($product['customize']) ? serialize($product['customize']) : NULL,
                    'customize_type' => isset($product['customize_type']) ? $product['customize_type'] : 'none',
                    'is_gift' => 1,
                ));
		}

		Product::instance($product['id'])->hits_inc();
		return TRUE;
	}

	// delete all items for a specific product
	public function clean_items($key)
	{
		return DB::delete('order_items')
				->where('site_id', '=', $this->site_id)
				->where('order_id', '=', $this->id)
				->where('key', '=', $key)
				->execute();
	}

	public function clean_products()
	{
		$this->set(array( 'products' => '' ));
		return DB::delete('order_items')
				->where('site_id', '=', $this->site_id)
				->where('order_id', '=', $this->id)
				->execute();
	}

	
	/**
	 * 得到配件行的信息 
	 */
	public static function get_part_item($id)
	{
		$item = DB::select()
			->from('channel_orderitems')
			->where('id', '=', $id)
			->execute()
			->current();
		
		return $item;
	}
	
	/**
	 * 得到Channel Item的信息
	 * @param item $id
	 */
	public static function get_item($id)
	{
		$item = DB::select()
			->from('channel_orderitems')
			->where('id', '=', $id)
			->execute()
			->current();
		
		if( ! $item) return $item;
		
		/*翻译数据*/
		$productInstance = Service_Channel_Product::instance($item['product_id']);
		$merchantProduct = Service_Merchant_Product::instance($productInstance->get('product_id'));

		$item['model'] = $merchantProduct->get('model');
		
		return $item;
	}

	/*得到Item的Status*/
	public static function get_item_status($id)
	{
 		$status = DB::select('orc_orderline_status.line_status','channel_orderitems.status')->from('orc_orderline_status')
		->join('channel_orderitems')
		->on('orc_orderline_status.line_id', '=', 'channel_orderitems.erp_line_id')
		->where('channel_orderitems.id','=',$id)
		->execute()
		->current();
		
		if (($status['status']=='Cancelled' OR $status['status']=='CANCELLED') && $status['line_status'] != 'CANCELLED' )
			return $status['status'];
		
		return isset ($status['line_status'])
		? $status['line_status']
		: FALSE;
	}
	
	
	public function add_item($item)
	{   
        $productInstance = Service_Channel_Product::instance($item['product_id']);
            
		$item += array(
			'created' 	=> time(),
            'name'		=> $productInstance->get('name'),
            'sku' 		=> $productInstance->get('sku'),
            'link' 		=> $productInstance->get('link'),		
		);
		
		/*不做订单行的处理*/

		$ret = DB::insert('channel_orderitems', array_keys($item))
			->values($item)
			->execute();
			
		if( ! $ret) return $ret;

		return $ret[0];
	}
	
	public function add_part_item($item)
	{   
		$item += array(
			'created' 	=> time(),
		);
		
		/*不做订单行的处理*/

		$ret = DB::insert('channel_orderitems', array_keys($item))
			->values($item)
			->execute();
			
		if( ! $ret) return $ret;

		return $ret[0];
	}	

	public function insert_item($item)
	{
		$product = Product::instance($item['item_id']);
		if( ! $product) return FALSE;

		$item += array(
			'site_id' => $this->site_id,
			'order_id' => $this->id,
			'name' => $product->get('name'),
			'sku' => $product->get('sku'),
			'link' => $product->permalink(),
			'price' => $product->price(Arr::get($item, 'quantity', 1)),
			'cost' => $product->get('cost'),
			'weight' => $product->get('weight'),
			'created' => time(),
			'customize' => NULL,
			'status' => 'new',
		);

		return DB::insert('order_items', array_keys($item))
				->values($item)
				->execute();
	}

	public function update_item($id, $data)
	{
		return DB::update('channel_orderitems')
				->set($data)
				->where('id', '=', $id)
				->execute();
	}

	public function delete_item($id)
	{
		return DB::delete('channel_orderitems')
				->where('id', '=', $id)
				->execute();
	}

	public function cancel_item($id, $qty)
	{
		$item = $this->get_item($id);
		if($item['status'] == 'shipped')
		{
			$new_item = $item;
			unset($new_item['id']);
			$new_item['quantity'] = (-1) * $qty;
			$new_item['status'] = 'return';
			return $this->insert_item($new_item);
		}
		else if($item['status'] == 'new')
		{
			if($item['quantity'] > $qty)
			{
				$new_item = $item;
				unset($new_item['id']);
				$new_item['quantity'] = (-1) * $qty;
				$new_item['status'] = 'cancel';
				$this->update_item($id, array( 'quantity' => $item['quantity'] - $qty ));
				return $this->inert_item($new_item);
			}

			return $this->update_item($id, array( 'status' => 'cancel' ));
		}

		return FALSE;
	}

	public function ship_item($id, $qty)
	{
		$item = $this->get_item($id);
		if($qty < $item['quantity'])
		{
			// insert a line with status 'new'
			$new_item = $item;
			unset($new_item['id']);
			$new_item['quantity'] = $item['quantity'] - $qty;
			$this->insert_item($new_item);
			$this->update_item($id, array( 'quantity' => $qty ));
		}

		$this->update_item($id, array( 'status' => 'shipped' ));
	}

	public function return_item($id)
	{
		$this->update_item($id, array( 'status' => 'returned' ));
	}

	public function product_amount()
	{
		$amount = 0.0;
		$products = $this->products();

		foreach( $products as $product )
		{
			if ($product['status'] == 'Cancelled' || $product['status'] == 'CANCELLED') { continue;}
			
			$amount += $product['price'] * $product['quantity'];
		}

		return Service_Channel::price($amount);
	}
	
	public function product_sale_amount()
	{
		$amount = 0.0;
		$qty = 0;
		$products = $this->products();

		foreach( $products as $product )
		{
			if ($product['status'] == 'Cancelled' || $product['status'] == 'CANCELLED') { continue;}
			
			/*配件不计算*/
			if ($product['name'] == 'PART001') { continue;}
			
			$amount += $product['price'] * $product['quantity'];
			$qty += $product['quantity'];
		}

		return array(
		'amount_product' => Service_Channel::price($amount),
		'amount_product_qty' => $qty);
		
	}	
	public function product_quota()
	{
		$amount = 0;
		$products = $this->products();

		foreach( $products as $product )
		{
			if ($product['status'] == 'Cancelled' || $product['status'] == 'CANCELLED') { continue;}
			
			$productInstance = Service_Channel_Product::instance($product['product_id']);
			$merchantProduct = Service_Merchant_Product::instance($productInstance->get('product_id'));
			$amount += $product['quantity']*$merchantProduct->get('QUOTA Weight (lbs)');
		}

		return $amount.' lbs';
	}
	
	public function product_bol()
	{
		$amount = 0;
		$products = $this->products();

		foreach( $products as $product )
		{
			if ($product['status'] == 'Cancelled' || $product['status'] == 'CANCELLED') { continue;}
			
			if (is_null($product['bol_weight']))
			{
    			$productInstance = Service_Channel_Product::instance($product['product_id']);
				$merchantProduct = Service_Merchant_Product::instance($productInstance->get('product_id'));
			
				$amount += $product['quantity']*$merchantProduct->get('BOL Weight (lbs)');
			}
			else
			{
				$amount += $product['bol_weight']; 
			}
		}

		return $amount.' lbs';
	}
	
	public function getCustomerIfHaveOrder()
	{
		$customer = array();
		
		$customerList = DB::query(Database::SELECT,"select id from channel_customers where id not in (select customer_id  from channel_orders 
group by customer_id)")->execute()->as_array();
		
		if (is_array($customerList) && ! empty($customerList))
		{
			foreach ($customerList as $c) 
			{
				array_push($customer,$c['id']);
			}
		}
		else 
		{
			$customer = (array)0;
		}
		
		return $customer;
		
	}
	
	/*成品成本*/
	public function product_cost()
	{
		$cost = 0.0;
		$products = $this->products();

		foreach( $products as $product )
		{
			if ($product['status'] == 'Cancelled' || $product['status'] == 'CANCELLED') { continue;}
			
			$cost += $product['cost'] * $product['quantity'];
		}

		return Service_Channel::price($cost);
	}	

	public function amount()
	{
		return (float) $this->product_amount()
			+ (float) $this->get('amount_shipping');
	}

	public function update_amount()
	{
		$amount = (float) $this->amount();

		return TRUE;
	}

	public function erp_synced()
	{
		return $this->get('erp_header_id');
	}

	public function is_verified()
	{
		return $this->get('is_verified') == 1;
	}
	
	
	/**
	 * 添加退货单
	 */
	public function create_refund($customer,$products,$extend)
	{
       if ( ! isset($products) || ! count($products))
            return FALSE;
            
        // get customer
        
        $orderInsertObject = array();
        $orderInsertObject['channel_id'] = $extend['channel'];
		$orderInsertObject['customer_id'] = $customer['customer_id'];
		$orderInsertObject['customer_erp_id'] = $customer['customer_erp_id'];
		$orderInsertObject['erp_shipto_address_id'] = $customer['erp_shipto_address_id'];
		$orderInsertObject['erp_billto_address_id'] = $customer['erp_billto_address_id'];
		$orderInsertObject['erp_shipto_contact_id'] = $customer['erp_shipto_contact_id'];
		$orderInsertObject['erp_billto_contact_id'] = $customer['erp_billto_contact_id'];
				
		$orderInsertObject['email'] = $customer['email'];
		$orderInsertObject['salesman_id'] = $customer['salesman_id'];
		$orderInsertObject['place_order'] = 1;		
		$orderInsertObject['operator_id'] = $customer['operator_id'];	
        $orderInsertObject['ordernum'] = $this->create_ordernum();
        
        $orderInsertObject['shipping_method'] = $extend['shipping_method'];
        $orderInsertObject['shipping_code']   = $extend['shipping_code'];
		$orderInsertObject['subinventory_code'] = $extend['subinventory_code'];
		
        $orderInsertObject['created'] = time();
        $orderInsertObject['ip'] = sprintf("%u", ip2long(Request::$client_ip));
        $orderInsertObject['amount_shipping'] = $extend['amount_shipping'];
        $orderInsertObject['parent_id'] = $extend['parent_id'];
        $orderInsertObject['order_type'] = 3;
        $orderInsertObject['return_reason'] = $extend['return_reason'];
        $orderInsertObject['return_exception'] = $extend['return_exception'];
	    $orderInsertObject['return_serial'] = $extend['serial'];  
        /*插入产品行*/
        if ($order_id = $this->add($orderInsertObject))
        {
            $this->addErpProduct($order_id, $products ,$extend['channel']);

            return $order_id;
        }
        
        return FALSE;
	}
	
	/**
	 * 添加订单
	 */
	public function create($customer, $products,$extend)
    {
        if ( ! isset($products) || ! count($products))
        {
            if ( ! isset($extend['partproducts']) || ! count($extend['partproducts']))
            	return FALSE;
        }
            
        // get customer
        $_customer = Service_Channel_Customer::instance($customer['id']);
        
        $orderInsertObject = array();
        $orderInsertObject['channel_id'] = $extend['channel'];
		$orderInsertObject['customer_id'] = $customer['id'];
		$orderInsertObject['customer_erp_id'] = $_customer->get('customer_erp_id');
		
        if (isset($customer['sale_id']))
        {
            $orderInsertObject['salesman_id'] = $customer['sale_id'];
        }
        
        /*前端处理代下单*/
		if (isset($extend['place_order']))
		{
			$orderInsertObject['place_order'] = 1;		
			$orderInsertObject['operator_id'] = $customer['operator_id'];	
		}
        //$order->payment_method = $billing['payment_method'];
        
		$orderInsertObject['email'] = $_customer->get('email');
        $orderInsertObject['ordernum'] = $this->create_ordernum();
        
        $carrier = Service_Channel_Carrier::instance($extend['carrier_id'])->get();
        $orderInsertObject['shipping_method'] = $carrier['erp_code'];
        $orderInsertObject['shipping_code'] = $carrier['carrier_name'];

		$orderInsertObject['subinventory_code'] = $extend['store'];
		/*$orderInsertObject['org_id'] = '158';*/
		
        $orderInsertObject['created'] = time();
        $orderInsertObject['ip'] = sprintf("%u", ip2long(Request::$client_ip));
		$orderInsertObject['customer_po'] = $extend['customer_po'];
        $orderInsertObject['comment'] = $extend['comment'];
        $orderInsertObject['amount_products'] = $extend['total'];	
        $orderInsertObject['amount_order'] = $extend['total'];		
        $orderInsertObject['amount_shipping'] = $extend['carrier_fee'];
        $orderInsertObject['handing_fee'] = $extend['handing_fee'];
        $orderInsertObject['shipping_liftgate'] = $extend['shipping_liftgate']?1:0;
        $orderInsertObject['shipping_residential'] = $extend['shipping_residential']?1:0;
        $orderInsertObject['shipping_notify'] = $extend['shipping_notify']?1:0;
        $orderInsertObject['payment_method'] = $extend['order_payment'];
      
        /*处理相关费用*/
        if (isset($extend['assembly_fee']))
        {
        	$orderInsertObject['assembly_fee'] = $extend['assembly_fee'];
        }
        if (isset($extend['pdi_fee']))
        {
        	$orderInsertObject['pdi_fee'] = $extend['pdi_fee'];
        }
        if (isset($extend['license_fee']))
        {
        	$orderInsertObject['license_fee'] = $extend['license_fee'];
        }
        if (isset($extend['credit_handling_fee']))
        {
        	$orderInsertObject['credit_handling_fee'] = $extend['credit_handling_fee'];
        }
        if (isset($extend['wire_fee']))
        {
        	$orderInsertObject['wire_fee'] = $extend['wire_fee'];
        }       
        if (isset($extend['labor_fee']))
        {
        	$orderInsertObject['labor_fee'] = $extend['labor_fee'];
        }  

        if (isset($extend['quote']))
        {
         	$orderInsertObject['quote'] = $extend['quote'];       	
        }
        
        if (isset($extend['quote_number']))
        {
         	$orderInsertObject['quote_number'] = $extend['quote_number'];       	
        }      
        
        /*判断是否存在discount*/
        if (isset($extend['discount']) && $extend['discount'] > 0)
        {
        	$orderInsertObject['discount'] = $extend['discount'];
        }
        
        if ($order_id = $this->add($orderInsertObject))
        {
            $this->addErpProduct($order_id, $products ,$extend['channel']);
            /*添加配件行*/
            if ( ! $products)
            {	
            	$customer['order_type'] = 2;
            }

            /*添加配件*/
            if (isset($extend['partproducts']) && $extend['partproducts'])
            	$this->addPartProduct($order_id,$extend['partproducts'],$extend['channel']);
            
            // save shipping/billing address
			$customer['id'] = $order_id;
			unset($customer['sale_id']);
			
			$customer['erp_shipto_address_id'] = $customer['shipping_address_id'];
			$customer['ship_address'] = $customer['ship_address'][0];
			$customer['shipping_company_name'] = $customer['ship_address']['shipping_company_name'];			
			$customer['shipping_contact_name'] = $customer['ship_address']['shipping_contact_name'];
			$customer['shipping_address'] = $customer['ship_address']['shipping_address'];
			$customer['shipping_city'] = $customer['ship_address']['shipping_city'];
			$customer['shipping_state'] = $customer['ship_address']['shipping_state'];
			$customer['shipping_country'] = $customer['ship_address']['shipping_country'];
			$customer['shipping_zip'] = $customer['ship_address']['shipping_zip'];
			$customer['shipping_phone'] = $customer['ship_address']['shipping_phone'];

			$customer['erp_billto_address_id'] = $customer['billing_address_id'];
			$customer['bill_address'] = $customer['bill_address'][0];
			$customer['billing_company_name'] = $customer['bill_address']['billing_company_name'];				
			$customer['billing_contact_name'] = $customer['bill_address']['billing_contact_name'];
			$customer['billing_address'] = $customer['bill_address']['billing_address'];
			$customer['billing_city'] = $customer['bill_address']['billing_city'];
			$customer['billing_state'] = $customer['bill_address']['billing_state'];
			$customer['billing_country'] = $customer['bill_address']['billing_country'];
			$customer['billing_zip'] = $customer['bill_address']['billing_zip'];
			$customer['billing_phone'] = $customer['bill_address']['billing_phone'];			

			/*Drop Ship*/
			if (isset($extend['drop_ship']) && $extend['drop_ship'])
				$customer['order_type'] = 4;
			
			$this->update($customer);
			
            return $order_id;
        }
        
        return FALSE;
    }
    
    
    /**
     * Update order item.
     * @param array $data
     * @param array $filters
     * @return bool
     */
    public function update_orderitem($update_data, $filters)
   {
        $sql = DB::update('channel_orderitems')->set($update_data);

        foreach($filters as $k => $filter)
        {
            if (is_array($filter))
            {
                $sql->where($k, 'IN', $filter);
            }
            else
            {
                $sql->where($k, '=', $filter);
            }
        }
        
        $updated = $sql->execute();
        
        //TODO add order history.
        return $updated;
    }
        
    public function addErpProduct($order_id, $products = array( ) ,$channel_id = NULL)
    {
        if ( ! $products)
            return FALSE;
			
            
        foreach($products as $product)
        {
            $productInstance = Service_Channel_Product::instance($product['id']);
            $item_data = array();
            $item_data['product_id'] = $product['id'];
            $item_data['order_id'] = $order_id;
            $item_data['channel_id'] = $channel_id;
            $item_data['item_id'] = $product['erp_item_number'];
            $item_data['name'] = $productInstance->get('name');
            $item_data['sku'] =  $productInstance->get('sku');
            $item_data['link'] = $productInstance->get('link');
			$item_data['price'] = $product['price'];
            $item_data['created'] = time();
            $item_data['quantity'] = $product['quantity'];
            $item_data['lot_no'] = $product['lot_no'];
            $item_data['subinventory'] = $product['subinventory'];
            /*退货行的处理*/
            if (isset($product['referenceHeadId']))
            	$item_data['referenceHeadId'] = $product['referenceHeadId'];
            if (isset($product['referenceLineId']))
            $item_data['referenceLineId'] = $product['referenceLineId'];          
            
            DB::insert('channel_orderitems', array_keys($item_data))->values($item_data)->execute();
            //    echo kohana::debug($item_data);die;

        }
        
    }  

    /**
     * PID !UAT 444 UAT 1841 @todo线上还没有添加产品
     * ITEM_ID 39907 UAT 68897  IN_PRODUCTION 44989
     */
    public function addPartProduct($order_id, $products = array( ) ,$channel_id = NULL)
    {
        if ( ! $products)
            return FALSE;
            
        foreach($products as $product)
        {
        	if ( isset($product['model']) && $product['model']
				 && isset($product['item']) && $product['item']
				 && isset($product['qty']) && $product['qty']
				 && isset($product['price']) && intval($product['price']) >= 0)
			{
				
       			$item_data = array();
				$item_data['product_id'] = ! UAT ? 444 : 1841;
    			if (IN_PRODUCTION) { $item_data['product_id'] = 325;}
				$item_data['order_id'] = $order_id;
				$item_data['channel_id'] = $channel_id;
				$item_data['item_id'] = ! UAT ? 68897 : 39907;
				if (IN_PRODUCTION) { $item_data['item_id'] = 44989;}
				/*取值时候处理*/
				$item_data['sku'] = $product['model']; /*SKU存放Model值用来判断*/
	    		$item_data['name'] = 'PART001'; 
				$item_data['link'] = 'PART001'; 	    
	    		$item_data['quantity'] = $product['qty'];
	    		$item_data['price'] = $product['price'];
	    		$item_data['memo'] =  $product['item'];
            
				DB::insert('channel_orderitems', array_keys($item_data))->values($item_data)->execute();
			}

        }
    } 
        
    /**
     * BOOK到ERP 
     * @param int $orderId
     */
    public static function bookToErp($orderId = null)
    {
    	if (is_null($orderId))
    		return FALSE;
    		
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
		return $ErpService->bookErpOrder(Service_Channel_Order::instance($orderId));
    }

    
    /**
     * 得到ERP的OrderTotal
     */
    public function get_erp_order_total($erp_order_id = NULL)
    {
    	if( ! $erp_order_id)
    		return 0.00;
    		
    	$order = DB::query(Database::SELECT , "select sum(extended) as total from orc_order_status where line_status <> 'CANCELLED' and order_number='{$erp_order_id}'")->execute()->current();
    	
    	return $order['total']
    			? $order['total']
    			: '0.00';
    }    
    
    public function get_erp_order_shipping($erp_order_id = NULL)
    {
    	if( ! $erp_order_id)
    		return 0.00;
    		
    	$order = DB::query(Database::SELECT , "select extended from orc_order_status where line_status <> 'CANCELLED' and order_number='{$erp_order_id}' and item_no=7640")->execute()->current();
    	
    	return $order['extended']
    			? $order['extended']
    			: '0.00';
    }

    
    /**
     * 订单跟踪号 
     * @param string $erp_order_id
     */
    public function get_order_track($erp_order_id = NULL)
    {
    	return FALSE;
    	
    	if( ! $erp_order_id)
    		return FALSE;
    		
    	$order = DB::query(Database::SELECT , "select attribute8 as track from orc_order_status where line_status <> 'CANCELLED' and order_number='{$erp_order_id}' and  `attribute5` != 'D'")->execute()->current();
    	
    	return $order['track'];
    }     
    
    public function get_rma_order_status($order_id = NULL)
    {
		if ( ! $order_id)
			return 'Entered';
			
		$order = Service_Channel_Order::instance($order_id)->get();

		if ( ! $order OR ! $order['erp_ordernum'])
		{
			return 'Entered';
		}
		
    	if($order['approve_id'] == NULL)
		{
			return 'Awaiting Approval';
		}
        else if($order['return_staff_id'] == NULL)
		{
			return 'Awaiting Return';
		}
		else if($order['approve_id'] <> NULL && $order['return_staff_id'] <> NULL) 
		{	
			return 'Returned';
		}
    }
    
    public function get_order_shipdate($erp_order_id = NULL)
    {
    	if( ! $erp_order_id)
    			return '';
    							   
    	$orderStatus = DB::query(Database::SELECT , "select max(ship_date) as ship from orc_orderline_status where  order_number={$erp_order_id} ")->execute()->current();
    	
    	if ( ! $orderStatus['ship'] OR $orderStatus['ship'] == '0000-00-00 00:00:00')
    		return '';
    	
    	return date('Y-m-d',strtotime($orderStatus['ship']));
    }
    
    public function get_order_trackingno($erp_order_id = NULL)
    {
    	if( ! $erp_order_id)
    			return '';
    							   
    	$tracking = DB::query(Database::SELECT , "select tracking_no from orc_orderline_status where  order_number={$erp_order_id} limit 1")->execute()->current();
    	
    	if ( ! $tracking['tracking_no'])
    		return '';
    	
    	return $tracking['tracking_no'];
    }
    /**
     * 从ERP订单行中读取订单状态
     * @prarm int erp_order_id
     * @param string format 
     * return string
     */
    public function get_order_status($erp_order_id = NULL,$format = 'string')
    {
    	/*处理*/
    	
    	try 
    	{
    	if( ! $erp_order_id)
    		return ($format=='string') ? 'Unassociated'
    								   : 4;/*5未关联*/
    		
    								   
		if (IN_PRODUCTION)
		{
    		$orderStatus = DB::query(Database::SELECT , "select line_status,inventory_item_id from orc_orderline_status where  order_number={$erp_order_id} and inventory_item_id not in(7640,44167,44168,44166,44160,44161,44162,44163,44164,44165,29951,50977) group by line_status")->execute()->as_array();
		}
		else if(UAT)
		{
    		$orderStatus = DB::query(Database::SELECT , "select line_status,inventory_item_id from orc_orderline_status where  order_number={$erp_order_id} and inventory_item_id not in(7640,33901,33902,33903,33904,33905,33906,33908,33909,33900,29951,44907) group by line_status")->execute()->as_array();
		}
		else 
		{
    		$orderStatus = DB::query(Database::SELECT , "select line_status,inventory_item_id from orc_orderline_status where  order_number={$erp_order_id} and inventory_item_id not in(7640,52897,52898,52899,62897,62899,62900,62901,62902,62903,29951) group by line_status")->execute()->as_array();
		}    	
									   
    	#$orderStatus = DB::query(Database::SELECT , "select line_status,inventory_item_id from orc_orderline_status where  order_number={$erp_order_id} and inventory_item_id!=7640 group by line_status")->execute()->as_array();
    	
		$orderStatu[$erp_order_id] = array();    	
    	/*根据赵姐提供规则重新设计*/
    	/*
    	 * 
ERP订单头	电商订单头状态		订单行状态
Entered		Awaiting Validating	Entered
Booked		Awaiting shipping	Awaiting shipping
								Supply Eligible
								Picked
								Partial Picked
								Partial Shipped
Booked		Completed			All lines with the following status：
								Shipped
								Cancelled
								Closed
Cancelled	Cancelled			Cancelled
    	 */
    	

		
    	#得到订单的行信息
    	    	#得到订单的行信息
    	if ( count($orderStatus) >0)
    	foreach ($orderStatus as $value) 
    	{    		
    		switch($value['line_status'])
    		{
    			case 'CLOSED':
    			case 'SHIPPED':	
    			case 'BOOKED':
    				$orderStatu[$erp_order_id]['Closed'] = 1;
    				break;
    			case 'CANCELLED':
    				$orderStatu[$erp_order_id]['Cancelled'] = 2;
    				break;
    			case 'ENTERED':
    				$orderStatu[$erp_order_id]['AwaitingValidating'] = 4;
    				break;
    			case 'AWAITING_SHIPPING':
    			case 'SUPPLY_ELIGIBLE':
    			case 'PICKED':
    			case 'PARTIAL PICKED':
    			case 'PARTIAL SHIPPED':
    				$orderStatu[$erp_order_id]['Awaitingshipping'] = 8;
    				break;
    			default:
    				$orderStatu[$erp_order_id]['Entered'] = 0;
    				break;
    				
    		}
    	}
    	
		if (array_sum($orderStatu[$erp_order_id]) >8)
		{
    		return ($format=='string') ? 'Awaiting Shipping'
    								   : 1;     			
		}
		else if (array_sum($orderStatu[$erp_order_id]) ==8)
    	{
    		return ($format=='string') ? 'Awaiting Shipping'
    								   : 1;   		
    	}
    	else if(array_sum($orderStatu[$erp_order_id])==4)
    	{
    		return ($format=='string') ? 'Entered'
    								   : 0;      		
    	}
    	else if(array_sum($orderStatu[$erp_order_id])== 3 ||array_sum($orderStatu[$erp_order_id])== 1)
    	{
    		return ($format=='string') ? 'Completed'
    								   : 3;       		
    	}
    	else if(array_sum($orderStatu[$erp_order_id])== 2)
    	{
    		return ($format=='string') ? 'Voided'
    								   : 2;       	    		
    	}
    	else 
    	{
    		return ($format=='string') ? 'Entered'
    								   : 0;        		
    	}	

   	 	}
   	 	catch(Exception $e)
   		{
    		/*TODO*/
    	}
	}
	
	/**
	 * 根据订单状态返回order_id
	 */
	public function getOrderByStatus($order_status = NULL)
    {
    	if ( ! $order_status)
    		return false;    
    	
    	#处理复杂模块信息
    	$order_in = array(
    	'Entered' => array(),
    	'Awaitingshipping'	=>	array(),
    	'Voided'	 =>	array(),
    	'Completed'	 => array(),
    	'Unassociated' => array(),
    	
    	);
    	
//    	$order = DB::query(Database::SELECT,"SELECT order_number FROM orc_order_status where `attribute5` != 'D' GROUP BY order_number")->execute()->as_array();
    	$order = DB::query(Database::SELECT,"SELECT order_number FROM orc_orderline_status  GROUP BY order_number")->execute()->as_array();
    	
    	foreach($order  as $value)
    	{
    		$status   = $this->get_order_status($value['order_number']);
    		switch ($status)
    		{
    			case "Entered":
    			array_push($order_in['Entered'],$value['order_number']);
    			break;
    			case "Voided":
    			array_push($order_in['Voided'],$value['order_number']);
    			break;
    			case "Completed":
    			array_push($order_in['Completed'],$value['order_number']);	
    			break;
				case "Awaiting Shipping":
				array_push($order_in['Awaitingshipping'],$value['order_number']);
				break;
    			default:
				case "Unassociated":
				array_push($order_in['Unassociated'],$value['order_number']);	    				
    			break;	
    		}
    	}
    	
    	switch ($order_status)
    	{
    		case 'Entered':
    			return $order_in['Entered'];
    			break;
    		case 'AwaitingShipping':
    			return $order_in['Awaitingshipping'];
    			break;
    		case 'Voided':
    			return $order_in['Voided'];
    			break;    			    			    			
    		case 'Completed':
    			return $order_in['Completed'];
    			break;
			case "Unassociated":
				return $order_in['Unassociated'];
    			break;	    			
    		default:
    			break; 
    	}
    	return false;
		
    }	
    
	public function getOrderByShip($max_time ,$min_time)
    {
      	$order = DB::query(Database::SELECT,"SELECT order_number,max(`ship_date`) FROM orc_orderline_status where ship_date <='{$max_time}' and ship_date>= '{$min_time}' group by order_number")->execute()->as_array();
    	
      	$return = array();
      	
      	if (count($order) )
      	{
    		foreach($order  as $value)
    		{
    			$return[] = $value['order_number'];
    		}
      	}
      	
      	
      	return $return;

		
    }
    
    public function getOrderByTracking($tracking)
    {
      	$order = DB::query(Database::SELECT,"SELECT order_number FROM orc_orderline_status where tracking_no = {$tracking} group by order_number")->execute()->as_array();
    	
      	$return = array();
      	
      	if (count($order) )
      	{
    		foreach($order  as $value)
    		{
    			$return[] = $value['order_number'];
    		}
      	}
      	
      	
      	return $return;

		
    }	    
}
