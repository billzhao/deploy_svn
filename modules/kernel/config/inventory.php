<?php defined('SYSPATH') or die('No direct script access.'); 

return array(
	'codetoname'=>array(
				'901FG_CA' => 'In Stock_CA',
				'901INTR_CA' => 'Coming_CA',
				'901REP_CA' => 'Repairing_CA',
				'901RMA_CA' => 'Returned_CA',
				'911FG_GA' => 'In Stock_GA',
				'911INTR_GA' => 'Coming_GA',
				'911REP_GA'	=> 'Repairing_GA',
				'911RMA_GA' => 'Returned_GA',
				'Staging'	=>	'Staging',
		),
	'nametocode'=>array(
				'In Stock_CA' => '901FG_CA',
				'Coming_CA' => '901INTR_CA',
				'Repairing_CA' => '901REP_CA',
				'Staging'	=> 'Staging',
				'Returned_CA' => '901RMA_CA',
				'In Stock_GA' => '911FG_GA',
				'Coming_GA' => '911INTR_GA',
				'Repairing_GA' => '911REP_GA',
				'Returned_GA' => '911RMA_GA',
		),
);