<?php defined('SYSPATH') or die('No direct script access.');
/**
 * B2B Product Controller的策略
 * @ClassName: Entity_Controller_B2B_Prodcut
 *
 * @author bzhao
 *
 * @version $Id: product.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
Interface Entity_Controller_B2B_Product
{
	public function controllerB2BProductAdd();
}
