<?php defined('SYSPATH') OR die('No direct access allowed.');
  /**
   * 
   * DAO credit card
   * 
   * @author zhou.shuo
   *
   * @package DAO
   *
   * @version $Id: creditcard.php 6283 2012-02-16 09:27:48Z zhao.yang $
   *
   * @copyright Cofree Develop Term
   */

class DAO_Channel_CreditCard extends DAO 
{
   	/**
   	 * 表名
   	 * @var unknown_type
   	 */
   	 protected $table_name = 'channel_creditcard';
   	 
   	 protected $disabled = 'disabled';
   	 
   	 public function getCreditCardCustomer($id)
   	 {
   	  	return $this->orm($id)->channel_customers->find()->as_array();
   	 }

}
