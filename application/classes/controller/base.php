<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Base extends Controller_Template
{
    protected $user;

    public function before()
    {
        $this->user = ORM::factory('user', Session::instance()->get('uid'));
        if (!$this->user->loaded() && $this->request->action != 'login') {
            $this->request->redirect('/user/login');
        }

        View::set_global('user', $this->user);

        parent::before();
    }

    public function _is_admin()
    {
        return $this->user->id == 1;
    }
}
