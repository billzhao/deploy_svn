<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Customer Address
 * @version $$Id$$ 
 * @copyright 2012 Cofree Development Term
 */
class DAO_Channel_Customer_Address extends DAO {

    protected $table_name = 'channel_customer_address';
}