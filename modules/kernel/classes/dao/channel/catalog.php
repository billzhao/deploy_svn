<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @ClassName: DAO_Channel_Catalog
 *
 * @author bzhao
 *
 * @version $Id: catalog.php 7563 2012-09-18 03:25:15Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_Channel_Catalog extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'channel_catalog';
    
    
    /**
     * 得到Catalog的 Product ID集
     * @param int $catalog_id
     */
    public function getChannelCatalogProductIds($catalog_id = NULL)
    {
    	//ORM实现
    	
    	$product_ids = array();
    	
    	foreach ($this->orm($catalog_id)->channel_products->order_by('sort')->find_all() as $product)
    	{
    		array_push($product_ids,$product->id);
    	}
    	
    	//Database实现,数据量大
    	
//    	$product_ids = DB::select('channel_products.id')->from('channel_products') 
//			->join('channel_catalog_products') 
//			->on('channel_products.id', '=', 'channel_catalog_products.channel_product_id') 
//			->where('channel_catalog_products.channel_catalog_id', '=', $catalog_id)
//			->execute()
//			->as_array('id','id');

//    	if ( ! $product_ids)
//    		return array(0);
    	
    	return $product_ids;

    }
    
}