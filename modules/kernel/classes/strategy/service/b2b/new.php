<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Strategy_Service_B2B_New
 *
 * @author bzhao
 *
 * @version $Id: new.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Strategy_Service_B2B_New extends Strategy
{
	/**
	 * rework newslist
	 */
	public function serviceB2BNewsList(DAO $dao ,$query_struct = array(), $select = array(),$key = NULL ,$value = NULL)
	{
		return $dao->db_find_all(
				$dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
}



