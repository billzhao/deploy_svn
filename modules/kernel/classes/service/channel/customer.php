<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Service_Channel_Customer
 *
 * @author bzhao
 *
 * @version $Id: customer.php 7271 2012-05-26 04:42:05Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Channel_Customer extends Service
{
	private $_data = NULL;

    private $_dao_object = 'channel_customer';
	
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }
        
    
	protected  function __construct($id = 0)
	{
		$this->_load($id);

	}
	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
//		if ( ! is_null($this->strategy))
//		{
//			if (method_exists($this->strategy , 'serviceB2BNewsList'))
//			{
//				return $this->strategy->serviceB2BNewsList($this->_dao,$query_struct,$select,$key,$value);
//			}	
//		}
		
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
   /**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
     /**
	 * Load Data层数据
	 * @param int $key  primary_id value
	 * @param string $field 字段名
	 * @return mixed
	 */
	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
   /**
	 * 用户登录
	 * @param array $data
	 * @return boolen|Model_Customer
	 */
	public function login($query_struct)
    {
		if ( $customer = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $customer['id'];
		}
		return FALSE;
    }
    /**
     * session login状态
     */
    public function loginAction()
    {
    	$data = array( );
    	$data['id'] = $this->_data['id'];
		$data['email'] = $this->_data['email'];
		$session = Session::instance();
		$session->set('customer', $data);
    }
    /**
     * 判断登入状态
     * Enter description here ...
     */
    public static function loggedIn()
    {
        $session = Session::instance();
        $customer = $session->get('customer');

        if (isset($customer['id']) && $customer['id'] != 0)
        {
            return $customer['id'];
        }
        else
        {
            return FALSE;
        }
    }
    /**
     * 修改密码
     */
    public function  update_password($data)
    {
       	 return $this->dao($this->_dao_object)->edit($data);
    }
    /**
     * 更新信息
     * @param array $data
     */
    public function update($data)
    {
     	 return $this->dao($this->_dao_object)->edit($data);
    }
    
    
	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}
	
	
    public function add($data)
    {
    	 return $this->dao($this->_dao_object)->add($data);
    }
    
    /**
     * 客户关联到ERP 
     * @param int $customerId
     */
    public static function associateErp($customerId = null)
    {
    	if (is_null($customerId))
    		return FALSE;
    		
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
		return $ErpService->getCustomerErpId($customerId);
    }    
    
    public function count($query_struct)
    {
        return $this->dao($this->_dao_object)->count($query_struct);
    }
    
    public function get_customer_address()
    {
    	$customer = $this->_data;

    	$query_struct['where']['customer_id'] = $customer['customer_erp_id'];
    	$query_struct['where']['status'] = '1';
		$query_struct['orderby'] = 'shipping_company_name';
		
        $addresses = Service_Channel_Customer_Address::instance()
        	->tree($query_struct);
        	
        $_addresses = $this->filter_customer_address($addresses);
        
        $customer['shipping_addresses'] = $_addresses['shipping_addresses'];
        $customer['billing_addresses'] = $_addresses['billing_addresses'];
        
        return $customer;
        	
    }
    
    public function filter_customer_address($addresses)
    {
    	$customer['shipping_addresses'] = array();
        $customer['billing_addresses'] = array();
        $step = 0;
        $option_bill = array();
        $option_ship = array();
        
        if (count($addresses))
        {
        	foreach($addresses as $addr)
        	{
        		$billing = array();
        		$shipping = array();
        		
        							
					if( ! in_array($addr['bill_id'],$option_bill))
					{
						$billing = array(
						'bill_id' => $addr['bill_id'],
						'billing_company_name' => $addr['billing_company_name'], 
						'billing_contact_name' => $addr['billing_contact_name'],
						'billing_address' => $addr['billing_address'],
						'billing_city' => $addr['billing_city'],
						'billing_state' => $addr['billing_state'],
						'billing_country' => $addr['billing_country'],
						'billing_zip' => $addr['billing_zip'],
						'billing_phone' => $addr['billing_phone']);   
					
						$customer['billing_addresses'][$step] = $billing;
						
						$option_bill[] = $addr['bill_id'];
					}
					
					        							
					if( ! in_array($addr['ship_id'],$option_ship))
					{
        				$shipping = array(
						'ship_id' => $addr['ship_id'],
						'shipping_company_name' => $addr['shipping_company_name'], 
						'shipping_contact_name' => $addr['shipping_contact_name'],
						'shipping_address' => $addr['shipping_address'],
						'shipping_city' => $addr['shipping_city'],
						'shipping_state' => $addr['shipping_state'],
						'shipping_country' => $addr['shipping_country'],
						'shipping_zip' => $addr['shipping_zip'],
						'shipping_phone' => $addr['shipping_phone']);   
        				
						$customer['shipping_addresses'][$step] = $shipping;
						
						$option_ship[] = $addr['ship_id'];
					}	
        		
				$step++;
        	}
        }
        return $customer;
    }
    
    
    public function get_addresses()
    {
		$customer = $this->_data;
		
        $addresses = DB::select()->from('orc_customer_site')
            ->where('CUSTOMER_ID', '=', $customer['customer_erp_id'])
            ->where('status', '=', 'A')
            ->execute();
            
        $_addresses = $this->filter_address($addresses->as_array());
        
        $customer['shipping_addresses'] = $_addresses['shipping_addresses'];
        $customer['billing_addresses'] = $_addresses['billing_addresses'];
        
        return $customer;
    }
    
    public function filter_address($addresses)
    {
        $customer['shipping_addresses'] = array();
        $customer['billing_addresses'] = array();
        foreach($addresses as $addr)
        {
            $contacts = DB::select()->from('orc_hz_peraon')
                ->where('CUST_ACCT_SITE_ID', '=', $addr['CUSTOMER_SITE_ID'])
                ->where('status', '=', 'A')
                ->execute();
            if ($addr['SITE_USE_CODE'] == 'SHIP_TO')
            {
                $addr['contacts'] = $contacts->as_array();
                $customer['shipping_addresses'][] = $addr;
            }
            elseif ($addr['SITE_USE_CODE'] == 'BILL_TO')
            {
                $addr['contacts'] = $contacts->as_array();
                $customer['billing_addresses'][] = $addr;
            }
        }
        return $customer;
    }

	public function get_erp_address($customer_id, $erp_address_id)
    {
        $address = DB::select()->from('orc_customer_site')
            ->where('CUSTOMER_ID', '=', $customer_id)
            ->where('SITE_USE_ID', '=', $erp_address_id)
            ->where('status', '=', 'A')
            ->execute()->current();
        return $address;
    }

 
    
    public function get_erp_contact($customer_id, $erp_contact_id)
    {
        $erp_contact = DB::select()->from('orc_hz_peraon')
            ->where('CUST_ACCOUNT_ID', '=', $customer_id)
            ->where('CUST_ACCOUNT_ROLE_ID', '=', $erp_contact_id)
            ->where('status', '=', 'A')
            ->execute()->current();
        return $erp_contact;
    }
}
