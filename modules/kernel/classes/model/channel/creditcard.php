<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * @ClassName: Model_Channel_CreditCard
 *
 * @author bzhao
 *
 * @version $Id: creditcard.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_CreditCard extends ORM
{
	// Validate
	protected $_filters = array(
		TRUE => array( 'trim' => NULL )
	);
	protected $_rules = array(
		'channel_id' => array(
			'not_empty' => NULL,
		),
		'customer_id' => array(
			'not_empty' => NULL,
		),
	    
	);
    
}
