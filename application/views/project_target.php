<?php if (!empty($servers)): ?>
<h3>选择要部署的目的主机和代码版本</h3>
<form id="frm-select-target" method="post" action="">
<ul id="target-list">
    <?php foreach ($servers as $server): ?>
    <li style="list-style-type:none">
    <input type="radio" name="target" value="<?php print $server->id; ?>" />
    <?php print "$server->name (".long2ip($server->ip).")"; ?>
    </li>
    <?php endforeach ?>
</ul>
<input type="text" name="branch" value="trunk" />
<input type="text" name="rev" value="HEAD" />
<input type="submit" value="检出代码" />
<?php print html::anchor('/project/list', 'Cancel'); ?>
</form>
<?php endif ?>
