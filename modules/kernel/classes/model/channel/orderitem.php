<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_Orderitem
 *
 * @author bzhao
 *
 * @version $Id: orderitem.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_Orderitem extends ORM {

	/**
	 * 定义Order的对应关系 子类1对多
	 * @var array $_has_many
	 */
	protected $_belongs_to = array(
        'order' => array('model' => 'order'), 
    );
	
}

