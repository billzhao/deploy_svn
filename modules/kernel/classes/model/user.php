<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 用户Model
 *
 * @author bzhao
 * @package Model
 * @version $Id: user.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 * @copyright 2011 Cofree Development Term
 */
class Model_User extends ORM {

	/**
	 * 定义User的对应关系 多对多
	 * @var array $_has_many
	 * @example 
	 * $user->remove('roles', ORM::factory('role', $roleid)); //update 关联表 
	 * $user->add('roles', ORM::factory('role', $roleid));    //
	 */
	protected $_has_many = array(
        'roles' => array('model' => 'role','through' => 'user_roles'), 
    );
    
    /**
     * 得到当前用户的Merchant信息
     * @var array $_belong_to
     */
	protected $_belong_to = array(
		'merchant' => array('model' => 'merchant'),
	);    
    
    /**
	 * 定义User的对应关系 1对1 
	 * @var array $_has_one
	 * @example 
	 * ORM::factory('user',1)->detail = ORM::factory('detail',1);
	 */
    protected $_has_one = array(
    	'detail' => array('model' => 'user_detail' ,'foreign_key' => 'id'));
	
	
    protected $_filters = array(
        TRUE => array('trim' => NULL)
    );
    
    protected $_rules = array(
    	'name' 			=> 	array('not_empty' => NULL,'max_length' => array(255)),//name
    	'email'			=>  array('not_empty' => NULL),
    	'merchant_id'	=>	array('not_empty' => NULL),
    );
 
}

