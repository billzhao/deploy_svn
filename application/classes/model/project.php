<?php defined('SYSPATH') or die('No direct script access.');

class Model_Project extends ORM
{
    protected $_has_many = array(
        'users' => array('through' => 'user_project'), 
        'servers' => array('through' => 'project_server')
    );

    protected $_filters = array(
        TRUE => array(
            'trim' => NULL, 
        ), 
    );

    protected $_rules = array(
        'name' => array(
            'not_empty' => NULL, 
            'max_length' => array(255), 
        ),
        'svn_path' => array(
            'not_empty' => NULL, 
        ), 
        'rsync_module' => array(
            'not_empty' => NULL, 
        ), 
        'description' => array(
            'max_length' => array(255), 
        ), 
    );

    public function get_checked()
    {
        $checked = array();
        if (!$this->loaded()) {
            return $checked;
        }

        foreach ($this->servers->find_all() as $server) {
            array_push($checked, $server->id);
        }

        return $checked;
    }

    public function delete_user_rel()
    {
        return DB::delete('user_project')
            ->where('project_id', '=', $this->id)
            ->execute();
    }

    public function delete_server_rel()
    {
        return DB::delete('project_server')
            ->where('project_id', '=', $this->id)
            ->execute();
    }

    public function checkout_path()
    {
        $branch = Session::instance()->get('branch', 'trunk');

        return "/var/www/repo/data/$branch"."/$this->name/.";
    }
}
