<?php defined('SYSPATH') or die('No direct script access.');

Route::set('glasseslens', 'glasseslens((/<id>))')
    ->defaults(array(
        'controller' => 'glasseslens',
        'action'     => 'index',
    ));
Route::set('glasseslens/action', 'glasseslens(/<action>(/<pid>(/<ppid>)))')
    ->defaults(array(
        'controller' => 'glasseslens',
        'action'     => 'index',
    ));
Route::set('glasseslens/media', 'glasseslens/media(/<file>)', array('file' => '.+'))
    ->defaults(array(
        'controller' => 'glasseslens',
        'action'     => 'media',
        'file'       => NULL,
    ));