<style>
.middle {width:200px;}
</style>
<form id="frm-project-edit" action="" method="post">
    <input type="hidden" name="submit" value="true" />
    <table class="layout">
        <tr>
            <td>项目名：</td>
            <td><input type="text" name="name" class="middle" value="<?php print $project->name; ?>" /></td>
        </tr>
        <tr>
            <td>SVN路径：</td>
            <td><input type="text" name="svn_path" class="middle" value="<?php print $project->svn_path; ?>" /></td>
        </tr>
        <tr>
            <td>Rsync模块名：</td>
            <td><input type="text" name="rsync_module" class="middle" value="<?php print $project->rsync_module; ?>" /></td>
        </tr>
        <tr>
            <td>备注：</td>
            <td><textarea name="description"><?php print $project->description; ?></textarea></td>
        </tr>
        <?php if ($servers): ?>
        <tr>
            <td>部署目的主机：</td>
            <td>
                <ul style="padding-left:0">
                <?php foreach ($servers as $server): ?>
                <li style="list-style-type:none">
                    <input type="checkbox" name="servers[]" value="<?php print $server->id; ?>" <?php print in_array($server->id, $checked) ? 'checked="checked"' : ''; ?> />
                    <?php print $server->name . '(' . long2ip($server->ip) . ')'; ?>
                </li>
                <?php endforeach ?>
                </ul>
            </td>
        </tr>
        <?php endif ?>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="修改" />
                <?php print html::anchor('/project/list', '取消'); ?>
            </td>
        </tr>
    </table>
</form>
