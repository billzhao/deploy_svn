<?php defined('SYSPATH') OR die('No direct access allowed.');
  /**
   * 
   * DAO credit card
   * 
   * @author zhou.shuo
   *
   * @package DAO
   *
   * @version $Id: product.php 6283 2012-02-16 09:27:48Z zhao.yang $
   *
   * @copyright Cofree Develop Term
   */

class DAO_Merchant_Product extends DAO 
{
   	/**
   	 * 表名
   	 * @var unknown_type
   	 */
   	 protected $table_name = 'product';
   	 
   	 public function showColumns()
   	 {
   	 	return array_keys(Database::instance()->list_columns('products','attribute%'));
   	 }

}
