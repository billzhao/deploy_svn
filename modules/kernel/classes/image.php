<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: image
 *
 * @author bzhao
 *
 * @version $Id: image.php 7402 2012-06-28 09:35:54Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class image
{
	/*
	 * 1: Color;
	 * 2: Angle;
	 * 3: Feature;
	 * 4: Parts diagram;
	 * 5: Catalog Banner
	 */
	const TYPE_COLOR = 0x01;
	const TYPE_ANGLE = 0x02;
	const TYPE_FEATURE = 0x03;
	const TYPE_PARTS = 0x04;
	const TYPE_BANNER = 0x05;

	public static function image_url($image_id,$w,$h)
	{
		$image=Service_Merchant_Image::instance()->tree(array('where'=>array('img_url'=>$image_id)));
		$image=$image[0];
		$product_link=Service_Merchant_Product::instance($image['product_id'])->get('link');
		if(!is_numeric($w)||!is_numeric($h))
			return false;
		return '/pimages/'.$product_link.'_'.$image_id.'_'.$w.'x'.$h.'.'.$image['extension'];
	}
	
	public static function get_product_images_id($product_id,$type='')
	{
		if(!$product_id)
		{
			return array();
		}
		if($type)
			$condition=array(
				'where'=>array('product_id'=>$product_id,'img_tag'=>$type)
			);
		else 
			$condition=array(
				'where'=>array('product_id'=>$product_id)
			);
		$image_ids_temp=Service_Merchant_Image::instance()->tree($condition,array('img_url'));
		$image_ids=array();
		foreach ($image_ids_temp as $image_id_temp)
			$image_ids[]=$image_id_temp['img_url'];
		$images_order=Service_Merchant_Product::instance($product_id)->get('images_order');
		$images_order=explode(',', $images_order);
		$images=array();
		foreach($images_order as $image)
		{
			if(in_array($image, $image_ids))
			{
				$images[]=$image;
			}
		}
		$result = array_diff($image_ids, $images);
		$images=array_merge($images,$result);
		return $images;
	}
	
	public static function delete_image($image_id,$rpc_delete=false)
	{
		Service_Merchant_Image::instance($image_id)->delete();
		if($rpc_delete)
			RpcService::instance()->delete($image_id);
	}
	
	public static function channel_image_url($image_id, $w, $h)
	{
		if (!$image_id) return kohana::config('image.defaulturl');
		$image = Service_Channel_Image::instance()->tree(array('where'=>array('img_url'=>$image_id)));
		if ($image)
		{
			$image = $image[0];
			$product_link = Service_Channel_Product::instance($image['product_id'])->get('link');
			if(!is_numeric($w) || !is_numeric($h))
			return false;
			return '/pimages/'.$product_link.'_'.$image_id.'_'.$w.'x'.$h.'.'.$image['extension'];
		}
		else
		{
			return kohana::config('img.defaulturl');
		}
	}
	
	public static function get_channel_images_id($product_id, $type='')
	{
		if (!$product_id)
		{
			return array();
		}
		if ($type)
			$condition = array(
				'where' => array('product_id' => $product_id, 'img_type' => $type)
			);
		else 
			$condition=array(
				'where' => array('product_id' => $product_id)
			);
			
		/*处理排序问题*/
		$image_ids_temp = Service_Channel_Image::instance()->tree($condition, array('img_url'));
		$image_ids = array();
		foreach ($image_ids_temp as $image_id_temp)
			$image_ids[] = $image_id_temp['img_url'];
			
		$main_img_url = Service_Channel_Product::instance($product_id)->get('main_image_url');
		
		Service_Channel_Product::resetInstance($product_id);
		
		if($images_order = Service_Channel_Product::instance($product_id)->get('images_order'))
		{
			$images_order = explode(',', $images_order);
			$image_ids = array_intersect($images_order,$image_ids);
		}
		
		if ($main_img_url && ! $type)
			array_unshift($image_ids, $main_img_url);
			
		$result = $image_ids;

		return $result;
	}
	
	public static function delete_channel_image($image_id, $rpc_delete = false)
	{
		/*图片ID不对*/
		Service_Channel_Image::instance()->delete(array('where' => array('img_url' => $image_id)));
		
		if ($rpc_delete)
			RpcService::instance()->delete($image_id);
	}
}
