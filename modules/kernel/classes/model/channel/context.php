<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_Channel
 * @author 
 *
 * @version $Id: context.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_Context extends ORM
{

    protected $_filters = array(
            TRUE => array('trim' => NULL)
    );
    protected $_rules = array(
        'channel_id' => array(
            'not_empty' => NULL,
        ),
        'name' => array(
            'not_empty' => NULL, 
            'max_length' => array(50),
        ), 
        'attribute' => array(
            'not_empty' => NULL, 
            'max_length' => array(255), 
        ), 
        'attribute_label' => array(
            'not_empty' => NULL, 
            'max_length' => array(50),
        ), 
//        'attribute_description' => array(
//            'not_empty' => NULL, 
//            'max_length' => array(255), 
//        ), 
//        'attribute_option' => array(
//            'not_empty' => NULL, 
//            'max_length' => array(255), 
//        ), 
    );
}
?>