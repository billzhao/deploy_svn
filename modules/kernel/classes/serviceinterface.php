<?php defined('SYSPATH') or die('No direct script access.');
/**
 * REST 接口
 * @version $Id: serviceinterface.php 7517 2012-08-31 05:48:41Z qin.yazhuo $ 
 * @copyright 2012 Cofree Development Term
 */
Interface REST
{
	/**
	 * 添加参数
	 * @param string $name 参数key
	 * @param string $value 参数value
	 */
	public function addParam($name, $value);
	
	/**
	 * 设置Command 
	 * @param string $command
	 */
	public function setCommand($command);
	
	/**
	 * 添加错误
	 * @param string $error
	 */
	public static function addError($error);
	
	/** 
	 * 返回错误对象
	 */
	public function returnError();
	
	/**
	 * Process请求 
	 */
	public function process();
	
}


class ServiceInterface implements REST 
{
	/**
	 * 参数
	 * @var  $params
	 */	
	protected $params;

	/**
	 * 请求
	 * @var  $resquest
	 */
	protected $resquest;
	
	/**
	 * 请求Host
	 * @var string
	 */
	protected $resquestHost;
	
	/**
	 * 请求错误集
	 * @var array
	 */
	protected static $resquestErr = array();
	
	/**
	 * 请求返回
	 * @var array $response
	 */
	protected $response;

	/**
	 * Live
	 * @var bool $isLive
	 */
	protected  $isLive 	= FALSE;
	
	/**
	 * Debug
	 * @var bool $isDebug
	 */
	protected $isDebug = FALSE;
	
	/**
	 * Error
	 * @var bool $isError
	 */
	protected $isError = FALSE;
	
	/**
	 * dieOnError
	 * @var bool $dieOnError
	 */
	protected $dieOnError = FALSE;
	
	/**
	 * Log Error
	 * @var bool
	 */
	protected $logOnError = FALSE;
	
	/**
	 * LOG支持的类型 
	 * @var array
	 */
	protected $levels = array('ERROR' => 0,'REQUEST' => 1,'RESPONSE' => 2,'RESQUESTHOST' => 3);
	
	/**
	 * 时间 格式
	 * @var string
	 */
	protected $logDateFormat = 'Y-m-d H:i:s';
	
	/**
	 * 测试 Oracle URL
	 * @var string
	 */
	//CRP3 const SERVER_TEST = 'http://jira.cofreeonline.com/OracleInfTest/index.jsp';
	const SERVER_FANSE = 'http://172.16.0.73:8080/OracleInf/index.jsp';
	const SERVER_UAT  = 'http://jira.cofreeonline.com/OralceInfTest/index.jsp';
	const SERVER_TEST = 'http://jira.cofreeonline.com/OracleInfTest/index.jsp';
	
	
	/**
	 * 生产环境 Oracle URL
	 * @var string
	 */	
    const SERVER_PRO  = 'http://api.cofreeonline.com/OracleInf/index.jsp';
	
	/**
	 * 构造函数
	 * @param bool $islive
	 */
	public function __construct($islive = FALSE)
	{
		$this->isLive = $islive;
	}

	
	/**
	 * 添加错误
	 * @param string $error
	 * @see REST::addError()
	 */
	public static function addError($error)
	{
		self::$resquestErr['Err'][] = $error;
	}
  
	/**
	 * 客户端获取最新的错误信息
	 */
	public static function getCurrentError()
	{
		if (isset(self::$resquestErr['Err']))
		{
			return current(self::$resquestErr['Err']);
		}
	}
	
	/**
	 * 返回错误
	 * @see REST::returnError()
	 * @return void
	 */
	public function returnError()
	{
		$this->isError = true;
    	
		//trigger_error("Failed while executing command: <i>{$this->params['option']}</i>", E_USER_WARNING);
		
		if ($this->isDebug)
		{
			$error = '';
			$error .= "<pre>{$this->resquestHost}执行command: <i>{$this->params['option']}</i>\r\n\r\n<b><u>参数</u></b>\r\n";
			//pras
			$error .= htmlspecialchars(stripslashes(print_r($this->params , TRUE)));
			$error .= "\r\n<b><u>捕捉到的错误:</u></b>\r\n";
			$error .= '<ul>';
			for($i=0; $i<count(self::$resquestErr['Err']); $i++)
			{
				$error .= '<li>' . self::$resquestErr['Err'][$i] . '</li>';
			}
			$error .= '</ul>';
			$error .= "\r\n<b><u>服务器返回:</u></b>\r\n";
			$error .= print_r($this->response, TRUE);;
			$error .= '</pre>';

			echo $error;
			
			if ($this->logOnError)
			{
				$this->_log($error);
			}
		}

		//TODO 生产环境的错误处理
		
		if($this->dieOnError) exit;

		return FALSE;
	}
	
	public function addParam($name, $value)
	{
		$this->params[$name] = $value;
	}
	
	public function setCommand($command)
	{
		/*请可选项*/
		$this->params = array();
    	$this->addParam('option', $command);
	}
	
	public function process()
	{
		if ( ! count($this->params) )
		{
			$this->addError("没有设置process()的参数");
			return $this->returnError();
		}
		
		if ( ! $this->isLive)
		{
			if (UAT)
			{
				$this->resquestHost = self::SERVER_UAT;
			}
			else 
				$this->resquestHost = self::SERVER_TEST;
		}

		/*URL强制*/
		if (IN_PRODUCTION)
			$this->resquestHost = self::SERVER_PRO;
		
		
	    if ( ! function_exists('curl_init') )
    	{
      		$this->addError('Could not connect to Server though SSL - libcurl is not supported');
      		return $this->returnError();
    	}
    	
        $ch = curl_init();
        curl_setopt_array($ch, array(
//            CURLOPT_URL => $this->resquestHost.'?'.http_build_query($this->params),
			CURLOPT_URL => $this->resquestHost,
            CURLOPT_RETURNTRANSFER => TRUE, 
            CURLOPT_TIMEOUT => 60, 
        ));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->params));
		/*Log Request*/        
        $this->_log(print_r($this->resquestHost,true) ,'resquestHost');
        $this->_log(print_r($this->params,true) ,'request');
                
        $this->response = curl_exec($ch);
        
        /*处理CURL异常*/
        if (curl_errno($ch))
        {
        	 $errno = curl_errno($ch);
    		 $errstr = curl_error($ch);
        	 $this->addError("Could not connect to Server - Error($errno) $errstr");
     		 return $this->returnError();
        }
        
        /*Log Response*/
		$this->_log(print_r($this->response,true),'response');
        
        //fclose($fp);
        return self::_is_success(json_decode($this->response,TRUE));
	}
	
	/**
	 * Erp Log
	 * @param string $level
	 * @param string $message
	 */
	protected function _log($message ,$level = 'error')
	{				
		if ($this->logOnError === FALSE)
		{
			return FALSE;
		}

		$level = strtoupper($level);

		if ( ! isset($this->levels[$level]))
		{
			$result['errorCode'] = '0001';
			$result['errorDescription'] = sprintf("ERP LOG 不支持Tag %s",$level);
			throw new ServiceException($result);
		}
		
		$path = Kohana::config('cloud.erp_log_dir');
		
		if ( ! is_dir($path) OR ! is_writable($path))
		{
			$result['errorCode'] = '0001';
			$result['errorDescription'] = sprintf("ERP LOG 目录错误 %s",$path);
			throw new ServiceException($result);
		}
		
		$filepath = $path.'/log-'.date('Y-m-d').EXT;
		
		$header  = '';

		/*定义文件头部*/
		if ( ! file_exists($filepath))
		{
			$header .= "<"."?php  defined('SYSPATH') or die('No direct script access.'); ?".">\n\n";
		}

		if ( ! $fp = @fopen($filepath, 'ab'))
		{
			return FALSE;
		}
		
		$message = $level . ' ' 
					. '--'
					. date($this->logDateFormat)
					. '-->' 
					. ' ' . $message . "\n";
					
		/*进行排它型锁定,独占锁定，写入程序*/
		if (flock($fp, LOCK_EX))
		{
			fwrite($fp, $header.$message);
			flock($fp, LOCK_UN);
		}
		
		fclose($fp);
		
		/*修改文件权限*/
		@chmod($filepath, 0666);
        
	}

	/**
	 * 判断结果集 
	 * @param array $result
	 */
    protected  function _is_success($result)
    {
        if ( ! $result 
            || ! isset($result['retCode']) 
            || $result['retCode'] != 'S')
        {
            if (isset($result['retMsg']))
            {
            	$this->addError($result['retMsg']);
     		 	return $this->returnError();
            }
            else
            {
            	$this->addError('ERP通信失败');
     		 	return $this->returnError();
            }
        }
        
        return $result;
    }
    
    public function getIsLive()
    {
    	return $this->isLive;
    }
	
}
/**
 *异常类
 * 错误类型对照表
 * Code Description
 * @example
 * <code>
 * 1	ERP LOG出错
 * 2	非法的LOG 标签
 * 3	库存组织无效
 * </code>
 */ 
class ServiceException extends Exception
{

	var $errorCode;
	var $errorDescription;

	function ServiceException($result)
	{
		$this->result = $result;
		$this->errorCode = $result['errorCode'];
		$this->errorDescription = $result['errorDescription'];
		
		parent::__construct($this->errorDescription, $this->errorCode);
	}

	function getErrorCode()
	{
		return $this->errorCode;
	}

	function setErrorDescription($newVal)
	{
		$this->errorDescription = $newVal;
	}
	
	function setErrorCode($newVal)
	{
		$this->errorCode = $newVal;
	}

	function getErrorDescription()
	{
		return $this->errorDescription;
	}
}

/* End of file ServiceInterface.php */