<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Notify
 *
 * @author bzhao
 *
 * @version $Id: notify.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
abstract class Notify
{
    /**
     * 默认异常对象名称
     *
     * @var string
     */
    const DEFAULT_EXCEPTION = 'Kohana_Exception';//Cloud_Exception
    
	final public static function factory($model, $config = NULL)
	{
		// Set class name
		if ( ! in_array($model,array_map('ucfirst',Kohana::config("cloud.notify_available"))))
		{
			$exception = self::DEFAULT_EXCEPTION;
			throw new $exception(sprintf('未找到名称为 "%s" 的 Notify', $model));
		}
		
		$model = 'Notify_'.ucfirst($model);

		return new $model($config);
	}
   
	protected function __construct($config = NULL)
	{
		$this->_config = $config; 
	}
	
	abstract public function inform();//消息通知模板
}
