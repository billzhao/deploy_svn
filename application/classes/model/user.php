<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends ORM
{
    protected $_has_many = array(
        'projects' => array('through' => 'user_project'));

    protected $_filters = array(
        TRUE => array(
            'trim' => NULL, 
        ), 
    );

    protected $_rules = array(
        'username' => array(
            'not_empty' => NULL, 
            'max_length' => array(64), 
        ), 
        'password' => array(
            'not_empty' => NULL, 
        ), 
    );

    public function get_checked()
    {
        $checked = array();
        if (!$this->loaded()) {
            return $checked;
        }

        foreach ($this->projects->find_all() as $project) {
            array_push($checked, $project->id);
        }

        return $checked;
    }
	
	public function delete_project_rel()
    {
        return DB::delete('user_project')
            ->where('user_id', '=', $this->id)
            ->execute();
    }
	
}
