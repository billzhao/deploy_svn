<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_Doc
 *
 * @author bzhao
 *
 * @version $Id: doc.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_Doc extends ORM
{
    protected $_filters = array(
        TRUE => array('trim' => NULL)
    );

    protected $_rules = array(
        'name' => array
        (
            'not_empty' => NULL,
        ),
        'link' => array
        (
            'not_empty'	=> NULL,
        )
    );

}
