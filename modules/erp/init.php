<?php

if (!Kohana::config('erp.enabled'))
{
    return;
}

function erp_order_payment_success($data)
{
    $order = $data['order'];
    if ($order->get('payment_count') > 1)
        return TRUE;

    return ERP::create_order($data['order']);
}

function erp_order_payment_partial_refund($data)
{
    return ERP::order_gl($data);
}

function erp_order_payment_refund($data)
{
    return ERP::order_gl($data);
}

function erp_order_payment_verify_pass($data)
{
    return ERP::order_gl($data);
}

Event::add('Order.payment', 'erp_order_payment_success');
Event::add('Order.payment.partial_refund', 'erp_order_payment_partial_refund');
Event::add('Order.payment.refund', 'erp_order_payment_refund');
Event::add('Order.payment.verify_pass', 'erp_order_payment_verify_pass');
