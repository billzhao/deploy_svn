<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @ClassName: Controller_Webpage
 *
 * @author bzhao
 *
 * @version $Id: webpage.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_Webpage extends Controller
{
    public $meta_title;
    public $meta_keywords;
    public $meta_description;
    
    protected $channel_id;

   	const ID_ROUTE = 0x01;
    const LINK_ROUTE = 0x02;
    
    /**
     * 1整车  2配件
     * @var PRODUCT
     */
    const PRODUCT = 0x01;
    const PART	  = 0x02;
    
    
    public function before()
    {
		$this->meta_title = Service_Channel::instance()->get('meta_title');
        $this->meta_keywords = Service_Channel::instance()->get('meta_keywords');
        $this->meta_description = Service_Channel::instance()->get('meta_description');
        
        //绑定参数 
        View::bind_global('meta_title', $this->meta_title);
        View::bind_global('meta_keywords', $this->meta_keywords);
        View::bind_global('meta_description', $this->meta_description);
        
        $this->channel_id = Service_Channel::instance()->get('id');
    }
    
}
