<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * Channel Service
 *
 * @ClassName: Service_Channel
 *
 * @author bzhao
 *
 * @version $Id: product.php 7530 2012-09-05 09:29:31Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Merchant_Product extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'merchant_product';
    
    private $_data = NULL;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  

        $class = __CLASS__;
        self::$instances[$id] = new $class($id);
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }    
    
	protected  function __construct($id = 0)
	{
	    if ($id)
            $this->_load($id);
	}

	public function tree($query_struct=array(),$select = array(),$key='',$value='')
	{
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value
			);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			if (isset($this->_data[$field]))
			{
				return $this->_data[$field];
			}
			else
			{
				$query_struct['where'] = array('attribute_label' => $field);
				$query_struct['where'] += array('name' => $this->_data['context_name']);
				$query_struct['where'] += array('merchant_id' => $this->_data['merchant_id']);
				$query_struct['limit'] = 1;
				if ($result = Service_Merchant_Context::instance()->tree($query_struct, array('attribute')))
				{
					$attr = $result[0]['attribute'];
					return isset($this->_data[$attr]) ? $this->_data[$attr] : NULL;
				}
			}
		}
		return NULL;
	}
	
	public function add($data = array())
	{
		if($data['merchant_id']==''||$data['sku']==''||$data['name']==''
			//||$data['link']==''
		)
		{
			Message::setc('Some of required fields are empty','error');
			return FALSE;
		}
		
		$check=$this->tree(array('where'=>array('sku'=>$data['sku'],'merchant_id'=>$data['merchant_id'])));
		if ( !empty($check))
		{
			Message::set('Duplicate SKU','error');
			return FALSE;
		}
//		$check=$this->tree(array('where'=>array('link'=>$data['link'],'merchant_id'=>$data['merchant_id'])));
//		if ( !empty($check))
//		{
//			Message::set('Duplicate link','error');
//			return FALSE;
//		}
		
		//TODO image upload and save into DB
		
		if($id=$this->dao($this->_dao_object)->add($data))
		{
			Message::set('Add product success');
			return $id;
		}
		else
			return false;
	}
	
	public function default_image($image_id)
	{
		if($this->_data['id'])
			$this->dao($this->_dao_object)->edit(array('id'=>$this->_data['id'],'main_image_url'=>$image_id));
	}
	
	public function edit($data = array())
	{
		if(!$this->_data['id'])
		{
			Message::set('No product id');
			return false;
		}
		$data['id']=$this->_data['id'];
		$i=1;
		
		while ($i <= 70)
		{
			$attribute_name='attribute'.str_pad($i,2,'0',STR_PAD_LEFT);
			if(isset($this->_data[$attribute_name]))
				break;
			if(!isset($data[$attribute_name]))
				$data[$attribute_name]='';
			$i++;
		}
		if($this->dao($this->_dao_object)->edit($data))
		{
			Message::set('Edit product success');
			return true;
		}
		else
		{
			Message::set('Edit product fail');
			return false;
		}
	}
	
	public function delete()
	{
		if(!$this->_data['id'])
		{
			Message::set('No product id');
			return false;
		}
		if($this->dao($this->_dao_object)->delete($this->_data['id']))
		{
			Message::set('Delete product success');
			return true;
		}
		else 
		{
			Message::set('Delete product fail');
			return false;
		}
	}
	
	public function getAttributesArray()
	{
		if(!$this->_data['id'])
		{
			return array();
		}
		$attributes_lists=Service_Merchant_Context::instance()->getContextNameArray($this->_data['merchant_id']);
		$attributes_lists_format=array();
		foreach($attributes_lists as $key=>$attribute)
		{
			foreach ($attribute as $a)
			{
				if(isset($a['attribute'])&&$a['attribute'])
					$attributes_lists_format[$key][$a['attribute']]=$a;
			}
		}
		if(!isset($attributes_lists_format[$this->_data['context_name']]))
			return array();
		$i=1;
		$arributes=array();
		while (true)
		{
			$attribute_name='attribute'.str_pad($i,2,'0',STR_PAD_LEFT);
			if(!isset($this->_data[$attribute_name]))
				break;
			if($this->_data[$attribute_name])
				$arributes[$attributes_lists_format[$this->_data['context_name']][$attribute_name]['attribute_label']]=$this->_data[$attribute_name];
			$i++;
		}
		return $arributes;
	}
	
	public function showColumnsArray()
	{
		return $this->dao($this->_dao_object)->showColumns();
	}
	
    public function erpDomain()
    {
        return substr($this->get('domain'), 4);
    }

    public function erpEnabled()
    {
        return (bool)$this->get('erp_enabled');
    }
    
    /**
     * 产品关联到ERP 
     * @param int $customerId
     */
    public static function associateErpProduct($productId = null, $orgId = '404', $inventory_planer = null, $serial_control = null, $lot_control = null, $inventory_plan_method = null, $default_buyer = null, $inventory_min = null, $inventory_max = null, $locator_ctrl = null)
    {
    	if (!$productId)
    		return FALSE;
    	if (!is_array($productId))
    		$productId = array($productId);
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
		return $ErpService->syncProduct($productId, $orgId, $inventory_planer, $serial_control, $lot_control, $inventory_plan_method, $default_buyer, $inventory_min, $inventory_max, $locator_ctrl);
    }
        
    /**
     * 产品关联到ERP 
     * @param int $customerId
     */
    public static function updateErpProduct($productId = null)
    {
    	if (is_null($productId))
    		return FALSE;
    		
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
		return $ErpService->updateProduct($productId);
    }
	
}
