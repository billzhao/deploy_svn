<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Glasses lens
 *
 * @package    Cola/Glasseslens
 * @category   Controllers
 */
class Controller_Glasseslens extends Controller
{
    public function action_index($url = NULL)
    {
        if (!$url)
        {
            $this->request->redirect('/');
        }

        $route_type = Site::instance()->get('route_type');
        switch ($route_type)
        {
            case 2:
                $result = DB::select('id')->from('products')->where('link', '=', $url)->execute()->current();
                if(isset($result['id']) AND $result['id'])
                {
                    $gproduct = Product::instance($result['id']);
                }
                else
                {
                    $gproduct = NULL;
                }
                break;

            case 1:
                $result = DB::select('id')->from('products')->where('link', '=', $url)->execute()->current();
                if(isset($result['id']) AND $result['id'])
                {
                    $gproduct = Product::instance($result['id']);
                }
                else
                {
                    $gproduct = NULL;
                }
                break;

            default:
                $gproduct = Product::instance($url);
                break;
        }

        if(!isset($gproduct) OR !$gproduct->get('id'))
        {
            $this->request->redirect('404');
        }

//        View::set_global('meta_title', $gproduct->get('meta_title'));
//        View::set_global('meta_keywords', $gproduct->get('meta_keywords'));
//        View::set_global('meta_description', $gproduct->get('meta_description'));

        $product_data = array(
            'gproduct' => $gproduct->get(),
            'product' => $gproduct->get(),
            'attributes' => $gproduct->set_data(),
        );

        $glasseslens = Glasseslens::instance();
        if ($_POST)
        {
            //TODO check post values
            $post = $_POST;
            if (isset($post['id']))
                $post['product_id'] = $post['id'];
            if (isset($post['items']))
                $post['product_items'] = $post['items'];
            if (isset($post['quantity']))
                $post['product_quantity'] = $post['quantity'];
            $valid = true;//TODO
            if ($valid)
            {
                Cart::instance()->add2cart($post);
                $key = $post['product_id'].'_'.implode($post['product_items'], ',');
                $glasseslens->add_prescription($key, $post);
                $this->request->redirect('/cart/view');
                if ($post['_add_to_cart'])
                {
                    //if add to cart, add glasses lens to cart, then direct to cart list page.
                    $this->request->redirect('/cart/view');
                }
                else
                {
                    //if add to cart then continue shopping, add glasses lens to cart, then direct to home page.
                    $this->request->redirect('/');
                }
            }
        }

        $this->request->response = View::factory('base')
            ->set('product', $product_data['product'])
            ->set('gproduct', $product_data['gproduct'])
            ->set('attributes', $product_data['attributes'])
            ->set('lens', $glasseslens->lens)
            ->set('sph', $glasseslens->sph)
            ->set('cyl', $glasseslens->cyl)
            ->set('axis', $glasseslens->axis)
            ->set('add', $glasseslens->add)
            ->set('pd', $glasseslens->pd)
            ->set('near_pd', $glasseslens->near_pd)
            ->set('mono', $glasseslens->mono)
            ->set('lens_index', $glasseslens->lens_index)
            ->set('color_tint', $glasseslens->color_tint)
            ->set('ptp', $glasseslens->ptp)
            ->set('ptv', $glasseslens->ptv)
            ->set('anti', $glasseslens->anti)
            ->render();
    }

    public function action_reduce($key = NULL, $ppkey = NULL)
    {
        $cart_product = Cart::instance()->get_product($key);
        Cart::instance()->quantity($key, -1);
        $cart_prescription = Glasseslens::instance()->get_prescription($key);
        if (isset($cart_prescription[$ppkey]))
        {
            Glasseslens::instance()->quantity($key, $ppkey, -1);
        }
        $this->request->redirect('cart/view');
    }

    public function action_increase($key = NULL, $ppkey = NULL)
    {
        $cart_product = Cart::instance()->get_product($key);
        Cart::instance()->quantity($key, 1);
        $cart_prescription = Glasseslens::instance()->get_prescription($key);
        if (isset($cart_prescription[$ppkey]))
        {
            Glasseslens::instance()->quantity($key, $ppkey, 1);
        }
        $this->request->redirect('cart/view');
    }

    public function action_delete($key = NULL, $ppkey = NULL)
    {
        if ($ppkey == NULL)
        {
            $pp_qty = 0;
            $product = Cart::instance()->get_product($key);
            $cart_prescription = Glasseslens::instance()->get_prescription($key);
            if (isset($cart_prescription[$key]))
            {
                $product_prescription = $cart_prescription[$key];
                foreach($product_prescription as $pp)
                {
                    $pp_qty += $pp['quantity'];
                }
            }
            Cart::instance()->quantity($key, $pp_qty-$product['quantity']);
        }
        else
        {
            $product = Cart::instance()->get_product($key);
            $cart_prescription = Glasseslens::instance()->get_prescription($key);
            if (isset($cart_prescription[$ppkey]))
            {
                if ($product['quantity'] == $cart_prescription[$ppkey]['quantity'])
                {
                    Cart::instance()->delete($key);
                }
                else
                {
                    Cart::instance()->quantity($key, 0-$cart_prescription[$ppkey]['quantity']);
                }
                Glasseslens::instance()->delete_prescription($key, $ppkey);
            }
        }
        $this->request->redirect('/cart/view');
    }

    public function action_media()
    {
        // Generate and check the ETag for this file
        $this->request->check_cache(sha1($this->request->uri));

        // Get the file path from the request
        $file = $this->request->param('file');

        // Find the file extension
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        // Remove the extension from the filename
        $file = substr($file, 0, -(strlen($ext) + 1));

        if (($file = Kohana::find_file('media', $file, $ext)) !== FALSE)
        {
            // Send the file content as the response
            $this->request->response = file_get_contents($file);
        }
        else
        {
            // Return a 404 status
            $this->request->status = 404;
        }

        // Set the proper headers to allow caching
        $this->request->headers['Content-Type']   = File::mime_by_ext($ext);
        $this->request->headers['Content-Length'] = filesize($file);
        $this->request->headers['Last-Modified']  = date('r', filemtime($file));
    }
}