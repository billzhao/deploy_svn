<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @ClassName: DAO_Channel_New
 *
 * @author bzhao
 *
 * @version $Id: new.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_Channel_New extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'channel_new';
}