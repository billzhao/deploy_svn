<?php

if (!Kohana::config('points.enabled'))
{
    return;
}

function points_order_payment($data)
{
    $amount = Arr::get($data, 'amount', 0);
    $order = Arr::get($data, 'order', NULL);

    $amount =  $amount / $order->get('rate');

    if (!$order)
        return FALSE;
  
    $customer = Customer::instance($order->get('customer_id'));
    
    $order_detail = $order->get();

    if($customer->get('id'))
    {
        $customer->add_point(array(
            'type' => 'order',
            'status' => 'pending',
            'amount' => (int) $amount,
        	'order' => $order_detail,
        ));
    }

    return TRUE;
}

function points_customer_register($customer)
{
    if (!$customer->get('id'))
        return FALSE;

    $amount = Kohana::config('points.register');
    if ($customer->point_inc($amount)) 
    {
        $customer->add_point(array(
            'type' => 'register', 
            'amount' => $amount, 
            'status' => 'activated', 
        ));
    }

    return TRUE;
}

Event::add('Order.payment', 'points_order_payment');
Event::add('Customer.register', 'points_customer_register');
