<?php defined('SYSPATH') or die('No direct script access.');

class Model_Project_Server extends ORM
{
    protected $_belongs_to = array(
        'project' => array(), 
        'server' => array(), 
    );
}
