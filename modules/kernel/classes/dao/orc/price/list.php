<?php defined('SYSPATH') OR die('No direct access allowed.');

class DAO_Orc_Price_List extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'orc_price_list';
}