<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Service User
 * @package Service 
 * @version $Id: user.php 6699 2012-04-11 12:53:10Z zhao.yang $ 
 * @copyright 2012 Cofree Development Term
 */
class Service_User extends Service
{
	private $_data = NULL;

    private $_dao_object = 'user';
	
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }    
    
	protected  function __construct($id = 0)
	{
		parent::__construct();
		$this->_load($id);
	}
	
	public function tree($query_struct,$column = array())
	{
		return $this->dao($this->_dao_object)->find_all($query_struct, $column);
	}

	public function treeList($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
//		if ( ! is_null($this->strategy))
//		{
//			if (method_exists($this->strategy , 'serviceB2BNewsList'))
//			{
//				return $this->strategy->serviceB2BNewsList($this->_dao,$query_struct,$select,$key,$value);
//			}	
//		}
		
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param int $key  primary_id value
	 * @param string $field 字段名
	 * @return mixed
	 */
	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	/**
	 * Cloud平台用户注册
	 * <code>        
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code> 
	 * @param array $data
	 */
	public function register($data = array())
	{
		if (isset($data['password']))
			$data['password'] = toolkit::hash($data['password']);
		
		if ($this->isRegisterd($data['email']))
		{
			return FALSE;
		}
		
		return $this->dao($this->_dao_object)->add($data);
	}

	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }

        return $this->dao($this->_dao_object)->edit($data);
	}

	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}
	
	/**
	 * 用户登录
	 * @param array $data
	 * @return boolen|Model_User
	 */
	public function login($data)
    {
        $email 		= $data['email'];
        $password 	= $data['password'];

        $query_struct = array(
                'where' => array(
                	'email' 	=> $email,
        			'password' 	=> toolkit::hash($password))      		
            );
        
		if ( $user = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $user['id'];
		}
		return FALSE;
    }
    
    public function find($query_struct)
    {
		if ( $user = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $user['id'];
		}
		return FALSE;
    }

    /**
     * 判断登录状态
     */
    public static function loggedIn()
    {
        $session = Session::instance();
        $user_id = $session->get('user_id');
        
        if (isset($user_id) && $user_id != 0)
        {
            $user = Service_User::instance($user_id)->get();
            
            /*加载用户当前Schema*/
            $user = $user + Service_User::instance($user_id)->getUserSchema();
            
            return $user;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Session User Action 
     */
	public function loginAction()
	{
		$data = array( );
		$data['id'] = $this->get('id');
		$data['email'] = $this->get('email');
		$session = Session::instance();
		$session->set('user', $data);
	}


	/**
	 * 邮箱判断
	 * @param string $email
	 * @return int | bool 
	 */
    public function isRegisterd($email)
    {
		$query_struct = array(
			'where'	=> array('email' => $email));
		
		if ( $user = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $user['id'];
		}
		return FALSE;
    }
    
    public function load($query_struct = array())
    {
		if ( $user = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $user['id'];
		}
		
    }
    
    /**
     * 获取用户的Detail信息
     * @param int $uid
     * @return array
     */
    public function getUserDetail($uid)
    {
    	return $this->dao($this->_dao_object)->getUserDetail($uid);
    }

    
    /**
     * 得到用户的Role详情,多对多关系需要处理
     * @param int $id
     * @return array
     */
    public function getUserRoleSchema($uid = NULL)
    {
    	if ( ! $uid)
		{
			$uid = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
    	/**
    	 * 获取用户的角色信息
    	 */
    	$response = $this->dao($this->_dao_object)->getUserRole($uid);

    	$user_roles = array();
    	
    	foreach ($response as $res)
    	{
				$user_roles[$res->id] = array(
    			'role_description' 	=> $res->role_description,
    			'resource_id'		=> $res->resource_id,
    			'resource_type'		=> $res->resource_type,
    			'role_schema'		=> $res->role_schema,
    			'role_link'			=> $res->role_link,
    			);
    	}
    	
    	return $user_roles;
    }
    
    /**
     * 得到用户唯一的 Schema信息 
     * @param int $uid
     * @return array 
     * @example
     * <code>
     * array(
     * 'role_description'	=>	'CLOUD',
     * 'role_schema'		=>	'010000'
     * )
     * </code>
     */
    public function getUserSchema($uid = NULL)
    {
        if ( ! $uid)
		{
			$uid = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
		
		try
		{
    		$response = $this->dao($this->_dao_object)->getUserRole($uid);

    		$user_roles = array();
    
    		$user_roles = array(
    			'role_description'	=>	$response[0]->role_description,
    			'role_schema'		=> 	$response[0]->role_schema,
    			'resource_id'		=>	$response[0]->resource_id,
    			'resource_type'		=>  $response[0]->resource_type,
    		);
    	
    		return $user_roles;
		}
		catch(Exception $e)
		{
			Request::instance()->redirect('cloud/user/logout');	
		}
    }
    
	/**
	 * 设置用户角色
	 * @param int or array $schema
	 * @param int $resource_id
	 * @param int $resource_type
	 * @return bool
	 * @example
	 * <code>
	 * 分配所有权限-->权限树Push
	 * return Service_User::instance()->setUserRole(
	 * 	$user_id,  //用户ID
	 * 	array(3,13,14,16), //Schema集合
	 * 	1,//资源ID
	 * 	2);//资源类型
	 * 分配商户权限
	 * return Service_User::instance()->setUserRole(
	 * 	$user_id,	//用户ID
	 * 	2,			//商户Schema
	 * 	$merchant_id,	//商户ID
	 * 	1);				//商户类型
	 * 分配渠道权限(channel)
	 * return Service_User::instance()->setUserRole(
	 * 	$user_id,	//用户ID
	 * 	3|4|5|6,			//渠道SchemaID
	 * 	$channel_id,	//渠道ID
	 * 	2);				//渠道类型
	 * </code>
	 */
    public function setUserRole($uid = NULL,$schema,$resource_id = NULL,$resource_type = 1)
    {
        if ( ! $uid)
		{
			$uid = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
		
    	if (is_array($schema))
		{
			$error = 0;
			
			for ($i = 0; $i < count($schema); $i++)
			{
				if ( ! $this->setUserRole($uid,$schema[$i],$resource_id,$resource_type))
					$error++;
			}
			return $error == 0;
		}
		
		//validate
        if ( ! $uid OR is_null($uid))
    	{
    		return FALSE;
    	}
    	//resource
    	if ( is_null($resource_id))
		{
			return FALSE;
		}
		//schema
		if ( ! Service_Schema::instance($schema)->get('id'))
		{
			return FALSE;
		}
		
		//插入Role表
		$data['schema_id'] = $schema;
		$data['resource_id'] = $resource_id;
		$data['resource_type'] = $resource_type;//TODO 判读写入
		
		if ( ! $roleid = Service_Role::instance()->set($data))//根据schema_id写入
		{
			return FALSE;
		}
		
		return $this->dao($this->_dao_object)->setUserRole($uid,$roleid);
		
    }
    
    /**
     * 得到当前用户的默认ChannelRoles数组
     * @param int $uid
     */
    public function getUserChannelRole($uid = NULL)
    {
        if ( ! $uid OR is_null($uid))
    	{
    		return FALSE;
    	}

    	if ( ! $user_roles = $this->getUserRoleSchema($uid))
    	{
    		return FALSE;
    	}
    	
    	$channel_roles = array();

    	foreach ($user_roles as $role)
    	{
    		if ( $role['resource_type'] == 2)
    		{
    			array_push($channel_roles,$role['resource_id']);
    		}
    	}
    	
    	return $channel_roles;
    }
    
    /**
     * 得到当前用户的默认MerchantRoles数组
     * @param int $uid
     */
    public function getUserMerchantRole($uid = NULL)
    {
    	//1个用户默认1个商户
        if ( ! $uid OR is_null($uid))
    	{
    		return FALSE;
    	}
    	
    	if ( ! $user_roles = $this->getUserRoleSchema($uid))
    	{
    		return FALSE;
    	}
    	
    	$merchant_roles = array();
    	
    	foreach ($user_roles as $role)
    	{
    		if ( $role['resource_type'] == 1)
    		{
    			array_push($merchant_roles,$role['resource_id']);
    			break;
    		}
    	}
    	
    	return array_unique($merchant_roles);
    }
    
    
    /**
     * User关联到B2B Sale 
     */
    public function setB2BSaleRep()
    {
    	$uid = $this->_data['id'];
    	
    	/*
    	 * 佣金
    	 * Uid
    	 */
    	
    }
	
    /**
     * 用户关联到ERP Sales
     * @param int $saleId
     */
	public static function associateErp($saleId = null)
    {
    	if (is_null($saleId))
    		return FALSE;
    		
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
		return $ErpService->getSaleErpId($saleId);
    }  
 
    
    public static function updateErpSale($saleId = null)
    {
    	if (is_null($saleId))
    		return FALSE;
    		
   		$ErpService = new ErpService(FALSE,'icebearatv.com');
   	
   		
		return $ErpService->updateSaleErpId($saleId);    	
    }

}
