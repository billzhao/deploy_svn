<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 商户观察
 * @ClassName: Subject_Merchant
 *
 * @author bzhao
 *
 * @version $Id: merchant.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Subject_Merchant implements SplSubject
{
	private $observers;
	
	public function __construct()
	{
		$this->observers = new SplObjectStorage();
	}
	
	public function attach(SplObserver $observer)
	{
		$this->observers->attach($observer);
	}
	
	public function detach(SplObserver $observer)
	{
		$this->observers->detach($observer);
	}
	
	/**
	 * 编辑商户 
	 */
	public function editMerchant()
	{
		$this->notify();
	}
	
	public function notify()
	{
		foreach ($this->observers as $observer)
		{
			$observer->update($this);
		}

	}
}

class MerchantOberver implements SplObserver
{
	public function update(SplSubject $subject)
	{
		//邮件通知
		Message::setc('Edit Merchant','info');
	}
}