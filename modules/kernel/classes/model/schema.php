<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * @ClassName: Model_Schema
 *
 * @author bzhao
 *
 * @version $Id: schema.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Schema extends ORM
{	
	protected $_filters = array(
		TRUE => array('trim' => NULL)
	);
	
    protected $_rules = array(
    	'schema_description'	=> 	array('not_empty' => NULL,'max_length' => array(100)),
    	'schema_link'			=>  array('not_empty' => NULL,'max_length' => array(100)),
    	'schema_code'			=>	array('not_empty' => NULL),
        'schema_level'			=>	array('not_empty' => NULL),
    );


}
