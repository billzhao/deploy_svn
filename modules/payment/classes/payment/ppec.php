<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * Payment PPEC
 * @category	Payment
 * @author     Vincent
 * @copyright  (c) 2009-2012 Cofree
 */
class Payment_Ppec extends Payment
{

	protected $_config;
	private $_ec_config;

	public function __construct($config)
	{

		$this->_config = $config;
		$this->_ec_config = array(
			'VERSION' => Site::instance()->get('pp_api_version'),
			'USER' => Site::instance()->get('pp_api_user'),
			'PWD' => Site::instance()->get('pp_api_pwd'),
			'SIGNATURE' => Site::instance()->get('pp_api_signa'),
			'SUBMITURL' => Site::instance()->get('pp_submit_url'),
			'JUMPURL' => Site::instance()->get('pp_payment_url'),
			'RETURNURL' => Site::instance()->get('pp_ec_return_url'),
			'CANCELURL' => Site::instance()->get('pp_cancel_return_url'),
			'NOTIFYURL' => Site::instance()->get('pp_ec_notify_url'),
			'HDRIMG' => Site::instance()->get('pp_logo_url'),
		);
	}

	/**
	 *  SetExpressCheckout
	 */
	public function set($amount, $currency)
	{
		$nvpstr = "&METHOD=SetExpressCheckout".
			"&VERSION=".$this->_ec_config['VERSION'].
			"&USER=".$this->_ec_config['USER'].
			"&SIGNATURE=".$this->_ec_config['SIGNATURE'].
			"&PWD=".$this->_ec_config['PWD'].
			"&RETURNURL=".$this->_ec_config['RETURNURL'].
			"&CANCELURL=".$this->_ec_config['CANCELURL'].
			"&NOTIFYURL=".$this->_ec_config['NOTIFYURL'].
			"&CURRENCYCODE=".$currency.
			"&AMT=".$amount;
		parse_str(Toolkit::curl_pay($this->_ec_config['SUBMITURL'], $nvpstr), $result);
		return $result;
	}

	/**
	 * GetExpressCheckoutDetails
	 */
	public function get($token)
	{
		$nvpstr = "&METHOD=GetExpressCheckoutDetails".
			"&VERSION=".$this->_ec_config['VERSION'].
			"&USER=".$this->_ec_config['USER'].
			"&SIGNATURE=".$this->_ec_config['SIGNATURE'].
			"&PWD=".$this->_ec_config['PWD'].
			"&TOKEN=".$token;
		parse_str(Toolkit::curl_pay($this->_ec_config['SUBMITURL'], $nvpstr), $result);
		return $result;
	}

	/**
	 * DoExpressCheckoutPayment
	 */
	public function go($order, $payerid, $token)
	{
		$nvpstr = "&METHOD=DoExpressCheckoutPayment".
			"&VERSION=".$this->_ec_config['VERSION'].
			"&USER=".$this->_ec_config['USER'].
			"&SIGNATURE=".$this->_ec_config['SIGNATURE'].
			"&PWD=".$this->_ec_config['PWD'].
			"&PAYMENTACTION=sale".
			"&PAYERID=".$payerid.
			"&AMT=".$order['amount'].
			"&CURRENCYCODE=".$order['currency'].
			"&INVNUM=".$order['ordernum'].
			"&NOTIFYURL=".$this->_ec_config['NOTIFYURL'].
			"&TOKEN=".$token;
		parse_str(Toolkit::curl_pay($this->_ec_config['SUBMITURL'], $nvpstr), $result);
		return $result;
	}

	/**
	 * Paypal payment
	 * @param array $order	Order detail
	 * @param array $data		Paypal return
	 * @return stirng		SUCCESS
	 */
	public function pay($order, $data = NULL)
	{
		$payment_log_status = "";

		$order_update = array(
			'amount_payment' => $order['amount'],
			'currency_payment' => $order['currency'],
			'transaction_id' => $data['txn_id'],
			'payment_date' => time(),
			'updated' => time(),
			'shipping_firstname' => $data['first_name'],
			'shipping_lastname' => $data['last_name'],
			'shipping_address' => $data['address_street'],
			'shipping_zip' => $data['address_zip'],
			'shipping_city' => $data['address_city'],
			'shipping_state' => $data['address_state'],
			'shipping_country' => $data['address_country'],
			'shipping_phone' => '',
			'billing_firstname' => $data['first_name'],
			'billing_lastname' => $data['last_name'],
			'billing_address' => $data['address_street'],
			'billing_zip' => $data['address_zip'],
			'billing_city' => $data['address_city'],
			'billing_state' => $data['address_state'],
			'billing_country' => $data['address_country'],
			'billing_phone' => '',
		);

		switch( $data['payment_status'] )
		{
			case 'Completed':
				//payment platform sync
				$post_var = "order_num=".$order['ordernum']
					."&order_amount=".$order['amount']
					."&order_currency=".$order['currency']
					."&card_num=".$order['cc_num']
					."&card_type=".$order['cc_type']
					."&card_cvv=".$order['cc_cvv']
					."&card_exp_month=".$order['cc_exp_month']
					."&card_exp_year=".$order['cc_exp_year']
					."&card_inssue=".$order['cc_issue']
					."&card_valid_month=".$order['cc_valid_month']
					."&card_valid_year=".$order['cc_valid_year']
					."&billing_firstname=".$data['first_name']
					."&billing_lastname=".$data['last_name']
					."&billing_address=".$data['address_street']
					."&billing_zip=".$data['address_zip']
					."&billing_city=".$data['address_city']
					."&billing_state=".$data['address_state']
					."&billing_country=".$data['address_country']
					."&billing_telephone=".''
					."&billing_ip_address=".long2ip($order['ip'])
					."&billing_email=".$order['email']
					."&shipping_firstname=".$data['first_name']
					."&shipping_lastname=".$data['last_name']
					."&shipping_address=".$data['address_street']
					."&shipping_zip=".$data['address_zip']
					."&shipping_city=".$data['address_city']
					."&shipping_state=".$data['address_state']
					."&shipping_country=".$data['address_country']
					."&shipping_telephone=".''
					.'&trans_id='.$data['txn_id']
					.'&payer_email='.$data['payer_email']
					.'&receiver_email='.$data['receiver_email']
					."&site_id=".Site::instance()->get('cc_payment_id')
					."&secure_code=".Site::instance()->get('cc_secure_code');

				$result = unserialize(stripcslashes(Toolkit::curl_pay(Site::instance()->get('pp_sync_url'), $post_var)));

				if(is_array($result))
				{
					$result['status_id'] = isset($result['status_id']) ? $result['status_id'] : '';
					$result['status'] = isset($result['status']) ? $result['status'] : '';
					$result['trans_id'] = isset($result['trans_id']) ? $result['trans_id'] : '';
					$result['message'] = isset($result['message']) ? $result['message'] : '';
					$result['api'] = isset($result['api']) ? $result['api'] : '';
					$result['avs'] = isset($result['avs']) ? $result['avs'] : '';
				}

				$order_update['payment_count'] = $order['payment_count'] + 1;
				$order_update['payment_status'] = 'success';
				$payment_log_status = 'success';
				break;
			case 'Pending':
				$order_update['payment_count'] = $order['payment_count'] + 1;
				$order_update['payment_status'] = 'pending';
				$payment_log_status = 'pending';
				break;
			case 'Refunded':
				$order_update['refund_status'] = 'refund';
				$payment_log_status = 'refund';
				break;
			case 'Failed':
				$order_update['payment_status'] = 'failed';
				$payment_log_status = 'failed';
				break;
		}

		Order::instance($order['id'])->set($order_update);

		$payment_log = array(
			'site_id' => Site::instance()->get('id'),
			'order_id' => $order['id'],
			'customer_id' => $order['customer_id'],
			'payment_method' => $this->_config['name'],
			'trans_id' => $data['txn_id'],
			'amount' => $data['mc_gross'],
			'currency' => $data['mc_currency'],
			'comment' => $data['payment_status'],
			'cache' => serialize($data),
			'payment_status' => $payment_log_status,
			'ip' => ip2long(Request::$client_ip),
			'created' => time(),
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['payer_email'],
			'address' => $data['address_street'],
			'zip' => $data['address_zip'],
			'city' => $data['address_city'],
			'state' => $data['address_state'],
			'country' => $data['address_country'],
			'phone' => '',
		);
		$this->log($payment_log);

		return 'SUCCESS';
	}

}
