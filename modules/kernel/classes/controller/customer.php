<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @ClassName: Controller_Customer
 *
 * @author bzhao
 *
 * @version $Id: customer.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_Customer extends Controller_Webpage
{

    public function action_view()
    {
        $content['redirect'] = Arr::get($_GET,'redirect',0);
        $response = Request::factory('order/list',$content)->execute();
        $this->request->response = $response;
    }
    
    /**
     * customer 登入
     */
    public function action_login()
    {
       if ($_POST)
       {
            $data = array();
            /**
             * 账号密码处理
             */
            $data['email'] = Arr::get($_POST, 'email', '');
            $data['password'] = Arr::get($_POST, 'password', '');
            $data['remember'] = Arr::get($_POST, 'remember', '');//是否记录账号密码
            
            $email 		= $data['email'];
            $password 	= $data['password'];

            $query_struct = array(
                'where' => array(
                	'email' 	=> $email,
        			'password' 	=> toolkit::hash($password),
                    'channel_id' => Service_Channel::instance()->get('id'), 
                    'active' => 1
                )      		
             );
            
            if ($customer_id = Service_Channel_Customer::instance()->login($query_struct))
            {   
                /**
                 * 记住账号密码 cookies
                 */
            	 if($data['remember'] == 1)
                 {
                	Cookie::set('icebear_customer_remeber' , serialize($data) , 2592000);
                 }
                
           		 $redirect = Arr::get($_REQUEST, 'redirect', 0);
           	     $referer = Arr::get($_GET,'referer','');
              
                /**
                 * 记录登陆状态
                 */
                 Service_Channel_Customer::instance($customer_id)->loginAction();	//存session

                 ($redirect && toolkit::is_our_url($redirect)) ?
                  	$this->request->redirect($redirect) :
                  		($referer && toolkit::is_our_url($referer)) ?
                  			$this->request->redirect($referer):
                 				 $this->request->redirect(URL::base());
            }
            else
            {
                Message::set(__('site_login_error'),'error');
            }
        }
        
        
        $content['referer'] = (isset($_SERVER['HTTP_REFERER']) AND toolkit::is_our_url($_SERVER['HTTP_REFERER'])) ?
            $_SERVER['HTTP_REFERER']:0;
            
        $content['redirect'] = Arr::get($_REQUEST,'redirect',0);
        
        
        $this->request->response = View::factory('/customer/customer_login',$content)->render();
    }
    
    /**
     * customer 登出
     * 
     */
    public function action_logout()
    {
        $referer = (isset($_SERVER['HTTP_REFERER']) AND toolkit::is_our_url($_SERVER['HTTP_REFERER'])) ?
            $_SERVER['HTTP_REFERER']:NULL;
        Session::instance()->delete('customer');
        Cookie::delete('icebear_customer_remeber');
        $this->request->redirect(URL::base());
    }


    public function action_retailer_register()
    {
    	if ($_POST)
        {
        	$data =array();
            $data['channel_id'] = Service_Channel::instance()->get('id');
            $data['name'] = $_POST['dealername'];
            $data['email'] = $_POST['email'];
            $data['password'] = $_POST['password'];
            $data['contact_name'] = $_POST['contactname'];
            $data['state'] = $_POST['state'];
            $data['city'] = $_POST['city'];
            $data['zip'] = $_POST['zip'];
            $data['country'] = $_POST['country'];
            $data['phone'] = $_POST['phone'];
            
            $do = array(
                 'where' => array(
                         'email' => $data['email'],
                         'channel_id' => $data['channel_id']
                         )
                 );
                 /**
                  * 邮箱是否被注册
                  */
         
            if(Service_Channel_Customer::instance()->login($do))
            {
                Message::set(__('The email is already registered'),'error');
                $this->request->redirect('customer/retailer_register');
            }
           if(Service_Channel_Customer::instance()->add($data))
            {
              message::set(__('An application has been received, please wait for confirmation'));
              $this->request->redirect('customer/login');
            }
            else{
              message::set(__('failed'),'error');
              $this->request->redirect('customer/retailer_register');
            }
        }
          $this->request->response = View::factory('/customer/customer_register_part')
             ->render();
    }
    
    public function action_dealer_register()
    {
        if ($_POST)
        {
        	
            $data =array();
            $data['channel_id'] = Service_Channel::instance()->get('id');
            $data['name'] = $_POST['dealername'];
            $data['email'] = $_POST['email'];
            $data['contact_name'] = $_POST['contactname'];
            $data['state'] = $_POST['state'];
            $data['city'] = $_POST['city'];
            $data['zip'] = $_POST['zip'];
            $data['country'] = $_POST['country'];
            $data['phone'] = $_POST['phone'];
            $data['federal_tax_id'] = $_POST['taxidno'];
            $data['seller_permit'] = $_POST['permitno'];
            $data['dealership_license'] = $_POST['licenseno'];
            $data['fax'] = $_POST['fax'];
            $do = array(
                 'where' => array(
                         'email' => $data['email'],
                         'channel_id' => $data['channel_id']
                         )
                 );
                 /**
                  * 邮箱是否被注册
                  */
         
            if(Service_Channel_Customer::instance()->login($do))
            {
                Message::set(__('The email is already registered'),'error');
                $this->request->redirect('customer/dealer_register');
            }
           if(Service_Channel_Customer::instance()->add($data))
            {
              message::set(__('An application has been received, please wait for confirmation'));
              $this->request->redirect('customer/login');
            }
            else{
              message::set(__('failed'),'error');
              $this->request->redirect('customer/dealer_register');
            }
         
           
        }
        
        $this->request->response = View::factory('/customer/customer_register')
             ->render();
        
    }
    
    /**
     *customer 信息 
     */
    public function action_profile()
    {
    	/**
    	 * 判断是否登入
    	 */
        if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
        {
            $this->request->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
        }
        /**
         *获取customer信息
         */
        $customer = Service_Channel_Customer::instance($customer_id)->get();
 
        if(!$customer['sale_id'])
        {
            $this->request->redirect('site/404');
        }
        /**
         * 获得customer credit card 信息
         */
        $id = array(
             'where' => array('customer_id' => $customer_id
                 			 )
                 );
         $card = service_Channel_CreditCard::instance()->tree($id);
        
        $this->request->response = View::factory('/customer/customer_profile')
            ->set('title',Service_Channel::instance()->get('meta_title'))
            ->set('data',$customer)
            ->set('card',$card)
             ->render();
    }
    
    /**
     *customer 信息修改
     */
    public function action_profile_edit()
    {
       /**
    	 * 判断是否登入
    	 */
        if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
         {
            $this->request->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
         }
       /**
         *获取customer信息
         */
        $customer = Service_Channel_Customer::instance($customer_id)->get();
 
        if(!$customer['sale_id'])
         {
             $this->request->redirect('404');
         }
        
        if($_POST)
        {
          $post = array_filter($_POST);
          $data = array();
          if($post['contactname']){ 
            $data['contact_name'] = $post['contactname'];
            }
          if($post['phone']){
            $data['phone'] = $post['phone'];
            }
          if($post['taxidno']){
            $data['federal_tax_id'] = $post['taxidno'];
            }
          if($post['permitno']){
            $data['seller_permit'] = $post['permitno'];
            }
          if($post['licenseno']){
            $data['dealership_license'] = $post['licenseno'];
            }
          if($post['faxno']){
            $data['fax'] = $post['faxno'];
            }
          if(Service_Channel_Customer::instance($customer_id)->update($data + array('id' => $customer_id)))
            {
          	  message::set(__('update_customer_info_success'));
              $this->request->redirect('customer/profile');
            }
        }
       $this->request->response = View::factory('/customer/customer_profile_edit')
            ->set('title',Service_Channel::instance()->get('meta_title'))
            ->set('data',$customer)
            ->set('keywords',Service_Channel::instance()->get('meta_keywords'))
            ->set('description',Service_Channel::instance()->get('meta_description'))
            ->render();
    }
   
    
    /**
     * 修改密码
     * Enter description here ...
     */
    public function action_password()
    {
        if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
        {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
        }

        $customer = Service_Channel_Customer::instance($customer_id)->get();
        
        if ($_POST)
        {	
        	/**
        	 * 检查old password 是否正确
        	 */
            $oldpassword = Arr::get($_POST, 'oldpassword', '');
            $old_info =array();
            $old_info['password'] = $oldpassword;
            $old_info['email'] = $customer['email'];
            $customer_info = Service_Channel_Customer::instance()->login($old_info);
            
            
            if($customer_info)
            {
                $password = Arr::get($_POST, 'password', '');
                $data = array(
                       'password' => toolkit::hash($password)
                         );
                if (Service_Channel_Customer::instance($customer_id)->update_password($data + array('id' => $customer_id)))
                {
                	message::set(__('update_password_success'));
                	Request::instance()->redirect('customer/profile');
                }
                else
                {
                    message::set(__('update_password_error'), 'error');
                }
            }
            else
            {
                message::set(__('your oldpassword is error'), 'error');
            }

            
        }

        $this->request->response = View::factory('/customer/customer_password')
            ->set('title',Service_Channel::instance()->get('meta_title'))
            ->set('keywords',Service_Channel::instance()->get('meta_keywords'))
            ->set('description',Service_Channel::instance()->get('meta_description'))
            ->set('data',$customer)
            ->render();
    }
    /**
     * 信用卡信息
     */
    public function action_credit_card_info()
    {
      if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
          {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
           }
      $customer = Service_Channel_Customer::instance($customer_id)->get();
      
      $id = array(
             'where' => array('customer_id' => $customer_id
                 			 )
                 );
      $card = service_Channel_CreditCard::instance()->tree($id);
      
      $this->request->response = View::factory('/customer/credit_card_info')
            ->set('card',$card)
            ->render();
    }
    /**
     * 信用卡 添加
     */
    public function action_credit_card_add()
    {
      if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
          {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
           }
       $customer = Service_Channel_Customer::instance($customer_id)->get();   
       if($_POST)
        {
          $data =array();
          $data['customer_id'] = $customer_id;
          $data['channel_id'] = Service_Channel::instance()->get('id');
          $data['customer_name'] = $_POST['contactname'];
          $data['card_number'] = $_POST['cc_num'];
          $data['security_code'] = $_POST['cc_cvv'];
          $data['expiration_date'] = $_POST['cc_exp_month'].'/'.$_POST['cc_exp_year'];
          $data['bill_address'] = $_POST['billing_address'];
          $data['purchaser_name'] = $_POST['purchaser_name'];
          $data['purchase_order'] = $_POST['purchase_order'];
          $data['signature'] = $_POST['signature'];
          $data['print_name'] =$_POST['printname'];
          $data['date'] =$_POST['date'];
          
         if(Service_Channel_CreditCard::instance($customer_id)->add($data))
            {
          	  message::set(__('add_credit_card_success'));
              $this->request->redirect('customer/profile');
            }
          else
            {
              message::set(__('add_credit_card_failed'), 'error');
              $this->request->redirect('customer/credit_card_add');
            }
       }
      
      

     
       $this->request->response = View::factory('/customer/credit_card_add')
            ->set('customer',$customer)
            ->render();
    	
    }
    /**
     * 信用卡修改
     * Enter description here ...
     * @param string $data
     */
    public function action_credit_card_edit($id)
    {
       if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
          {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
          }
          
       $customer = Service_Channel_Customer::instance($customer_id)->get();  
       $card = Service_Channel_CreditCard::instance($id)->get();
       $data =array();
       $data = $card;
       $card_date = explode('/', $card['expiration_date']);
       $data['month'] = $card_date[0];
       $data['year'] = $card_date[1];
       
       if($_POST)
       {   
       	   if($card['active'] == 1)
       	   {
       	      message::set(__('You_cannot_edit_this_credit_card'));
              $this->request->redirect('customer/profile');
       	   }
           $cardinfo['customer_name'] = $_POST['contactname'];
           $cardinfo['card_number'] = $_POST['cc_num'];
           $cardinfo['security_code'] = $_POST['cc_cvv'];
           $cardinfo['expiration_date'] = $_POST['cc_exp_month'].'/'.$_POST['cc_exp_year'];
           $cardinfo['bill_address'] = $_POST['billing_address'];
           $cardinfo['purchaser_name'] = $_POST['purchaser_name'];
           $cardinfo['purchase_order'] = $_POST['purchase_order'];
           $cardinfo['signature'] = $_POST['signature'];
           $cardinfo['print_name'] = $_POST['printname'];
           $cardinfo['updated'] = time();
           $cardinfo['date'] = $_POST['date'];
           
           if(Service_Channel_CreditCard::instance($customer_id)->edit($cardinfo + array('id' => $id)))
             {
                  message::set(__('edit_credit_card_success'));
                  $this->request->redirect('customer/profile');
             }
             else
             {
                  message::set(__('edit_credit_card_failed'), 'error');
                  $this->request->redirect('customer/credit_card_info');
             }
       }
       
       $this->request->response = View::factory('/customer/credit_card_edit')
            ->set('customer',$customer)
            ->set('card',$data)
            ->render();
    
    }
    /**
     * 信用卡删除
     * @param string $id
     */
    public function action_credit_card_delete($id)
    {
       if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
          {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
          }
          
       $customer = Service_Channel_Customer::instance($customer_id)->get();
       if(Service_Channel_CreditCard::instance($customer_id)->del($id))
          {
                message::set(__('delete_credit_card_success'));
                $this->request->redirect('customer/profile');
          }
          else
          {
                message::set(__('delete_credit_card_failed'), 'error');
                $this->request->redirect('customer/credit_card_info');
          }
    
    }
    public function action_address()
    {
        if ( ! ($customer_id = Service_Channel_Customer::instance()->loggedIn()))
        {
            $this->request->redirect(URL::current(TRUE));
        }

        $addresses = Service_Channel_Customer::instance($customer_id)->addresses();
        $template = View::factory(Site::instance()->get('id').'/view/customer_address_list')
            ->set('data', $addresses)
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->render();
        $this->request->response = $template;
    }

    public function action_wishlist()
    {
        if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Message::set('need_log_in','notice');
            Request::instance()->redirect('/customer/login/?redirect=/customer/wishlist');
        }

        $count  = Wholesale_Customer::instance($customer_id)->count_wishlists();
        $pagination = Pagination::factory(array(
            'current_page' => array('source' => 'query_string', 'key' => 'page'),
            'total_items' => $count,
            'items_per_page' => Site::instance()->get('per_page'),
            'view' => Site::instance()->get('id').'/view/pagination',
            'auto_hide' => FALSE));
        $wishlists = Wholesale_Customer::instance($customer_id)->wishlists($pagination->offset, $pagination->items_per_page);

        $this->request->response = View::factory(Site::instance()->get('id').'/view/customer_wishlist_list')
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->set('page_view', $pagination->render())
            ->set('data', $wishlists)
            ->render();
    }

    public function action_find_password()
    {
        $site_id = $this->site_id;
        if ($_POST)
        {
            $email = Arr::get($_POST, 'email', '');
            $password = Wholesale_Customer::instance()->find_password($email);
            $customer_id = Wholesale_Customer::instance()->is_register($email);
            $data['password'] = $password;
            if ($password)
            {
                message::set(__('site_find_password_check'));
                mail::findpassword($customer_id, $data);
            }
            else
            {
                message::set(__('site_find_password_no_user'), 'error');
            }
        }
        $this->request->response = View::factory(Site::instance()->get('id').'/view/find_password')
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->render();
    }

    public function action_account()
    {
        if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }

        $news = ORM::factory('new')->order_by('id','desc')->limit(5)->find_all();

        $template = View::factory(Site::instance()->get('id').'/view/customer_account')
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->set('news',$news)
            ->render();
        $this->request->response = $template;
    }
    //customer自己下单了
   public function action_rx_place()
    {
    	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
        Wholesale_Erp::instance()->init();
    	#初始化ERP信息 
        $init = Wholesale_Erp::instance()->init();
        if($init == FALSE)
	 	{
	 		Message::set('Error: The current system can not process an order, please contact the administrator', 'error');
	 	}
        #判断有无 POST信息
        if ($_POST)
        {
            //TODO if post shipping_address_id, determine input shipping info the same as shipping_address_id's info(判断shipping address是否和input的值一样，如果不一样，shipping_address_id为0)
            //$_POST['shipping_address_id'] = 0;
            //TODO if post billing_address_id, determine input shipping info the same as billing_address_id's info
            //$_POST['billing_address_id'] = 0;
            $add = Request::factory('/customer/rx_internal_add')->execute()->response;
            if (!$add)
            {
                $this->request->redirect('/customer/rx_place');
            }
            else
            {
                $this->request->redirect('/customer/rx_place_confirm');
            }
        }
        #验证时间信息
        foreach(date::months() as $k => $v)
        {
            $month_arr[sprintf('%02d', $v)] = sprintf('%02d', $v);	//Array ( [01] => 01 [02] => 02 [03] => 03 [04] => 04 [05] => 05 [06] => 06 [07] => 07 [08] => 08 [09] => 09 [10] => 10 [11] => 11 [12] => 12 ) 
        }

        $exp_month_arr = $month_arr;
        $valid_month_arr = $month_arr;

        $exp_year_arr = date::years(date('Y'), date('Y') + 9);
        $valid_year_arr = date::years(date('Y') - 9, date('Y'));

   
       //判断用户是否已经关联
        $customer = Wholesale_Customer::instance($customer_id)->get();
        if(!$customer['erp_id'])
        {
        	Message::set("customer_not_associate_with_erp" , "error");
        	Request::instance()->redirect('/product-list');
        }
        else 
        {
        	$countries = Site::instance()->countries();
	        $cart = Wholesale_Cart::instance()->get($customer_id);
	        
	    	$carriers = Site::instance()->carriers('US');
            foreach($carriers as $key => $carrier)
        	{
            	$carrier_shipping_address['country'] = $countries[0]['isocode'];//print_r($carrier_shipping_address);
            	$carrier_price = Carrier::instance($carrier['id'])->get_price(($cart['total_weight'])?$cart['total_weight']:450, $carrier_shipping_address);
            	if($carrier['carrier'] == 'UsMail')
            	{
              		if($carrier_price !== FALSE)
            		{
                		$carriers[$key]['price'] = $carrier_price;
            		}
           	 		else
           	 		{
                		unset($carriers[$key]);
            		}
            	}
            	else 
            	{
             		unset($carriers[$key]);
           	 }
          
        	}
        }
        $form_arr['cc_exp_month'] = Form::select('cc_exp_month', $exp_month_arr, $cart['billing']['cc_exp_month']);
        $form_arr['cc_exp_year'] = Form::select('cc_exp_year', $exp_year_arr, $cart['billing']['cc_exp_year']);
        $form_arr['cc_valid_month'] = Form::select('cc_valid_month', $valid_month_arr, $cart['billing']['cc_valid_month']);
        $form_arr['cc_valid_year'] = Form::select('cc_valid_year', $valid_year_arr, $cart['billing']['cc_valid_year']);
        
        $customer_address = Wholesale_Customer::instance()->get_addresses($customer_id);
    	$rx_style = array('6001','6002','6003','6004','6005','6006','6007','6008','6009');
        $rx_color = array('Grey','Amber','Blue','Clear','VIOLET','Pink');
        $rx_lenstype = array(
        'Trivex Single Vision Lens HMC with Tint All Time Wear',
        'Trivex Single Vision Lens HMC with Tint Reading Only',
        'Trivex Single Vision Lens HMC with Tint Computer Glasses',
        'Trivex Progressive Lens HMC with Tint',
        'Trivex Freeform Lens HMC with Tint',
        '1.56 Bifocal Lens HMC FT28 with Tint');
		$rx_lensstyle = array('6001EYE54','6002EYE52','6003EYE54','6004EYE52','6005EYE52','6006EYE51','6007EYE52','6008EYE51','6009EYE52');
           
        $this->request->response = View::factory(Site::instance()->get('id').'/view/customer_rx_place')
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->set('cart',$cart)
            ->set('carriers',$carriers)
            ->set('countries',$countries)
            ->set('customer',$customer)
            ->set('customers',$customer_address)
            ->set('form_arr', $form_arr)
            ->set('rx_style', $rx_style)
            ->set('rx_color', $rx_color)
            ->set('rx_lenstype', $rx_lenstype)
            ->set('rx_lensstyle', $rx_lensstyle)
            ->render();

    } 
  public function action_rx_place_confirm()
    {
        if (Request::$method == 'POST')
        {
        	#处理ERP逻辑,设置到支付问题,处理支付这块的逻辑代码
            $this->request->response = Request::factory('/customer/rx_internal_confirm')->execute();
        }
        #得到订单中的SESSION用户信息
        $customer = Wholesale_Cart::instance()->customer();
        #得到Sale的信息
        #$customer_sale = Wholesale_Customer::instance($customer['parent_id'])->get();
        
        //confirm时候读取当前SESSIOn的信息
        $cart = Wholesale_Cart::instance()->get($customer['id']);
        //不读取当前的
        //$cart = unserialize(Wholesale_Customer::instance($this->customer_id)->get('cart_info'));
    	
        if(count($cart['products']) == 0 or ! isset($cart['products']))
        {
        	Message::set(__('Place order and add product first.'));
        	$this->request->redirect('/customer/rx_place');
        }
        $_action = '_place';
        $this->request->response = View::factory(Site::instance()->get('id').'/view/rx_confirm')
            ->set('cart', $cart)
            ->set('_action', $_action)
            ->set('customer',$customer)
            ->render();
    }
    #处方镜 
 	public function action_rx_internal_add()
    {
    	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
    	if ($_POST)
        {
            //TODO validate post
            Wholesale_Cart::instance()->clear();
            $post = $_POST;
            
            //add customer info to cart.
            $customer_info = Wholesale_Customer::instance($customer_id)->get();
            $customer_info['customer_balance'] = $post['customer_balance'];
            
            Wholesale_Cart::instance()->customer($customer_info);
            //check product sku & quantity, then add product/sku to cart session
                  $products = $post['product'];
            $has_products = FALSE;
            $has_recipe	  = TRUE;
            $has_memo 	  = TRUE;
            if (!$products)
                $has_products = FALSE;
            else 
            {
            	foreach($products as $key => $p)
            	{
            		$has_recipe	  = TRUE;
            		#处理处方信息,只做真假判断
            	#echo kohana::debug($p['recipe']);
            	if(isset($p['recipe']) && !empty($p['recipe']))
           		{
           			 if(!isset($p['recipe'][0][3]))
           					$p['recipe'][0][3] = '';
           				if(!isset($p['recipe'][0][9]))
           					$p['recipe'][0][9] = '';
           				if(!isset($p['recipe'][0][2]))
           					$p['recipe'][0][2] = '';
           				if(!isset($p['recipe'][0][4]))	
           					$p['recipe'][0][4] = '';
           				if(!isset($p['recipe'][0][7]))
           					$p['recipe'][0][7] = '';
           				if(!isset($p['recipe'][0][8]))
           					$p['recipe'][0][8] = '';
           				if(!isset($p['recipe'][1][2]))
           					$p['recipe'][1][2] = '';
           				if(!isset($p['recipe'][1][7]))
           					$p['recipe'][1][7] = '';
           				ksort($p['recipe'][0]);
           				ksort($p['recipe'][1]);
 
            		foreach($p['recipe'] as $re_key => $recipe)
            		{
            		       		if($p['hidden_lenstype'] == NULL && $p['hidden_lenstype'] == '')
            					{
            						$has_recipe = FALSE;
            						continue;
            					}
            					if($re_key == 1)
            					{
            						$re_loop = 0;
            						foreach($recipe as $re)
            						{
            							//前端判断PD信息
            							if($p['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint All Time Wear' || $p['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint Computer Glasses' || $p['hidden_lenstype'] == 'Trivex Progressive Lens HMC with Tint' || $p['hidden_lenstype'] == 'Trivex Freeform Lens HMC with Tint' || $p['hidden_lenstype'] == '1.56 Bifocal Lens HMC FT28 with Tint')
            							{
            								if($re_loop == 3 && empty($re))
            								{
												$has_recipe = FALSE;
            								}
            				    			if($re_loop == 8 && empty($re))
            								{
												$has_recipe = FALSE;
												#echo "L.DIST PD";
            								}
            							}
            							if($p['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint Reading Only')
            							{
            							    if($re_loop == 4 && empty($re))
            								{
												$has_recipe = FALSE;
            								}
            				    			if($re_loop == 9 && empty($re))
            								{
												$has_recipe = FALSE;
												#echo "L.DIST PD";
            								}
            							}
            							$re_loop++;
            						}
            					}
            		}
            	}
            	else
            	{
            		$has_recipe = FALSE;
            	} 
            	#处方判断
            	#styleno,erp_number,color,lenstype,确定一个SKU,确认是否存在数量信息,判断处方信息是否存在
                if ($p['styleno'] && $p['erp_item_number'] && $p['color'] && $p['lenstype'])
                {
                    $has_products = TRUE;
                    if(!$p['memo'])
                    	$has_memo = FALSE;
                    break;
                }
            }}
          	if(!$has_products)
          	{
                Message::set(__('order_add_product_no_product_error'), 'error');
                $this->request->response = FALSE;
          	}
            else
          	{
            	$total_price = 0;
            	if(!$has_memo)
            	{
            		Message::set(__('Patient name and PD must be filled out completely'), 'error');
                	$this->request->response = FALSE;
           		}
            	else 
            	{
            	if (!$has_recipe)
            	{
               	 	Message::set(__('Patient name and PD must be filled out completely'), 'error');
               	 	$this->request->response = FALSE;
           		}
            	else
            	{
            		//设置不可执行的Case数量
            		$_debugCase = 0;
                	foreach($products as $key => $product)
                	{
                    	$filters = array();
                    	$filters['styleno'] = $product['styleno'];
                    	$filters['erp_item_number'] = $product['erp_item_number'];
                    	//$filters['lenstype'] = $product['lenstype'];
                    	//$filters['color'] = $product['color'];
                    	//TODO 根据 collection, color, styleno, eyesize确定唯一的产品
                    	$_product = Wholesale_Product::instance()->filter_product($filters);

                	if (Request::$instance->action == 'rx_place')
                    {
                        $product['price'] = Wholesale_Product::instance($_product['id'])->price($post['customer_id']);
                        if($product['lenstype'] && $product['price'])
                        	$product['price']+=Wholesale_Product::instance()->rx_price($product['lenstype']);
                    }
                    
                    if (!$product['price'] && !$post['customer_id'])
                    {
                        $product['price'] = $_product['market_price'];
                    }
                    
          				$has_recipe = TRUE;
                	     #二次读取处方信息
            			if(isset($product['recipe']) && !empty($product['recipe']))
           				{

           				if(!isset($product['recipe'][0][3]))
           					$product['recipe'][0][3] = '';
           				if(!isset($product['recipe'][0][9]))
           					$product['recipe'][0][9] = '';
           				if(!isset($product['recipe'][0][2]))
           					$product['recipe'][0][2] = '';
           				if(!isset($product['recipe'][0][4]))	
           					$product['recipe'][0][4] = '';
           				if(!isset($product['recipe'][0][7]))
           					$product['recipe'][0][7] = '';
           				if(!isset($product['recipe'][0][8]))
           					$product['recipe'][0][8] = '';
           				if(!isset($product['recipe'][1][2]))
           					$product['recipe'][1][2] = '';
           				if(!isset($product['recipe'][1][7]))
           					$product['recipe'][1][7] = '';
           					
           				ksort($product['recipe'][0]);
           				ksort($product['recipe'][1]);
           				//echo kohana::debug($product['recipe']);exit();
            				foreach($product['recipe'] as $re_key => $recipe)
            				{
            					$re_loop = 0;
            				 if($product['hidden_lenstype'] == NULL && $product['hidden_lenstype'] == '')
            				{
            					$has_recipe = FALSE;
            					continue;
            				}
            				if($re_key == 1)
            				{
            					
            					foreach($recipe as $re)
            					{
            						#取出变态的字符 ',',';'
            						$re = str_replace(',','',$re);
            						$re = str_replace(';','',$re);
            					 	//DIST pD
            				     	if($product['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint All Time Wear' || $product['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint Computer Glasses' || $product['hidden_lenstype'] == 'Trivex Progressive Lens HMC with Tint' || $product['hidden_lenstype'] == 'Trivex Freeform Lens HMC with Tint' || $product['hidden_lenstype'] == '1.56 Bifocal Lens HMC FT28 with Tint')
            					 	{
            							if($re_loop == 3 && empty($re))
            							{
											$has_recipe = FALSE;
            							}
            				    		if($re_loop == 8 && empty($re))
            							{
											$has_recipe = FALSE;
											#echo "L.DIST PD";
            							}
            						}
            						//NEAR PD
            						if($product['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint Reading Only' || $product['hidden_lenstype'] == 'Trivex Single Vision Lens HMC with Tint Computer Glasses')
            						{
            							if($re_loop == 4 && empty($re))
            							{
											$has_recipe = FALSE;
            							}
            				    		if($re_loop == 9 && empty($re))
            							{
											$has_recipe = FALSE;
												#echo "L.DIST PD";
            							}
            						}
            						//ADD
            						if($product['hidden_lenstype'] == '1.56 Bifocal Lens HMC FT28 with Tint')
            						{
            						    if($re_loop == 0 && empty($re))
            							{
											$has_recipe = FALSE;
            							}
            				    		if($re_loop == 5 && empty($re))
            							{
											$has_recipe = FALSE;
												#echo "L.DIST PD";
            							}
            						}
            					//SETHT
            					    if($product['hidden_lenstype'] == 'Trivex Progressive Lens HMC with Tint' || $product['hidden_lenstype'] == 'Trivex Freeform Lens HMC with Tint')
            						{
            						    if($re_loop == 1 && empty($re))
            							{
											$has_recipe = FALSE;
            							}
            				    		if($re_loop == 6 && empty($re))
            							{
											$has_recipe = FALSE;
												#echo "L.DIST PD";
            							}
            						}
            						if($re_loop>=5)
            						{
            							$p_recipe['L'.($re_key+1)][$re_loop-5] = $re;
            						}
            						else 
            						{
            							$p_recipe['R'.($re_key+1)][$re_loop] = $re;
            						}
            						$re_loop++;
            					}
            				}
            				else
            				{
            					//axis,base的常规判断
            					
            					foreach($recipe as $re)
            					{
            						$re = str_replace(',','',$re);
            						$re = str_replace(';','',$re);

											
            						if($re_loop>=5)
            						{
            							//判断两个disabled的参数
            							$p_recipe['L'.($re_key+1)][$re_loop-5] = $re;
            						}
            						else 
            						{
            							$p_recipe['R'.($re_key+1)][$re_loop] = $re;
            							
            						}
            						$re_loop++;
            					}
            					//R
            					if($p_recipe['R1'][1])
            					{
            						if(!$p_recipe['R1'][2])
            						{
            							$has_recipe = FALSE;
            						}
            					}
            				    if($p_recipe['R1'][3])
            					{
            						if(!$p_recipe['R1'][4])
            						{
            							$has_recipe = FALSE;
            						}
            					}
            					//
            				    if($p_recipe['L1'][1])
            					{
            						if(!$p_recipe['L1'][2])
            						{
            							$has_recipe = FALSE;
            						}
            					}
            				    if($p_recipe['L1'][3])
            					{
            						if(!$p_recipe['L1'][4])
            						{
            							$has_recipe = FALSE;
            						}
            					}
            				}

            			}
            		}
            			else
            			{
            				$has_recipe = FALSE;
            			} 
                    	if ($_product['id'] && $product['quantity'] && $product['price'] > 0 &&$has_recipe &&$product['lenstype'])
                    	{
                        	$data = array(
                            'product_id' => $_product['id'],
                            'product_items' => array($_product['id']),
                            'product_quantity' => $product['quantity'],
                            'product_price' => $product['price'],
                        	'memo' => $product['memo'],
                        	'recipe' => serialize($p_recipe),
                        	'key'	=>	$key,
                        	'rx_memo' => $product['rx_memo'],#行备注
                        	'hidden_lenstype' => $product['hidden_lenstype'],#默认处方信息

                        	'lensstyle' => $product['lensstyle'],
                        	

                        	'lensstyle' => $product['lensstyle'],
                        	'lenscolor'	=> $product['color'],

                        	);
                        	Wholesale_Cart::instance()->add2cart_rx_place_order($data);
                        	$total_price += $product['quantity'] * $product['price'];
                    	}
                    	else 
                    	{
                    		 $_debugCase+=$product['quantity'];
                    	}
                	}
#眼睛信息暂时不处理
                	if (isset($post['case']))
                	{
                    	foreach($post['case'] as $id => $case)
                    	{
                        	if ($case['quantity'] > 0)
                        	{
                            	if (Request::$instance->action == 'rx_place')
                            	{
                                	$case['price'] = Wholesale_Product::instance($id)->price($post['customer_id']);
                            	}
                            	if (!$case['price'])
                            	{
                                	$case['price'] = Wholesale_Product::instance($id)->get('market_price');;
                            	}
                            	$product = array(
                                'product_id' => $id,
                                'product_items' => array($id),
                                'product_quantity' => $case['quantity']-$_debugCase,
                                'product_price' => $case['price'],
                            	);

                            	Wholesale_Cart::instance()->add2cart_place_order($product);
                        	}
                    	}
                    	$cart = Wholesale_Cart::instance()->get($post['customer_id']);
                    	$cart_cases = $cart['cases'];
                    	foreach($cart_cases as $cart_case)
                    	{
                        	$total_price += $cart_case['price'] * $cart_case['quantity'];
                    	}
                	}

                	Wholesale_Cart::instance()->total_price($total_price);
                	Wholesale_Cart::instance()->shipping_billing($post);
          
                	$this->request->response = TRUE;
            	}
        	}
          	}
        }
        else
        {
            $this->request->response = FALSE;
        }
   

    }
    
    #处方镜的订单create
 	public function action_rx_internal_confirm()
 	{
 	   	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
        if (Request::$method == 'POST')
        {
            $customer = Wholesale_Cart::instance()->customer();
            $cart = Wholesale_Cart::instance()->get($customer_id);
            if ($cart['customer']['id'])
            {
                $customer['customer_id'] = $cart['customer']['id'];
                $customer['customer_balance'] = $cart['customer']['customer_balance'];
            }
            $customer['ip'] = Request::$client_ip;
            $products = array_merge($cart['products'], $cart['cases']);
            $shipping = $cart['shipping'];
            $shipping_address = $cart['shipping_address'];
            $billing = $cart['billing'];
            $billing_address = $cart['billing_address'];
            $coupon_code = $cart['coupon'];
            $total_price = $cart['total'];
            
            $commission_rate = 0.00;	//by ding 20110215 没有commision_rate程序过不去，后期调整
//            if ($cart['customer']['id'])
//            {
//                $commission_rate = Wholesale_Customer::instance($this->parent_id)->get('commission_rate');
//            }
//            else
//            {
//                $commission_sale = Wholesale_Customer::instance($this->parent_id)->get('commission_sale');
//                if ($commission_sale)
//                {
//                    $commission_rate = kohana::config('commission_rate.house');
//                }
//                else
//                {
//                    $commission_rate = kohana::config('commission_rate.independent');
//                }
//            }
            $order_id = Wholesale_Order::instance()->create($customer, $products, $shipping, $shipping_address, $billing_address, $billing, $total_price, $commission_rate, $coupon_code,1);
            $ordernum = Wholesale_Order::instance($order_id)->get('ordernum');
            Wholesale_Cart::instance()->clear();

            //request ERP to create order
            $erp = Wholesale_Erp::instance()->create($order_id);
                
            //Update order confirmed
            $confirmed = Wholesale_Order::instance()->confirmed($order_id);
            //if payment method is PO, redirect to order details page.
            #add by bzhao这里需要判断其他的支付类型
            if ($billing['payment_method'] == 'PO')
            {
                Wholesale_Cart::instance()->clear_shipping_billing();
                //update book to erp middleware
                $erp = Wholesale_Erp::instance()->book($order_id);
                Message::set(__('Place order success.'));
                $this->request->redirect('/order/view/'.$ordernum);
            }
            else
            {
                //TODO if payment method is creditcart, pay and then redirect to order details page.
                $this->request->response = Request::factory('payment/pay/'.$ordernum)->execute()->response;
            }
        }
        else
        {
            $this->request->response = FALSE;
        }
 	}
    //customer自己快速下单place order
    public function action_place()
    {
        if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
        Wholesale_Erp::instance()->init();
        if ($_POST)
        {
        	$add = Request::factory('/customer/internal_add')->execute()->response;
            if (!$add)
            {
                $this->request->redirect('/customer/place');
            }
            else
            {
                $this->request->redirect('/customer/place_confirm');
            }
        }
        foreach(date::months() as $k => $v)
        {
            $month_arr[sprintf('%02d', $v)] = sprintf('%02d', $v);	//Array ( [01] => 01 [02] => 02 [03] => 03 [04] => 04 [05] => 05 [06] => 06 [07] => 07 [08] => 08 [09] => 09 [10] => 10 [11] => 11 [12] => 12 ) 
        }

        $exp_month_arr = $month_arr;
        $valid_month_arr = $month_arr;

        $exp_year_arr = date::years(date('Y'), date('Y') + 9);
        $valid_year_arr = date::years(date('Y') - 9, date('Y'));

        $form_arr['cc_exp_month'] = Form::select('cc_exp_month', $exp_month_arr);
        $form_arr['cc_exp_year'] = Form::select('cc_exp_year', $exp_year_arr);
        $form_arr['cc_valid_month'] = Form::select('cc_valid_month', $valid_month_arr);
        $form_arr['cc_valid_year'] = Form::select('cc_valid_year', $valid_year_arr);   
         
        //判断用户是否已经关联
        $customer = Wholesale_Customer::instance($customer_id)->get();
        if(!$customer['erp_id'])
        {
        	Message::set("customer_not_associate_with_erp" , "error");
        	Request::instance()->redirect('/product-list');
        }
        else 
        {
        	$countries = Site::instance()->countries();
	        $cart = Wholesale_Cart::instance()->get($customer_id);
	    	$carriers = Site::instance()->carriers('US');
	        foreach($carriers as $key => $carrier)
	        {
	            $carrier_shipping_address['country'] = $countries[0]['isocode'];//print_r($carrier_shipping_address);
	            $carrier_price = Carrier::instance($carrier['id'])->get_price(($cart['total_weight'])?$cart['total_weight']:450, $carrier_shipping_address);
	            if($carrier_price !== FALSE)
	            {
	                $carriers[$key]['price'] = $carrier_price;
	            }
	            else
	            {
	                unset($carriers[$key]);
	            }
	        }
        }
        $customer_address = Wholesale_Customer::instance()->get_addresses($customer_id);

        $this->request->response = View::factory(Site::instance()->get('id').'/view/customer_place')
            ->set('title', Site::instance()->get('meta_title'))
            ->set('keywords', Site::instance()->get('meta_keywords'))
            ->set('description', Site::instance()->get('meta_description'))
            ->set('cart',$cart)
            ->set('carriers',$carriers)
            ->set('countries',$countries)
            ->set('customer',$customer)
            ->set('customers',$customer_address)
            ->set('form_arr', $form_arr)
            ->render();
    }
    
    public function action_internal_add()
    {
    	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
    	if ($_POST)
        {
            //TODO validate post
            Wholesale_Cart::instance()->clear();
            $post = $_POST;
            
            //add customer info to cart.
            $customer_info = Wholesale_Customer::instance($customer_id)->get();
            $customer_info['customer_balance'] = $post['customer_balance'];
            
            Wholesale_Cart::instance()->customer($customer_info);
            //check product sku & quantity, then add product/sku to cart session
            $products = $post['product'];
            $has_products = FALSE;
            if (!$products)
                $has_products = FALSE;
            foreach($products as $p)
            {
                if ($p['styleno'] && $p['erp_item_number'])
                {
                    $has_products = TRUE;
                    break;
                }
            }
            $total_price = 0;
            if (!$has_products)
            {
                Message::set(__('order_add_product_no_product_error'), 'error');
                $this->request->response = FALSE;
            }
            else
            {
                foreach($products as $product)
                {
                    $filters = array();
                    $filters['styleno'] = $product['styleno'];
                    $filters['erp_item_number'] = $product['erp_item_number'];
                    //TODO 根据 collection, color, styleno, eyesize确定唯一的产品
                    $_product = Wholesale_Product::instance()->filter_product($filters);
                    //add by bzhao
                    $gproduct_attributes = Wholesale_Product::instance($_product['id'])->set_data();
            
                    if (Request::$instance->action == 'place')
                    {
                        $product['price'] = Wholesale_Product::instance($_product['id'])->price($post['customer_id']);
                    }
                    if (!$product['price'] && !$post['customer_id'])
                    {
                        $product['price'] = $_product['market_price'];
                    }
                    //同步库存
                    $available_stock = Wholesale_Product::instance()->filter_inventory(array('item_no' => $product['erp_item_number']));
                    //personal取消判断库存 
                    if(in_array($gproduct_attributes[Kohana::config('glassesattrid.collection_id')]['value'] , Kohana::config('glassesattrid.personal_collection')))
                    {
	                   $available_stock = $product['quantity'];
                    }
                    if ($_product['id'] && $product['quantity'] && $product['price'] > 0 && $available_stock >= $product['quantity'])
                    {
                        $data = array(
                            'product_id' => $_product['id'],
                            'product_items' => array($_product['id']),
                            'product_quantity' => $product['quantity'],
                            'product_price' => $product['price'],
                        	'memo' => $product['memo'],
                        );
                        Wholesale_Cart::instance()->add2cart_place_order($data);
                        $total_price += $product['quantity'] * $product['price'];
                    }
                }

                if (isset($post['case']))
                {
                    foreach($post['case'] as $id => $case)
                    {
                        if ($case['quantity'] > 0)
                        {
                            if (Request::$instance->action == 'place')
                            {
                                $case['price'] = Wholesale_Product::instance($id)->price($post['customer_id']);
                            }
                            if (!$case['price'])
                            {
                                $case['price'] = Wholesale_Product::instance($id)->get('market_price');;
                            }
                            $product = array(
                                'product_id' => $id,
                                'product_items' => array($id),
                                'product_quantity' => $case['quantity'],
                                'product_price' => $case['price'],
                            );

                            Wholesale_Cart::instance()->add2cart_place_order($product);
                        }
                    }

                    $cart = Wholesale_Cart::instance()->get($customer_id);
                    $cart_cases = $cart['cases'];
                    foreach($cart_cases as $cart_case)
                    {
                        $total_price += $cart_case['price'] * $cart_case['quantity'];
                    }
                }

                Wholesale_Cart::instance()->total_price($total_price);
                Wholesale_Cart::instance()->shipping_billing($post);
                $this->request->response = TRUE;
            }
        }
        else
        {
            $this->request->response = FALSE;
        }
    }
    
    public function action_place_confirm()
    {
    	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
        if (Request::$method == 'POST')
        {
            $this->request->response = Request::factory('/customer/internal_confirm')->execute();
        }
        $customer = Wholesale_Cart::instance()->customer();
        $cart = Wholesale_Cart::instance()->get($customer['id']);
        if(count($cart['products']) == 0 or ! isset($cart['products']))
        {
        	Message::set(__('Place order and add product first.'));
        	$this->request->redirect('/customer/place');
        }
        
        $_action = '_place';
        $this->request->response = View::factory(Site::instance()->get('id').'/view/confirm')
            ->set('cart', $cart)
            ->set('customer',$customer)
            ->set('_action', $_action)
            ->render();
    }
    
    public function action_internal_confirm()
    {
    	if ( ! ($customer_id = Wholesale_Customer::instance()->logged_in()))
        {
            Request::instance()->redirect(URL::current(TRUE));
        }
        if (Request::$method == 'POST')
        {
            $customer = Wholesale_Cart::instance()->customer();
            $cart = Wholesale_Cart::instance()->get($customer_id);
            if ($cart['customer']['id'])
            {
                $customer['customer_id'] = $cart['customer']['id'];
                $customer['customer_balance'] = $cart['customer']['customer_balance'];
            }
            $customer['ip'] = Request::$client_ip;
            $products = array_merge($cart['products'], $cart['cases']);
            $shipping = $cart['shipping'];
            $shipping_address = $cart['shipping_address'];
            $billing = $cart['billing'];
            $billing_address = $cart['billing_address'];
            $coupon_code = $cart['coupon'];
            $total_price = $cart['total'];
            
            $commission_rate = 0.00;	//by ding 20110215 没有commision_rate程序过不去，后期调整
//            if ($cart['customer']['id'])
//            {
//                $commission_rate = Wholesale_Customer::instance($this->parent_id)->get('commission_rate');
//            }
//            else
//            {
//                $commission_sale = Wholesale_Customer::instance($this->parent_id)->get('commission_sale');
//                if ($commission_sale)
//                {
//                    $commission_rate = kohana::config('commission_rate.house');
//                }
//                else
//                {
//                    $commission_rate = kohana::config('commission_rate.independent');
//                }
//            }
            $order_id = Wholesale_Order::instance()->create($customer, $products, $shipping, $shipping_address, $billing_address, $billing, $total_price, $commission_rate, $coupon_code);
            $ordernum = Wholesale_Order::instance($order_id)->get('ordernum');
            Wholesale_Cart::instance()->clear();

            //request ERP to create order
            $erp = Wholesale_Erp::instance()->create($order_id);
                
            //Update order confirmed
            $confirmed = Wholesale_Order::instance()->confirmed($order_id);
            //if payment method is PO, redirect to order details page.
            if ($billing['payment_method'] == 'PO')
            {
                Wholesale_Cart::instance()->clear_shipping_billing();
                //update book to erp middleware
                $erp = Wholesale_Erp::instance()->book($order_id);
                Message::set(__('Place order success.'));
                $this->request->redirect('/order/view/'.$ordernum);
            }
            else
            {
                //TODO if payment method is creditcart, pay and then redirect to order details page.
                $this->request->response = Request::factory('payment/pay/'.$ordernum)->execute()->response;
            }
        }
        else
        {
            $this->request->response = FALSE;
        }
    	
    }
    
    function action_balance()
    {
       if (!($customer_id = Wholesale_Customer::logged_in()))
        {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
        }
        $customer = Wholesale_Customer::instance($customer_id)->get();
        $orders = array();
        if($_GET['date_range'])
        {
        	$date = explode(' - ' , $_GET['date_range']);
        	if($date[0] && $date[1])
        	{
        		$date_from = strtotime($date[0]);
        		$date_to = strtotime($date[1]);
        		$time_range['from'] = date("Y-m-d H:i:s",$date_from);
        		$time_range['to'] = date("Y-m-d H:i:s",$date_to);
        	}
        	else 
        	{
        		Message::set("date_format_error" , 'error');
        		$this->request->redirect('/customer/balance');
        	}
        }
        if($_GET['invoice_number'])
        {
        	$filters['invoice_no'] = Wholesale_Order::instance()->get_invoiceId($_GET['invoice_number']);
        }
        //print_r($filters['invoice_no']);exit();
        $filters['customer_name'] = $customer['name'];

        $orders = Wholesale_Order::instance()->get_balance($filters,$time_range);
        //print_r($orders);exit();
        $orders = array_reverse($orders);
        $count = count($orders);
        $pagination = Pagination::factory(array(
            'current_page' => array('source' => 'query_string', 'key' => 'page'),
            'total_items' => $count,
            'items_per_page' => $count+1,
            'view' => Site::instance()->get('id').'/view/pagination',
            'auto_hide' => TRUE));
        $orders = array_slice($orders, $pagination->offset, $pagination->items_per_page);
        $order_statuses = kohana::config('order_status');
        $template = View::factory(Site::instance()->get('id').'/view/balance_list')
            ->set('orders', $orders)
            ->set('order_statuses', $order_statuses)
            ->set('title', '')
            ->set('page_view', $pagination->render())
            ->render();
        $this->request->response = $template;
    }
    
    #用户的PO支付
    function action_po_pay()
    {
    	//Make Payments
       	if (!($customer_id = Wholesale_Customer::logged_in()))
        {
            Request::instance()->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
        }
        #得到用户信息
        $customer = Wholesale_Customer::instance($customer_id)->get();
        
        if($_POST)
        {
//            if($_POST['customer_id'] && $_POST['tips_invoice_number'])
//        	{
//        		#一次性处理多份发票
//        		if(is_array(explode("_",$_POST['tips_invoice_number'])))
//        		{
//        			$_order_number_str = array();
//        			foreach(explode("_",$_POST['tips_invoice_number']) as $tn)
//        			{
//        				$sql = "select order_no from orc_invoice where invoice_no = ".$tn." and due_remaining>0 ";
//    					$_result = DB::query(Database::SELECT, $sql)->execute()->current();
//        				$filters['erp_ordernum'] = $_result['order_no'];
//            			$order = Wholesale_Order::instance()->get_order($filters);
//         				$_order_number_str[] =$order['ordernum'];
//        			}
//        			$this->request->response = Request::factory('payment/popay/'.implode('_',$_order_number_str))->execute()->response;
//        		}
//        		else 
//        		{	
//        			$sql = "select order_no from orc_invoice where invoice_no = ".$_POST['tips_invoice_number']." and due_remaining>0 ";
//    				$_result = DB::query(Database::SELECT, $sql)->execute()->current();
//        			$filters['erp_ordernum'] = $_result['order_no'];
//            		$order = Wholesale_Order::instance()->get_order($filters);
//            		$this->request->response = Request::factory('payment/popay/'.$order['ordernum'])->execute()->response;
//        		}
//        	}
			if($_POST['customer_id'] && $_POST['invoice_number'])
				$this->request->response = Request::factory('payment/popay/'.time())->execute()->response;
        }
        
        #判断是否为ERP用户
		if (!$customer['erp_id'])
        {
            Message::set(__('customer_not_associate_with_erp'), 'error');
        	$this->request->redirect('/order/list');
        }
//        else
//        {
        foreach(date::months() as $k => $v)
        {
           	 $month_arr[sprintf('%02d', $v)] = sprintf('%02d', $v);
        }

        $exp_month_arr = $month_arr;
        $valid_month_arr = $month_arr;

        $exp_year_arr = date::years(date('Y'), date('Y') + 9);
        $valid_year_arr = date::years(date('Y') - 9, date('Y'));

        $form_arr['cc_exp_month'] = Form::select('cc_exp_month', $exp_month_arr);
        $form_arr['cc_exp_year'] = Form::select('cc_exp_year', $exp_year_arr);
        $form_arr['cc_valid_month'] = Form::select('cc_valid_month', $valid_month_arr);
        $form_arr['cc_valid_year'] = Form::select('cc_valid_year', $valid_year_arr);

        $countries = Site::instance()->countries();
        	
        //得到用户默认的发票
		//$_get_invoice = array();
		//$_get_invoice = Wholesale_Customer::instance($customer_id)->get_invoice($customer['erp_id']); 
    	
		$this->request->response = View::factory(Site::instance()->get('id').'/view/cus_topay')
            ->set('customer',  $customer)
           	->set('countries', $countries)
           	//->set('invoice',$_get_invoice)
            ->set('form_arr', $form_arr)
            ->render();
    }
    
    public function action_addErpCustomer()
    {
    	//实现登录
    	$ErpService = new ErpService();
    	
		$ErpService->getCustomerErpId(1);
    }
}

