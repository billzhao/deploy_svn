<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * DAO 基类
 * 
 * @author zhao.yang
 *
 * @package DAO
 *
 * @version $Id: dao.php 6285 2012-02-16 10:20:30Z zhao.yang $
 *
 * @copyright Cofree Develop Term
 */
class DAO {
    
    /**
     * 表名称
     * 
     * @var string
     */
    protected $table_name = NULL;

    /**
     * 主键名称
     *
     * @var string
     */
    protected $primary_key = 'id';

    /**
     * 软删除标记字段名称，当为空时，不启用软删除功能
     *
     * @var string
     */
    protected $disabled = NULL;

    /**
     * 更新时间字段名称
     *
     * @var string
     */
    protected $updated = 'updated';

    /**
     * 创建时间字段名称
     *
     * @var string
     */
    protected $created = 'created';

    /**
     * 是否允许自动维护更新时间字段的值
     *
     * @var bool
     */
    protected $allow_updated = TRUE;

    /**
     * 是否允许自动维护创建时间字段的值
     *
     * @var bool
     */
    protected $allow_created = TRUE;

    /**
     * 异常对象名称
     *
     * @var string
     */
    protected $exception = NULL;

    /**
     * 表字段信息
     *
     * @var array
     */
    protected $fields = NULL;

    /**
     * DAO 对象名称前缀
     *
     * @var string
     */
    const NAME_PREFIX = 'DAO_';

    /**
     * 默认异常对象名称
     *
     * @var string
     */
    const DEFAULT_EXCEPTION = 'Kohana_Exception';//Cloud_Exception

    /**
     * LIMIT 默认值
     *
     * @var int
     */
    const DEFAULT_LIMIT = 1000;

    /**
     * DAO 实例容器
     *
     * @var array
     */
    static protected $instances = array();

    /**
     * 通过 DAO 名称获取 DAO 实例
     */
    final static public function & instance($name)
    {
        $name = trim($name, ' _');
        $name = strtolower($name);

        if (!isset(self::$instances[$name]))
        {
            if (Kohana::auto_load($clsname = self::NAME_PREFIX.$name))
            {
                self::$instances[$name] = new $clsname();
            } 
            else 
            {
            	echo $clsname;
                $exception = self::DEFAULT_EXCEPTION;
                throw new $exception(sprintf('未找到名称为 "%s" 的 DAO', $name));
            }
        }

        return self::$instances[$name];
    }

    /**
     * 指示 DAO 对象是否已注册
     *
     * @param string $name  DAO 对象名称
     * @return boolean
     */
    final static public function is_register($name)
    {
        $name = trim($name, ' _');
        $name = strtolower($name);

        return isset(self::$instances[$name]) ? TRUE : FALSE;
    }

    /**
     * 构造函数
     */
    protected function  __construct()
    {
        if (empty($this->exception))
            $this->exception = self::DEFAULT_EXCEPTION;
        if (empty($this->table_name))
            $this->table_name = substr(get_class($this), strlen(self::NAME_PREFIX));

        $this->allow_updated = TRUE;
        $this->allow_created = TRUE;
    }

    /**
     * 通过主键值或查询结构体获取单条记录，当失败时，返回空数组
     *
     * @param int,array $query_struct  查询结构体
     * @return array
     */
    public function find($query_struct = array())
    {
        if (!is_array($query_struct))
        {
            $query_struct = array(
                'where' => array($this->primary_key => $query_struct)
            );
        }
        // 仅能返回一条记录
        $query_struct['limit'] = array(1);
        $query_struct['orderby'] = !empty($query_struct['orderby'])?$query_struct['orderby']:'';
      
        $records = $this->find_all($query_struct);
        $record = array();
        if (!empty($records) AND isset($records[0]) AND is_array($records[0]))
            $record = $records[0];
        return $record;
    }

     /**
     * 通过查询结构体获取记录集，当失败时，返回空数组
     *
     * @param int,array $query_struct  查询结构体
     * $query_struct = array(
     *   'where'=>array(
     *       'site_id' => $this->site_id,
     *       'on_sale' => 1,
     *       'id notin' => $relative_ids,
     *   ),
     *   'orderby'   => array(
     *       'position'   =>'DESC',
     *   ),
     *   'limit'     => array(
     *       'per_page'  => 10,
     *       'offset'    => ($page-1)*10,
     *   ),
     * );
     * @param string $index  索引字段名称，当不能空时，将以制定的字段的值索引记录
     * @return array
     */
    public function find_all($query_struct = array(), $index  = NULL)
    {
        if (!empty($index) AND !$this->is_field($index))
            throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $index));

        $outputer = method_exists($this, '_outputer');
        $records = array();
        $orm = $this->confine($this->orm(), $query_struct)->find_all();
        if (!empty($index))
        {
            foreach($orm as $record)
            {
                $record = $record->as_array();
                if ($outputer === TRUE)
                    $record = $this->_outputer($record);
                $records[$record[$index]] = $record;
            }
        } else {
            foreach($orm as $record)
            {
                $record = $record->as_array();
                if ($outputer === TRUE)
                    $record = $this->_outputer($record);
                $records[] = $record;
            }
        }
        
        return $records;
    }
    
    /**
     * 通过查询结构体获取符合条件的记录行数
     *
     * @param int,array $query_struct  查询结构体
     * @return int
     */
    public function count($query_struct = array())
    {
        if (isset($query_struct['limit']))
            unset($query_struct['limit']);

        return $this->confine($this->orm(), $query_struct)->count_all();
    }

    /**
     * 查询关联数组
     *
     * select_list(array(), 'id', 'name') => array(
     *     12 => '王浩',
     *     23 => '牛哄哄',
     * )
     *
     * select_list(array(), 'name') => array(
     *     0 => '王浩',
     *     1 => '牛哄哄',
     * )
     *
     * @param array $query_struct  查询结构体
     * @param string $key  作为索引的字段名称
     * @param string $value  作为值的字段名称，当此字段留空时，$key 参数将作为值
     * @return array
     */
    public function select_list($query_struct, $key = 'id', $value = NULL)
    {
        if (!$this->is_field($key))
            throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $key));
        if (!empty($value) AND !$this->is_field($value))
            throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $value));

        $outputer = method_exists($this, '_outputer');
        $records = array();
        $orm = $this->confine($this->orm(), $query_struct)->find_all();
        if (!empty($value))
        {
            foreach($orm as $record)
            {
                $record = $record->as_array();
                if ($outputer === TRUE)
                    $record = $this->_outputer($record);
                $records[$record[$key]] = $record[$value];
            }
        } else {
            foreach($orm as $record)
            {
                $record = $record->as_array();
                if ($outputer === TRUE)
                    $record = $this->_outputer($record);
                $records[] = $record[$key];

            }
        }
        
        return $records;
    }

    /**
     * 创建记录
     *
     * @param array $record  记录数组
     * @return int 成功
     *          false 失败
     */
    public function add($record)
    {
    	//$record = Security::xss_clean($record); hack stat_code
    	
        $datetime = time();
        if ($this->allow_created AND !empty($this->created) AND empty($record[$this->created]))
            $record[$this->created] = $datetime;
        if ($this->allow_updated AND !empty($this->updated) AND empty($record[$this->updated]))
            $record[$this->updated] = $datetime;
        if (!empty($this->disabled) AND !isset($record[$this->disabled]))
            $record[$this->disabled] = 0;

        unset($record[$this->primary_key]);
        if (method_exists($this, '_inputer'))
            $record = $this->_inputer($record);
        
        $orm = $this->orm();
        foreach ($record as $key => $value)
        {
            if ($this->is_field($key))
                $orm->$key = $value;
        }
		if($orm->check())
		{
        	$orm->save();
		}
		else 
			return FALSE;
			
        if ($orm->_saved  === TRUE)
            return $orm->{$this->primary_key};
        else
            return FALSE;
    }

    /**
     * 更新记录
     *
     * @param array $record 记录数组，其中必须包含主键值，主键值将作为查询条件
     * @return boolean
     */
    public function edit($record)
    {
    	//Tim tip
    	
    	//$record = Security::xss_clean($record); hack stat_code
    	
        if (empty($record[$this->primary_key]))
            return FALSE;

        $query_struct = array(
            'where' => array($this->primary_key => $record[$this->primary_key]),
        );
        if (!empty($this->disabled) AND isset($record[$this->disabled]))
            $query_struct['where'][$this->disabled] = array(0, 1);

        if ($this->allow_updated AND !empty($this->updated) AND empty($record[$this->updated]))
            $record[$this->updated] = time();
        if (!empty($this->disabled) AND !isset($record[$this->disabled]))
            $record[$this->disabled] = 0;

        if (method_exists($this, '_inputer'))
            $record = $this->_inputer($record);
        
        $orm = $this->confine($this->orm(), $query_struct)->find();
        if ($orm->_loaded === TRUE)
        {
            unset($record[$this->primary_key]);
            foreach ($record as $key => $value)
            {
                if ($this->is_field($key))
                    $orm->$key = $value;
            }
            
        	if($orm->check())
			{
        		$orm->save();
			}
			else 
			{
				return FALSE;
			}
			
            if ($orm->_saved === TRUE)
                return TRUE;
        }

        return FALSE;
    }

    /**
     * 通过主键值或查询结构体删除记录，每次仅允许删除一行记录
     *
     * @param int,array $query_struct  查询结构体
     * @return boolean
     */
    public function delete($query_struct = array(),$limit_1=true)
    {
        if (!is_array($query_struct))
        {
            $query_struct = array(
                'where' => array($this->primary_key => $query_struct),
            );
        }
        // 每次仅允许删除一条记录
        if($limit_1)
        {
	        $query_struct['limit'] = array(1);
	        $orm = $this->confine($this->orm(), $query_struct)->find();
	        if ($orm->_loaded === TRUE)
	        {
	            if (!empty($this->disabled) AND $orm->{$this->disabled} == 0)
	            {
	                $orm->{$this->disabled} = 1;
	                $orm->save();
	            } else {
	                $orm->delete();
	            }
	            return TRUE;
	        } 
	        else 
	        {
	            return FALSE;
	        }
        }
        else
        {
        	if($this->is_field('disabled'))
        	{
        		$orm = $this->confine($this->orm(), $query_struct);
        		$orm->disabled=1;
        		$orm->save_all();
        	}
        	else
        	{
        		$orm = $this->confine($this->orm(), $query_struct)->delete_all();
        	}
        	return TRUE;
        }
    }

    /**
     * 通过字段名和值获取一条记录
     *
     * @param string $field
     * @param mixed $value
     * @return array
     */
    public function find_by_fval($field, $value)
    {
        return $this->find(array(
            'where' => array($field => $value),
        ));
    }

    /**
     * 通过字段名和值获取记录
     *
     * @param string $field
     * @param mixed $value
     * @return array
     */
    public function find_all_by_fval($field, $value, $index = NULL)
    {
        return $this->find_all(array(
            'where' => array($field => $value),
        ), $index);
    }

    /**
     * 通过字段值和名获取记录行数
     *
     * @param string $field
     * @param mixed $value
     * @return int
     */
    public function count_by_fval($field, $value)
    {
        return $this->count(array(
            'where' => array($field => $value),
        ));
    }

    /**
     * 开启或关闭更新时间字段的自动维护功能
     *
     * @param bool $allow
     * @return bool
     */
    public function allow_updated($allow)
    {
        return $this->allow_updated = (boolean)$allow;
    }

    /**
     * 开启或关闭创建时间字段的自动维护功能
     *
     * @param bool $allow
     * @return bool
     */
    public function allow_created($allow)
    {
        return $this->allow_created = (boolean)$allow;
    }

    /**
     * 指示字段是否存在
     *
     * @param string $field  字段名
     * @return boolean
     */
    protected function is_field($field)
    {
        if ($this->fields === NULL)
            $this->fields = $this->orm()->list_columns();

        return isset($this->fields[$field]);
    }

    /**
     * 获取表名称
     *
     * @return string
     */
    protected function get_table_name()
    {
        return $this->table_name;
    }

    /**
     * 获取主键名称
     *
     * @return string
     */
    protected function get_primary_key()
    {
        return $this->primary_key;
    }

    /**
     * 获取创建时间字段名称
     *
     * @return string
     */
    protected function get_created()
    {
        return $this->created;
    }

    /**
     * 获取更新时间字段名称
     *
     * @return string
     */
    protected function get_updated()
    {
        return $this->updated;
    }

    /**
     * 创建 ORM 对象
     *
     * @return object
     */
    protected function orm($id = NULL)
    {
    	if ( ! is_null($id))
    	{
    		return ORM::factory($this->table_name,$id);
    	}
        return ORM::factory($this->table_name);
    }
    /**
     * 为 ORM 对象加入限定条件，包括 where,orderby,limit
     *
     * @param object $orm
     * @return object
     */
    final protected function confine($orm, $query_struct = array())
    {
        //array(
        //    'where' => array(
        //        'name like' => '王',
        //        'and id >=' => 1,
        //        'or name notlike' => '浩',
        //    ),
        //    'orderby' => array(),
        //);
        if (is_array($query_struct))
        {
            if (!empty($this->disabled))
            {
                if (!isset($query_struct['where']))
                    $query_struct['where'] = array();
                if (!isset($query_struct['where'][$this->disabled]))
                    $query_struct['where'][$this->disabled] = 0;
            }

            if (!empty($query_struct['where']) AND is_array($query_struct['where']))
            {
                $types = array(
                    'IN'       => TRUE,
                    'NOT IN'    => TRUE,
                    'LIKE'     => TRUE,
                    'REGEX'    => TRUE,
                    'NOTREGEX' => TRUE,
                );

                foreach ($query_struct['where'] as $key => $value)
                {
                    $key = trim($key);
                    
                    if (is_numeric($key) OR empty($key))
                    {
                        $orm->where($this->primary_key,'=',$value);
                    } 
                    else 
                    {
                        // 过滤连续空格
                        $key = preg_replace('/\s+/', ' ', $key);
                        $key = explode(' ', $key);

						
                        $relation = '';
                        switch (strtoupper($key[0]))
                        {
                            case 'AND':
                                break;
                            case 'OR':
                                $relation = 'or_';
                                break;
                            default:
                                array_unshift($key, '');
                        }
                       
                        if (is_array($value))
                        {    
                        	if (strtoupper($key[2]) !== 'NOTIN')
                        		$key[2] = 'in';
                        	else 	
                        	{
                        		$key[2] = 'not in';
                        	}
                        }
                           
                        
                        $type = 'where';
                        
                        //if (isset($key[2]) AND isset($types[strtoupper($key[2])]))
                            //$type = $key[2];
                            
                        $key[2] = (isset($key[2]) AND ! empty($key[2])) ? $key[2] :'=';
                       
                        $orm->{$relation.$type}($key[1],$key[2],$value);
                        

                    }
                }
            }

            if (!empty($query_struct['orderby']))
            {
            	if ( ! is_array($query_struct['orderby']))
            		$orm->order_by($query_struct['orderby']);
            	else
     			//$column, $direction = NULL
            	{
            		$query_struct['orderby'][1] = isset($query_struct['orderby'][1]) ? $query_struct['orderby'][1] : 'ASC';
            		$orm->order_by($query_struct['orderby'][0],$query_struct['orderby'][1]);
            	}
            }
                
            if (!empty($query_struct['limit']))
            {
                $limit = !is_array($query_struct['limit'])
                       ? explode(',', $query_struct['limit'])
                       : $query_struct['limit'];
			
                if (isset($limit['per_page']))
                    $limit[0] = $limit['per_page'];
                if (isset($limit['offset']))
                    $limit[1] = $limit['offset'];
                if (isset($limit[0]) AND isset($limit[1]))
                {
                    $orm->limit(intval($limit[0]));
                    $orm->offset(intval($limit[1]));
                }
                elseif (isset($limit[0]))
                    $orm->limit(intval($limit[0]));
                else
                    $orm->limit(self::DEFAULT_LIMIT);
            }
        }

        return $orm;
    }
    
   	/*****************支持 Database的实现******************/
    /**
     * 创建 DB::select对象
     * @param array $config
     */
    public function dbselect($columns = array())
    {
    	$select = array();

    	if (count($columns))
    	{
    		foreach ($columns as $key) 
			{
				if ( ! empty($key) && ! $this->is_field($key))
            		throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $key));

        		 ! empty($key) && array_push($select,$key);   	
			}
    	}

    	/*处理orc的表信息*/
    	$table_name = preg_match('/^orc/i',$this->table_name)
    				? $this->table_name
    				: Inflector::plural($this->table_name);
    				
		return DB::select_array($select)->from($table_name);	
    }
    
    /**
     * 得到DB::select数据集
     * @param DB::select $dbselect
     * @param array $query_struct
     * @param string $key
     * @param string $value
     */
    public function db_find_all($dbselect , $query_struct = array() , 
    			$key = NULL , 
    			$value = NULL)
    {
    	if ( ! empty($key) && ! $this->is_field($key))
            throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $key));
        if ( ! empty($value) && ! $this->is_field($value))
            throw new $this->exception(sprintf('表 "%s" 中未找到字段 "%s"', $this->get_table_name (), $value));
    	
    	
    	$records = array();	
    	$result = $this->confine($dbselect,$query_struct)->execute()->as_array();

        if ( ! empty($value))
        {
            foreach($result as $record)
            {
                $records[$record[$key]] = $record[$value];
            }
        } 
        elseif ( ! empty($key))
        {
            foreach($result as $record)
            {
                $records[$record[$key]] = $record;
            }
        }
        else
        {
           	foreach($result as $record)
            {
                $records[] = $record;
            }
        }

        return $records;
    }
}