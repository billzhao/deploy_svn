<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: toolkit
 *
 * @author bzhao
 *
 * @version $Id: toolkit.php 7405 2012-06-28 10:25:44Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class toolkit
{

	public static function hash($password)
	{
		return sha1($password);
	}

	public static function price($price, $action = 'show', $currency_name = NULL)
	{
		$currency = Site::instance()->currency_get($currency_name);

		$price = $price * $currency['rate'];

		switch( $action )
		{
			case 'show' :
				return $currency['code'].$price;
				break;
			case 'data' :
				return $price;
				break;
		}
	}
	
	public function clear_point($pointer)
	{
	     return str_replace
	       (
	       array("！","、","，","。","？","；","：","‘","“","”",
	       "’"," 【","】","～","！","＠","＃","＄","％","＾","＆","＊","，","．"," ＜",
	       "＞","；","：","＇","＂","［","］","｛","｝","／","＼","《","》","-","_"),
	   		'',
	          $pointer
	       );
	}	
		
	
	public static function make_semiangle($str)   
	{   
    	$arr = array('０' => '0', '１' => '1', '２' => '2', '３' => '3', '４' => '4',   
                 '５' => '5', '６' => '6', '７' => '7', '８' => '8', '９' => '9',   
                 'Ａ' => 'A', 'Ｂ' => 'B', 'Ｃ' => 'C', 'Ｄ' => 'D', 'Ｅ' => 'E',   
                 'Ｆ' => 'F', 'Ｇ' => 'G', 'Ｈ' => 'H', 'Ｉ' => 'I', 'Ｊ' => 'J',   
                 'Ｋ' => 'K', 'Ｌ' => 'L', 'Ｍ' => 'M', 'Ｎ' => 'N', 'Ｏ' => 'O',   
                 'Ｐ' => 'P', 'Ｑ' => 'Q', 'Ｒ' => 'R', 'Ｓ' => 'S', 'Ｔ' => 'T',   
                 'Ｕ' => 'U', 'Ｖ' => 'V', 'Ｗ' => 'W', 'Ｘ' => 'X', 'Ｙ' => 'Y',   
                 'Ｚ' => 'Z', 'ａ' => 'a', 'ｂ' => 'b', 'ｃ' => 'c', 'ｄ' => 'd',   
                 'ｅ' => 'e', 'ｆ' => 'f', 'ｇ' => 'g', 'ｈ' => 'h', 'ｉ' => 'i',   
                 'ｊ' => 'j', 'ｋ' => 'k', 'ｌ' => 'l', 'ｍ' => 'm', 'ｎ' => 'n',   
                 'ｏ' => 'o', 'ｐ' => 'p', 'ｑ' => 'q', 'ｒ' => 'r', 'ｓ' => 's',   
                 'ｔ' => 't', 'ｕ' => 'u', 'ｖ' => 'v', 'ｗ' => 'w', 'ｘ' => 'x',   
                 'ｙ' => 'y', 'ｚ' => 'z',   
                 '（' => '(', '）' => ')', '〔' => '[', '〕' => ']', '【' => '[',   
                 '】' => ']', '〖' => '[', '〗' => ']', '“' => '[', '”' => ']',   
                 '‘' => '[', '’' => ']', '｛' => '{', '｝' => '}', '《' => '<',   
                 '》' => '>',   
                 '％' => '%', '＋' => '+', '—' => '-', '－' => '-', '～' => '-',   
                 '：' => ':', '。' => '.', '、' => ',', '，' => '.', '、' => '.',   
                 '；' => ',', '？' => '?', '！' => '!', '…' => '-', '‖' => '|',   
                 '”' => '"', '’' => '`', '‘' => '`', '｜' => '|', '〃' => '"',   
                 '　' => ' ','＄'=>'$','＠'=>'@','＃'=>'#','＾'=>'^','＆'=>'&','＊'=>'*',
                 '＂'=>'"');
  
    	return strtr($str, $arr);   
	}
			
	public static function curl_pay($API_Endpoint, $nvpStr)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpStr);
		$response = curl_exec($ch);
		if(curl_errno($ch))
		{
			$curl_error_no = curl_errno($ch);
			$curl_error_msg = curl_error($ch);
		}
		else
		{
			curl_close($ch);
		}
		return $response;
	}
	
	/**
	 * 判断一个url是否本站的url。
	 * @param <type> $url 用于判断的url字符串
	 * @param <type> $domain 用作判断依据的主域名，如此参数未提供，则取数据库中的本站主域名。
	 */
	public static function is_our_url($url , $domain = NULL)
	{
		$url = parse_url($url);
		
		if ($domain == NULL)
		{
			$domain = self::getDomain();
		}
		
		if ( ! isset($url['host']) OR preg_match('/^(([-\w\.]+)\.)?'.str_replace('.','\.',$domain).'$/', $url['host']))
		{
			return TRUE;
		}
		return FALSE;
	}
	
	public static function catalog_menu($tree,$names)
	{
		$html='';
		$count=1;
		foreach ($tree as $key=>$value)
		{
			if(empty($value))
			{
				if($count==count($tree))
					$html.='<li class="catalog_tree_name catalog_tree_last_name">
								<ins class="tree_icon">&nbsp;</ins>
								<a title="Edit" href="/admin/site/catalog/edit/'.$key.'">'.$names[$key].'</a>
								<a class="delete_catalog" title="Delete" href="/admin/site/catalog/delete/'.$key.'">X</a>
							</li>';
			}
			else 
			{
				
			}
		}
	}

	public static function date_format($time)
	{
        return date('Y-m-d H:i:s', $time);
    }

    public static function fill_zero($num_str,$zeros)
    {
        $nums = explode('.',$num_str);
        $i = 0;
        if(isset($nums[1]))
        {
            $i = strlen($nums[1]);
        }
        $j = $zeros - $i;
        if($j)
        {
            $num_str = $num_str.($i == 0 ? '.':'').str_repeat('0',$j);
        }
        return $num_str;
    }

    /**
	 * BBCODE to HTML
	 *
	 * @param string $text 
	 * @param string $emoticon 
	 * @return string
	 */
	public static function format_html($text, $whitespace = FALSE, $br = TRUE)
	{
		// Convert special characters to HTML entities
		$text = htmlspecialchars($text);

		// image and link
		$regular = array(
			'#\[img\]([\w]+?://[\w\#$%&~/.\-;:=,' . "'" . '?@\[\]+]*?)\[/img\]#is',
			// [img=xxxx://www.kohana.cn]image url[/img]
			'#\[img=([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*?)\]([\w]+?://[\w\#$%&~/.\-;:=,' . "'" . '?@\[\]+]*?)\[/img\]#is',
			// [img=www.kohana.cn]image url[/img]
			'#\[img=((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*?)\]([\w]+?://[\w\#$%&~/.\-;:=,' . "'" . '?@\[\]+]*?)\[/url\]#is',
			
			// [url]xxxx://www.kohana.cn[/url]
			'#\[url\]([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*?)\[/url\]#is',
			// [url]www.kohana.cn[/url]
			'#\[url\]((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*?)\[/url\]#is',
			// [url=xxxx://www.kohana.cn]KohanaCN[/url]
			'#\[url=([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*?)\]([^?\n\r\t].*?)\[/url\]#is',
			// [url=www.kohana.cn]KohanaCN[/url]
			'#\[url=((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*?)\]([^?\n\r\t].*?)\[/url\]#is',
			
			'/\[b\](.*?)\[\/b\]/i',
			'/\[strong\](.*?)\[\/strong\]/i',
			'/\[i\](.*?)\[\/i\]/i',
			'/\[em\](.*?)\[\/em\]/i',
			'/\[u\](.*?)\[\/u\]/i',
			'/\[s\](.*?)\[\/s\]/i',
			'/\[strike\](.*?)\[\/strike\]/i',
		);
		
		$replace = array
		(
			'<img class="tpi" src="$1" border="0" />',
			'<a href="$1" rel="nofollow external" class="tpa"><img class="code" src="$2" border="0" /></a>',
			'<a href="http://$1" rel="nofollow external" class="tpa"><img class="code" src="$2" border="0" /></a>',
			
			'<a href="$1" rel="nofollow external" class="tpa">$1</a>',
			'<a href="http://$1" rel="nofollow external" class="tpa">http://$1</a>',
			'<a href="$1" rel="nofollow external" class="tpa">$2</a>',
			'<a href="http://$1" rel="nofollow external" class="tpa">$2</a>',
			
			'<strong>$1</strong>',
			'<strong>$1</strong>',
			'<em>$1</em>',
			'<em>$1</em>',
			'<u>$1</u>',
			'<strike>$1</strike>',
			'<strike>$1</strike>',
		);
		$text = preg_replace($regular, $replace, $text);
		
		// Quote
		preg_match('/\[quote\]/i', $text, $bbcode_quote_open);
		preg_match('/\[\/quote\]/i', $text, $bbcode_quote_close);
		if (count($bbcode_quote_open) == count($bbcode_quote_close))
		{
			$text = str_ireplace("[quote]\n", '[quote]', $text);
			$text = str_ireplace("\n[/quote]", '[/quote]', $text);
			$text = str_ireplace("[quote]\r", '[quote]', $text);
			$text = str_ireplace("\r[/quote]", '[/quote]', $text);
			$text = str_ireplace('[quote]', '<blockquote>', $text);
			$text = str_ireplace('[/quote]', '</blockquote>', $text);
		}
	
		// Code
		preg_match('/\[code\]/i', $text, $bbcode_code_open);
		preg_match('/\[\/code\]/i', $text, $bbcode_code_close);
		if (count($bbcode_code_open) == count($bbcode_code_close))
		{
			$text = str_ireplace("[code]\n", '[code]', $text);
			$text = str_ireplace("\n[/code]", '[/code]', $text);
			$text = str_ireplace("[code]\r", '[code]', $text);
			$text = str_ireplace("\r[/code]", '[/code]', $text);
			$text = str_ireplace('[code]', '<pre class="code">', $text);
			$text = str_ireplace('[/code]', '</pre>', $text);
		}
		
		// Inserts HTML line breaks before all newlines in a string
		$text = self::auto_p($text, $whitespace, $br);
		if (strpos($text, '<pre') !== FALSE)
		{
			$text = preg_replace_callback('!(<pre[^>]*>)(.*?)</pre>!is', array('self', 'clean_pre'), $text);
		}
		
		return $text;
	}
	
	/**
	 * Copy Kohana Text::auto_p method
	 * but added one parameter to control if it auto corvert whitespace
	 *
	 * @param string $str 
	 * @param boolean $whitespace 
	 * @param boolean $br 
	 * @return string
	 */
	public static function auto_p($str, $whitespace = FALSE, $br = TRUE)
	{
		// Trim whitespace
		if (($str = trim($str)) === '')
			return '';

		// Standardize newlines
		$str = str_replace(array("\r\n", "\r"), "\n", $str);

		// Trim whitespace on each line
		if ($whitespace === TRUE)
		{
			$str = preg_replace('~^[ \t]+~m', '', $str);
			$str = preg_replace('~[ \t]+$~m', '', $str);
		}

		// The following regexes only need to be executed if the string contains html
		if ($html_found = (strpos($str, '<') !== FALSE))
		{
			// Elements that should not be surrounded by p tags
			$no_p = '(?:p|div|h[1-6r]|ul|ol|li|blockquote|d[dlt]|pre|t[dhr]|t(?:able|body|foot|head)|c(?:aption|olgroup)|form|s(?:elect|tyle)|a(?:ddress|rea)|ma(?:p|th))';

			// Put at least two linebreaks before and after $no_p elements
			$str = preg_replace('~^<'.$no_p.'[^>]*+>~im', "\n$0", $str);
			$str = preg_replace('~</'.$no_p.'\s*+>$~im', "$0\n", $str);
		}

		// Do the <p> magic!
		$str = '<p>'.trim($str).'</p>';
		$str = preg_replace('~\n{2,}~', "</p>\n\n<p>", $str);

		// The following regexes only need to be executed if the string contains html
		if ($html_found !== FALSE)
		{
			// Remove p tags around $no_p elements
			$str = preg_replace('~<p>(?=</?'.$no_p.'[^>]*+>)~i', '', $str);
			$str = preg_replace('~(</?'.$no_p.'[^>]*+>)</p>~i', '$1', $str);
		}

		// Convert single linebreaks to <br />
		if ($br === TRUE)
		{
			$str = preg_replace('~(?<!\n)\n(?!\n)~', "<br />\n", $str);
		}

		return $str;
	}
	
	/**
	 * preg_replace_callback method to clean pre (No <br>, <p> labels)
	 * 
	 *
	 * @param string $matches 
	 * @return string
	 */
	private static function clean_pre($matches)
	{
		if (is_array($matches))
		{
			$text = $matches[1] . $matches[2] . "</pre>";
		}	
		else
		{
			$text = $matches;
		}
		
		$text = str_replace('<br />', '', $text);
		$text = str_replace('<p>', "\n", $text);
		$text = str_replace('</p>', '', $text);

		return $text;
	}

    public static function array_2d_search($array,$keywords,$multiple = FALSE)
    {
        foreach($array as $idx=>$arr)
        {
            $find = TRUE;
            foreach($keywords as $k => $v)
            {
                if(!isset($arr[$k]) OR $arr[$k] != $v)
                {
                    $find = FALSE;
                    break;
                }
            }
            if($find)
            {
                if(!$multiple)
                {
                    return $idx;
                }
                else
                {
                    $keys[] = $idx;
                }
            }
        }

        if($multiple)
        {
            return isset($keys) ? $keys : array();
        }
        return FALSE;
    }

    public static function get_instance($class_name)
    {
        try
        {
            $class = new ReflectionClass($class_name);
            $instance = $class->newInstance();
        }
        catch(Exception $e){
            $instance = FALSE;
        }

        return $instance;
    }

    public static function generate_catalog_query($param,$current_params)
    {
        $queries = array();

        if(isset($param['price_range']))
        {
            if(!empty($param['price_range']) AND (empty($param['price_cancel_self']) OR $param['price_range'] != $current_params['price_range_key']))
            {
                $queries[] = 'prg='.$param['price_range'];
            }
        }
        elseif(!empty($current_params['price_range_key']) AND $current_params['price_range_key'] > 0)
        {
            $queries[] = 'prg='.$current_params['price_range_key'];
        }

        if(!empty($param['option']['single_option_one_time']))
        	$filter_options=array();
        else 
        	$filter_options = $current_params['options'];
        if(isset($param['option']))
        {
            $attribute_id = $param['option']['attribute_id'];
            $option_id = $param['option']['option_id'];
            if(isset($filter_options['at_'.$attribute_id][$option_id]) AND !empty($param['option']['cancel_self']))
            {
                unset($filter_options['at_'.$attribute_id][$option_id]);
            }
            else
            {
                if(!empty($param['option']['single_option_in_one_attribute']))
                {
                    $filter_options['at_'.$attribute_id] = array($option_id => $option_id);
                }
                else
                {
                    $filter_options['at_'.$attribute_id][$option_id] = $option_id;
                }
            }

            if(isset($filter_options['at_'.$attribute_id]) AND ($option_id == 0 OR !count($filter_options['at_'.$attribute_id])))
            {
                unset($filter_options['at_'.$attribute_id]);
            }

        }
        foreach($filter_options as $attr_key => $opts)
        {
            $queries[] = $attr_key.'='.implode('_',array_keys($opts));
        }

        if(isset($param['order_by']))
        {
            if($param['order_by']['by'] !== NULL)
            {
                $queries[] = 'orderby='.$param['order_by']['by'];
                if($param['order_by']['desc'] !== NULL)
                {
                    $queries[] = 'desc='.$param['order_by']['desc'];
                }
            }
            /*if(isset($_GET['page']))
            {
                $queries[] = 'page='.$_GET['page'];
            }*/
        }
        elseif(isset($current_params['order_by']))
        {
            if($current_params['order_by']['by'] !== NULL)
            {
                $queries[] = 'orderby='.$current_params['order_by']['by'];
                if($current_params['order_by']['desc'] !== NULL)
                {
                    $queries[] = 'desc='.$current_params['order_by']['desc'];
                }
            }
        }

        return implode('&',$queries);
    }

    public static function generate_price_range_string($range,$split = ' - ')
    {
        if(count($range) != 2)
        {
            return '';
        }

        if($range[0] == -1)
        {
            $range_string = Site::instance()->price(0,'code_view').$split.Site::instance()->price($range[1],'code_view');
        }
        elseif($range[1] == -1)
        {
            $range_string = Site::instance()->price($range[0],'code_view').'+';
        }
        else
        {
            $range_string = Site::instance()->price($range[0],'code_view').$split.Site::instance()->price($range[1],'code_view');
        }

        return $range_string;
    }
    
   /**
 	* xml object convert to array
 	*
 	* return array
 	*/
	public static function get_object_vars_final($obj)
	{
		if (is_object($obj))
  		{
  			$obj = get_object_vars($obj);
  		}	
  
  		if (is_array($obj))
  		{
  			foreach ($obj as $key => $value)
    		{
    			$obj[$key]=self::get_object_vars_final($value);
   			}
  		}
  
  		return $obj;
	}
	
	/**
	 * get domain
	 */
	public static function getDomain()
	{
		return 	isset($_SERVER['HTTP_X_FORWARDED_HOST']) ?
                	$_SERVER['HTTP_X_FORWARDED_HOST'] :
                	(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
	}
	
	public static function shared_route()
	{	
		Route::set('pimages', 'pimages(/<link>).<format>',array('format'=>'jpg|png|gif|jpe|jpeg|bmp'))
			->defaults(array(
				'controller' => 'pimages',
				'action' => 'index',
			));
	} 
	
	/**
	 * 根据lot_no得到eta_date
	 * @param string $lotno
	 */
	public static function getEtaDateByLotNo($lotno = NULL)
	{
		if ( ! empty($lotno))
		{
			$rst = DB::select('eta_date')
    					->from('orc_inventory_detail')
						->where('lot_no','=',$lotno)
						->execute()
						->current();
						
			return $rst['eta_date'];
		}
		return $lotno;		
	}
	
	public static function getCurrentStockBySubInventory($subinventory_code = NULL,$itemId = NULL)
	{
	      $inventoryTag = explode('-', $subinventory_code,2);
         		
         		if ( ! $inventoryTag[0])
         			return FALSE;
         			
         		$available_stock = 0;
         		
         		if (empty($inventoryTag[1])
         			OR in_array($inventoryTag[0],array('901FG_CA','911FG_GA')))
         		{
         			$itemInventory = DB::select(DB::expr('SUM(available_stock) AS `stock`'))
    					->from('orc_inventory_detail')
    					/*处理成品仓*/
						->where('subinventory','=',$inventoryTag[0])
						->where('item_id','=',$itemId)
						->where('status','!=','D')
						->group_by('item_id','subinventory')
						->execute()
						->current();
					
					if ( ! empty($itemInventory['stock']))
					{						
						$available_stock = intval($itemInventory['stock']);
					}
         		}
         		else 
         		{
					$itemInventory = DB::select(DB::expr('`available_stock` AS `stock`'))
						->from('orc_inventory_detail')
						->where('item_id','=',$itemId)
						->where('subinventory','=',$inventoryTag[0])
						->where('lot_no','=',$inventoryTag[1])
						->where('status','!=','D')
						->execute()	
						->current();
						
         			if ( ! empty($itemInventory['stock']))
					{						
						$available_stock = intval($itemInventory['stock']);
					}						
         		}
         		
         		return $available_stock;
	}
	
	public static function getPartsByMemo($parts)
	{
		if ( ! $parts)
		{		
			return false;
		}
		
		$implode_line_string = '|||';
		$implode_field_string = '||';
		$arrParts = array();
		
		if ($products = explode($implode_line_string,$parts))
		{
			foreach ($products as $product)
			{
				if ($product)
				{
					$arrParts[] = explode($implode_field_string,$product);
				}
			}
			return $arrParts;
		}
		else
		{
			return $arrParts;
		}		
	} 
}
