<?php if ($servers): ?>
<ul id="server-list">
    <?php foreach ($servers as $server): ?>
    <li>
        <form action="" method="post">
            <?php print $server->name . ' (' . long2ip($server->ip) . ')'; ?>
            <input type="hidden" name="server" value="<?php print $server->id; ?>" />
            <input type="text" name="revision" value="HEAD" style="width:64px" />
            <input type="submit" value="部署" onclick="return window.confirm('确定要部署到 <?php print $server->name; ?> ?');" />
        </form>
    </li>
    <?php endforeach ?>
</ul>
<?php else: ?>
<h3>未配置部署的目标主机</h3>
<?php endif ?>
