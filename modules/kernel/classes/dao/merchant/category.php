<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 *
 * DAO Merchant Category
 *
 * @author Spike.Qin
 *
 * @package DAO
 *
 * @version $Id: $
 *
 * @copyright Cofree Develop Term
 */

class DAO_Merchant_Category extends DAO
{
	/**
	 * 表名
	 * @var unknown_type
	 */
	protected $table_name = 'category';
}
