<?php
defined('SYSPATH') or die('No direct script access.');

class Model_Channel_Newsletter extends ORM
{
    protected $_filters = array(
            TRUE => array('trim' => NULL)
    );
    
    protected $_rules = array(
        'channel_id' => array(
            'not_empty' => NULL,
        ),
    );
}
?>