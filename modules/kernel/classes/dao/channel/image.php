<?php defined('SYSPATH') OR die('No direct access allowed.');
  /**
   * 
   * DAO credit card
   * 
   * @author Spike.Qin
   *
   * @package DAO
   *
   * @version $Id: image.php 6283 2012-02-16 09:27:48Z zhao.yang $
   *
   * @copyright Cofree Develop Term
   */

class DAO_Channel_Image extends DAO 
{
   	/**
   	 * 表名
   	 * @var unknown_type
   	 */
   	 protected $table_name = 'channel_image';
   	 
}
