<?php
defined('SYSPATH') or die('No direct script access.');
  
  /**
   * 
   * Credit Card Service 信用卡
   * 
   * @author zhou.shuo
   *
   * @package DAO
   *
   * @version $Id: creditcard.php 6283 2012-02-16 09:27:48Z zhao.yang $
   *
   * @copyright Cofree Develop Term
   */
  class Service_Channel_CreditCard extends  Service
  {
  	/**
  	 * DAO 名称
  	 * 
  	 * @var string
  	 */
  	 private $_dao_object = 'channel_creditcard';
  	 
  	 private $_data = NULL;
  	 
  	 protected static $instances = array();
  	 
  public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
	protected  function __construct($id = 0)
	{
		$this->_load($id);

	}
    public function tree($query_struct,$index = 'id')
	{
		return $this->dao($this->_dao_object)->find_all($query_struct, $index);
	}
   /**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
     /**
	 * Load Data层数据
	 * @param int $key  primary_id value
	 * @param string $field 字段名
	 * @return mixed
	 */
	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	
	/**
	 * 添加信用卡
	 * 
	 * @param array $data
	 */
	 
	 
    public function add($data)
    {
    	return $this->dao($this->_dao_object)->add($data);
    }
    
   
    /**
     * 修改信用卡信息
     * @param array $data
     */
    public function edit($data)
    { 
    	if ( ! isset($data['id'])) 
    	{
    	   return FALSE;
    	}
    
    	return $this->dao($this->_dao_object)->edit($data);
    }
    /**
     *删除信用卡
     * @param int $id
     */
    public function del($id)
    {
		return $this->dao($this->_dao_object)->delete($id);
    }
  }