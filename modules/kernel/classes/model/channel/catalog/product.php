<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_Catalog_Product
 *
 * @author Spike.Qin
 *
 * @version $Id: product.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_Catalog_Product extends ORM {

	protected $_belongs_to = array('channel_catalog' => array());
	
	protected $_filters = array(
		TRUE => array('trim' => NULL)
	);
}
