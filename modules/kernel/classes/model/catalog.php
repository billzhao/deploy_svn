<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Catalog
 *
 * @author bzhao
 *
 * @version $Id: catalog.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Catalog extends ORM {

	//$catalog->remove('products', $product);
	//$catalog->add('products', $product);
	protected $_has_many = array(
		'products' => array('model' => 'product' , 'through' => 'catalog_products')
	);
		
    protected $_filters = array(
        TRUE => array('trim' => NULL)
    );

    protected $_rules = array
        (
            'name' => array(
                'not_empty'	=> NULL,
                'max_length' => array(255),
            ),
            'link' => array(
                'not_empty'	=> NULL,
                'max_length' => array(255),
            ),
            'image_src' => array(
                'max_length' => array(255),
            ),
            'image_link' => array(
                'max_length' => array(255),
            ),
            'image_alt' => array(
                'max_length' => array(255),
            ),
            'meta_title' => array(
                'max_length' => array(65535),
            ),
            'meta_keyword' => array(
                'max_length' => array(65535),
            ),
            'meta_description' => array(
                'max_length' => array(65535),
            ),
			'description' => array(
                'max_length' => array(65535),
            ),
            'parent_id' => array(
                'not_empty'	=> NULL,
            ),
        );

}

