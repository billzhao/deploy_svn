<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 认证before
 * @ClassName: Controller_Auth
 *
 * @author bzhao
 *
 * @version $Id: auth.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_Auth extends Controller_Webpage
{
	protected $customer_id;
	
    public function before()
    {
		parent::before();
        
		if ( ! ($customer_id = Service_Channel_Customer::loggedIn()))
        {
            $this->request->redirect(URL::base().'customer/login?redirect='.URL::current(TRUE));
        }
        
        $this->customer_id = $this->customer_id;
    }
    
}
