<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'category'	=>	array(
		'0'		=>	'category01',
		'1'		=>	'category02',
		'2' 	=>	'category03',
		'3' 	=>  'category04',
	),
	'price_category'	=>	array(
		'0'		=>	'price_category01',
		'1'		=>	'price_category02',
		'2' 	=>	'price_category03',
		'3' 	=>  'price_category04',
	),
	'erp_category'	=>	array(
		'category00'		=>	'Not In Use',
		'0'		=>	'SEGMENT1',
		'1'		=>	'SEGMENT2',
		'2' 	=>	'SEGMENT3',
		'3' 	=>  'SEGMENT4',
	),
);
