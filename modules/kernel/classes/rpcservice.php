<?php defined('SYSPATH') or die('No direct script access.');
class RpcService
{	
	private $_apiKey;
	
	private $_attachmentService;
	
    protected static $instances = array();
	
	public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
	
	protected function __construct()
	{
		require_once(Kohana::find_file('classes', 'rpc/phprpc_client'));
		
		if (IN_PRODUCTION)
		{
			$this->_attachmentService = new PHPRPC_Client('http://127.0.0.1:8101/phprpc/attachment');
		}
		else
			$this->_attachmentService = new PHPRPC_Client(Kohana::config('rpc.remote_config.URL'));
		
		$this->_apiKey = Kohana::config('rpc.remote_config.ApiKey');
	}

	public function add($file_path,$site_id,$site_domain,$file_name='')
	{
		$file_path=trim($file_path);
		if(!$file_name)
		{
			$path_parts =pathinfo($file_path);
			$file_name=$path_parts["basename"];
			$file_type=$path_parts["extension"];
		}
		else 
		{
			$file_type=explode('.', $file_name);
			$file_type=$file_type[count($file_type)-1];
		}
		$file_type=strtolower($file_type);
		$file_name=strip_tags(strtolower($file_name));
		if(!in_array($file_type, Kohana::config('rpc.file_type')))
			return false;
		$file_size=filesize($file_path);
		if(Kohana::config('rpc.fileSizePreLimit') > 0 && $file_size > Kohana::config('rpc.fileSizePreLimit'))
			return false;
		$filePostTypes=Kohana::config('rpc.filePostType');
		if(array_key_exists($file_type, $filePostTypes))
			$file_mime=$filePostTypes[$file_type];
		else 
			$file_mime = 'application/octet-stream';
        $attachment_data_original = array(
        	'filePostfix' => $file_type,
	        'fileMimeType' => $file_mime,
	        'fileSize' => $file_size,
	        'fileName' => $file_name,
	        'srcIp' => isset($_SERVER["REMOTE_ADDR"])?$_SERVER["REMOTE_ADDR"]:'',
	        'attachMeta' => json_encode(array(
                'siteId' => $site_id,
                'siteDomain' => $site_domain
            )),
	        'createTimestamp' => time(),
	        'modifyTimestamp' => time()
        );
		return $this->_attachmentService->phprpc_addAttachmentFileData($attachment_data_original, @file_get_contents($file_path), md5(json_encode(array($attachment_data_original)) . $this->_apiKey));
	}
	
	public function delete($attachmentID)
	{
		return $this->_attachmentService->phprpc_removeAttachmentDataByAttachmentId($attachmentID,md5(json_encode(array($attachmentID)) . $this->_apiKey));
	}
	
	public function getAttachmentData($attachmentID)
	{
		return $this->_attachmentService->phprpc_getAttachmentDataById($attachmentID,md5(json_encode(array($attachmentID)) . $this->_apiKey));
	}
	
	public function getStoreData($attachmentID)
	{
		return $this->_attachmentService->phprpc_getStoreDataByAttachmentId($attachmentID,md5(json_encode(array($attachmentID)) . $this->_apiKey));
	}
}