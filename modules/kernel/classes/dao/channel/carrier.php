<?php defined('SYSPATH') OR die('No direct access allowed.');

class DAO_Channel_Carrier extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'channel_carrier';
}