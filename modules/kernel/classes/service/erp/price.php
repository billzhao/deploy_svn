<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * ERP price List Service
 * @ClassName: Service_Erp_Price
 *
 * @author bzhao
 *
 * @version $Id: price.php 7339 2012-06-02 04:50:16Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Erp_Price extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'orc_price_list';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	
	/**
	 * 返回产品的价格 
	 */
	public function genePrice($extend = array())
	{
		//判断参数
		$query_struct = array();
		$price = '';
		/*
		 * 读取Customer的记录，不存在读取-1的记录
		 */
		$query_struct['where']['customer_id'] = $extend['customer_id'];
		$query_struct['where']['code'] = $extend['code'];
		
		if (stripos($extend['inventory'], 'CA'))
		{
			$query_struct['where']['price_name like'] = '%CA';
		}
		else if (stripos($extend['inventory'], 'GA'))
		{
			$query_struct['where']['price_name like'] = '%GA';
		}
		else
		{
			if (stripos($extend['default'], 'CA'))
			{
				$query_struct['where']['price_name like'] = '%CA';
			}
			else if (stripos($extend['default'], 'GA'))
			{
				$query_struct['where']['price_name like'] = '%GA';
			}
			else
				return 0.00;
		}	
//		$query_struct['where']['price_name'] = 'PRIWI_Price List_GA';
//		$query_struct['where']['price_name'] = 'PRIWI_Price List_CA';
		$customerModelPrice = $this->tree($query_struct,array('price','min','max'));

		
		if ( count ($customerModelPrice))
		{
			foreach ($customerModelPrice as $CMP)
			{
				if ($extend['quantity'] > $CMP['min'] && $extend['quantity'] <= $CMP['max'])
				{
					$price = $CMP['price'];
					break;
				}
			}
		}
		else 
		{
			$query_struct['where']['customer_id'] = -1;
			$ModelPrice = $this->tree($query_struct,array('price','min','max'));
			
			if ( count ($ModelPrice))
			{
				foreach ($ModelPrice as $MP)
				{
					if ($extend['quantity'] > $MP['min'] && $extend['quantity'] <=$MP['max'])
					{
						$price = $MP['price'];
						break;
					}
				}
			}
		
		}
		return $price;
								
	}
}
