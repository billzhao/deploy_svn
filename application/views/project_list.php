<?php if ($user->id == 1): ?>
<?php print html::anchor('/project/add', 'Add Project'); ?>
<?php endif ?>

<?php if ($projects): ?>
<ul id="project-list">
    <?php foreach ($projects as $project): ?>
    <li>
        <?php print html::anchor("/project/target/$project->id", $project->name); ?>
        <?php if ($user->id == 1) { print html::anchor("/project/edit/$project->id", 'Edit'); } ?>
        <?php if ($user->id == 1) { print html::anchor("/project/delete/$project->id", 'Delete', array('onclick' => 'return window.confirm("delete?");')); } ?>
    </li>
    <?php endforeach ?>
</ul>
<?php else: ?>
<h3>Oops, There is no projects you have privelege to deploy.</h3>
<?php endif ?>
