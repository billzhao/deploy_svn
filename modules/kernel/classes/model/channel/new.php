<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_New
 *
 * @author bzhao
 *
 * @version $Id: new.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_New extends ORM
{

    protected $_filters = array(
            TRUE => array('trim' => NULL)
    );
    
    protected $_rules = array(
        'channel_id' => array(
            'not_empty' => NULL,
        ),
        'title' => array(
            'not_empty' => NULL, 
        ), 
        'author' => array(
            'not_empty' => NULL, 
        ), 
        'content' => array(
            'max_length' => array(65535), 
        ), 
    );
}
?>