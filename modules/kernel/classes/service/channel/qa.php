<?php
defined('SYSPATH') or die('No direct script access.');

class Service_Channel_Qa extends Service
{
    private $_dao_object = 'channel_qa';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{      
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}


	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	

	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache

		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}
	

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	public function set($data = array())
	{
		return $this->_dao->add($data);
	}

	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }
        
        return $this->_dao->edit($data);
	}

	public function delete($id)
	{
		return $this->_dao->delete($id);
	}
	
	
    public function count($query_struct)
    {
        return $this->_dao->count($query_struct);
    }

}
