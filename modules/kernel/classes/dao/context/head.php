<?php defined('SYSPATH') OR die('No direct access allowed.');
  /**
   * 
   * DAO Context Head
   * 
   * @author Spike.Qin
   *
   * @package DAO
   *
   * @version $Id: head.php 6283 2012-02-16 09:27:48Z zhao.yang $
   *
   * @copyright Cofree Develop Term
   */

class DAO_Context_Head extends DAO 
{
   	/**
   	 * 表名
   	 * @var unknown_type
   	 */
   	 protected $table_name = 'context_head';
   	 
}
