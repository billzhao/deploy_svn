Dear <?php echo Service_Channel_Customer::instance($order['customer_id'])->get('name');?>,<br/><br/>

Thank you for placing your Ice Bear order!<br/><br/>

You can view order details at <?php echo ! IN_PRODUCTION ? html::anchor('http://motob2b.cofreeonline.com/order/view/'.$order['ordernum']) : html::anchor('http://www.icebearatv.com/order/view/'.$order['ordernum']);?><br/><br/>

Your order Number is <b><?php echo $order['erp_ordernum'];?></b><br/><br/>

You can also view it directly in your own account.<br/><br/>

If you have any questions or need assistance please contact your sales representative. <br/><br/>

Thank you for you business!<br/><br/>

Ice Bear Powersports<br/><br/>

Email: <a href="mailto:icebearatv@gmail.com">icebearatv@gmail.com<br/></a>
<a href="http://www.icebearatv.com/" target="_blank">http://www.icebearatv.com/</a>