_add_to_cart = true;//if true, all values are valid, glasses lens can be added to cart.
function submit_form()
{
	check_form();
    var od_sph = document.getElementById('od_sph').value;
    var os_sph = document.getElementById('os_sph').value;
    var od_cyl = document.getElementById('od_cyl').value;
    var os_cyl = document.getElementById('os_cyl').value;
    var od_axis = document.getElementById('od_axis').value;
    var os_axis = document.getElementById('os_axis').value;
    var od_add = document.getElementById('od_add').value;
    var os_add = document.getElementById('os_add').value;
    var pd = document.getElementById('pd').value;
    var near_pd = document.getElementById('near_pd').value;
    var mono_l = document.getElementById('mono_l').value;
    var mono_r = document.getElementById('mono_r').value;
    var lens_index = 0;
    for(i=0; i < document.getElementsByName('lens_index').length; i++)
    {
        if (document.getElementsByName('lens_index')[i].checked)
        {
            lens_index = document.getElementsByName('lens_index')[i].value;
            break;
        }
    }
    var is_uploaded = document.getElementById('upload_file').value;
    if(is_uploaded && lens_index)
    {
        if((!pd && !near_pd) && (!mono_r || !mono_l))
        {
            showMsg('Please select your PD. Then click the ADD TO CART button to continue your shopping when all your prescriptions are ready.','e');
            return false;
        }
        else
        {
        	document.glasseslens.submit();
        }
    }
    else{
        if((!pd && !near_pd) && (!mono_r || !mono_l))
            showMsg('Please select your PD. Then click the ADD TO CART button to continue your shopping when all your prescriptions are ready.','e');
        else if(lens_index==0)
            showMsg('Please select lens index. Then click the ADD TO CART button to continue your shopping when all your prescriptions are ready.','e');
        else if(od_cyl!='0' && od_axis=='0')
            showMsg('Please select your Axis.','e');
        else if(os_cyl!='0' && os_axis=='0')
            showMsg('Please select your Axis.','e');
        else if(document.getElementById('chb_photochromic').checked==true && document.getElementById('photochromic_color').value=="0")
            showMsg('Please select your color.','e');
        else if(document.getElementById('chb_color_tint').checked==true && document.getElementById('color_tint').value=="0")
            showMsg('Please select your color.','e');
        else if(document.getElementById('chb_transition').checked==true && document.getElementById('transition_color').value=="0")
            showMsg('Please select your color.','e');
        else if(lens_index=="7" && document.getElementById('lens_color').value=="0")
            showMsg('Please select your color.','e');
        else if(_add_to_cart==true)
        {
            if(frame_bifocal && (od_add>0 || os_add>0))
            {
                if(!confirm('Are you sure the type of lens you want is Single Vision Lenses (Reading Only) instead of Bifocal or Progressive lenses? If not please change your Lens Type.'))
                    return false;
            }
            if(od_sph>0 && os_sph<0)
            {
                if(!confirm('Are you sure the SPH sign of right eye is \'+\', but left eye is \'-\'?'))
                    return false;
            }
            if(od_sph<0 && os_sph>0)
            {
                if(!confirm('Are you sure the SPH sign of right eye is \'-\', but left eye is \'+\'?'))
                    return false;
            }
            if(od_cyl>0 && os_cyl<0)
            {
                if(!confirm('Are you sure the CYL sign of right eye is \'+\', but left eye is \'-\'?'))
                    return false;
            }
            if(od_cyl<0 && os_cyl>0)
            {
                if(!confirm('Are you sure the CYL sign of right eye is \'-\', but left eye is \'+\'?'))
                    return false;
            }
            if(od_add != os_add)
            {
                if(!confirm('Are you sure the ADD of right eye NOT equals left eye?'))
                    return false;
            }
            if(od_sph=='0' && os_sph=='0' && document.getElementById('upload_file').value=='')
            {
                if(!confirm('Are you sure you need a Plano eyeglasses? If not please select your SPH.'))
                    return false;
            }
            document.glasseslens.submit();
        }
    }
}
/**
 * Validate lens type, lens index, sph, cyl, axis, add, color tint, photochromic, transition.
 * @return bool
 */
function check_form()
{
    var obj_chb_color_tint = document.getElementById('chb_color_tint');
    var obj_chb_photochromic = document.getElementById('chb_photochromic');
    var lens = document.getElementById('lens').value;
    var od_sph = document.getElementById('od_sph').value;
    var os_sph = document.getElementById('os_sph').value;
    var od_cyl = document.getElementById('od_cyl').value;
    var os_cyl = document.getElementById('os_cyl').value;
    var od_axis = document.getElementById('od_axis').value;
    var os_axis = document.getElementById('os_axis').value;
    var od_add = document.getElementById('od_add').value;
    var os_add = document.getElementById('os_axis').value;
    var pd = document.getElementById('pd').value;
    var near_pd = document.getElementById('near_pd').value;
    var mono_l = document.getElementById('mono_l').value;
    var mono_r = document.getElementById('mono_r').value;
    var lens_index = 0;
    for(i=0; i < document.getElementsByName('lens_index').length; i++)
    {
        if (document.getElementsByName('lens_index')[i].checked)
        {
            lens_index = document.getElementsByName('lens_index')[i].value;
            break;
        }
    }
    _add_to_cart = true;
    var _continue = false;
    if(!frame_bifocal && (lens == 3 || lens ==4))
        showMsg('This frame is not suitable for progressive and bifocal lenses, please <a href="/special/detail/bifocal/">click here</a> to view progressive and bifocal frames.','e');
    else if(lens == 1 || lens == 2)
    {
        // color tint
        // photochromic
    	//sph
        switch(lens_index)
        {
            case '1':
                set_color_tint(true);
                set_photochromic('t');
                if ((od_sph<-16 || od_sph>10) || (os_sph<-16 || os_sph>10))
                    showMsg('Single Vision - Plastic Lenses Index 1.499 SPH must be between -16.00 and +10.00.','e');
                break;
            case '2':
                set_color_tint(true);
                set_photochromic('p');
                if ((od_sph<-12 || od_sph>8) || (os_sph<-12 || os_sph>8))
                    showMsg('Single Vision - Thinner Lenses Middle Index 1.553 Photochromic Lenses SPH must be between -12.00 and +8.00.','e');
                else if ((od_sph<-16 || od_sph>12) || (os_sph<-16 || os_sph>12))
                    showMsg('Single Vision - Thinner Lenses Middle Index 1.553 SPH must be between -16.00 and +12.00.','e');
                break;
            case '3':
                set_color_tint(true);
                set_photochromic('t');
                if ((od_sph<-18 || od_sph>12) || (os_sph<-18 || os_sph>12))
                    showMsg('Single Vision - Super Thin Lenses High Index 1.600 SPH must be between -18.00 and +12.00.','e');
                break;
            case '4':
            	set_color_tint(false);
                set_photochromic('');
                if ((od_sph<-18 || od_sph>12) || (os_sph<-18 || os_sph>12))
                    showMsg('Single Vision - Diamond Clear Polycarbonate High Index 1.591 SPH must be between -18.00 and +12.00.','e');
                break;
            case '5':
                set_color_tint(false);
                set_photochromic('t');
                if ((od_sph<-14 || od_sph>12) || (os_sph<-14 || os_sph>12))
                    showMsg('Single Vision - Ultra Thin Lenses High Index 1.670 SPH must be between -14.00 and +12.00.','e');
                break;
            case '6':
                set_color_tint(false);
                set_photochromic('');
                if ((od_sph<-14 || od_sph>12) || (os_sph<-14 || os_sph>12))
                    showMsg('Single Vision - Extreme Thin Lenses High Index 1.740 SPH must be between -14.00 and +12.00.','e');
                break;
            case '7':
                set_color_tint(false);
                set_photochromic('');
                if ((od_sph<-8 || od_sph>5) || (os_sph<-8 || os_sph>5))
                    showMsg('Single Vision - Polarized Lenses Index 1.499 SPH must be between -8.00 and +5.00.','e');
                break;
            default:
                break;
        }
        // cyl
        if (lens_index && ((od_cyl<-6 || od_cyl>6) || ((os_cyl<-6 || os_cyl.value>6))))
        {
            showMsg('Single Vision - CYL must be between -6.00 and +6.00.','e');
        }
        else
            _continue = true;
    }
    else if(lens=="3")
    {
        // color tint
        // photochromic
    	//sph
        switch(lens_index)
        {
            case '1':
                set_color_tint(true);
                document.getElementById("chb_photochromic").disabled=true;
                if ((od_sph<-5 || od_sph>7) || (os_sph<-5 || os_sph>7))
                    showMsg('Progressive - Plastic Lenses Index 1.499 SPH must be between -5.00 and +7.00.','e');
                break;
            case '2':
                set_color_tint(true);
                set_photochromic('');
                if ((od_sph<-5 || od_sph>7) || (os_sph<-5 || os_sph>7))
                    showMsg('Progressive - Thinner Lenses Middle Index 1.553 SPH must be between -5.00 and +7.00.','e');
                break;
            case '3':
                set_color_tint(true);
                set_photochromic('t');
                if ((od_sph<-6 || od_sph>5) || (os_sph<-6 || os_sph>5))
                    showMsg('Progressive - Super Thin Lenses High Index 1.600 SPH must be between -6.00 and +5.00.','e');
                break;
            case '4':
                set_photochromic('');
                if ((od_sph<-6 || od_sph>5) || (os_sph<-6 || os_sph>5))
                    showMsg('Progressive - Diamond Clear Polycarbonate High Index 1.591 SPH must be between -6.00 and +5.00.','e');
                break;
            case '5':
                set_color_tint(false);
                set_photochromic('t');
                if ((od_sph<-6 || od_sph>5) || (os_sph<-6 || os_sph>5))
                    showMsg('Progressive - Ultra Thin Lenses High Index 1.670 SPH must be between -6.00 and +5.00.','e');
                break;
            case '6':
                set_color_tint(false);
                set_photochromic('');
                if ((od_sph<-8.75 || od_sph>9) || (os_sph<-8.75 || os_sph>9))
                    showMsg('Progressive - Extreme Thin Lenses High Index 1.740 SPH must be between -8.75 and +9.00.','e');
                break;
            case '7':
                set_color_tint(false);
                set_photochromic('');
                if ((od_sph<-6 || od_sph>7) || (os_sph<-6 || os_sph>7))
                    showMsg('Progressive - Polarized Lenses Index 1.499 SPH must be between -6.00 and +7.00.','e');
                break;
            default:
            	break;
        }
        //cyl
        if (lens_index && ((od_cyl<-4 || od_cyl>4) || (os_cyl<-4 || os_cyl>4)))
            showMsg('Progressive - CYL must be between -4.00 and +4.00.','e');
            //add
        else if (lens_index && ((od_add<0.75 || od_add>3.5) || (os_add<0.75 || os_add>3.5)))
                showMsg('Progressive - ADD must be between 0.75 and 3.50.','e');
        else
        	_continue = true;
    }
    else if(lens=="4")
    {
        set_photochromic('');
        // color tint
        // photochromic
    	set_color_tint(false);
        switch(lens_index)
        {
            case '1':
                set_color_tint(true);
                if ((od_sph<-8 || od_sph>6) && (os_sph<-8 || os_sph>6))
                    showMsg('Bifocal - Plastic Lenses Index 1.499 SPH must be between -8.00 and +6.00.','e');
                break;
            case '2':
                showMsg('Bifocal - Thinner Lenses Middle Index 1.553 Not Available','e');
                break;
            case '3':
                showMsg('Bifocal - Super Thin Lenses High Index 1.600 Not Available','e');
                break;
            case '4':
                if ((od_sph<-8 || od_sph>6) && (os_sph<-8 || os_sph>6))
                    showMsg('Bifocal - Diamond Clear Polycarbonate High Index 1.591 SPH must be between -8.00 and +6.00.','e');
                break;
            case '5':
                showMsg('Bifocal - Ultra Thin Lenses High Index 1.670 Not Available','e');
                break;
            case '6':
                showMsg('Bifocal - Extreme Thin Lenses High Index 1.740 Not Available','e');
                break;
            case '7':
                showMsg('Bifocal - Polarized Lenses Index 1.499 Not Available','e');
                break;
            default:
                break;
        }
        // cyl
        if (lens_index && ((od_cyl<-4 || od_cyl>4)) && (os_cyl<-4 || os_cyl>4))
            showMsg('Bifocal - CYL must be between -4.00 and +4.00.','e');
        // add
        if (lens_index && (od_add<1 || od_add>3.5) && (os_add<1 || os_add>3.5))
            showMsg('Bifocal - ADD must be between 1.00 and 3.50.','e');
        if((pd<58 && pd!=''))
            showMsg('The PD value you entered is smaller than average, please confirm it. You can refer to <a href="/HowdoIreadmyprescription.html#pup_dis"><b style="font-weight:bold;color:red">what\'s PD</b></a>','i');
        else if(pd>72)
            showMsg('The PD value you entered is higher than average, so we suggest you to confirm it. You can refer to <a href="#">what\'s PD</a>','i');
        else if(frame_circle=='Rimless' && ((Math.abs(od_sph)+Math.abs(od_cyl)>6) ||(Math.abs(os_sph)+Math.abs(os_cyl)>6)))
            showMsg('The rimless frame does not suit your prescription, please select a full frame or half frame.','e');
        else if((lens_index=="1" || lens_index=="2" || lens_index=="7") && ((Math.abs(od_sph)+Math.abs(od_cyl)>12) || (Math.abs(os_sph)+Math.abs(os_cyl)>12)))
            showMsg('We suggest you to select the lenses with higher index in consideration of your comfortable wearing, for the lenses will be quite thick','i');
        else
        	_continue = true;
    }
    else
    	_continue = true;
    
    if(_continue)
    {
    	if((pd && pd<58)){
            showMsg('The PD value you entered is smaller than average, please confirm it. You can refer to <a href="/HowdoIreadmyprescription.html#pup_dis"><b style="font-weight:bold;color:red">what\'s PD</b></a>','i');
        }else if(pd>72)
            showMsg('The PD value you entered is higher than average, so we suggest you to confirm it. You can refer to <a href="#">what\'s PD</a>','i');
        else if(frame_circle.toLowerCase()=='rimless' && ((Math.abs(od_sph)+Math.abs(od_cyl)>6) || (Math.abs(os_sph)+Math.abs(os_cyl)>6)))
            showMsg('The rimless frame does not suit your prescription, please select a full frame or half frame.','e');
        else if((lens_index=="1" || lens_index=="2" || lens_index=="7") && ((Math.abs(od_sph)+Math.abs(od_cyl)>12) || (Math.abs(os_sph)+Math.abs(os_cyl)>12)))
            showMsg('We suggest you to select the lenses with higher index in consideration of your comfortable wearing, for the lenses will be quite thick','i');
        else
            closeMsg();
    }
    // calculate price
    calc_price();
}
function closeMsg()
{
    document.getElementById('lens_error').style.display = "none";
}
function showMsg(str,level)
{
    document.getElementById('lens_error').style.display = "";
    document.getElementById('liErrorMsg').innerHTML=str;
    if(level=='i')
        document.getElementById('liErrorTitle').innerHTML='Suggestion:';
    else
    {
        document.getElementById('liErrorTitle').innerHTML='Attention:';
        _add_to_cart = false;
    }
}
function changeBGColor(obj)
{
    for(var i=0;i<obj.length;i++)
    {
        if(obj[i].selected == true)
        {
            obj.style.background=obj[i].style.background;
            break;
        }
    }
    check_form();
}
function set_color_tint(bln)
{
    var color_tint = document.getElementById('color_tint');
    var check_color_tint = document.getElementById('chb_color_tint');
    if(bln)
    {
    	check_color_tint.disabled = false;
    	color_tint.disabled = false;
    }
    else
    {
    	check_color_tint.disabled = true;
        disable_color_tint();
    }
}
function set_photochromic(str)
{
    if(str=='p')
    {
        document.getElementById("chb_transition").disabled = true;
        disable_transition();
        document.getElementById("chb_photochromic").disabled = false;
        document.getElementById("photochromic_color").disabled = false;
    }
    else if(str=='t')
    {
        document.getElementById("chb_transition").disabled=false;
        document.getElementById("transition_color").disabled=false;
        document.getElementById("chb_photochromic").disabled=true;
        disable_photochromic();
    }
    else
    {
        document.getElementById("chb_transition").disabled=true;
        disable_transition();
        document.getElementById("chb_photochromic").disabled=true;
        disable_photochromic();
    }
}
function disable_photochromic()
{
    document.getElementById('photochromic_color').disabled = true;
    document.getElementById('photochromic_color').value = '';
    document.getElementById('photochromic_color').style.background = 'white';
    document.getElementById('chb_photochromic').checked = false;
}
function disable_transition()
{
    document.getElementById('transition_color').disabled = true;
    document.getElementById('transition_color').value = '';
    document.getElementById('transition_color').style.background = 'white';
    document.getElementById('chb_transition').checked = false;
}
function disable_color_tint()
{
    document.getElementById('color_tint').disabled = true;
    document.getElementById('color_tint').value = '';
    document.getElementById('color_tint').style.background = 'white';
    document.getElementById('chb_color_tint').checked = false;
}
function color_tint_click(obj)
{
    if(obj.checked==true)
    {
        obj.disabled = false;
        if(document.getElementById('chb_photochromic').checked)
        {
            disable_photochromic();
        }
        if(document.getElementById('chb_transition').checked)
        {
            disable_transition();
        }
    }
    else
    {
        disable_color_tint();
    }
    check_form();
}
function photochromic_click(obj)
{
    if(obj.checked==true)
    {
        obj.disabled=false;
        disable_color_tint();
        disable_transition();
    }
    else
    {
        disable_photochromic();
    }
    check_form();
}
function transition_click(obj)
{
    if(obj.checked==true)
    {
        obj.disabled=false;
        disable_color_tint();
        disable_photochromic();
    }
    else
    {
        disable_transition();
    }
    check_form();
}
function lens_index_change(obj)
{    switch(obj.value)
    {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
            document.getElementById('lens_color').disabled = true;
            break;
        case '7':
            document.getElementById('lens_color').disabled = false;
            set_color_tint(false);
            set_photochromic('');
            break;
        default:
            break;
    }
    check_form();
}
function lens_change(obj)
{
    if(obj.value=='2')
    {
        document.getElementById('pd').style.display = 'none';
        document.getElementById('pd').value = '';
        document.getElementById('near_pd').style.display = '';
        document.getElementById('near_pd').value = '';
    }
    else
    {
        document.getElementById('pd').style.display = '';
        document.getElementById('pd').value = '';
        document.getElementById('near_pd').style.display = 'none';
        document.getElementById('near_pd').value = '';
    }
    //Addition
    if(obj.value=='1' || obj.value=='2')
    {
        if(obj.value=='1')
        {
            document.getElementById('od_add').disabled = true;
            document.getElementById('os_add').disabled = true;
        }
        else
        {
            document.getElementById('od_add').disabled = false;
            document.getElementById('os_add').disabled = false;
        }
    }
    else
    {
        document.getElementById('od_add').disabled = false;
        document.getElementById('os_add').disabled = false;
    }
    //set lens index
    if(obj.value=='4')
    {
        document.getElementById('lens_1').disabled=false;
        document.getElementById('lens_2').disabled=true;
        document.getElementById('lens_3').disabled=true;
        document.getElementById('lens_4').disabled=false;
        document.getElementById('lens_5').disabled=true;
        document.getElementById('lens_6').disabled=true;
        document.getElementById('lens_7').disabled=true;
        document.getElementById('lens_color').disabled=true;
    }
    else
    {
        document.getElementById('lens_1').disabled=false;
        document.getElementById('lens_2').disabled=false;
        document.getElementById('lens_3').disabled=false;
        document.getElementById('lens_4').disabled=false;
        document.getElementById('lens_5').disabled=false;
        document.getElementById('lens_6').disabled=false;
        document.getElementById('lens_7').disabled=false;
        document.getElementById('lens_color').disabled=false;
    }
    check_form();
}
function linkChange(obj,type)
{
    var disable = true;
    if(obj.value!="0" && obj.value!="")
    {
        disable = false;
    }
    document.getElementById(type+'_axis').disabled = disable;
    check_form();
}
function calc_price()
{
	total_price = parseFloat(frame_price);
	lens = document.getElementById('lens').value;
	total_price += parseFloat(lens_price[lens]);
	lens_index = 0;
    for(i=0; i < document.getElementsByName('lens_index').length; i++)
    {
        if (document.getElementsByName('lens_index')[i].checked)
        {
            lens_index = document.getElementsByName('lens_index')[i].value;
            break;
        }
    }
    if (lens_index)
    	total_price += parseFloat(lens_index_price[lens_index]);
    color_tint = document.getElementById('chb_color_tint').checked;
    if (color_tint)
    	total_price += parseFloat(color_tint_price);
    photochromic = document.getElementById('chb_photochromic').checked;
    if (photochromic)
    	total_price += parseFloat(photochromic_price);
    transition = document.getElementById('chb_transition').checked;
    if (transition)
    	total_price += parseFloat(transition_price);
	anti = document.getElementById('anti').checked;
    if (anti)
    	total_price += parseFloat(anti_price);

    total = document.getElementById('fntTotal');
	total.innerHTML = currency['code']+(Math.round(parseFloat(total_price*currency['rate'])*100)/100);
}
function changePd(obj)
{
    document.getElementById('pd').value = '';
    document.getElementById('mono_l').value = '';
    document.getElementById('mono_r').value = '';
    document.getElementById('near_pd').value = '';
    if(obj.checked==true)
    {
        document.getElementById('twoPd').style.display = '';
        document.getElementById('onePd').style.display = 'none';
    }else{
        document.getElementById('onePd').style.display = '';
        document.getElementById('twoPd').style.display = 'none';
    }
}