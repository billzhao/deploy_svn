<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * Channel Service
 *
 * @ClassName: Service_Channel
 *
 * @author bzhao
 *
 * @version $Id: channel.php 7504 2012-08-21 06:57:01Z qin.yazhuo $
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Channel extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel';

    private $_data = NULL;

    /**
     * Channel对应的RoleID
     * @var array $channel_role_mapping
     */
    protected $channel_role_mapping = array(
    	'b2b' => 3,
    	'b2c' => 4,
    	'multi' => 5,
    	'product' => 6);

    protected static $instances = array();

    public static function & instance($id = 0)
    {
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }

        return self::$instances[$id];
    }

	protected  function __construct($id = 0)
	{
	    if ( ! $id)
        {
            $domain = toolkit::getDomain();
            $this->_load($domain);
        }
        else
        {
            $this->_load('',$id,FALSE);
        }
	}

	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}

	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($domain,$id = 0,$dieError = TRUE)
	{
		if ( ! $id)
		{
			$query_struct = array(
			'where' => array('domain' => $domain));
			$data = $this->dao($this->_dao_object)->find($query_struct);
		}
		else
		{
			$data = $this->dao($this->_dao_object)->find($id);
		}
        if (is_array($data) && count($data))
        {
            $this->_data = $data;
        }
        else
        {
            if ($dieError)
            {
        		//echo 'no domain data';
        		//throw new Kohana_Exception('no domain data');
            	//exit;
            }
        }
	}

	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}

	public function count($query_struct = array())
	{
		return $this->dao($this->_dao_object)->count($query_struct);
	}

	/**
	 * <code>
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code>
	 * @param array $data
	 */
	public function set($data = array())
	{
		//hidden merchant_id
		if ( ! isset($data['merchant_id']) OR ! isset($data['email']) OR ! isset($data['config']))
		{
			return FALSE;
		}

		if ( $this->isRegisterd($data['email']))
		{
			return FALSE;
		}

		if ( ! $channel_id = $this->dao($this->_dao_object)->add($data))
		{
			//TODO log or exception
			return FALSE;
		}


		//构造user
		$user_data = array();
		$user_data['name']  = $data['config'].'-channel-admin-'.$channel_id;
		$user_data['email'] = $data['email'];
		$user_data['merchant_id'] = $data['merchant_id'];
		$user_data['password'] 	  = '123456';

		//check registered,pass register method
		if ( ! $user_id = Service_User::instance()->isRegisterd($user_data['email']));
		{
			//添加User
			if ( ! $user_id = Service_User::instance()->register($user_data))
			{
				//TODO log or exception
				return FALSE;
			}
		}

		return Service_User::instance()->setUserRole($user_id,$this->channel_role_mapping[$data['config']],$channel_id,2);//2为商户管理员的SchemaID
	}

	public function isRegisterd($email)
	{
		$query_struct = array(
			'where'	=> array('email' => $email));

		if (Model_Channel::factory($model,id))
		{
			return $channel['id'];
		}
		return FALSE;
	}

	public function update($data)
	{
        if ( ! isset($data['id']))
        {
        	return FALSE;
        }

        return $this->dao($this->_dao_object)->edit($data);
	}

	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}

	public function docs($channel_id = NULL)
	{
		if ( ! $channel_id)
		{
			$channel_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}

		return $this->dao($this->_dao_object)->getChannelDoc($channel_id);
	}

	public function news($channel_id = NULL)
	{
		if ( ! $channel_id)
		{
			$channel_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}

		return $this->dao($this->_dao_object)->getChannelNew($channel_id);
	}

	public function catalogs($channel_id = NULL)
	{
		if ( ! $channel_id)
		{
			$channel_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}

		return $this->dao($this->_dao_object)->getChannelCatalog($channel_id);
	}


	/**
	 * 站点产品ID的读取
	 * @param int $channel_id
	 * @param array $query_struct
	 */
	public function products($query_struct = array())
	{
//		return $this->dao($this->_dao_object)->getChannelProduct($channel_id);

//		$query_struct = array(
//			'where' => array('channel_id' => $channel_id));

		return Service_Channel_Product::instance()->tree(
				$query_struct,
				array('id'),
				'id',
				'id');
	}

	/**
	 * 读取当前站点的SalesRep
	 */
	public function salesrep($channel_id = NULL ,$extend_struct = array())
	{
		if ( ! $channel_id)
		{
			$channel_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}

		return Service_User::instance()->tree($extend_struct);
	}

	/**
	 * 读取当前站点的管理员
	 */
	public function admin($channel_id = NULL)
	{
		if ( ! $channel_id)
		{
			$channel_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}

		return Service_User::instance()->tree($extend_struct);

	}

	public function route()
	{
		if ( ! isset($this->_data['id']) OR ! $this->_data['id'])
		{
			return FALSE;
		}

		$docs = $this->docs();

		foreach( $docs as $doc)
		{
			Route::set($doc->name, '<link>', array( 'link' => $doc->link))
				->defaults(array(
					'controller' => 'channel',
					'action' => 'doc'
				));
		}

		//TODO product ,catelog
        switch( $this->_data['route_type'] )
        {
        	//ID route
        	case self::ID_ROUTE:
            	Route::set('product', $this->_data['product'].'/<id>')
            		->defaults(array(
                		'controller' => 'channel',
                		'action' => 'product',
            	));

            	Route::set('catalog', $this->_data['catalog'].'/<id>')
            		->defaults(array(
               		 	'controller' => 'channel',
                		'action' => 'catalog',
           		));

            	break;
			//link route
        	case self::LINK_ROUTE:
           	 	Route::set('product', $this->_data['product'].'/<id>',
           	 				array( 'id' => '[\w-]+' ))
            		->defaults(array(
                		'controller' => 'channel',
                		'action' => 'product',
            	));

            	// catalog url: catalog_link
            	$catalogs = $this->catalogs();
            	foreach( $catalogs as $catalog )
            	{
                	Route::set('catalog/'.$catalog->id, '<id>', array( 'id' => $catalog->link ))
                    	->defaults(array(
                        	'controller' => 'channel',
                        	'action' => 'catalog'
                    	));
            	}
            	break;

        	default:
            	// product
            	Route::set('product', $this->_data['product'].'/<id>',
            	                array( 'id' => '\d+' ))
            		->defaults(array(
                		'controller' => 'channel',
                		'action' => 'product',
            	));
				// catalog
            	Route::set('catalog', $this->_data['catalog'].'/<id>',
            		            array( 'id' => '\d+' ))
            		->defaults(array(
               		 	'controller' => 'channel',
                		'action' => 'catalog',
           		));

	            break;
		}

		Route::set('api', 'api/<controller>/<action>(/<id>)')->defaults(array( 'directory' => 'api', ));

	}

	/**
	 * 仓库信息
	 * @return array
	 * @example
	 * array(
	 * subinventory => '',
	 * subinventory => '',)
	 */
	public function inventory()
	{
		$inventory_result = DB::select('subinventory_code')->from('orc_inventory_data')
				->where('subinventory_code','NOT IN',array('901RMA_CA','Staging','901REP_CA','901INTR_CA','911INTR_GA'))
				->group_by('subinventory_code')
				->execute();

		//处理return
		/*
		 * array(
		 * 'code'	=>	'');
		 */

		$inventory = array();

		foreach ($inventory_result as $code)
		{
			$subinventory_code = Kohana::config('cloud.inventory.'.$code['subinventory_code']);

			if ( ! is_null($subinventory_code))

			$inventory[ $code['subinventory_code'] ] = $subinventory_code;
		}

		return $inventory;
	}


	/**
	 * 加载站点的国家信息
	 */
	public function country()
	{
		$result = DB::select('name', 'isocode')
            ->from('channel_countries')
            ->where('channel_id', '=', $this->_data['id'])
            ->and_where('is_active', '=', 1)
            ->order_by('name','asc')
            ->execute()->as_array();

        return $result;
	}

    public function erpDomain()
    {
        return substr($this->get('domain'), 4);
    }

    public function erpEnabled()
    {
        return (bool)$this->get('erp_enabled');
    }


    public static function price($price_exchange, $format = 2)
    {
    	if (empty($price_exchange))
    		$price_exchange = 0.00;

        $price = number_format($price_exchange, $format, '.', '');

		return $price;
    }

    public function carriers($isocode = FALSE)
    {
        $result = DB::select()
            ->from('channel_carriers')
            ->where('channel_id', '=', $this->_data['id'])
            ->order_by('carrier')
            ->execute()->as_array();

        $carriers = array( );

        foreach( $result as $key => $value )
        {
            $carriers[$value['carrier']] = $value;
        }

        return $carriers;
    }

    public function carriersCodeToName()
    {
    	$result = DB::select()
            ->from('channel_carriers')
            ->where('channel_id', '=', $this->_data['id'])
            ->order_by('carrier')
            ->execute()->as_array();

        $carriers = array( );

        foreach( $result as $key => $value )
        {
            $carriers[$value['erp_code']] = $value['carrier_name'];
        }

        return $carriers;
    }
}
