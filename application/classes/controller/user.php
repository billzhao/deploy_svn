<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Base
{
    public function action_list() 
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        $this->template->content = View::factory('user_list')
            ->set('users', ORM::factory('user')->find_all()->as_array());
    }

    public function action_add()
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        if ($_POST) {
            if (isset($_POST['password']))
                $_POST['password'] = md5($_POST['password']);

            $user = ORM::factory('user');
            $user->values($_POST + array(
                'created' => time(), 
            ));

            if ($user->check()) {
                $user->save();
                $this->request->redirect('/user/list');
            }
        }

        $this->template->content = View::factory('user_add');
    }

    public function action_edit($id)
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        $user = ORM::factory('user', $id);
        if (!$user->loaded()) {
            die('User not found');
        }

        if (isset($_POST['submit'])) {
            foreach ($user->get_checked() as $pid) {
                $user->remove('projects', ORM::factory('project', $pid));
            }

            if (isset($_POST['projects'])) {
                foreach ($_POST['projects'] as $pid)
                    $user->add('projects', ORM::factory('project', $pid));
            }

            $this->request->redirect('/user/list');
        }

        $this->template->content = View::factory('user_edit')
            ->set('projects', ORM::factory('project')->find_all()->as_array())
            ->set('checked', $user->get_checked())
            ->set('user', $user);
    }

	
	public function action_delete($id)
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));
        $user = ORM::factory('user', $id);
        if (!$user->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }
    
        $user->delete_project_rel();
        $user->delete();

        $this->request->redirect('/user/list');
    }
	
    public function action_login()
    {
        if ($this->user->loaded())
            $this->request->redirect('/');

        if ($_POST) {
            // use ldap for authentication
            //$ldap = Kohana::config('ldap', NULL);
            //$ds = ldap_connect($ldap['host']);
            //if (!$ds) {
                //die('failed to connect to LDAP server');
            //}

            //ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, $ldap['version']);
            //$valid = @ldap_bind($ds, "uid={$_POST['username']},{$ldap['base']}", $_POST['password']);
            $valid = true;
            if ($valid) {
                $user = ORM::factory('user')
                    ->where('username', '=', $_POST['username'])
                    ->find();

                // create new user for valid ldap authentication
                if (!$user->loaded()) {
                    $user->username = $_POST['username'];
                    $user->password = 'not used';
                    $user->created = time();
                    $user->save();
                }
            } else {
                die('invalid username or password');
            }

            Session::instance()->set('uid', $user->id);
            $this->request->redirect('/');
        }

        $this->template->content = View::factory('user_login');
    }

    public function action_logout()
    {
        Session::instance()->delete('uid');
        $this->request->redirect('/user/login');
    }
}
