<?php
defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	"user_profile" => array(
		"shipping_company_name",
	    "shipping_address",
	    "shipping_phone",
	    "shipping_city",
	    "shipping_state",
	    "shipping_country",
	    "shipping_zip",
	    "shipping_contact_name",
		"billing_company_name",
	    "billing_address",
	    "billing_city",
	    "billing_state",
	    "billing_country",
	    "billing_zip",
	    "billing_phone",
	    "billing_contact_name",
	)
);