<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Service_Channel_Temppart
 *
 * @author Spike.Qin
 *
 * @version $Id:$ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Channel_Temppart extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel_temppart';
    
    private $_data = NULL;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
        $this->_load($id);
	}

	public function tree($query_struct=array(),$select = array(),$key='',$value='')
	{
		return $this->dao($this->_dao_object)
					->db_find_all(
						$this->dao($this->_dao_object)->dbselect($select),
						$query_struct,
						$key,
						$value
					);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find(array('where'=>array('id'=>$id))))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	public function add($data = array())
	{	
		return $this->dao($this->_dao_object)->add($data);
	}
	
	public function edit($data = array())
	{
		if(!$this->_data['id'])
		{
			Message::set('No part id');
			return false;
		}
		$data['id']=$this->_data['id'];
		if($this->dao($this->_dao_object)->edit($data))
		{
			Message::set('Update part success');
			return true;
		}
		else
		{
			Message::set('Update part fail');
			return false;
		}
	}
	
	public function delete($data = array())
	{
		if(!$this->_data['id'])
		{
			Message::set('No part id');
			return false;
		}
		if($this->dao($this->_dao_object)->delete($this->_data['id']))
			return true;
		else
			return false;
	}
	
}
