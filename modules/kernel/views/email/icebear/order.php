Dear <?php echo $salename;?><br/><br/>

Please Note that a new sales order has been created.<br/><br/>

<ul>
<li>Order Create Date: <?php echo date('m/d/Y',$orderdate);?></li> 
<li>Order Number: <?php echo $ordernum;?></li>
<li>Customer Name: <?php echo $customername;?></li>
</ul>
<br/>
<br/>
Please access system at <?php echo ! IN_PRODUCTION ? html::anchor('http://motob2badmin.cofreeonline.com/channel/order/edit/'.$orderid) : html::anchor('http://admin.icebearatv.com/channel/order/edit/'.$orderid);?> to view the sales order details and contact with customer as soon as possible.<br/><br/>

Kind Regards!<br/><br/>

Icebear team<br/><br/><br/>


Email: <a href="mailto:icebearatv@gmail.com">icebearatv@gmail.com<br/></a>
<a href="http://www.icebearatv.com/" target="_blank">http://www.icebearatv.com/</a>