<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * DAO Role
 * 
 * @author zhao.yang
 *
 * @package DAO
 *
 * @version $Id: role.php 6283 2012-02-16 09:27:48Z zhao.yang $
 *
 * @copyright Cofree Develop Term
 */
class DAO_role extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'role';
}