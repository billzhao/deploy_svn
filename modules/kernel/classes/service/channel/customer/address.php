<?php
defined('SYSPATH') or die('No direct script access.');

class Service_Channel_Customer_Address extends Service
{
	private $_data = NULL;

    private $_dao_object = 'channel_customer_address';
	
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    public static function resetInstance($id = 0)
    {
        if (isset(self::$instances[$id]))
        {
			unset(self::$instances[$id]);
        }
    }
        
    
	protected  function __construct($id = 0)
	{
		$this->_load($id);

	}

	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}

	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
   
	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
  
    public function update($data)
    {
     	 return $this->dao($this->_dao_object)->edit($data);
    }
       
	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}
	
	
    public function add($data)
    {
    	 return $this->dao($this->_dao_object)->add($data);
    }
    
    public function count($query_struct)
    {
        return $this->dao($this->_dao_object)->count($query_struct);
    }
    
    /**
     * 加载客户地址信息 
     */
    public function getCustomerAddress($type = 'bill',$addres_id)
    {
    	$query = array();
    	
    	switch($type)
    	{
    		case 'bill':
    			$query['where']['bill_id'] = $addres_id;
    			$query['limit'] = 1;
//    			return $this->dao($this->_dao_object)->find($query);
    			return $this->tree($query,array(
    			'billing_company_name',
    			'billing_contact_name',
    			'billing_address',
    			'billing_city',
    			'billing_state',
    			'billing_country',
    			'billing_zip',
    			'billing_phone'
    			));
    		case 'ship':
    		    $query['where']['ship_id'] = $addres_id;
    			$query['limit'] = 1;
//    			return $this->dao($this->_dao_object)->find($query);
    			return $this->tree($query,array(
    			'shipping_company_name',    			
    			'shipping_contact_name',
    			'shipping_address',
    			'shipping_city',
    			'shipping_state',
    			'shipping_country',
    			'shipping_zip',
    			'shipping_phone'
    			));
    		default:
//    			$query['where']['id'] = 0;
//    			return $this->tree($query);
				return array();
    	}
    	
    	
    	
    }
    

}
