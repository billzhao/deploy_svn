<?php
defined('SYSPATH') OR die('No direct access allowed.');

return array
(
	/*0:Unassociated;1:AwaitingValidation;2:AwaitingShipping;3:Cancelled;4:Completed'*/
	"status" => array(
	    0	=>	'Unassociated',
	    1	=>	'AwaitingValidation',
	    2	=>	'AwaitingShipping',
	    3	=>	'Cancelled',
	    4	=>	'Completed',
	)
);