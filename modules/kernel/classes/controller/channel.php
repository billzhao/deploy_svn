<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Controller_Channel
 *
 * @author bzhao
 *
 * @version $Id: channel.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Controller_Channel extends Controller_Webpage
{   
  	public function action_view()
    {     
        /*
         * 加载首页的虚拟目录
         */
    	$catalog = Service_Channel_Catalog::instance(1);
    	
    	$query_struct = array();
    	$query_struct['where'] = array('channel_id' => $this->channel_id);
        $query_struct['where']+= array('id IN' => $catalog->getChannelCatalogProductIds());
    	$query_struct['orderby'] = array('id','DESC');//TODO
    	$query_struct['limit'] = 20;//首页加载20
        $this->request->response = View::factory('index')
        	->set('index_ids',$catalog->products($query_struct))
        	->render();
    }

    public function action_404()
    {
    	$content['referer'] = (isset($_SERVER['HTTP_REFERER']) 
     	 							AND toolkit::is_our_url($_SERVER['HTTP_REFERER'])) ?
            							$_SERVER['HTTP_REFERER']:NULL;
      	$this->request->response = View::factory('404',$content)->render();
    }

    public function action_search()
    {
        if( !isset($_GET['q']) OR $_GET['q'] == '')
        {
            $this->request->redirect(URL::base());
        }

        //TODO 防注入
		$q = mysql_real_escape_string($_GET['q']);
		
		$channel = Service_Channel::instance();
      
		/*
		 * name,sku,keywords
		 */
        $query_struct['where'] = array('name like ' => "%{$q}%");
        $query_struct['where'] += array('or sku like ' => "%{$q}%");
        $query_struct['where'] += array('or keywords like ' => "%{$q}%");
		$query_struct['where'] += array('channel_id' => $this->channel_id);
		$query_struct['where'] += array('id notin' => Service_Channel_Catalog::instance(16)->getChannelCatalogProductIds());
        
        $count = count($channel->products($query_struct));
        
        $pagination = Pagination::factory(
                array(
                'current_page' => array( 'source' => 'query_string', 'key' => 'page' ),
                'total_items'  => $count,
                'items_per_page' => 20,//20 product
                'view' => 'pagination',
                )
        );
        

        //显示结果集
        $query_struct['orderby'] = array('created','DESC');
        $query_struct['limit']   = array(
        					'per_page' => $pagination->items_per_page,
        					'offset'   => $pagination->offset);

       $product_ids = $channel->products($query_struct,array('id'),'id','id');
        
       #echo Database::instance()->last_query;
        
       $this->request->response = $pagination->render();
        
        
       $this->request->response = View::factory('search')
       		->set('keywords',$q)
        	->set('product_ids', $product_ids)
        	->set('page_view', $pagination->render())
        	->render();

    }

    public function action_sitemap()
    {
        //TODO
        $this->request->response = '';
    }

    public function action_doc()
    {
        $link = $this->request->param('link');
        try
        {
            $template = View::factory('doc_'.$link)->render();
            $this->request->response = $template;
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            $this->request->redirect('404');
        }
    }

    public function action_product()
    {
        $id = $this->request->param('id');
        
        $route_type = Service_Channel::instance()->get('route_type');
                    
        switch ($route_type)
        {
        	case self::LINK_ROUTE:
        		
        		$query_struct['where'] = array('link' => $id);
        		$query_struct['where'] += array('channel_id' => $this->channel_id);
        		$query_struct['limit'] = 1;
        		
        		$result = Service_Channel_Product::instance()->tree($query_struct,array('id'));
        					
            	if (isset($result[0]['id']) && $result[0]['id'])
           		{
                	$product = Service_Channel_Product::instance($result[0]['id']);
            	}
            	else
            	{
                	$product = NULL;
            	}
           	 break;
        case self::ID_ROUTE:
			$product = Service_Channel_Product::instance($id);
			break;
        default:
            $product = Service_Channel_Product::instance($id);
            break;
        }
        

        if ( ! isset($product) OR ! $product->get('id'))
        {
            $this->request->redirect('404');
        }
        
        //echo $product->permalink();
        
		
        //echo Service_Channel_Catalog::instance($product->catalog())->permalink();
        
        //composedof
        
        switch ($product->get('type'))
        {
        	case self::PRODUCT:
     				$this->request->response = View::factory('product_view')
							->set('product',$product)
							->render();
        			break;
        		 
        	case self::PART:
        			//显示爆炸图
        			
        		  	//composedof
        			/*
        			 * moto
        			 * 车型Name
        			 * PART?
        			 */  	
//        			$array = array();
//        			$array['model'] = 1;
//        			$array['product'] = 1;
//        			$array['part']	= 12;
//        			$array['pp'] = array(1,2);
//					
//        			$data = array();
//        			$data['id'] = $product->get('id');
//        			$data['composeof'] = serialize($array);

        			
     				$this->request->response = View::factory('part_view')
							->set('product',$product)
							->render();
        			break;
        	default:
        		 $this->request->redirect('404');
        }
        
        
        
        //下午生成页面
        
        //配件的URL->Home > Products > UTV > UTV800-A - PART18 - abchor rope suspension hook
        //配件detal
        
        
        //根据product跳转执行的页面
        /*
         * View--.product_view
         * View--.product_part_view
         */
    }

    public function action_catalog()
    {
        $link = $this->request->param('id');
        
        $route_type = Service_Channel::instance()->get('route_type');
      
        $catalog = NULL;
        
        switch ($route_type)
        {
        	case self::LINK_ROUTE:
        		
        		$query_struct['where'] = array('link' => $link);
        		$query_struct['where'] += array('channel_id' => $this->channel_id);
        		$query_struct['limit'] = 1;

        		$result = Service_Channel_Catalog::instance()->tree($query_struct,array('id'));

            	if (isset($result[0]['id']) && $result[0]['id'])
           		{
                	$catalog = Service_Channel_Catalog::instance($result[0]['id']);
            	}
           	 break;
           	 
       	 	case self::ID_ROUTE:
				$catalog = Service_Channel_Catalog::instance($link);
				break;
			
        	default:
            	$catalog = Service_Channel_Catalog::instance($link);
            	break;
        }

        if ( ! isset($catalog) OR ! $catalog->get('id'))
        {
            $this->request->redirect('404');
        }

        unset($query_struct);
        
        
        //只可以搜索当前的目录ID集
        $query_struct['where'] = array('channel_id' => $this->channel_id);
        $query_struct['where']+= array('id IN' => $catalog->getChannelCatalogProductIds());
        
        //是否存在$_GET参数
//        if( isset($_GET['q']) && ! empty($_GET['q']))
//        {
//        	$q = mysql_real_escape_string($_GET['q']);
//       		$query_struct['where'] += array('name like ' => "%{$q}%");
//        }
        if ( isset($_GET['a1']) && ! empty($_GET['a1']))
        {
         	$a1 = mysql_real_escape_string($_GET['a1']);
       		$query_struct['where'] += array('sku like ' => "%{$a1}%");       	
        }
        if ( isset($_GET['key']) && ! empty($_GET['key']))
        {
         	$key = mysql_real_escape_string($_GET['key']);
       		$query_struct['where'] += array('name like ' => "{$key}%");       	
        }
        
        if ( isset($_GET['w']) && ! empty($_GET['w']))
        {
         	$w = mysql_real_escape_string($_GET['w']);
       		$query_struct['where'] += array('sku like ' => "%{$w}%");//item id
       		$query_struct['where'] += array('or name like ' => "%{$w}%");       	
        }	
        	
        $count = count($catalog->products($query_struct));
        
        $pagination = Pagination::factory(
                array(
                'current_page' => array( 'source' => 'query_string', 'key' => 'page' ),
                'total_items'  => $count,
                'items_per_page' => 20,//20 catalog
                'view' => 'pagination',
                )
        );
        
        //显示结果集
        $query_struct['orderby'] = array('created','DESC');
        $query_struct['limit']   = array(
        					'per_page' => $pagination->items_per_page,
        					'offset'   => $pagination->offset);
        
        //print_r($catalog->products($query_struct));
        
        //echo Database::instance()->last_query;
       
        if ( $catalog->get('id') == 16)
        {
        	$this->request->response =  View::factory('part_order') 
        		->set('page_view', $pagination->render())
        		->set('product_ids',$catalog->products($query_struct))
        		->set('catalog', $catalog)
        		->render();
        }
        else 
        {
		        
       	 	$this->request->response =  View::factory('catalog_view') 
        		->set('page_view', $pagination->render())
        		->set('product_ids',$catalog->products($query_struct))
        		->set('catalog', $catalog)
        		->render();
        }

    }

}
