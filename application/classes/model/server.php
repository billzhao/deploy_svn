<?php defined('SYSPATH') or die('No direct script access.');

class Model_Server extends ORM
{
    protected $_has_many = array(
        'projects' => array('through' => 'project_server'), 
    );

    protected $_filters = array(
        TRUE => array(
            'trim' => NULL, 
        ), 
    );

    protected $_rules = array(
        'name' => array(
            'not_empty' => NULL, 
        ), 

        'ip' => array(
            'not_empty' => NULL, 
        ), 
    );
    
   
    public function delete_project_rel()
    {
        return DB::delete('project_server')
            ->where('server_id', '=', $this->id)
            ->execute();
    }
}
