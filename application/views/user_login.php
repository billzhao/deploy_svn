<div id="user-login">
    <form id="frm-user-login" method="post" action="">
        <table class="frm-layout">
            <tr>
                <td><label for="username">Username: </label></td>
                <td>
                    <input type="text" id="username" name="username" />
                </td>
            </tr>
            <tr>
                <td><label for="password">Password: </label></td>
                <td>
                    <input type="password" id="password" name="password" />
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Login" /></td>
            </tr>
        </table>
    </form>
</div>
