<?php
defined('SYSPATH') or die('No direct script access.');
return array
	(
	// Credit Card
	'CC' => array
		(
		'name' => 'CreditPay',
		'driver' => 'creditcard',
	),
	//PP Jump
	'PP' => array(
		'name' => 'PPJump',
		'driver' => 'ppjump',
	),
	// PP Express
	'EC' => array(
		'name' => 'PPExpress',
		'driver' => 'ppec'
	),
	// PP Express
	'PPM' => array(
		'name' => 'PPMobile',
		'driver' => 'ppmobile'
	),
	//ThirdPartyPay
	'TPP' => array(
		'name' => 'ThirdPartyPay',
		'driver' => 'tpp'
	),
);
