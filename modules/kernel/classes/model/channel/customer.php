<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Model_Channel_Customer
 *
 * @author bzhao
 *
 * @version $Id: customer.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Channel_Customer extends ORM
{
    protected $_has_many = array(
        'orders'  => array('model' => 'order'),//customer_id
        'creditcards' => array('model' => 'credit')
    );
	 protected $_has_one = array(
		'channel' => array('model' => 'channel') //当前客户属于的channel
	);
	
	protected $_filters = array(
		TRUE => array( 'trim' => NULL )
	);
	protected $_rules = array(
		'channel_id' => array(
			'not_empty' => NULL,
		),
		'email' => array(
			'not_empty' => NULL,
		),
	    /*'firstname' => array(
			'not_empty' => NULL,
			'min_length' => array( 1 ),
			'max_length' => array( 255 ),
		),
		'lastname' => array(
			'not_empty' => NULL,
			'min_length' => array( 1 ),
			'max_length' => array( 255 ),
		),*/
		/*'password' => array(
			'not_empty' => NULL,
			'min_length' => array( 5 ),
			'max_length' => array( 255 ),
		),*/
	);

		
    
}
