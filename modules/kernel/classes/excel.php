<?php
class Excel
{

	
    /**
     * Header of excel document (prepended to the rows)
     * 
     * Copied from the excel xml-specs.
     * 
     * @access private
     * @var string
     */
    private $header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?\>
 <Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"
 xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
 xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"
 xmlns:html=\"http://www.w3.org/TR/REC-html40\">";

    /**
     * Footer of excel document (appended to the rows)
     * 
     * Copied from the excel xml-specs.
     * 
     * @access private
     * @var string
     */
	 private $style  = '
    <Styles>
    <Style ss:ID="Default" ss:Name="Normal">
    <Alignment ss:Vertical="Center"/>
    <Borders/>
    <Font ss:FontName="Times New Roman" x:CharSet="134" ss:Size="12"/>
    <Interior/>
    <NumberFormat/>
    <Protection/>
	</Style>
	<Style ss:ID="s21">
		<Font ss:FontName="Times New Roman" x:CharSet="134" ss:Size="18" ss:Bold="1"/>
	</Style>
	<Style ss:ID="s20">
	<Font ss:FontName="Times New Roman" x:CharSet="134" ss:Size="12" ss:Bold="1"/>
	<Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   	</Borders>
	</Style>
	<Style ss:ID="s29">
	<Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   	</Borders>
  	</Style>
  	<Style ss:ID="s35">
   	<Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   	<Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   	</Borders>
   	<NumberFormat ss:Format="@"/>
  	</Style>
 	</Styles>';
	
	
    private $footer = "</Workbook>";

    /**
     * Document lines (rows in an array)
     * 
     * @access private
     * @var array
     */
    private $lines = array ();

    /**
     * Worksheet title
     *
     * Contains the title of a single worksheet
     *
     * @access private 
     * @var string
     */
    public $worksheet_title = "";
	  
    public $excelhead = " ";
    
    function __construct($action = NULL,$extend = array())
    {
		$action = ($action === 'NULL') ? 'inventory_list' : $action;
		
		switch ($action) 
		{
			//配置header
			case 'incoming_inventory_list':
					$this->worksheet_title = 'Incoming Inventory List';
					$_Header = array('ETA Date','Container No.','Model','COLOR','Description','Warehouse','Total Qty','Hold Qty','Available Qty','PO Number','SKU','Ship Date','Action');
					break;
			case 'inventory_list':		
					$this->worksheet_title = 'Current Inventory List';
					$_Header = array('Body Type','Model','COLOR','SKU','Description','Warehouse','On-hand Qty','Available Qty','Action');
					break;
			case 'product':
					$this->worksheet_title = 'Product List';
					$_Header = array('Sku','Name','Model','Context','Description','Short Description');
					break;
			case 'sales_report':
					$this->worksheet_title = 'Sales Report';
					$_Header = array('','Ship Date','Sales Order#','Customer','CONSINEE','TOTAL','SALES AMOUNT','UNIT SOLD','GA/CA/DROP SHIP','SalesRep');	
					break;    
			case 'customer_list':
					$this->worksheet_title = 'Customer List';	
					$_Header = array('ID','Create','Customer','Email','SalesRep','Contact','Address','City','State','Zip Code','Phone No.','Federal Tax ID NO.','Seller’s Permit NO.','Dealership License NO.','Fax NO.','Shipping Company Name/Consignee Name & Contact','Shipping Contact Person','Shipping Address','Shipping City','Shipping Country','Shipping State','Shipping Zip','Shipping Phone NO.','Billing Company Name/Consignee Name & Contact','Billing Contact Person','Billing Address','Billing City','Billing Country','Billing State','Billing Zip','Billing Phone NO.');
					break;		
			case 'order_list':
					$this->worksheet_title = 'Order List';
					$_Header = array('ID','ERP#','Customer','Consignee','SalesRep','Created','Ship Date','Order Type','Amount','Warehouse','Trucking CO','LiftGate','Residential','Call Before Delivery','Shipping & Handling Fee','ASSEMBLY FEE','PDI FEE','License Fee','Credit Card Handling Fee','WIRE Fee','Labor Fee','Email','Order Payment','Customer PO','TOTAL QUOTE WEIGHT','TOTAL BOL WEIGHT','QUOTE','QUOTE NO','Discount','Shipping Address','Billing Address','Model','ITEM NO#','Qty','Price','Extended Value','Subinventory','QUOTA Weight','BOL Weight','Tracking Number');	
					break;
			case 'shipment_log':
					$this->worksheet_title = 'Shipment Log';
					$_Header = array('Ship Date', 'Dealer', 'Sales Order Number', 'Model and Color', 'Quantity', 'VIN NO.', 'Consignee', 'Consignee Co.', 'Consignee Address');	
					break;
			default:
					throw new Kohana_Exception(sprintf("Export Type {$action} Invalid"),$action);
					exit();
		}
		
		/*添加头部导航*/
		$this->excelhead = "<Row/>\n<Row>\n"; 
		
		foreach($_Header as $row)
		{
			$this->excelhead .= "<Cell ss:StyleID=\"s20\"><Data ss:Type=\"String\">".htmlspecialchars($row)."</Data></Cell>\n";
		}
		if (count($extend))
		{
			foreach($extend as $ext)
			{
				$this->excelhead .= "<Cell ss:StyleID=\"s20\"><Data ss:Type=\"String\">".htmlspecialchars($ext)."</Data></Cell>\n";
			}
		}
		
		$this->excelhead .= "</Row>\n";
    	
    }
    /**
     * Add a single row to the $document string
     * 
     * @access private
     * @param array 1-dimensional array
     * @todo Row-creation should be done by $this->addArray
     */
    private function addRow ($array)
    {

        // initialize all cells for this row
        $cells = "";

        // foreach key -> write value into cells
        foreach ($array as $k => $v)
        {
//           	if(is_numeric($v))
//			{
//				$cells .= "<Cell ss:StyleID=\"s29\"><Data ss:Type=\"String\">" . $v . "</Data></Cell>\n"; 
//			}
//			else
//			{
				/*处理 A 标签*/
				if (strpos($v, 'hold_detail') === FALSE)
					$cells .= "<Cell ss:StyleID=\"s29\" ><Data ss:Type=\"String\">" . htmlspecialchars($v). "</Data></Cell>\n";
				else 
					$cells .= "<Cell ss:StyleID=\"s29\" ss:HRef=\"http://".htmlspecialchars($v)."\" ><Data ss:Type=\"String\">View Hold Details</Data></Cell>\n";
//			}
        }
        // transform $cells content into one row
        $this->lines[] = "<Row>\n" . $cells . "</Row>\n";
    }

    /**
     * Add an array to the document
     * 
     * This should be the only method needed to generate an excel
     * document.
     * 
     * @access public
     * @param array 2-dimensional array
     * @todo Can be transfered to __construct() later on
     */
    public function addArray ($array)
    {
        // run through the array and add them into rows
        foreach ($array as $k => $v):
            $this->addRow ($v);
        endforeach;

    }

    /**
     * Set the worksheet title
     * 
     * Checks the string for not allowed characters (:\/?*),
     * cuts it to maximum 31 characters and set the title. Damn
     * why are not-allowed chars nowhere to be found? Windows
     * help's no help...
     *
     * @access public
     * @param string $title Designed title
     */
    public function setWorksheetTitle ($title)
    {

        // strip out special chars first
        $title = preg_replace ("/[\\\|:|\/|\?|\*|\[|\]]/", "", $title);

        // now cut it to the allowed length
        $title = substr ($title, 0, 31);

        // set title
        $this->worksheet_title = $title;

    }

    /**
     * Generate the excel file
     * 
     * Finally generates the excel file and uses the header() function
     * to deliver it to the browser.
     * 
     * @access public
     * @param string $filename Name of excel file to generate (...xls)
     */
    function generateXML($condition = NULL)
    {

        // deliver header (as recommended in php manual)
        header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
        //Enom sdsa Domain Record
        header("Content-Disposition: inline; filename=\"".date("Y-m-d") . $this->worksheet_title ."\"");

        // print out document to the browser
        // need to use stripslashes for the damn ">"
        echo stripslashes ($this->header);
		echo stripslashes($this->style);
		echo "\n<Worksheet ss:Name=\"" . $this->worksheet_title . "\">\n<Table ss:DefaultColumnWidth=\"90\" ss:DefaultRowHeight=\"17.25\">\n";
 	   
		if ($this->worksheet_title == 'Sales Report' OR $this->worksheet_title == 'Order List')
		{
					echo '
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
		';
		}
		else if ($this->worksheet_title == 'Shipment Log')
		{
					echo '
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="30"/>
		';
		}
		else if ($this->worksheet_title == 'Customer List')
		{
					echo '
			<Column ss:AutoFitWidth="0" ss:Width="30"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
		';
		}
		else
			echo '
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="90"/>
			<Column ss:AutoFitWidth="0" ss:Width="250"/>
		';
		
        echo  "<Row ss:Height=\"30.5\"><Cell/><Cell/><Cell ss:StyleID=\"s21\"><Data ss:Type=\"String\">".$this->worksheet_title . "</Data></Cell></Row>\n";

        /*过滤行*/
 		if ( ! is_null($condition))
			echo  "<Row ss:Height=\"40.5\"><Cell ss:StyleID=\"s29\"><Data ss:Type=\"String\">Filter: ".$condition." </Data></Cell></Row>\n";
         
		echo stripslashes($this->excelhead);
		
        echo implode ("\n", $this->lines);
        
        echo "</Table>\n</Worksheet>\n";
        
        echo $this->footer;
    }
    
    /*
     * Demo
     * $_testDownExcel = new Excel('statistic');
     * $_testDownExcel->worksheet_title("Statistic Report");
     * $_testDownExcel->addArray($array);
     * $_testDownExcel->generateXML("Statistic Report");
     */

}

