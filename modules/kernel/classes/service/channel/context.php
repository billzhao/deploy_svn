<?php
defined('SYSPATH') or die('No direct script access.');

class Service_Channel_Context extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'channel_context';
    
    private $_data = NULL;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
        $this->_load($id);
	}

	public function tree($query_struct=array(),$select = array(),$key='',$value='')
	{
		return $this->dao($this->_dao_object)
					->db_find_all(
						$this->dao($this->_dao_object)->dbselect($select),
						$query_struct,
						$key,
						$value
					);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	public function add($data = array())
	{
		//$attribute_keys = array_keys(Database::instance()->list_columns('products','attribute%'));
		if (!isset($data['attribute']))
		{
		$attributes = $this->tree(array('where'=>array('name'=>$data['name'],'channel_id'=>$data['channel_id'])),array('attribute'));
		$i = 1;
		while ($i <= 50)
		{
			$attribute='attribute'.str_pad($i,2,'0',STR_PAD_LEFT);
			if(!in_array(array('attribute'=>$attribute), $attributes)) break;
			$i++;
		}
		// attribute50 超出处理
		$data+=array('attribute'=>$attribute);
		}
		return $this->dao($this->_dao_object)->add($data);
	}
	
	public function getContextNameArray($channel_id=0)
	{
		if($channel_id)
			$where=array('where'=>array('channel_id'=>$channel_id));
		else 
			$where=array();
		$attributes=$this->tree($where);
		$context_array=array();
		foreach($attributes as $attribute)
		{	
			$context_array[$attribute['name']][]=$attribute;
		}
		return $context_array;
	}
	
	public function getConfigAttributesbyName($channel_id = 0, $context_name = '')
	{
		if (!$channel_id || !$context_name)
			return array();
		$where = array(
			'where'=>array(
				'channel_id'=>$channel_id,
				'name' => $context_name,
				'is_config' => 1));
		$attributes = $this->tree($where);
		$config_array = array();
		foreach($attributes as $attribute)
		{
			$config_array[]=$attribute;
		}
		return $config_array;
	}
	public function getConfigAttributes($channel_id = 0)
	{
		if (!$channel_id)
			return array();
		$where = array(
			'where'=>array(
				'channel_id'=>$channel_id,
				'is_config' => 1));
		$attributes = $this->tree($where);
		$config_array = array();
		foreach($attributes as $attribute)
		{
			$config_array[$attribute['name']][]=$attribute;
		}
		return $config_array;
	}

	public function getContextDisplayArray($channel_id=0)
	{
		if($channel_id)
			$where = array('where'=>array('channel_id'=>$channel_id));
		else 
			$where = array();
		$attributes = $this->tree($where);
		$context_array = array();
		foreach ($attributes as $attribute)
		{	
			$context_array[$attribute['name']][$attribute['attribute']] = $attribute['attribute_display'];
		}
		return $context_array;
	}
	public function update($data = array())
	{
        if (! isset($data['id']))
        {
            return FALSE;
        }
        
        return $this->dao($this->_dao_object)->edit($data);
	}

	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}
	
    public function erpDomain()
    {
        return substr($this->get('domain'), 4);
    }

    public function erpEnabled()
    {
        return (bool)$this->get('erp_enabled');
    }
	
}
