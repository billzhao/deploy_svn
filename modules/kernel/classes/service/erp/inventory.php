<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: Service_Erp_Inventory
 *
 * @author bzhao
 *
 * @version $Id: inventory.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Erp_Inventory extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'orc_inventory_data';
    private $_dao;
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	public function tree($query_struct = array() ,$select = array() , $key = NULL,$value = NULL)
	{
		return $this->_dao
			->db_find_all(
				$this->_dao->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
}
