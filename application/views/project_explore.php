<h3>选择文件部署到：<?php print $target['name']; ?></h3>
<script type="text/javascript">
function toggle_select(slt_all)
{
    var checked = slt_all.checked;
    var check_boxes = document.getElementsByName('files[]');
    for (var i=0; i<check_boxes.length; i++) {
        check_boxes[i].checked = checked;
    }

    return true;
}
</script>
<input type="checkbox" id="toggle_check" onchange="toggle_select(this);" />
<label for="toggle_check">All</label>
<form id="frm-deploy-file" method="post" action="/project/deploy/<?php print $id; ?>">
    <input type="hidden" name="base" value="<?php print $base; ?>" />
    <ul id="file-list">
	    <?php if (!empty($base)): ?>
	    <li style="list-style-type:none">
	        <?php echo html::anchor("/project/explore/$id?base=".substr($base, 0, strrpos($base, '/')), '返回上级目录'); ?>
	    </li>
	    <?php endif ?>
	    <?php if (!empty($entries)): ?>
	    <?php $i=0;?>
	    <?php foreach ($entries as $entry): ?>
	    <?php $i++;?>
	    <li style="list-style-type:none">
	    <div class="filter_condition_checkboxs">    
	        <input type="checkbox" name="files[]" value="<?php echo $entry['name']; ?>" />
	        <?php if ($entry['type'] == 'file'): ?>
	        <label><?php echo $entry['name']; ?></label>
	        <?php else: ?>
	        <?php echo html::anchor("/project/explore/$id?base=$base/{$entry['name']}", $entry['name']); ?>
	        <?php endif ?>
	        </div>
	    </li>
	    <?php endforeach ?>
	</ul>
	<input type="submit" value="部署" onclick="return window.confirm('部署选中的文件到<?php echo $target['name'], '(', long2ip($target['ip']) ,')'  ?>, 确定？')" ?>
<?php echo html::anchor('/project/list', 'Cancel'); ?>
</form>
<?php endif ?>
