<?php defined('SYSPATH') OR die('No direct access allowed.');
  /**
   * 
   * DAO channel contxt
   * 
   * @author spike.shin
   *
   * @package DAO
   *
   * @version 
   *
   * @copyright Cofree Develop Term
   */

class DAO_Channel_Context extends DAO 
{
   	/**
   	 * 表名
   	 * @var unknown_type
   	 */
   	 protected $table_name = 'channel_context';
   	 
}
