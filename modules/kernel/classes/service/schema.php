<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * @ClassName: Service_Schema
 *
 * @author bzhao
 *
 * @version $Id: schema.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Schema extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'schema';
    
    private $_data;
    
    protected static $instances = array();
    
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
    
	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_load($id);
	}

	public function tree($query_struct ,$index = 'id')
	{
		return $this->dao($this->_dao_object)->find_all($query_struct, $index);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache
		
		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	/**
	 * <code>        
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code> 
	 * @param array $data
	 */
	public function set($data = array())
	{
		return FALSE;
		/*
		 * 目前逻辑手动设置
		 */		
	}


	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
            return FALSE;
        }
        	
        return $this->dao($this->_dao_object)->edit($data);

	}

	public function delete($id)
	{
		return $this->dao($this->_dao_object)->delete($id);
	}

	/**
	 * 根据提供的attribute得到Schema的树桩结构图
	 * @param  array attributes
	 * @return View|boolen
	 */
	public static function getSchemaTree($attributes = NULL)
	{
		return FALSE;
		/*
		 * $attributes = array(
		 * 'parent_id' => 2,
		 * 'schema_level' => 1,
		 */
	}
	
	/**
	 * 根据SchemaCode生成头部导航 
	 * @param string $schemaCode
	 */
	public static function SchemeLink($schemaCode = NULL)
	{
		if ( ! $schemaCode)
		{
			return array();
		}
				
		if (Kohana::config('cloud.'.$schemaCode))
		{
			return Kohana::config('cloud.'.$schemaCode);
		}
		
		return array();
		
	}
      
}
