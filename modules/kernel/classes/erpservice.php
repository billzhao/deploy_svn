<?php defined('SYSPATH') or die('No direct script access.');
header("content-type:text/html;charset=utf-8");
/**
 * @version $Id: erpservice.php 7542 2012-09-11 04:05:27Z qin.yazhuo $ 
 * @copyright 2012 Cofree Development Term
 */
Class ErpService extends ServiceInterface
{
	const ORDER_CREATE = 0x01;
    const ORDER_UPDATE = 0x02;
    const ORDER_DELETE = 0x04;

    const ORDER_LINE_CREATE = 0x08;
    const ORDER_LINE_UPDATE = 0x10;
    const ORDER_LINE_DELETE = 0x20;

    const ORDER_GL = 0x40;
    const STATUS_NEW = 0x01;
    const STATUS_RUN = 0x02;
    const STATUS_DONE = 0x03;
    const STATUS_FAIL = 0x04;
    

	/**
	 * B2B Customer
	 * @var string
	 */
    const CREATE_B2B_CUSTOMER = 'createB2BCustomer';
    
    /**
     * B2B PriceList 
     * @var string
     */
    const CREATE_B2B_PRICELIST = 'createB2BPriceList';
    
    /**
     * B2B Salesrep
     * @var string
     */
    const CREATE_B2B_SALE = 'createB2BSale';
    const UPDATE_B2B_SALE = 'updateB2BSale';
    
    /**
     * B2B Process Order
     * @var string
     */
	const PROCESS_B2B_ORDER = 'processB2BOrder';
	
	/**
	 * Add Products
	 * @var string
	 */
	const ADD_PRODUCTS = 'addProducts';
	
	/**
	 * Update Products 
	 * @var string
	 */
	const UPDATE_PRODUCTS = 'updateProducts';
	
	/**
	 * Process Context
	 * @var string
	 */
	const PROCESS_CONTEXT = 'processContext';

	const B2B_BOOK = 'bookB2B';
	

	const RESERVE_BY_LOT = 'reserveByLot';
	
	
	const B2B_BALANCE = 'get_B2B_balance';
	

	const PAYMENT_RECEIVE = 'payment_recieve';
	const RMA_RECEIVE  ='rma_recieve';

	/*更新客户头*/
	const UPDATE_CUSTOMER_HEADER = 'updateCustomer';
	
    private $_domain;
    

    private $_carrier_item_id = 7640;
    private $drop_item_id = 44907;

 	private $re_stocking_fee = 52897;
 	private $re_damage_fee = 52898;
 	private $re_freight_fee = 52899;

 	private $assembly_fee = 62897;
	private $pdi_fee	  = 62899;
	private $license_fee = 62900;
	private $credit_handling_fee = 62901;
	private $wire_fee = 62902;
	private $labor_fee = 62903; 
	private $discount = 29951;

	/*21*/	
//	private $re_stocking_fee = 33904;
// 	private $re_damage_fee = 33906;
// 	private $re_freight_fee = 33905;
//
// 	private $assembly_fee = 33898;
//	private $pdi_fee	  = 33899;
//	private $license_fee = 33900;
//	private $credit_handling_fee = 33901;
//	private $wire_fee = 33902;
//	private $labor_fee = 33903; 

    public function __construct($islive = FALSE,$domain = 'icebearatv.com')//hack Moto
    {
    	parent::__construct($islive);
    	$this->params = array();
    	$this->response = array();
    	$this->resquest = array();
		$this->isError = FALSE;
		$this->isDebug = FALSE;
		$this->dieOnError = FALSE;
		if (IN_PRODUCTION OR UAT)
			$this->logOnError = TRUE;
		else 
			$this->logOnError = FALSE;
			
    	$this->_domain = $domain;

    	/*UAT环境*/
    	if (IN_PRODUCTION)
    	{
    		$this->re_stocking_fee = 44167;
			$this->re_damage_fee = 44168;
			$this->re_freight_fee = 44166;
			$this->assembly_fee = 44160;
			$this->pdi_fee	  = 44161;
			$this->license_fee = 44162;
			$this->credit_handling_fee = 44163;
			$this->wire_fee = 44164;
			$this->labor_fee = 44165;     
			$this->drop_item_id = 50977;		
    	}
    	else if(UAT)
    	{
    		$this->re_stocking_fee = 33904;
			$this->re_damage_fee = 33906;
			$this->re_freight_fee = 33905;
			$this->assembly_fee = 33898;
			$this->pdi_fee	  = 33899;
			$this->license_fee = 33900;
			$this->credit_handling_fee = 33901;
			$this->wire_fee = 33902;
			$this->labor_fee = 33903;
			$this->drop_item_id = 44907;
    	}
    }
    
    
    function rmaRecieve($data = array())
    {
   		if ( ! $data)
    	{
			return FALSE;
    	}

    	$rmas = array();
    	$rma = new com_cofreeonline_erp_services_RMAReceiveLine();
    	$rma->line_id = $data['line_id'];
    	$rma->lot_no  = $data['lot_no'];
    	$rma->serial_no = trim($data['serial_no']);
  		$rma->subinventory = $data['subinventory'];
    	$rmas[] = $rma;
    	
    	$this->setCommand(self::RMA_RECEIVE);
		$this->addParam('domain' , $this->_domain);
		$this->addParam('params', json_encode($rmas));
		$this->addParam('head_id', $data['head_id']);
		
		
		return $this->process(); 	
    }
    
    
    /**
     * 更新客户头地址
     */
    function updateCustomerHeader($erpCustomerId = NULL,$customerName = NULL,$customerClass = NULL, $permit_no = NULL, $tax_no = NULL, $dealer_license = NULL)
    {
    	if ( ! $erpCustomerId OR ! $customerName)
    	{
			return FALSE;
    	}
    		
    	$this->setCommand(self::UPDATE_CUSTOMER_HEADER);
		$this->addParam('domain' , $this->_domain);
		$this->addParam('id', $erpCustomerId);
		$this->addParam('name', $customerName);
		$this->addParam('class', $customerClass);
		$this->addParam('permit_no', $permit_no);
		$this->addParam('tax_no', $tax_no);
		$this->addParam('dealer_license', $dealer_license);
		
		return $this->process();
    	
    }    

    function getCustomerBalance($erpCustomerId = NULL)
    {
    	if ( ! $erpCustomerId)
    	{
			return FALSE;
    	}
    		
    	$this->setCommand(self::B2B_BALANCE);
		$this->addParam('domain' , $this->_domain);
		$this->addParam('cust_id', $erpCustomerId);
		
		return $this->process();
    	
    }

    function setPaymentRecieve($data = array())
    {
    	if ( ! $data)
    	{
			return FALSE;
    	}
    		
    	$payment = new com_cofreeonline_erp_services_B2BArRecieve();
    	
    	$payment->ccy = 'USD';
    	$payment->amount = $data['amount'];
    	$payment->comments = $data['comment'];
    	$payment->cust_bill_id = $data['bill_id'];
    	$payment->cust_id = $data['customer_id'];
    	$payment->order_no = $data['order_no'];
    	$payment->payment_method = $data['payment_code'];
    	$payment->receipt_date = date("Y-m-d H:i:s",time());
    	
    	$this->setCommand(self::PAYMENT_RECEIVE);
		$this->addParam('domain' , $this->_domain);
		$this->addParam('params', json_encode($payment));
		
		return $this->process();
    	
    }    

 	function getCustomerErpId($customerId = NULL , $priceListId = 0)
 	{  		
  	    if ( ! $customerId)
    	{
			return FALSE;
    	}
    	
		$this->setCommand(self::CREATE_B2B_CUSTOMER);
		$this->addParam('domain' , $this->_domain);	
    	
    	$customer = new com_cofreeonline_erp_services_Customer();
    	
		Service_Channel_Customer::resetInstance($customerId);
    	
    	$customer_profile = Service_Channel_Customer::instance($customerId)->get();
    	$customer->companyName = $customer_profile['name'];
	    $customer->companyAddress = $customer_profile['city']
	    							.' '.$customer_profile['state']
	    							.' '.$customer_profile['zip']
	    							.' '.$customer_profile['country'];
		$customer->contactName = $customer_profile['contact_name'];
		$customer->businessPhone = $customer_profile['phone'];
		$customer->tax_no =  $customer_profile['federal_tax_id'];
		$customer->permit_no =  $customer_profile['seller_permit'];
		$customer->dealer_license =  $customer_profile['dealership_license'];				
		$customer->fax = $customer_profile['fax'];

		$shipping_billing = unserialize($customer_profile['user_profile']);
		
		$customer->shippingAddress = $shipping_billing['shipping_address'];
		$customer->shippingPhone = mb_substr($shipping_billing['shipping_phone'],0,40);
		$customer->shippingCity = $shipping_billing['shipping_city'];
		$customer->shippingState = $shipping_billing['shipping_state'];
		$customer->shippingCountry = $shipping_billing['shipping_country'];
		$customer->shippingZipcode = $shipping_billing['shipping_zip'];

		$customer->shippingContact = $shipping_billing['shipping_company_name'].'%'.$shipping_billing['shipping_contact_name'];

		$customer->customerClass = $customer_profile['customer_class'];

		$customer->billingAddress =  $shipping_billing['billing_address'];
		$customer->billingPhone = mb_substr($shipping_billing['billing_phone'],0,40);
		$customer->billingCity = $shipping_billing['billing_city'];
		$customer->billingState = $shipping_billing['billing_state'];
		$customer->billingCountry = $shipping_billing['billing_country'];
		$customer->billingZipcode = $shipping_billing['billing_zip'];
		
		$customer->billingContact = $shipping_billing['billing_company_name'].'%'.$shipping_billing['billing_contact_name'];

		$customer->pricelistHeadId = $priceListId;
		$customer->currencyCode = 'USD';
		$customer->email = $customer_profile['email'];
		$customer->creditLimit = 1;
		
    	$this->addParam('params', json_encode($customer));
		
    	return $this->process();
 	}
 	
 	/**
 	 * 地址更新接口，地址信息从POST参数中读取 
 	 */
	function getCustomerErpIdAddr($customerId = NULL , $user_profile = array())
 	{  		
  	    if ( ! $customerId)
    	{
			return FALSE;
    	}
    	
		$this->setCommand(self::CREATE_B2B_CUSTOMER);
		$this->addParam('domain' , $this->_domain);	
    	
    	$customer = new com_cofreeonline_erp_services_Customer();
    	
		Service_Channel_Customer::resetInstance($customerId);
    	
    	$customer_profile = Service_Channel_Customer::instance($customerId)->get();
    	$customer->companyName = $customer_profile['name'];
	    $customer->companyAddress = $customer_profile['city']
	    							.' '.$customer_profile['state']
	    							.' '.$customer_profile['zip']
	    							.' '.$customer_profile['country'];
		$customer->contactName = $customer_profile['contact_name'];
		$customer->businessPhone = $customer_profile['phone'];
		$customer->tax_no =  $customer_profile['federal_tax_id'];
		$customer->permit_no =  $customer_profile['seller_permit'];
		$customer->dealer_license =  $customer_profile['dealership_license'];				
		$customer->fax = $customer_profile['fax'];

		$shipping_billing = $user_profile;
		
		$customer->shippingAddress = $shipping_billing['shipping_address'];
		$customer->shippingPhone = mb_substr($shipping_billing['shipping_phone'],0,40);
		$customer->shippingCity = $shipping_billing['shipping_city'];
		$customer->shippingState = $shipping_billing['shipping_state'];
		$customer->shippingCountry = $shipping_billing['shipping_country'];
		$customer->shippingZipcode = $shipping_billing['shipping_zip'];
		
		$customer->shippingContact = $shipping_billing['shipping_company_name'].'%'.$shipping_billing['shipping_contact_name'];
		
		$customer->billingAddress =  $shipping_billing['billing_address'];
		$customer->billingPhone = mb_substr($shipping_billing['billing_phone'],0,40);
		$customer->billingCity = $shipping_billing['billing_city'];
		$customer->billingState = $shipping_billing['billing_state'];
		$customer->billingCountry = $shipping_billing['billing_country'];
		$customer->billingZipcode = $shipping_billing['billing_zip'];
		
		$customer->billingContact = $shipping_billing['billing_company_name'].'%'.$shipping_billing['billing_contact_name'];
		

		$customer->pricelistHeadId = 0;
		$customer->currencyCode = 'USD';
		$customer->email = $customer_profile['email'];
		$customer->creditLimit = 1;
		
    	$this->addParam('params', json_encode($customer));
		
    	return $this->process();
 	} 	
 	
 	
 	public function getSaleErpId($saleId)
 	{
 		if ( ! $saleId)
    	{
			return FALSE;
    	}
		
		$this->setCommand(self::CREATE_B2B_SALE);
		$this->addParam('domain' , $this->_domain);
		
    	$sale_profile = Service_User::instance($saleId)->get();
		
    	$this->addParam('name' , $sale_profile['name']);
    	
    	if ($sale_profile['extend_sale_type'] == 'Parts_Manager'
    		OR $sale_profile['extend_sale_type'] == 'Sale_Director')
    	{
    		$sale_profile['extend_sale_type'] = 'Sale_Manager';
    	}
    	
		$this->addParam('role' , $sale_profile['extend_sale_type']);
		$this->addParam('uid' ,  $sale_profile['id']);
		
    	return $this->process();
 	}
 	
 	
 	public function updateSaleErpId($saleId)
 	{
 		if ( ! $saleId)
    	{
			return FALSE;
    	}
		
		$this->setCommand(self::UPDATE_B2B_SALE);
		$this->addParam('domain' , $this->_domain);
		
		Service_User::resetInstance($saleId);
		
    	$sale_profile = Service_User::instance($saleId)->get();
		
    	$this->addParam('name' , $sale_profile['name']);
		$this->addParam('saleId' , $sale_profile['erp_sale_id']);
		
    	return $this->process();
 	} 	
 	
 	public function getCustomerPriceList($customerName = NULL , $products = array())
 	{
 		if ( ! $customerName)
 		{
 			return FALSE;
 		}
 		
 		if ( ! $products)
 		{
 			return FALSE;
 		}
 		
 		$defaultProductItemPrice = array();
 		
 		$this->setCommand(self::CREATE_B2B_PRICELIST);
    	$this->addParam('domain' , $this->_domain);
    	$this->addParam('customer_name', trim($customerName));
 		
 		$item_price = array();
 		$item_key 	= 0;
 		
		$item_price[$item_key] = new com_cofreeonline_erp_services_ItemPrice();
		$item_price[$item_key]->product_attribute = 'PRICING_ATTRIBUTE1';
		$item_price[$item_key]->item_no		 = 'item_no';
		$item_price[$item_key]->item_unit	 = 'unit';
		$item_price[$item_key]->price		 = 'price';
 	}


 	public function processErpOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;
		
		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);

 		$params = self::_geneOrderHeader($order->get());
        $params->operation = 'CREATE';
        $params->details = array();
        
        //67336 ERP_CUSTOMER_ID
        //100010075 ERP_SALES_ID
        
        $products = $order->products();
        
        foreach ($products as $product)
        {
            $line = self::_geneOrderItem($product);
            $line['operation'] = 'CREATE';
            $params->details[] = $line;
        }
        
        $carrier = array();
 		if ($order->get('order_type') == 4)
		{			
	    	$carrier['inventoryItemId'] = $this->drop_item_id;		
		}        
		else
	    	$carrier['inventoryItemId'] = $this->_carrier_item_id;
	    	
	    $carrier['orderedQuantity'] = 1;
	    $carrier['calculatePriceFlag'] = "N";
	    if($order->get('amount_shipping')==null){
	    	$carrier['unitListPrice'] = 100;
	    	$carrier['unitSellingPrice'] = 100;
	    }else{
	    	$carrier['unitListPrice'] = $order->get('amount_shipping');
	    	$carrier['unitSellingPrice'] = $order->get('amount_shipping');
	    }
	    
	    $carrier['operation'] = "CREATE";
	    
        $params->details[] = $carrier;
        
		
		if(abs($order->get('discount')))
		{
			$discount = array();
			
			$discount['inventoryItemId'] = $this->discount;
			$discount['orderedQuantity'] = 1;
	        $discount['calculatePriceFlag'] = "N";
	        $discount['unitListPrice'] = 0-abs($order->get('discount'));
	        $discount['unitSellingPrice'] = 0-abs($order->get('discount'));
	        $discount['operation'] = "CREATE";
			
			 $params->details[] = $discount;
        }	         
        
	    foreach ($this->_geneOrderFee($order) as $fee)
	    {
	    	/*0的费用行不处理*/
	    	if ( ! abs(floor($fee['unitSellingPrice'])))
	    		continue;
	    		
	    	$line = $fee;
	    	$line['operation'] = 'CREATE';
	    	$params->details[] = $line;
	    }        
        
        $this->addParam('params', json_encode($params));
		
    	$orderOut = $this->process();
		
		if ($orderOut['retCode'] == 'S')
        {
            $updated['erp_head_id'] = $orderOut['data']['headerId'];
            $updated['erp_ordernum'] = $orderOut['data']['orderNumber'];
            Service_Channel_Order::instance()->update($updated + array('id' => $order->get('id')));
            //update orderitems list price(vsp list price), unit price
           	 
            foreach($orderOut['data']['details'] as $item)
            {
            	switch($item['inventoryItemId'])
            	{
            		case $this->_carrier_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = $item['unitSellingPrice'];
                    	$update_data['erp_shipping_line_id'] = $item['lineId'];
                    	$update_data['erp_shipping_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->drop_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = $item['unitSellingPrice'];
                    	$update_data['erp_shipping_line_id'] = $item['lineId'];
                    	$update_data['erp_shipping_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;                 	
            		case $this->assembly_fee:
             			$update_data = array();
                    	$update_data['assembly_fee'] = $item['unitSellingPrice'];
                    	$update_data['assembly_fee_line_id'] = $item['lineId'];
                    	$update_data['assembly_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->pdi_fee:
             			$update_data = array();
                    	$update_data['pdi_fee'] = $item['unitSellingPrice'];
                    	$update_data['pdi_fee_line_id'] = $item['lineId'];
                    	$update_data['pdi_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->license_fee:
             			$update_data = array();
                    	$update_data['license_fee'] = $item['unitSellingPrice'];
                    	$update_data['license_fee_line_id'] = $item['lineId'];
                    	$update_data['license_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->credit_handling_fee:
             			$update_data = array();
                    	$update_data['credit_handling_fee'] = $item['unitSellingPrice'];
                    	$update_data['credit_handling_fee_line_id'] = $item['lineId'];
                    	$update_data['credit_handling_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->wire_fee:
             			$update_data = array();
                    	$update_data['wire_fee'] = $item['unitSellingPrice'];
                    	$update_data['wire_fee_line_id'] = $item['lineId'];
                    	$update_data['wire_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->labor_fee:
             			$update_data = array();
                    	$update_data['labor_fee'] = $item['unitSellingPrice'];
                    	$update_data['labor_fee_line_id'] = $item['lineId'];
                    	$update_data['labor_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;  
            		case $this->discount:
             			$update_data = array();
                    	$update_data['discount'] = $item['unitSellingPrice'];
                    	$update_data['discount_line_id'] = $item['lineId'];
                    	$update_data['discount_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;              			                  	                    	                    	                    	                    	           			
            		default:
            			$update_data = array();
                    	$filter_data = array();
                    	$update_data['erp_line_id'] = $item['lineId'];//
                    	$update_data['erp_line_number'] = $item['lineNumber'];
                    	$update_data['price'] = $item['unitSellingPrice'];
                    	$update_data['cost'] = $item['unitCost'];
                    	$filter_data['order_id'] = $order->get('id');
                    	$filter_data['item_id'] = $item['inventoryItemId'];
                    	$filter_data['quantity'] = $item['orderedQuantity'];  
                    	$filter_data['id'] = $item['originalLineId'];
                    	Service_Channel_Order::instance()->update_orderitem($update_data, $filter_data);
    					break;
            	}
            }
            return TRUE;
        }
   		return FALSE;
 	} 	

 	public function processErpRefundOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;
		
		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);

 		$params = self::_geneOrderHeader($order->get());
        $params->operation = 'CREATE';
        $params->details = array();
        
        $products = $order->products();
        
        foreach ($products as $product)
        {
            $line = self::_geneOrderItem($product);
            $line['operation'] = 'CREATE';
            $params->details[] = $line;
        }
        
        
//        $carrier = array();
//	    $carrier['inventoryItemId'] = $this->_carrier_item_id;
//	    $carrier['orderedQuantity'] = 1;
//	    $carrier['calculatePriceFlag'] = "N";
//	    if($order->get('amount_shipping')==null){
//	    	$carrier['unitListPrice'] = 100;
//	    	$carrier['unitSellingPrice'] = 100;
//	    }else{
//	    	$carrier['unitListPrice'] = $order->get('amount_shipping');
//	    	$carrier['unitSellingPrice'] = $order->get('amount_shipping');
//	    }
//	    
//	    $carrier['operation'] = "CREATE";
//        $params->details[] = $carrier;
	    
	    
	    foreach ($this->_geneRmaOrderFee($order) as $fee)
	    {
	    	$line = $fee;
	    	$line['operation'] = 'CREATE';
	    	$params->details[] = $line;
	    }    
	            
        $this->addParam('params', json_encode($params));
		
    	$orderOut = $this->process();
		
		if ($orderOut['retCode'] == 'S')
        {
            $updated['erp_head_id'] = $orderOut['data']['headerId'];
            $updated['erp_ordernum'] = $orderOut['data']['orderNumber'];
            Service_Channel_Order::instance()->update($updated + array('id' => $order->get('id')));
           	 
            foreach($orderOut['data']['details'] as $item)
            {
            	switch($item['inventoryItemId'])
            	{
            		case $this->_carrier_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = $item['unitSellingPrice'];
                    	$update_data['erp_shipping_line_id'] = $item['lineId'];
                    	$update_data['erp_shipping_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->re_damage_fee:
            			$update_data = array();
                    	$update_data['re_damage_fee_fee'] = $item['unitSellingPrice'];
                    	$update_data['re_damage_fee_line_id'] = $item['lineId'];
                    	$update_data['re_damage_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->re_stocking_fee:
            			$update_data = array();
                    	$update_data['re_stocking_fee'] = $item['unitSellingPrice'];
                    	$update_data['re_stocking_fee_line_id'] = $item['lineId'];
                    	$update_data['re_stocking_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->re_freight_fee:
            			$update_data = array();
                    	$update_data['re_freight_fee'] = $item['unitSellingPrice'];
                    	$update_data['re_freight_fee_line_id'] = $item['lineId'];
                    	$update_data['re_freight_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;                    	                    	
            		default:                    	
            			$update_data = array();
                		$filter_data = array();
                		$update_data['erp_line_id'] = $item['lineId'];//
                		$update_data['erp_line_number'] = $item['lineNumber'];
                		$filter_data['order_id'] = $order->get('id');
                		$filter_data['id'] = $item['originalLineId'];
                		Service_Channel_Order::instance()->update_orderitem($update_data, $filter_data);
//                		KOhana_Log::instance()->add('info', Database::instance()->last_query);
                		break;
            	}
            }
            return TRUE;
        }
   		return FALSE;
 	}
 	
 	public function cancelErpOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;
		
		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
			
        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);

		$params->operation = 'UPDATE';
        $params->headerId  = $order->get('erp_head_id');
        $params->cancelledFlag = 'Y';
        $params->details = array();
        
        $this->addParam('params', json_encode($params));
		
   		$orderOut = $this->process();
		
		if ($orderOut['retCode'] == 'S')
        {           	 
            foreach($orderOut['data']['details'] as $item)
            {
            	switch($item['inventoryItemId'])
            	{
            		case $this->_carrier_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->drop_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;                    	
            		case $this->assembly_fee:
             			$update_data = array();
                    	$update_data['assembly_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->pdi_fee:
             			$update_data = array();
                    	$update_data['pdi_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->license_fee:
             			$update_data = array();
                    	$update_data['license_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->credit_handling_fee:
             			$update_data = array();
                    	$update_data['credit_handling_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->wire_fee:
             			$update_data = array();
                    	$update_data['wire_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->labor_fee:
             			$update_data = array();
                    	$update_data['labor_fee'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;  
            		case $this->discount:
             			$update_data = array();
                    	$update_data['discount'] = 0;
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;              			                  	                    	                    	                    	                    	           			
            		default:
            			/*配件的Cancel,也按简单产品处理*/
            			$update_data = array();
                    	$filter_data = array();
                    	$update_data['quantity'] = 0.00;
                    	$filter_data['order_id'] = $order->get('id');
                    	$filter_data['item_id'] = $item['inventoryItemId'];
                    	$filter_data['id'] = $item['originalLineId'];
                    	Service_Channel_Order::instance()->update_orderitem($update_data, $filter_data);
    					break;
            	}
            }
            return TRUE;
        }
   		return FALSE;
 	}

 	public function updateErpRmaOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 					
 		$params = self::_geneOrderHeader($order->get());
        $params->operation = 'UPDATE';
        $params->headerId  = $order->get('erp_head_id');
        $params->details = array();

// 	    $products = $order->products();
//      
//        foreach ($products as $product)
//        {
//            $line = self::_geneOrderItem($product);
//            $line['operation'] = 'UPDATE';
//            $line['lineId'] = $product['erp_line_id'];
//            $params->details[] = $line;
//        }
        
	    foreach (self::_geneRmaOrderFee($order,TRUE) as $fee)
	    {
	    	/*退货的费用行，unitSellingPrice逻辑可以适用*/
	    	if (ceil($fee['unitSellingPrice']) == 0)
	    	{
	    		continue;
	    	}
	    	$fee['operation'] = 'UPDATE';
	    	$params->details[] = $fee;
	    }
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	} 	

 	
	public function updateErpOrderShipping($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 					
        $params = self::_geneOrderHeader(($order->get()));
//		$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
 		
        $params->operation = 'UPDATE';
        $params->headerId  = $order->get('erp_head_id');
        $params->details = array();

        $carrier = array();
        
 		if ($order->get('order_type') == 4)
		{			
	    	$carrier['inventoryItemId'] = $this->drop_item_id;		
		}        
		else
	    	$carrier['inventoryItemId'] = $this->_carrier_item_id;
	    	
	    $carrier['orderedQuantity'] = 1;
	    $carrier['calculatePriceFlag'] = "N";
	    $carrier['unitListPrice'] = $order->get('amount_shipping');
	    $carrier['unitSellingPrice'] = $order->get('amount_shipping');
	    $carrier['operation'] = "UPDATE";
	    $carrier['lineId'] = $order->get('erp_shipping_line_id');
        $carrier['lineNumber'] = $order->get('erp_shipping_line_number');
	    $params->details[] = $carrier;
	    
	    
	    /*更新已经存在的费用行*/
	    foreach (self::_geneOrderFee($order,TRUE) as $fee)
	    {	    
	    	/*存在LineId update*/		
	    	if (isset($fee['lineId']) && $fee['lineId'])
	    		$fee['operation'] = 'UPDATE';
	    	/*添加逻辑 价格不为0 的行可以添加*/
			else if (abs(floor($fee['unitSellingPrice'])))
			{	
				if (isset($fee['lineId'])) unset($fee['lineId']); 
				if (isset($fee['lineNumber'])) unset($fee['lineNumber']); 
				
				$fee['operation'] = 'CREATE';
			}
			else
				continue;
					    	
	    	$params->details[] = $fee;
	    }
	    
        $this->addParam('params', json_encode($params));
		
 		$orderOut = $this->process();
		
		if ($orderOut['retCode'] == 'S')
        {           	 
            foreach($orderOut['data']['details'] as $item)
            {
            	switch($item['inventoryItemId'])
            	{
           			case $this->_carrier_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = $item['unitSellingPrice'];
                    	$update_data['erp_shipping_line_id'] = $item['lineId'];
                    	$update_data['erp_shipping_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
           			case $this->drop_item_id:
            			$update_data = array();
                    	$update_data['amount_shipping'] = $item['unitSellingPrice'];
                    	$update_data['erp_shipping_line_id'] = $item['lineId'];
                    	$update_data['erp_shipping_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;                    	
            		case $this->assembly_fee:
             			$update_data = array();
                    	$update_data['assembly_fee'] = $item['unitSellingPrice'];
                    	$update_data['assembly_fee_line_id'] = $item['lineId'];
                    	$update_data['assembly_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
//                    	Kohana_Log::instance()->add("info",Database::instance()->last_query);
                    	break;
            		case $this->pdi_fee:
             			$update_data = array();
                    	$update_data['pdi_fee'] = $item['unitSellingPrice'];
                    	$update_data['pdi_fee_line_id'] = $item['lineId'];
                    	$update_data['pdi_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->license_fee:
             			$update_data = array();
                    	$update_data['license_fee'] = $item['unitSellingPrice'];
                    	$update_data['license_fee_line_id'] = $item['lineId'];
                    	$update_data['license_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->credit_handling_fee:
             			$update_data = array();
                    	$update_data['credit_handling_fee'] = $item['unitSellingPrice'];
                    	$update_data['credit_handling_fee_line_id'] = $item['lineId'];
                    	$update_data['credit_handling_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->wire_fee:
             			$update_data = array();
                    	$update_data['wire_fee'] = $item['unitSellingPrice'];
                    	$update_data['wire_fee_line_id'] = $item['lineId'];
                    	$update_data['wire_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;
            		case $this->labor_fee:
             			$update_data = array();
                    	$update_data['labor_fee'] = $item['unitSellingPrice'];
                    	$update_data['labor_fee_line_id'] = $item['lineId'];
                    	$update_data['labor_fee_line_number'] = $item['lineNumber'];
                    	Service_Channel_Order::instance()->update($update_data + array('id' => $order->get('id')));
                    	break;  
            		default:
            			break;          			                  	                    	                    	                    	                    	           			
            	}
            }
            return TRUE;
        }
   		return FALSE;
 	} 	

 	public function updateErpOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 					
        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
 		
        $params->operation = 'UPDATE';
        $params->headerId  = $order->get('erp_head_id');
        $params->details = array();

 	    $products = $order->products();
      
        foreach ($products as $product)
        {
            $line = self::_geneOrderItem($product);
            $line['operation'] = 'UPDATE';
            $line['calculatePriceFlag'] = "N";
            $line['lineId'] = $product['erp_line_id'];
            $params->details[] = $line;
        }
        
        $carrier = array();
 		if ($order->get('order_type') == 4)
		{			
	    	$carrier['inventoryItemId'] = $this->drop_item_id;		
		}        
		else
	    	$carrier['inventoryItemId'] = $this->_carrier_item_id;
	    	
	    $carrier['orderedQuantity'] = 1;
	    $carrier['calculatePriceFlag'] = "N";
	    $carrier['unitListPrice'] = $order->get('amount_shipping');
	    $carrier['unitSellingPrice'] = $order->get('amount_shipping');
	    $carrier['operation'] = "UPDATE";
	    $carrier['lineId'] = $order->get('erp_shipping_line_id');
        $carrier['lineNumber'] = $order->get('erp_shipping_line_number');
	    $params->details[] = $carrier;
	    
	   	/*订单更新时候,存在的费用行都需要进行处理,暂时逻辑不应用*/
	    foreach (self::_geneOrderFee($order,TRUE) as $fee)
	    {	
	    	if (ceil($fee['unitSellingPrice']) == 0)
	    	{
	    		continue;
	    	}
	    	
	    	$fee['operation'] = 'UPDATE';
	    	$params->details[] = $fee;
	    }
	    
	    
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	}
 	
 	
	public function updateErpOrderHeadre($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 					
 		$params = self::_geneOrderHeader($order->get());
        $params->operation = 'UPDATE';
        $params->headerId  = $order->get('erp_head_id');
        $params->details = array();

        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	} 	

	/**
 	 * Create Order Item
 	 * @param array $item
 	 */
 	public function processErpOrderItem($item)
 	{
 		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 		
        $order = Service_Channel_Order::instance($item['order_id']);
		
        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
        
        $params->operation = 'UPDATE';
        $params->headerId = $order->get('erp_head_id');

        $line = self::_geneOrderItem($item);
        $line['operation'] = 'CREATE';
        
        $params->details[] = $line; 		
        
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	}
 	 	

 	public function updateErpOrderItem($item)
 	{
 		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 		
        $order = Service_Channel_Order::instance($item['order_id']);

        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
        
        $params->operation = 'UPDATE';
        $params->headerId = $order->get('erp_head_id');

        $line = self::_geneOrderItem($item);
        $line['calculatePriceFlag'] = "N";
        $line['operation'] = 'UPDATE';
        $line['lineId'] = $item['erp_line_id'];
        
        $params->details = array($line); 		
        
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	}
 	
 	public function updateErpOrderDiscount($discount)
 	{
 		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 		
        $order = Service_Channel_Order::instance($discount['order_id']);

        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
        
        
        
        $params->operation = 'UPDATE';
        $params->headerId = $order->get('erp_head_id');

        
        
		/*折扣行*/        
        if($order->get('discount_line_id')
        	&& $order->get('discount_line_number'))
		{
        	/*更新*/
        	$item = array();
	    	$item['inventoryItemId'] = $this->discount;
	    	$item['orderedQuantity'] = 1;
	    	$item['calculatePriceFlag'] = "N";
	    	$item['unitListPrice'] = 0-abs($order->get('discount'));
	    	$item['unitSellingPrice'] = 0-abs($order->get('discount'));
	    	$item['operation'] = "UPDATE";
	    	$item['lineId'] = $order->get('discount_line_id');
        	$item['lineNumber'] = $order->get('discount_line_number');
        	$params->details[] = $item;
        }	
        /*折扣新建逻辑*/
        else
        {
        	$discount = array();
			$discount['inventoryItemId'] = $this->discount;
			$discount['orderedQuantity'] = 1;
	        $discount['calculatePriceFlag'] = "N";
	        $discount['unitListPrice'] = 0-abs($order->get('discount'));
	        $discount['unitSellingPrice'] = 0-abs($order->get('discount'));
	        $discount['operation'] = "CREATE";
			$params->details[] = $discount;
        }
        
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	} 	
 	
 	public function deleteErpOrderItem($item)
 	{
		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 		
        $order = Service_Channel_Order::instance($item['order_id']);

        $params = self::_geneOrderHeader(($order->get()));
        
        $params->operation = 'UPDATE';
        $params->headerId = $order->get('erp_head_id');

        $line = self::_geneOrderItem($item);
        $line['operation'] = 'DELETE';
        $line['lineId'] = $item['erp_line_id'];
        
        $params->details = array($line); 		
        
        $this->addParam('params', json_encode($params));
		
    	return $this->process();
 	}
 	
 	public function cancelErpOrderItem($item)
 	{
		$this->setCommand(self::PROCESS_B2B_ORDER);
 		$this->addParam('domain' , $this->_domain);
 		
        $order = Service_Channel_Order::instance($item['order_id']);

        if ($order->get('order_type') == 4)
      		$params = self::_geneOrderHeader(($order->get()));
      	else
			$params = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
        
        $params->operation = 'UPDATE';
        $params->headerId = $order->get('erp_head_id');

        $line = self::_geneOrderItem($item);
        $line['operation'] = 'UPDATE';
        $line['lineId'] = $item['erp_line_id'];
        $line['cancelledFlag'] = 'Y';
        $line['cancelledQuantity'] = $item['quantity'];
        
        $params->details = array($line); 		
        
        $this->addParam('params', json_encode($params));
		
    	return $this->process(); 		
 	} 	
 	
 	/**
 	 * B2B order Book
 	 * @param array $order
 	 */
 	public function bookErpOrder($order = null)
 	{
 		if ( ! $order->get('id'))
			return FALSE;
		
		$this->setCommand(self::B2B_BOOK);
 		$this->addParam('domain' , $this->_domain);
 		
 		if ( ! $order->get('erp_head_id'))
 		{
			$this->addError('ERP HEAD_ID NULL');
			return $this->returnError();
 		}
 		
 		$this->addParam('headId', $order->get('erp_head_id'));
 		
 		return $this->process();
 	}
 	
 	public function reserveByLot($item)
 	{
 		$this->setCommand(self::RESERVE_BY_LOT);
 		$this->addParam('domain' , $this->_domain);
        $order = Service_Channel_Order::instance($item['order_id']);
 		$this->addParam('headId',  $order->get('erp_head_id'));
 		$this->addParam('lineId',  $item['erp_line_id']);
 		if (isset($item['lot_no']))
 		{
 			$this->addParam('lotNo', $item['lot_no']);
 		}
 		
		return $this->process();
 	}

	/**
	 * processContext(processContext)
	 * @param array | int productItem
	 */
	public function syncContext($contextName, $merchantid, $erpId = null)
	{
		$this->setCommand(self::PROCESS_CONTEXT);

		$contextItemsObject = self::_geneContextItem($contextName, $merchantid);

		$this->addParam('name', $contextName);
		if ($erpId) $this->addParam('id', $erpId);
		$this->addParam('params', json_encode($contextItemsObject));

		return $this->process();
		
	}

	
	protected static function _geneContextItem($contextName, $merchantid)
	{
		$contexts = Service_Merchant_Context::instance()->tree(array(
				'where' => array(
					'name' => $contextName,
					'merchant_id' => $merchantid)));
		$contextItems = array();
		foreach ($contexts as $context)
		{
			$contextItem = new com_cofreeonline_erp_services_ContextItem();
			$contextItem->name = $context['attribute_label'];
			$contextItem->seq = substr($context['attribute'], 9);
			$contextItem->desc = $context['attribute_display'];
			$contextItems[] = $contextItem;
		}

		return $contextItems;
	}
	

 	public function syncProduct($productItems, $orgId = null, $inventory_planer = null, $serial_control = null, $lot_control = null, $inventory_plan_method = null, $default_buyer = null, $inventory_min = null, $inventory_max = null, $locator_ctrl = null, $template = '7087')
 	{
		$this->setCommand(self::ADD_PRODUCTS);
		
//		$syncProductItems = array(23,21);
		foreach ($productItems as $productItem) 
		{
		$productItemsObject[] = self::_geneProductItem($productItem, $orgId, $inventory_planer, $serial_control, $lot_control, $inventory_plan_method, $default_buyer, $inventory_min, $inventory_max, $locator_ctrl);
		}
		$this->addParam('template', $template);
  		$this->addParam('params' , json_encode($productItemsObject));
  		    	
  		return $this->process();

 	}

 	
 	protected static function _geneProductItem($productId, $orgId = null, $inventory_planer = null, $serial_control = null, $lot_control = null, $inventory_plan_method = null, $default_buyer = null, $inventory_min = null, $inventory_max = null, $locator_ctrl = null)
 	{
 		$productItem = new com_cofreeonline_erp_services_ProductItem();

 		/*clean up*/
 		Service_Merchant_Product::resetInstance($productId);
 		
 		$productLine = Service_Merchant_Product::instance($productId)->get();
 		//$productItem->name = $productLine['short_description'];
 		//$productItem->name = $productLine['name'];
 		$productItem->name = $productLine['erp_description_en'];
 		$productItem->unit = $productLine['unit'];
 		$productItem->secondUnit = $productLine['second_unit'];
 		$productItem->convertRate = $productLine['convert_rate'];
 		
 		if ( ! empty($productLine['erp_id']))
 		{
 			$productItem->itemId = $productLine['erp_id'];
 		}
 		
 		if (!is_null($orgId))
 		{
 			$productItem->orgId = $orgId;
 		}
 		else
 		{
 			$productItem->orgId = Service_Merchant::instance($productLine['merchant_id'])->get('erp_org_id');
 		}
 		if (!is_null($inventory_planer))
 		{
 			$productItem->lotControl = $lot_control ? $lot_control : $productLine['lot_ctrl'];
 			$productItem->serialControl = $serial_control ? $serial_control:$productLine['serial_ctrl'];
 			$productItem->locatorControl = $locator_ctrl ? $locator_ctrl:$productLine['locator_ctrl'];

 			$productItem->buyerId = $default_buyer ? $default_buyer:$productLine['default_buyer'];
 			$productItem->plannerCode = $inventory_planer ? $inventory_planer:$productLine['inventory_planner'];
 		}
 	
 		$productItem->planMethod = $inventory_plan_method ? $inventory_plan_method:$productLine['inventory_plan_method'];

 		//only when planMethod is min-max, set maxQty
 		if ($productItem->planMethod == 2)
 		{
			$productItem->minQty = $inventory_min ? $inventory_min:$productLine['inventory_min'];
 			$productItem->maxQty = $inventory_max ? $inventory_max:$productLine['inventory_max'];
 		}
 		
 		$productItem->sku  = $productLine['sku'];
 		$productItem->itemCode  = $productLine['erp_no'];
 		//$productItem->comments =  $productLine['description'].';icebear';
 		$productItem->comments = $productLine['erp_description_zh'];
 		$productItem->model = $productLine['model'];
 		$productItem->weight = $productLine['weight'];
 		$productItem->weightUnit = $productLine['weight_unit'];

 		$category_config = Service_Merchant::instance($productLine['merchant_id'])->get('category_config');
 		$category_segments = explode('.', $category_config);
 		$productItem->category1 = (strpos($category_segments[0], 'category')===false)? $category_segments[0]:$productLine[$category_segments[0]];
 		$productItem->category2 = (strpos($category_segments[1], 'category')===false)? $category_segments[1]:$productLine[$category_segments[1]];
 		$productItem->category3 = (strpos($category_segments[2], 'category')===false)? $category_segments[2]:$productLine[$category_segments[2]];
 		$productItem->category4 = (strpos($category_segments[3], 'category')===false)? $category_segments[3]:$productLine[$category_segments[3]];
// 		$productItem->category2 = ! intval($productLine['category02']) ? '' :str_pad((int)$productLine['category02'],3,'0',STR_PAD_LEFT);
// 		$productItem->category3 = ! intval($productLine['category03']) ? '' :str_pad((int)$productLine['category03'],3,'0',STR_PAD_LEFT);
// 		$productItem->category4 = ! intval($productLine['category04']) ? '' :str_pad((int)$productLine['category04'],3,'0',STR_PAD_LEFT);
 		$productItem->priceCategory1 = ! intval($productLine['price_category01']) ? '' :str_pad((int)$productLine['price_category01'],3,'0',STR_PAD_LEFT);
 		$productItem->priceCategory2 = ! intval($productLine['price_category02']) ? '' :$productLine['price_category02'];
 		$productItem->priceCategory3 = ! intval($productLine['price_category03']) ? '' :$productLine['price_category03'];
 		$productItem->priceCategory4 = ! intval($productLine['price_category04']) ? '' :$productLine['price_category04'];
 		$productItem->context = $productLine['context_name'];

 		$attrKey = 1;
 		$hack = FALSE;
 		
 		while(TRUE)
 		{
 			$attrKeyValue = 'attribute'.str_pad($attrKey,2,'0',STR_PAD_LEFT);
 			$attrItemKeyValue = 'attr'.$attrKey;
 			
 			if ( ! isset($productLine[$attrKeyValue]))
 				break;
 			
 			if (($attrKey > 30) && in_array($productItem->context,array('MOTO-ELECTRIC SCOOTER','MOTO-ELECTRIC TRIKE')))
			{
				$attrItemKeyValue = 'attr'.($attrKey-1);
			} 				
 				
 			if (strlen($productLine[$attrKeyValue]) > 30)
 				$productItem->$attrItemKeyValue = substr($productLine[$attrKeyValue], 0, 30);
 			else
 				$productItem->$attrItemKeyValue = $productLine[$attrKeyValue];
 			

			
 			$attrKey++;
 			
 		}
 		
 		return $productItem;
 	}
 	
  	protected  function _geneRmaOrderFee($order = null,$update = FALSE)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$orderFee = array();
		
		/*Re Damage Fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->re_damage_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('re_damage_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('re_damage_fee');
	    	$fee['unitSellingPrice'] = $order->get('re_damage_fee');
	    }
	    
	    if ($update !== FALSE && ceil($fee['unitSellingPrice']) != 0)
	    {
	    	$fee['lineId'] = $order->get('re_damage_fee_line_id');
        	$fee['lineNumber'] = $order->get('re_damage_fee_line_number');	
	    }
	    $orderFee[] = $fee;
	    
 		/*re_stocking_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->re_stocking_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('re_stocking_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('re_stocking_fee');
	    	$fee['unitSellingPrice'] = $order->get('re_stocking_fee');
	    }
	    
	    if ($update !== FALSE && ceil($fee['unitSellingPrice']) != 0)
	    {
	    	$fee['lineId'] = $order->get('re_stocking_fee_line_id');
        	$fee['lineNumber'] = $order->get('re_stocking_fee_line_number');	
	    }
	    $orderFee[] = $fee;

 		/*re_freight_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->re_freight_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('re_freight_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('re_freight_fee');
	    	$fee['unitSellingPrice'] = $order->get('re_freight_fee');
	    }
	    
	    if ($update !== FALSE && ceil($fee['unitSellingPrice']) != 0)
	    {
	    	$fee['lineId'] = $order->get('re_freight_fee_line_id');
        	$fee['lineNumber'] = $order->get('re_freight_fee_line_number');	
	    }	    
		$orderFee[] = $fee;
		 
	    return $orderFee;
 	}
 	
 	/**
 	 * 处理订单运费行时，0的费用行 也可以传入(编辑状态下) 
 	 * @param unknown_type $order
 	 * @param unknown_type $update
 	 */
 	protected  function _geneOrderFee($order = null,$update = FALSE)
 	{
 		if ( ! $order->get('id'))
			return FALSE;

		$orderFee = array();
		
		/*AssemBly*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->assembly_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('assembly_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('assembly_fee');
	    	$fee['unitSellingPrice'] = $order->get('assembly_fee');
	    }
	    
	    if ($update !== FALSE)
	    {
	    	$fee['lineId'] = $order->get('assembly_fee_line_id');
        	$fee['lineNumber'] = $order->get('assembly_fee_line_number');	
	    }
	    
	    $orderFee[] = $fee;
	    
		/*pdi_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->pdi_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('pdi_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('pdi_fee');
	    	$fee['unitSellingPrice'] = $order->get('pdi_fee');
	    }
 		if ($update !== FALSE)
	    {
	    	$fee['lineId'] = $order->get('pdi_fee_line_id');
        	$fee['lineNumber'] = $order->get('pdi_fee_line_number');	
	    }	    
	    $orderFee[] = $fee;	  
			
		/*license_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->license_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('license_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('license_fee');
	    	$fee['unitSellingPrice'] = $order->get('license_fee');
	    }
 		if ($update !== FALSE)
	    {
	    	$fee['lineId'] = $order->get('license_fee_line_id');
        	$fee['lineNumber'] = $order->get('license_fee_line_number');	
	    }	    	    
	    $orderFee[] = $fee;	  

		/*credit_handling_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->credit_handling_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('credit_handling_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('credit_handling_fee');
	    	$fee['unitSellingPrice'] = $order->get('credit_handling_fee');
	    }
 		if ($update !== FALSE)
	    {
	    	$fee['lineId'] = $order->get('credit_handling_fee_line_id');
        	$fee['lineNumber'] = $order->get('credit_handling_fee_line_number');	
	    }		    
	    $orderFee[] = $fee;	  

		/*wire_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->wire_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('wire_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('wire_fee');
	    	$fee['unitSellingPrice'] = $order->get('wire_fee');
	    }
 		if ($update !== FALSE)
	    {
	    	$fee['lineId'] = $order->get('wire_fee_line_id');
        	$fee['lineNumber'] = $order->get('wire_fee_line_number');	
	    }		    
	    $orderFee[] = $fee;	  

		/*labor_fee*/
		$fee = array(); 
 		$fee['inventoryItemId'] = $this->labor_fee;
	    $fee['orderedQuantity'] = 1;
	    $fee['calculatePriceFlag'] = "N";
	    if($order->get('labor_fee')==null)
	    {
	    	$fee['unitListPrice'] = (float)0.00;
	    	$fee['unitSellingPrice'] = (float)0.00;
	    }
	    else
	    {
	    	$fee['unitListPrice'] = $order->get('labor_fee');
	    	$fee['unitSellingPrice'] = $order->get('labor_fee');
	    }
 		if ($update !== FALSE )
	    {
	    	$fee['lineId'] = $order->get('labor_fee_line_id');
        	$fee['lineNumber'] = $order->get('labor_fee_line_number');	
	    }		    
	    $orderFee[] = $fee;	  	    
		
		return $orderFee;
 	}
 	
 	protected static function _geneOrderHeader($order_data = array())
 	{
 		$old_timezone = date_default_timezone_get();
        date_default_timezone_set('Asia/Shanghai');
        
        $order = new com_cofreeonline_erp_services_OrderMain();//print_r($order_data);
        $order->orderedDate = date('Y-m-d H:i:s',time());
        $order->customerId = $order_data['customer_erp_id'];//
        $order->shippingMethodCode = $order_data['shipping_method'];
        $order->shipId = $order_data['erp_shipto_address_id'];
        $order->billId = $order_data['erp_billto_address_id'];
        $order->saleId = $order_data['salesman_id'];
        /*$order->subInventory = $order_data['subinventory_code'];*/ 
        //priceListId 
        //41772  CA
        //41773	 GA
		if (preg_match('/_CA$/i',$order_data['subinventory_code']))
		{      
        	$order->invOrgId = 158;//
			$order->priceListId = 36772;//41772
			if ( ! UAT)
				$order->priceListId = 41772;
			if (IN_PRODUCTION) 
			{ 
				$order->priceListId = 52874;
				$order->invOrgId = 198;
			 }
		}
		else if(preg_match('/_GA$/i',$order_data['subinventory_code']))
		{
			$order->invOrgId = 159;//
			$order->priceListId = 36773;// 41773
			if ( ! UAT)
				$order->priceListId = 41773;	
			if (IN_PRODUCTION) 
			{ 
				$order->priceListId = 52875;
				$order->invOrgId = 199; 
			}						
		}
		else
			throw new ServiceException(array('errorCode' => '0003','errorDescription' => '搴撳瓨缁勭粐鏃犳晥'));
			
		if($order_data['order_type'] == 4)
		{			
			if (UAT) { $order->orderTypeID = 1070;}	
			
			if (IN_PRODUCTION) { $order->orderTypeID = 1206;}			
		}
			
		$order->paymentTermId = 5;
        $order->packingInstructions = str_replace(array("\r\n","\n","\r"),"",$order_data['comment']);
        /*customer po*/
        $order->customerPo = $order_data['customer_po'];
        
        date_default_timezone_set($old_timezone);
      
        return $order;

 	}

 	protected static function _geneOrderItem($item_data)
 	{
        $line = array(
            'inventoryItemId' => $item_data['item_id'],
            'orderedQuantity' => $item_data['quantity'], 
            'unitSellingPrice' => $item_data['price'], 
            'originalLineId' => $item_data['id'], 
        	'attr1'	=> $item_data['lot_no'],
        	'subInventory' => $item_data['subinventory']
        );  
        
        if ( isset($item_data['memo']) && ! empty($item_data['memo']))
        {
        	$line['packingInstruction'] = $item_data['memo'];
        	$line['unitListPrice'] = $item_data['price'];
	    	$line['calculatePriceFlag'] = "N";        	
        	unset($line['attr1']);
        	unset($line['subInventory']);
        }
        
        if ( isset($item_data['referenceLineId'])&& ! empty($item_data['referenceLineId']))
        {
        	$line['referenceLineId'] = $item_data['referenceLineId'];
        }
 	    if ( isset($item_data['referenceHeadId'])&& ! empty($item_data['referenceHeadId']))
        {
        	$line['referenceHeadId'] = $item_data['referenceHeadId'];
        }       

        return $line;
 	}


 	public function updateProduct($productItem)
 	{
		$this->setCommand(self::UPDATE_PRODUCTS);
		
//		$syncProductItems = array(23,21);
//		
//		foreach ($syncProductItems as $productItem) 
//		{
		$productItemsObject[] = self::_geneProductItem($productItem);
//		}
		
  		$this->addParam('params' , json_encode($productItemsObject));
  		    	
  		return $this->process();
  		
 	}
	
	/**
	 * @return string $_domain
	 */
	public final function getDomain() 
	{
		return $this->_domain;
	}

	/**
	 * @param string $_domain
	 */
	public final function setDomain($_domain) 
	{
		$this->_domain = $_domain;
	}
	
	protected function throwAPIException($result)
	{
		$e = new ServiceException($result);
		throw $e;
	}
}


/**
 * Erp Customer Prototype
 * @ClassName: com_cofreeonline_erp_services_Customer
 *
 * @author bzhao
 *
 * @version $Id: erpservice.php 7542 2012-09-11 04:05:27Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class com_cofreeonline_erp_services_Customer
{
	public  $companyName;
	public  $companyAddress;
	public  $contactName;
	public  $businessPhone;
	public  $tax_no;
    public  $permit_no;
    public  $dealer_license;	
	public  $fax;
	public  $shippingAddress;
	public  $shippingPhone;
	public  $shippingCity;
	public  $shippingState;
	public  $shippingCountry;
	public  $shippingZipcode;
	public  $shippingContact;
	public  $customerClass;
	public  $billingAddress;
	public  $billingPhone;
	public  $billingCity;
	public  $billingState;
	public  $billingCountry;
	public  $billingZipcode;
	public  $billingContact;
	public  $email;
	public  $pricelistHeadId;
	public  $currencyCode;
	public  $creditLimit;
	
    public function __construct()
    {
    
    }
}


/**
 * @ClassName: com_cofreeonline_erp_services_ItemPrice
 *
 * @author bzhao
 *
 * @version $Id: erpservice.php 7542 2012-09-11 04:05:27Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class com_cofreeonline_erp_services_ItemPrice
{
	public $product_attribute;
	public $item_no;
	public $item_unit;
	public $price;

    public function __construct()
    {
    	
    }
}
/**
 * ERP ContextItem鍚屾瀵硅薄
 * @ClassName: com_cofreeonline_erp_services_ProductItem
 * @copyright 2011 Cofree Development Term
 */
class com_cofreeonline_erp_services_ContextItem { 
	public $name; 
	public $seq; 
	public $desc;
}

/**
 * CustomerAR
 * @copyright 2012 Cofree Development Term
 */
class com_cofreeonline_erp_services_B2BArRecieve
{ 
	public $order_no; 
	public $payment_method; 
	public $cust_id; 
	public $cust_bill_id; 
	public $ccy; 
	public $amount; 
	public $receipt_date; 
	public $reference; 
	public $comments;
}

class com_cofreeonline_erp_services_RMAReceiveLine 
{ 
	public $line_id; 
	public $lot_no; 
	public $serial_no;
	public $subinventory;
}
/**
 * ERP ProductItem鍚屾瀵硅薄
 * @ClassName: com_cofreeonline_erp_services_ProductItem
 * @copyright 2011 Cofree Development Term
 */
class com_cofreeonline_erp_services_ProductItem 
{
	public $itemId;//鏇存柊鏃跺繀濉紝鏂板鏄笉濉�
	public $name;//蹇呭～
    public $comments;//涓嶅繀濉�
    public $unit;//鍗曚綅锛屽繀濉�
    public $secondUnit;
    public $weight;
    public $weightUnit;
    public $lotControl;
    public $serialControl;
    public $locatorControl;
    public $itemCode;//蹇呭～
    public $buyerId;
    public $plannerCode;
    public $minQty;
    public $maxQty;
    public $planMethod;
    public $sku;//蹇呭～
    public $model;
    public $category1;//蹇呭～
    public $category2;//蹇呭～
    public $category3;//蹇呭～
    public $category4;//蹇呭～
    public $priceCategory1;//蹇呭～
    public $priceCategory2;//蹇呭～
    public $priceCategory3;//蹇呭～
    public $priceCategory4;//蹇呭～
    public $context;//涓婁笅鏂囷紝蹇呭～
    public $dim;//闀垮楂�
    public $dimUnit;
    public $orgId;
    public $convertRate;
    public $attr1;
    public $attr2;
    public $attr3;
    public $attr4;
    public $attr5;
    public $attr6;
    public $attr7;
    public $attr8;
    public $attr9;
    public $attr10;
    public $attr11;
    public $attr12;
    public $attr13;
    public $attr14;
    public $attr15;
    public $attr16;
    public $attr17;
    public $attr18;
    public $attr19;
    public $attr20;
    public $attr21;
    public $attr22;
    public $attr23;
    public $attr24;
    public $attr25;
    public $attr26;
    public $attr27;
    public $attr28;
    public $attr29;
    public $attr30;
    public $attr31;
    public $attr32;
    public $attr33;
    public $attr34;
    public $attr35;
    public $attr36;
    public $attr37;
    public $attr38;
    public $attr39;
    public $attr40;
    public $attr41;
    public $attr42;
    public $attr43;
    public $attr44;
    public $attr45;
    public $attr46;
    public $attr47;
    public $attr48;
    public $attr49;
    public $attr50;
    public $attr51;
    public $attr52;
    public $attr53;
    public $attr54;
    public $attr55;
    public $attr56;
    public $attr57;
    public $attr58;
    public $attr59;
    public $attr60;
    public $attr61;
    public $attr62;
    public $attr63;
    public $attr64;
    public $attr65;
    public $attr66;
    public $attr67;
    public $attr68;
    public $attr69;
    public $attr70;
    
    
    public function __construct()
    {
    	
    }
    
}

class com_cofreeonline_erp_services_OrderLine
{
    public $inventoryItemId;
    public $orderedQuantity;
    public $lineId;
    public $lineNumber;
    public $unitListPrice;
    public $unitSellingPrice;
    public $calculatePriceFlag;
    public $changeReason;
    public $operation;
    public $attr1;//鐥呬汉
	public $attr2;
	public $attr3;
	public $attr4;
	public $attr5;
	public $attr6;
	public $attr7;
	public $attr8;
	public $attr9;
	public $attr10;
	public $attr11;
	public $attr12;
	public $attr13;
	public $attr14;
	public $attr15;
	public $attr16;
	public $attr17;
	public $attr18;
	public $attr19;
	public $attr20;
	public $context;//CCVG Prescription
	public $shippingInstruction;
	public $packingInstruction;
	public $lotno;/*鎵瑰彿*/
	public $subInventory;/*瀛楀簱*/
	public $referenceHeadId;/*refund head ID*/
	public $referenceLineId;/*refund line ID*/	

	public function __construct()
	{
	}
}

class com_cofreeonline_erp_services_OrderMain
{
	public $orderTypeID; /*订单ID*/
	public $customerId;	//瀹㈡埛ID
	public $orderNumber;
	public $orderedDate;//璁㈠崟鏃堕棿蹇呭～
	public $shipId;//Shipping
	public $billId;//Billing
	public $headerId;//杈撳嚭鍐呭,鏇存柊鏃跺�蹇呴』杈撳叆
	public $operation;
	public $shippingInstructions;
	public $packingInstructions;
	public $paymentTermId;
	public $shippingMethodCode;
	public $saleId;
	public $cancelledFlag;	
	public $priceListId;
	public $details;
	public $invOrgId;
	public $attr1;
	public $attr2;
	public $attr3;
	public $attr4;
	public $attr5;
	public $attr6;
	public $attr7;
	public $attr8;
	public $attr9;
	public $attr10;
	public $customerPo;
	public $priceChangeReason;//杈撳嚭鍐呭锛屼笉闇�璁剧疆
    
    public function __construct()
    {
    }
}


/**
 * 瀹㈡埛浣欓 
 */
class com_cofreeonline_erp_services_CustomerBalance
{
	public $cust_id;
	public $credit_memo;
	public $open_balance;
	public $available_balance;


//    public function __construct($orderitem, $changeReason, $operation, $calculatePriceFlage = 'Y')
//    {
//        $this->inventoryItemId = Product::instance($orderitem['item_id'])->get('erp_item_id');
//        $this->orderedQuantity = $orderitem['quantity'];
//        $this->lineId = '';
//        $this->lineNumber = '';
//        $this->unitListPrice = '';
//        $this->UnitSellingPrice = '';
//        $this->calculatePriceFlag = $calculatePriceFlage;
//        $this->changeReason = $changeReason;
//        $this->operation = $operation;
//    }
    public function __construct()
    {
    }
}
