<?php //defined('SYSPATH') or die('No direct script access.');
class Glasseslens
{
    public $lens;
    public $lens_index;
    public $color_tint;
    public $ptp;
    public $ptv;
    public $anti_r;
    public $sph;
    public $cyl;
    public $axis;
    public $add;
    public $pd;
    public $mono;
    public $near_pd;

    private $glasses;
    private $prescription;
    private $prescription_fields = array(
        'od_sph' => 'float',//od: right eye
        'od_cyl' => 'float',
        'od_axis' => 'int',
        'od_add' => 'float',
        'os_sph' => 'float',//os: left eye
        'os_cyl' => 'float',
        'os_axis' => 'int',
        'os_add' => 'float',
        'pd' => 'float',
        'mono_r' => 'float',
        'mono_l' => 'float',
        'near_pd' => 'float',
        'up_prescription' => 'string',
    );
    private $glasses_fields = array(
        'lens' => 'int',
        'lens_index' => 'int',
        'lens_index_color' => 'int',
        'color_tint' => 'int',
        'color_tint_color' => 'int',
        'ptp' => 'int',
        'ptp_color' => 'int',
        'ptv' => 'int',
        'ptv_color' => 'int',
        'anti_r' => 'int',
    );

    private function __construct()
    {
        $this->lens = Kohana::config('glasseslens.lens'); // glasses arr_lenses
        $this->lens_index = Kohana::config('glasseslens.lens_index'); // glasses arr_lens_index
        $this->color_tint = Kohana::config('glasseslens.color_tint'); // glasses arr_color_tint
        $this->ptp = Kohana::config('glasseslens.ptp'); // glasses arr_ptp
        $this->ptv = Kohana::config('glasseslens.ptv'); // glasses arr_ptv
        $this->anti = Kohana::config('glasseslens.anti'); // glasses arr_anti_r

        $this->sph = Kohana::config('glasseslens.sph'); // prescription arr_sph
        $this->cyl = Kohana::config('glasseslens.cyl'); // prescription arr_cyl
        $this->axis = Kohana::config('glasseslens.axis'); // prescription arr_axis
        $this->add = Kohana::config('glasseslens.add'); // prescription arr_add
        $this->pd = Kohana::config('glasseslens.pd'); // prescription arr_pd
        $this->mono = Kohana::config('glasseslens.mono'); // prescription arr_mono
        $this->near_pd = Kohana::config('glasseslens.near_pd'); // prescription arr_near_pd
    }

    public static $instance;

    public static function instance()
    {
        if (self::$instance == NULL)
        {
            $class = __CLASS__;
            self::$instance = new $class();
        }
        return self::$instance;
    }

    /**
     * Get price of lens
     * @param $glass (name of lenses, e.g. lens_index)
     * @param $index (index in array, e.g. 1)
     * @return number price
     */
    public function get_price($glass, $index)
    {
        if (!$index) return 0;
        $var = $glass;
        $arr = $this->$var;
        if ($arr['price'])
        {
            return $arr['price'];
        }
        else
        {
            if (is_array($arr))
            {
                foreach ($arr as $key => $value)
                {
                    if ($key == $index)
                    {
                        return $value['price'];
                    }
                }
            }
        }
        return 0;
    }

    /**
     * Calculate lens price.
     * @param $arr_glasses
     * @return float
     */
    public function price_glasses($arr_glasses)
    {
        $price_glasses = 0;
        if(count($arr_glasses)>0)
        {
            $arr_lenses = $this->get_lenses();
            $price_glasses += $arr_lenses[$arr_glasses['lenses']]['price'];
            $arr_lens_index = $this->get_lens_index();
            $price_glasses += $arr_lens_index[$arr_glasses['lens_index']]['price'];
            if($arr_glasses['color_tint']>0)
            {
                $arr_color_tint = $this->get_color_tint();
                $price_glasses += $arr_color_tint['price'];
            }
            if($arr_glasses['ptp']>0)
            {
                $arr_ptp = $this->get_ptp();
                $price_glasses += $arr_ptp['price'];
            }
            if($arr_glasses['ptv']>0)
            {
                $arr_ptv = $this->get_ptv();
                $price_glasses += $arr_ptv['price'];
            }
            if($arr_glasses['anti_r']>0)
            {
                $arr_anti_r = $this->get_anti_r();
                $price_glasses += $arr_anti_r['price'];
            }
        }
        return($price_glasses);
    }

    /**
     * Get prescriptions.
     * @return array
     */
    public function get_prescription($key = NULL)
    {
        $prescriptions = Session::instance()->get('cart_prescriptions');
        if ($key)
        {
            if (isset($prescriptions[$key]))
            {
                return $prescriptions[$key];
            }
            else
            {
                return NULL;
            }
        }
        return $prescriptions;
    }

    /**
     * Add prescription
     * @param string $key Cartitem key.
     * @param array $post
     */
    public function add_prescription($key, $post)
    {
        $prescription = array(
            'od_sph' => $post['od_sph'],
            'od_cyl' => $post['od_cyl'],
            'od_axis' => $post['od_axis'],
            'od_add' => $post['od_add'],
            'os_sph' => $post['os_sph'],
            'os_cyl' => $post['os_cyl'],
            'os_axis' => $post['os_axis'],
            'os_add' => $post['os_add'],
            'pd' => $post['pd'],
            'mono_r' => $post['mono_r'],
            'mono_l' => $post['mono_l'],
            'near_pd' => $post['near_pd'],
            'upload_prescription' => $post['upload_prescription'],
            'lens' => $post['lens'],
            'lens_index' => $post['lens_index'],
            'lens_index_color' => $post['lens_color'],
            'color_tint' => isset($post['chb_color_tint'])?true:null,
            'color_tint_color' => isset($post['chb_color_tint'])?$post['color_tint']:null,
            'ptp' => isset($post['chb_photochromic'])?true:null,
            'ptp_color' => isset($post['chb_photochromic'])?$post['photochromic_color']:null,
            'ptv' => isset($post['chb_transition'])?true:null,
            'ptv_color' => isset($post['chb_transition'])?$post['transition_color']:null,
            'anti' => isset($post['anti'])?true:null,
            'quantity' => $post['quantity'],
            'message' => $post['message'],
            'price' => 0,
        );
        $price = 0;
        foreach($prescription as $k => $v)
        {
            $price += self::get_price($k, $v);
        }
        $prescription['price'] = $price;

        $product_prescription = Session::instance()->get('cart_prescriptions');
        if (isset($product_prescription[$key]) && count($product_prescription[$key]))
        {
            foreach($product_prescription[$key] as $k => $v)
            {
                $diff = array_diff_assoc($v, $prescription);
                if ($diff)
                {
                    $product_prescription[$key][] = $prescription;
                }
                else
                {
                    $product_prescription[$key][$k]['quantity'] += $prescription['quantity'];
                }
            }
        }
        else
        {
            $product_prescription[$key][] = $prescription;
        }
        Session::instance()->set('cart_prescriptions', $product_prescription);
    }

    public function quantity($key, $ppkey, $quantity, $offset = TRUE)
    {
        $product_prescription = Session::instance()->get('cart_prescriptions');
        if (isset($product_prescription[$key]))
        {
            if (isset($product_prescription[$key][$ppkey]))
            {
                if($offset)
                    $product_prescription[$key][$ppkey]['quantity'] += $quantity;
                else
                    $product_prescription[$key][$ppkey]['quantity'] = $quantity;
                if ($product_prescription[$key][$ppkey]['quantity']<1)
                    unset($product_prescription[$key][$ppkey]);
                Session::instance()->set('cart_prescriptions', $product_prescription);
            }
        }
    }

    public function delete_prescription($key, $prescription_key)
    {
        $product_prescription = Session::instance()->get('cart_prescriptions');

        if(isset($product_prescription[$key]))
        {
            if (isset($product_prescription[$key][$prescription_key]))
            {
                unset($product_prescription[$key][$prescription_key]);
            }
            $empty = TRUE;
            foreach($product_prescription[$key] as $v)
            {
                if (count($v))
                    $empty = FALSE;
            }
            if ($empty)
            {
                unset($product_prescription[$key]);
            }
            Session::instance()->set('cart_prescriptions', $product_prescription);
            return TRUE;
        }
        return FALSE;
    }
}