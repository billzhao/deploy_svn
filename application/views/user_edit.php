<form id="frm-user-edit" action="" method="post">
    <input type="hidden" name="submit" value="true" />
    <?php if ($projects): ?>
    <ul id="project-list">
        <?php foreach ($projects as $project): ?>
        <li>
<div class="filter_condition_checkboxs">    
            <input type="checkbox" name="projects[]" value="<?php print $project->id; ?>" <?php print in_array($project->id, $checked) ? 'checked="checked"' : ''; ?> />
            <label><?php print $project->name; ?></label>
    </div>
        </li>
        <?php endforeach ?>
    </ul>
    <?php endif ?>
    <input type="submit" value="更新" />
    <?php print html::anchor('/user/list', '取消'); ?>
</form>
