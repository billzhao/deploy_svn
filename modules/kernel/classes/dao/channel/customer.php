<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @ClassName: DAO_Channel_Customer
 *
 * @author bzhao
 *
 * @version $Id: customer.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_Channel_Customer extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'channel_customer';
    
	public function getCustomerOrder($id)
	{
		return $this->orm($id)->orders->find_all()->as_array();
	}
	
    public function getCreditCard($id)
	{
		return $this->orm($id)->creditcards->find_all()->as_array();
	}
	
	public function getCustomerChannel($id)
	{
		//读取Customer的Channel信息
		return $this->orm($id)->channel->as_array();
	}
	

    
}