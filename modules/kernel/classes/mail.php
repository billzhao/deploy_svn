<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 邮件 
 * @version $Id$ 
 * @copyright 2012 Cofree Development Term
 */
class Mail
{
	/**
	 * 注册邮件 
	 * @param array $customer
	 */
	public static function send_register_customer($customer)
	{
		/*wendy zhao sales manager*/
        $from = 'icebearatv@gmail.com';
        
        /*判断*/
        $receivers = array($customer['email']);
		#$receivers = array($customer['email'], 'zhao.yang@cofreeonline.com');
        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'Welcome to www.icebearatv.com!';
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] Welcome to www.icebearatv.com!';
        }
        
        $content = View::factory('email/icebear/register_customer')
            ->set('customer', $customer)
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);
	}
	
	public static function send_confirm_order($order)
	{
		/*wendy zhao sales manager*/
        $from = 'icebearatv@gmail.com';
        
        $receivers = array($order['email'], 'icebearatv12@gmail.com', 'icebearatv8@gmail.com','zhao.jinmei@cofreeonline.com');
		#$receivers = array($customer['email'], 'zhao.yang@cofreeonline.com');
        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'Ice Bear Powersports: Your new Sales order of '.$order['erp_ordernum'];
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] Ice Bear Powersports: Your new Sales order of '.$order['erp_ordernum'];
        }
        
        $content = View::factory('email/icebear/confirm_order')
            ->set('order', $order)
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);
	}	
	
	public static function send_customer_order($order = null)
	{
        $from = 'icebearatv@gmail.com';

        if ( ! $order->get('salesman_id')
        	 OR ! $order->get('erp_ordernum'))
        {
        	return FALSE;
        }
        
        $user = Service_User::instance()->treeList(array('where' => array('erp_sale_id' => $order->get('salesman_id'))),array('name','email'));
        
        if ( ! isset($user[0]['name']) 
        	 OR empty($user[0]['name'])
        	 OR ! isset($user[0]['email'])
        	 OR empty($user[0]['email']))
        {
        	return FALSE;
        }
        
        $receivers = array($user[0]['email']);

        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'NEW SALES ORDER!';
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] NEW SALES ORDER!';
        }
       
        
        $content = View::factory('email/icebear/order')
            ->set('salename', $user[0]['name'])
            ->set('orderid',$order->get('id'))
            ->set('orderdate',$order->get('created'))
            ->set('ordernum',$order->get('erp_ordernum'))
            ->set('customername',Service_Channel_Customer::instance($order->get('customer_id'))->get('name'))
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);
	}	
	
	public static function send_ticket($ticket = array())
	{
		if ( ! $ticket)
			return FALSE;
			
        $from = $ticket['email'];
        $to = $ticket['to'];/*测试*/
        $subject = '[ICEBEAR] '.$ticket['subject'];
        
        $content = View::factory('email/icebear/ticket')
            ->set('content', htmlspecialchars($ticket['content']))
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);
	}
		
	
	
	/**
	 * 提示SalesManage激活客户
	 */
	public static function send_sales_active($customer)
	{
        $from = 'icebearatv@gmail.com';
        
        $receivers = array($customer['email']);

        #$receivers = array($customer['email'],'zhao.yang@cofreeonline.com');
        
        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'Activate your customer!';
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] Activate your customer!';
        }
        
        $content = View::factory('email/icebear/manage_customer')
            ->set('customer', $customer)
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);		
	}
	/**
	 * 提示SalesManage上传价目表
	 */
	public static function send_pricelist_upload($pricelist)
	{
        $from = 'icebearatv@gmail.com';
        if ($pricelist['file_type'] == '2')
        	$receivers = array('icebearatv12@gmail.com');
        else
        	$receivers = array('icebearatv12@gmail.com');

        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'Price List had been uploaded!';
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] Price List had been uploaded!';
        }
        
        $content = View::factory('email/icebear/upload_pricelist')
            ->set('pricelist', $pricelist)
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);		
	}

	/**
	 * 客户active
	 */
	public static function send_active_customer($customer)
	{
        $from = 'icebearatv@gmail.com';
        
        $receivers = array($customer['email']);

        $to = implode(',', $receivers);

        if (IN_PRODUCTION)
        {
            $subject = 'Your account is now (ACTIVE).';
        }
        else
        {
            $subject = '[ICEBEAR-TESTENV] Your account is now (ACTIVE).';
        }
        
        $content = View::factory('email/icebear/active_customer')
            ->set('customer', $customer)
            ->render();
        
        return self::sendMail($to, $from, $subject, $content);
	}
	
    /**
     * 替换邮件中的参数,并发送
     */
    public static function  sendMail($to = '',$from = '',$mailTitle = '',$mailContent = '',$param_mail = FALSE)
    {
    	/*数据完成后打开请求*/
      /**
       * 替换变量
       */
    	if ( ! IN_PRODUCTION)
    		return TRUE;
    	
        if($param_mail)
        {
            foreach($param_mail as $key=>$value)
            {
                $mailTitle = str_ireplace($key, $value,$mailTitle);
                $mailContent = str_ireplace($key, $value,$mailContent);
            }
        }

        if($to == '' OR $from == '')
        {
            return FALSE;
        }

         /**
         * 邮件头部信息
         */
        $headers='';
        $headers.= 'From: '.$from. "\r\n";
        $headers.= 'Reply-To: '.$from. "\r\n" ;
        $headers.= 'Content-type: text/html; charset=utf8' . "\r\n";

        //title 进行UTF-8的转换
        $title  ="=?UTF-8?B?".base64_encode($mailTitle)."?=";
//echo kohana::debug($to).kohana::debug($mailTitle).($mailContent).kohana::debug($headers);
        /**
         * mail函数发送邮件
         */
        try 
        {

            $mail = @mail($to, $mailTitle, $mailContent, $headers);
            return $mail;
        }
        catch(Exception  $e)
        {
            return FALSE;
        }
    }	

}
