<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * DAO User
 * 
 * @author zhao.yang
 *
 * @package DAO
 *
 * @version $Id: user.php 6343 2012-02-24 07:31:45Z zhao.yang $
 *
 * @copyright Cofree Develop Term
 */
class DAO_user extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'user';
    
    protected $disabled = 'disabled'; 
    
	public function getUserDetail($id)
	{
		//has one关系读取
		//ORM::factory('orderitem',1)->order->as_array();  OrderItem->Order返回Order信息
		return $this->orm($id)->detail->as_array();
	}
	
	public function getUserRole($id)
	{
		//has many关系读取
		return $this->orm($id)->roles->find_all();
	}
	
	/**
	 * 添加用户角色
	 * @return void
	 */
	public function setUserRole($userid,$roleid)
	{
		return $this->orm($userid)->add('roles', ORM::factory('role', $roleid));
	}

    
}