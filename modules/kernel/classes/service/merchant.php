<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 商户Service
 * @version $Id: merchant.php 7510 2012-08-31 03:26:01Z qin.yazhuo $ 
 * @copyright 2011 Cofree Development Term
 */
class Service_Merchant extends Service
{
    /**
     * DAO名称
     * @var string
     */
    private $_dao_object = 'merchant';
    
    /**
     * 单例data
     * @var array
     */
    private $_data;
    
    
    protected static $instances = array();
    
    
    /**
     * 实例商户
     * @param int $id 商户ID
     * @example
     * <code>
     * $arrMerchant = Service_Merchant::instance($merchant_id);
     * </code>
     */
    public static function & instance($id = 0)
    {  
        if ( ! isset(self::$instances[$id]))
        {
        	$class = __CLASS__;
            self::$instances[$id] = new $class($id);
        }
        
        return self::$instances[$id];
    }
    
	protected function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_load($id);
	}

	
	/**
	 * Merchant通用查询
	 * @param array $query_struct  查询结构体
	 * @param array $select	select数组
	 * @param string $key	结果集主键
	 * @param string $value key:value结果
	 * @return array 查询结果集
	 */
	public function tree($query_struct = array() 
						 ,$select = array() 
						 ,$key = NULL 
						 ,$value = NULL)
	{	
		return $this->dao($this->_dao_object)
			->db_find_all(
				$this->dao($this->_dao_object)->dbselect($select),
				$query_struct,
				$key,
				$value);
	}
	
	/**
	 * 返回单列
	 * @param array $query_struct
	 * @param string $index
	 */
	public function select($query_struct ,$index = 'id')
	{
		return $this->dao($this->_dao_object)->select_list($query_struct,$index);
	}
	
	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;

		if ($data = $this->dao($this->_dao_object)->find($id))
		{
			$this->_data = $data;
		}
	}
	
	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{	
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}
	
	/**
	 * 添加商户
	 * <code>        
	 *   $data['id'] = $id;
     *   $data['position'] = $order;
	 * </code> 
	 * @param array $data
	 */
	public function set($data = array())
	{
		/*判断email是否存在*/
		if ($this->isRegisterd($data['service_email']))
		{
			return FALSE;
		}
		
		if ( ! $merchant_id = $this->dao($this->_dao_object)->add($data))
		{
			return FALSE;
		}
		
		/*Session Merchant Id*/
		Session::instance()->set('merchant_id',$merchant_id);
		
		/*构造user*/
		$user_data = array();
		$user_data['name'] = 'MerchantAdmin-'.$merchant_id;
		$user_data['email'] = $data['service_email'];
		$user_data['merchant_id'] = $merchant_id;
		$user_data['password'] = '123456';
		
		/*check registered,pass register method*/
		if ( ! $user_id = Service_User::instance()->isRegisterd($user_data['email']));
		{
			/*添加User*/
			if ( ! $user_id = Service_User::instance()->register($user_data))
			{
				return FALSE;
			}	
		}
		
		/*add role merchant管理员*/
		return Service_User::instance()->setUserRole($user_id,2,$merchant_id,1);
		
		//TODO Mail::registerMerchant($data);发送注册邮件
		
	}
	
	/**
	 * 判断商户是否注册
	 * @param string $email
	 * @return boolen|merchant_id
	 */
	public function isRegisterd($email)
	{
		$query_struct = array(
			'where'	=> array('service_email' => $email));
		
		/*
		 * MerchantID判断
		 * @return merchant_id
		 */
		if ( $merchant = $this->dao($this->_dao_object)->find($query_struct))
		{
			return $merchant['id'];
		}
		return FALSE;
	}

	
	public function update($data)
	{
        if ( ! isset($data['id'])) 
        {
        	/*
        	 * Debug
        	 * throw new $this->exception('请求失败，ID为空，请重试！');
        	 */
            return FALSE;
        }
        
        /*
         * Config字段的校验
         */
        
        return $this->dao($this->_dao_object)->edit($data);

	}

	public function delete($id)
	{
		/*
		 * 校验和监听
		 * TODO 删除当前Merchant的资源信息
		 * 
		 */
		return $this->dao($this->_dao_object)->delete($id);
	}
	
	/**
     * 获取Merchant数量
     *
     * @param array $query_struct
     */
    public function count($query_struct)
    {
        return $this->dao($this->_dao_object)->count($query_struct);
    }

    
    /**
     * 得到Merchant可以管理的Channel类型
     */
    public function getChecked()
    {
    	$checked = array();
    	
		if ( ! isset($this->_data['config']))
		{
			return $checked();
		}
		
		if ( $config = explode('-',$this->_data['config']))
		{
			return $config;
		}
		
		return $checked;
		
    }
   
    public function getCategoryMapping()
    {
    	$category_mapping = '';
    	
		if ( ! isset($this->_data['category_config']))
		{
			return $category_mapping;
		}

		return $this->_data['category_config'];
		
    }
    /**
     *	得到可以管理的Channel信息 
     */
    public static function getChannel()
    {
    	return kohana::config("cloud.channel");
    }
    
    public static function getCategory()
    {
    	return kohana::config("cloud.category");
    }

    public static function getPriceCategory()
    {
    	return kohana::config("cloud.price_category");
    }
    /**
     * 得到 Merchant Channels
     * @param int $merchant_id
     */
    public function getMerchantChannel($merchant_id = NULL,$type = NULL)
    {
    	/*
    	 * TODO
    	 * 没有设置Merchant_id时，默认读取当前Merchant->_data['id]
    	 */
    	if ( ! $merchant_id)
		{
			$merchant_id = isset($this->_data['id']) && $this->_data['id'] ? $this->_data['id'] : NULL;
		}
		
    	return $this->dao($this->_dao_object)->getMerchantChannel($merchant_id,$type);
    }
      
    
}
