<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * Channel Service
 *
 * @ClassName: Service_Merchant_Category
 *
 * @author Spike.Qin
 *
 * @version $Id: $
 *
 * @copyright 2011 Cofree Development Term
 */
class Service_Merchant_Category extends Service
{
	/**
	 * DAO名称
	 * @var string
	 */
	private $_dao_object = 'merchant_category';

	private $_data = NULL;

	protected static $instances = array();

	public static function & instance($id = 0)
	{
		if ( ! isset(self::$instances[$id]))
		{
			$class = __CLASS__;
			self::$instances[$id] = new $class($id);
		}

		return self::$instances[$id];
	}


	protected  function __construct($id = 0)
	{
		$this->_data = NULL;
		$this->_dao = $this->dao($this->_dao_object);
		$this->_load($id);
	}

	/**
	 * condition where
	 * @param array $query_struct
	 * @param array $select
	 * @param string $key
	 * @param string $value
	 */
	public function tree($query_struct = array() ,$select = array() , $key = NULL ,$value = NULL)
	{
		return $this->_dao
		->db_find_all(
		$this->_dao->dbselect($select),
		$query_struct,
		$key,
		$value);
	}

	/**
	 * 重新加载 $this->data
	 * @param int $id
	 * @param string $type GET,UPDATE,ADD,DELETE
	 * @return void
	 */
	private function _load($id)
	{
		if( ! $id) return FALSE;
		//TODO 判断cache

		if ($data = $this->_dao->find($id))
		{
			$this->_data = $data;
		}
	}

	/**
	 * Load Data层数据
	 * @param string $field 字段名
	 * @return mixed
	 */

	public function get($field = NULL)
	{
		if ( ! $field)
		{
			return $this->_data;
		}
		else
		{
			return isset($this->_data[$field]) ? $this->_data[$field] : NULL;
		}
	}

	/**
	 * <code>
	 *   $data['id'] = $id;
	 * </code>
	 * @param array $data
	 */
	public function set($data = array())
	{
		return $this->_dao->add($data);
	}

	public function update($data)
	{
		if ( ! isset($data['id']))
		{
			return FALSE;
		}

		return $this->_dao->edit($data);
	}

	public function delete($data = array())
	{
		if (!$data
			|| !isset($data['merchant_id'])
			|| !isset($data['type'])
			|| !isset($data['flex_value']))
			return false;
		return DB::delete('categories')
			->where('merchant_id', '=', $data['merchant_id'])
			->where('type', '=', $data['type'])
			->where('flex_value', '=', $data['flex_value'])
			->execute();
	}

	public function count($query_struct)
	{
		return $this->_dao->count($query_struct);
	}

	public function add($data = array())
	{
		if (!$data
			|| !isset($data['merchant_id'])
			|| !isset($data['type'])
			|| !isset($data['flex_value']))
			return false;
		return DB::insert('categories', array_keys($data))
				->values(array_values($data))
				->execute();
	}
	
	/**
	 * 
	 * @param 
	 * @return mixed
	 */
	public function getCategoryValues($merchant_id=0)
	{
		$categorys = $this->tree(array('where'=>array('merchant_id'=>$merchant_id), 'orderby'=>'flex_value'));
		$category_array = array();
		foreach($categorys as $category)
		{
			$category_array[$category['type']][$category['flex_value']] = $category['desc'];
		}
		return $category_array;
	}
	
	public function getInventoryCategoryValues($merchant_id=0)
	{
		$current_categorys = $this->tree(array('where'=>array('merchant_id'=>$merchant_id, 'type' => 'SEGMENT1'), 'orderby'=>'flex_value'), array('flex_value'));
		$category_array = array();
		foreach ($current_categorys as $category)
		{
			$segment1 = $category['flex_value'];
			
			$category_sets = DB::query(Database::SELECT, 'SELECT DISTINCT `segments` FROM `orc_category_set` WHERE `name` LIKE "%inventory%" AND `segments` LIKE "903.000.000.%"')->execute();
			$set_arr = array();
			foreach ($category_sets as $category_set)
			{
				$sets = explode('.', $category_set['segments']);
				if (isset($sets[3]))
				{
					array_push($set_arr, $sets[3]);
				}
			}
			$segment4s = Service_Orc_Category::instance()->tree(array('where'=>array('type' => 'SEGMENT4', 'parent'=>$segment1), 'orderby'=>'flex_value'), array('flex_value', 'desc'));
			foreach ($segment4s as $segment4)
			{
				if (in_array($segment4['flex_value'], $set_arr))
				{
					$category_array[$segment1][$segment4['flex_value']] = $segment4['desc'];
				}
			}
		}
		return $category_array;
	}
	
	public function getMappedInventoryCategoryValues($merchant_id=0, $category_mapping)
	{
		$category_array = array();
		$erp_segments = kohana::config("category.erp_category");
		//$category_config = Service_Merchant::instance($this->merchant_id)->get('category_config');
		$category_config = $category_mapping;
		$category_segments = explode('.', $category_config);
		
		$pattern = (!(strpos($category_segments[1], 'category') === false) ? "%": $category_segments[1])
					.'.'.(!(strpos($category_segments[2], 'category') === false) ? "%": $category_segments[2])
					.'.'.(!(strpos($category_segments[3], 'category') === false) ? "%": $category_segments[3]);
		$category_segments_filtered = preg_grep("/category/i",$category_segments);
		$keys = array_keys($category_segments_filtered);
		$current_categorys = $this->tree(array('where'=>array('merchant_id'=>$merchant_id, 'type' => $erp_segments[0]), 'orderby'=>'flex_value'), array('flex_value','desc'));

			foreach ($current_categorys as $category)
			{
				$segment1 = $category['flex_value'];
				
				
				$segments = Service_Orc_Category::instance()->tree(array('where'=>array('parent'=>$segment1), 'orderby'=>'flex_value'), array('flex_value', 'desc', 'type'));
				$segmentsArr = array();
				foreach ($segments as $segment)
				{
					$segmentsArr[$segment['type']][$segment['flex_value']] = $segment;
				}
				
				$current_pattern = $segment1.'.'.$pattern;
				$category_sets = DB::query(Database::SELECT, 'SELECT DISTINCT `segments` FROM `orc_category_set` WHERE `name` LIKE "%inventory%" AND `segments` LIKE "'.$current_pattern.'"')->execute();
				
				foreach ($category_sets as $category_set)
				{
					$set_arr = explode('.', $category_set['segments']);
					if (count($set_arr) == 4)
					{
						$segment2 = $set_arr[1];
						$segment3 = $set_arr[2];
						$segment4 = $set_arr[3];
						
						if (key_exists($segment2, $segmentsArr[$erp_segments[1]])
						&& key_exists($segment3, $segmentsArr[$erp_segments[2]])
						&& key_exists($segment4, $segmentsArr[$erp_segments[3]]))
						{
							$category_array[$segment1][$segment2][$segment3][$segment4] = $segment4;
						}
					}
				}
			}
		return $category_array;
	}
	
	public function getMappedPriceCategoryValues($merchant_id=0, $price_category_mapping)
	{
		$category_array = array();
		$erp_segments = kohana::config("category.erp_category");
		//$category_config = Service_Merchant::instance($this->merchant_id)->get('category_config');
		$category_config = $price_category_mapping;
		$category_segments = explode('...', $category_config);
		$pattern = (!(strpos($category_segments[1], 'category') === false) ? "%": $category_segments[1]);
		$category_segments_filtered = preg_grep("/category/i",$category_segments);
		$keys = array_keys($category_segments_filtered);
		$current_categorys = $this->tree(array('where'=>array('merchant_id'=>$merchant_id, 'type' => $erp_segments[0]), 'orderby'=>'flex_value'), array('flex_value','desc'));
		
			foreach ($current_categorys as $category)
			{
				$segment1 = $category['flex_value'];
				
				
				$segments = Service_Orc_Category::instance()->tree(array('where'=>array('parent'=>$segment1), 'orderby'=>'flex_value'), array('flex_value', 'desc', 'type'));
				$segmentsArr = array();
				foreach ($segments as $segment)
				{
					$segmentsArr[$segment['type']][$segment['flex_value']] = $segment;
				}
				
				$current_pattern = $segment1.'...'.$pattern;
				$category_sets = DB::query(Database::SELECT, 'SELECT DISTINCT `segments` FROM `orc_category_set` WHERE `name` LIKE "%price%" AND `segments` LIKE "'.$current_pattern.'"')->execute();
				foreach ($category_sets as $category_set)
				{
					$set_arr = explode('...', $category_set['segments']);
					if (count($set_arr) == 2)
					{
						
						$segment2 = $set_arr[1];
						
						if (key_exists($segment2, $segmentsArr[$erp_segments[3]]))
						{
							$category_array[$segment1][$segment2] = $segment2;
						}
					}
				}
			}
		return $category_array;
	}

	
	public function getPriceCategoryValues($merchant_id=0)
	{
		$current_categorys = $this->tree(array('where'=>array('merchant_id'=>$merchant_id, 'type' => 'PSEGMENT1'), 'orderby'=>'flex_value'), array('flex_value'));
		$category_array = array();
		foreach ($current_categorys as $category)
		{
			$segment1 = $category['flex_value'];
			
			$category_sets = DB::query(Database::SELECT, 'SELECT DISTINCT `segments` FROM `orc_category_set` WHERE `name` LIKE "moto%price%" AND `segments` LIKE "903...%"')->execute();
			$set_arr = array();
			foreach ($category_sets as $category_set)
			{
				$sets = explode('.', $category_set['segments']);
				if (isset($sets[3]))
				{
					array_push($set_arr, $sets[3]);
				}
			}
			$segment4s = Service_Orc_Category::instance()->tree(array('where'=>array('type' => 'SEGMENT4', 'parent'=>$segment1), 'orderby'=>'flex_value'), array('flex_value', 'desc'));
			foreach ($segment4s as $segment4)
			{
				if (in_array($segment4['flex_value'], $set_arr))
				{
					$category_array[$segment1][$segment4['flex_value']] = $segment4['desc'];
				}
			}
		}
		return $category_array;
	}
}
