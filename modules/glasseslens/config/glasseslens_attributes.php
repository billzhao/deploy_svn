<?php
/**
 * The attributes id in database.
 */
return array(
    'gender_id' => 0,
    'color_id' => 0,
    'material_id' => 0,
    'bifocal_id' => 0,
    'circle_id' => 0,
    'style_id' => 0,
    //frame size
    'lenswidth_id' => 0,
    'lensheight_id' => 0,
    'bridge_id' => 0,
    'templelength_id' => 0,
    'framewidth_id' => 0,
);