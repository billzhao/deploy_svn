<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * 客户地址
 * @version $$Id$$ 
 * @copyright 2012 Cofree Development Term
 */
class Model_Channel_Customer_Address extends ORM
{
	
	protected $_filters = array(
		TRUE => array( 'trim' => NULL )
	);

    
    protected $_rules = array(
        'channel_id' => array(
            'not_empty' => NULL,
        ),
        'customer_id' => array(
            'not_empty' => NULL, 
        ), 
        'bill_id' => array(
            'not_empty' => NULL, 
        ), 
        'ship_id' => array(
            'max_length' => array(65535), 
        ), 
    );

		
    
}
