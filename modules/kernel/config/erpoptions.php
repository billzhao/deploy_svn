<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'lot_ctrl'	=>	array(
		'N'		=>	'N : Not In Use',
		'Y'		=>	'Y : In Use',
	),
	
	'serial_ctrl'	=>	array(
		'1'		=>	'1 : Not in Use',
		'2'		=>	'2 : Predifined',
		'5'		=>	'5 : At Receipt',
		'6'		=>	'6 : At Sales Issue',
	),
	'locator_ctrl'	=>	array(
		'1'		=>	'1 : Not in Use',
		'2'		=>	'2 : Prespecified',
		'5'		=>	'5 : Dynamic Entry',
	),
	
	
	'inventory_plan_method'	=>	array(
		'2'		=>	'2 : Min-Max',
		'1'		=>	'1 : Recorder Point',
		'6'		=>	'6 : Not Planned',
	),
);
