<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @ClassName: URL
 *
 * @author bzhao
 *
 * @version $Id: url.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class URL extends Kohana_URL 
{
    public static function current($_get = TRUE)
    {
        $request = Request::instance();
        if($_get)
        {
            $current_uri = URL::base().$request->uri.URL::query($_GET);
        }
        else
        {
            $current_uri = URL::base().$request->uri;
        }
        return $current_uri;
    }
}
