<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @ClassName: DAO_channel
 *
 * @author bzhao
 *
 * @version $Id: channel.php 6283 2012-02-16 09:27:48Z zhao.yang $
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_channel extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'channel';

	/**
	 * 得到站点的Docs
	 * @param int $id
	 * @return array
	 */
	public function getChannelDoc($id)
	{
		return $this->orm($id)->docs->find_all();
	}

	/**
	 * 得到站点的新闻
	 * @param int $id
	 * @return array
	 */
    public function getChannelNew($id)
    {
    	return $this->orm($id)->news->find_all();
    }

	/**
	 * 得到站点的Catalog
	 * @param int $id
	 * @return array
	 */
    public function getChannelCatalog($id)
    {
    	return $this->orm($id)->catalogs->find_all();
    }

	/**
	 * 得到站点的product
	 * @param int $id
	 * @return array
	 */
    public function getChannelProduct($id)
    {
    	return $this->orm($id)->products->find_all();
    }

}