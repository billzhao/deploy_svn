<?php defined('SYSPATH') or die('No direct script access.');
header("content-type:text/html;charset=utf-8");
/**
 * YRC Service Carrier
 *  Rate Quote
 * @version $Id:$ 
 * @copyright 2012 Cofree Development Term
 */
Class YrcService
{
	const YRC_SERVER_HOST = 'https://my.yrc.com/dynamic/national/servlet';
	const YRC_CONTROLLER = 'com.rdwy.ec.rexcommon.proxy.http.controller.ProxyApiController';
	const YRC_REDIR = '/tfq561';
	const YRC_LOGIN_USERID = '1icebear';
	const YRC_LOGIN_PASSWORD = 'icebear88';
	const YRC_BUSID = '70288644255';

	public static function ratequote($params=array())
	{
		if (! function_exists('curl_init'))
		{
			return false;
		}
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => self::YRC_SERVER_HOST.'?'.http_build_query($params),
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT => 60,
		));
		Kohana_Log::instance()->add("info",self::YRC_SERVER_HOST.'?'.http_build_query($params));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

		$response = curl_exec($ch);
		if (curl_errno($ch))
		{
			$errno = curl_errno($ch);
			$errstr = curl_error($ch);
			Message::setc("Could not connect to Server - Error($errno) $errstr");
			return false;
		}

		curl_close($ch);

		return $response;
	}

}