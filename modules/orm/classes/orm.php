<?php defined('SYSPATH') or die('No direct script access.');

/**
 * 
 * ORM init 
 * 
 * @author zhou.shuo
 *
 * @package Kohana_ORM
 *
 * @version $Id: orm.php 6283 2012-02-16 09:27:48Z zhao.yang $
 *
 * @copyright Cofree Develop Term
 */

class ORM extends Kohana_ORM 
{
	public $_saved = FALSE;
	
	public $_loaded = FALSE;
	
}