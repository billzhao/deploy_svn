<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * 
 * @ClassName: DAO_merchant
 *
 * @author bzhao
 *
 * @version $Id: merchant.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class DAO_merchant extends DAO {
    /**
     * 表名称
     *
     * @var string
     */
    protected $table_name = 'merchant';
    
    /**
     * disabled字段
     * @var string $disabled
     */
    //protected $disabled = 'disabled';
    
    /**
     * 得到商户的User
     * @param int $id
     */
	public function getMerchantUser($id)
	{
		return $this->orm($id)->users->find_all();
	}
	
    /**
     * 得到商户的product
     * @param int $id
     */
	public function getMerchantProduct($id)
	{
		return $this->orm($id)->products->find_all();
	}
	
	/**
	 * 得到商户的channel
     * @param int $id
     * @param string $type 类型
	 */
	public function getMerchantChannel($id ,$type = NULL)
	{
		if ( ! is_null($type))
			return $this->orm($id)->channels->where('config','=',$type)->find_all();
		return $this->orm($id)->channels->find_all();
	}
	
	/**
	 * 得到商户的context
     * @param int $id
	 */
	public function getMerchantContext($id)
	{
		return $this->orm($id)->contexts->find_all();
	}
	
	/**
	 * 得到商户的image
     * @param int $id
	 */
	public function getMerchantImage($id)
	{
		return $this->orm($id)->images->find_all();
	}
}