<?php defined('SYSPATH') or die('No direct script access.');
/**
 * B2B Controller的策略
 * @ClassName: Entity_Controller_B2B
 *
 * @author bzhao
 *
 * @version $Id: b2b.php 6283 2012-02-16 09:27:48Z zhao.yang $ 
 *
 * @copyright 2011 Cofree Development Term
 */
Interface Entity_Controller_B2B
{
	public function controllerB2BManageData();
}
