<?php defined('SYSPATH') or die('No direct script access.');

class Service_Channel_Cart extends Service
{
    protected static $instance;
    
    public static function & instance()
    {
        if( ! isset(self::$instance))
        {
            $class = __CLASS__;
            self::$instance = new $class();
        }
        return self::$instance;
    }

    /**
     * Get cart all, rewrite Wholesale_Cart::get()
     * @return array
     */
    public function get($customer_id = NULL)
    {

        $cart = array(
            'products' => $this->products(),
            'customer' => $this->customer(),
            'extend' => $this->extended(),
        );
        
        return $cart;
    }
 
    /**
     * Clear cart, rewrite Wholesale_Cart::get()
     */
    public function clear()
    {
        Session::instance()->delete('cart_products');
        Session::instance()->delete('extended');
        Session::instance()->delete('customer_info');  
    }


    public function extended($extended = NULL)
    {
        if ($extended)
        {
            Session::instance()->set('extended', $extended);
        }
        else
        {
            return Session::instance()->get('extended');
        }
    }
    
    public function products($products = NULL)
    {
        if ($products)
        {
            Session::instance()->set('cart_products', $products);
        }
        else
        {
            return Session::instance()->get('cart_products');
        }
    }    
    
    public function customer($customer = NULL)
    {
        if ($customer)
        {
            Session::instance()->set('customer_info', $customer);
        }
        else
        {
            return Session::instance()->get('customer_info');
        }
    }    

 
    
}

