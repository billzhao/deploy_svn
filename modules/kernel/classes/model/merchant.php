<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @ClassName: Model_Merchant
 *
 * @author bzhao
 *
 * @version $Id: merchant.php 7510 2012-08-31 03:26:01Z qin.yazhuo $ 
 *
 * @copyright 2011 Cofree Development Term
 */
class Model_Merchant extends ORM {

	/**
	 * 定义1对多关系
	 * @var array $_has_many
	 */
	protected $_has_many = array(
        'products' 	=> 	array('model' => 'product'), 
		'users'		=>	array('model' => 'user'),
		'channels'	=>	array('model' => 'channel'),
		//TODO 
		'contexts'	=>	array('model' => 'context'),
		'images'	=>	array('model' => 'image'),	
    );
	
    
    protected $_filters = array(
        TRUE => array('trim' => NULL)
    );
    
    /**
     * 定义Merchant字段规则 
     * @var array $_rules
     */
    protected $_rules = array(
    	'name' 			=> 	array('not_empty' => NULL,'max_length' => array(255)),//name
    	'description' 	=>	array('max_length' => array(255)),//description
    	'config'		=> 	array('not_empty' => NULL,'max_length' => array(255)),//config
    	'category_config'	=> 	array('not_empty' => NULL,'max_length' => array(255)),//category_config
    	'comment' 		=>	array('max_length' => array(255)),//comment
    	'service_email' =>  array('not_empty' => NULL),
    );
 
}

