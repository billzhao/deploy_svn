<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Server extends Controller_Base
{
    public function action_list()
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        $this->template->content = View::factory('server_list')
            ->set('servers', ORM::factory('server')->find_all()->as_array());
    }

    public function action_add()
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));

        if ($_POST) {
            if (isset($_POST['ip']))
                $_POST['ip'] = sprintf("%u", ip2long($_POST['ip']));

            $server = ORM::factory('server');
            $server->values($_POST + array(
                'created' => time(), 
            ));

            if ($server->check()) {
                $server->save();
                $this->request->redirect('/server/list');
            }
        }

        $this->template->content = View::factory('server_add');
    }
    
    
    public function action_delete($id)
    {
        $this->_is_admin() or die(header('HTTP/1.1 403 Forbidden'));
        $server = ORM::factory('server', $id);
        if (!$server->loaded()) {
            die(header('HTTP/1.1 404 Not Found'));
        }
    
        $server->delete_project_rel();
        $server->delete();

        $this->request->redirect('/server/list');
    }
}
