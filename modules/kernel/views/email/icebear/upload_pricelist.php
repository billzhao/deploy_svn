Dear Sales Manager<br/><br/>

<?php echo $pricelist['customer_id'] ? 'Price List for : '.Service_Channel_Customer::instance($pricelist['customer_id'])->get('name') : "General Price List";?> had been uploaded at <?php echo date("m/d/Y h:i:s", $pricelist['updated']);?>.<br/><br/>

If you have any questions or need assistance please contact our service team at: <a href="mailto:icebearatv@gmail.com">icebearatv@gmail.com</a>.<br/><br/><br/>

Kind Regards!<br/><br/>

Icebear team<br/><br/><br/>

Email: <a href="mailto:icebearatv@gmail.com">icebearatv@gmail.com<br/></a>
<a href="http://www.icebearatv.com/" target="_blank">http://www.icebearatv.com/</a>