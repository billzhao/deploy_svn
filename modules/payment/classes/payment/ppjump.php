<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * Payment PPjump
 * @category	Carrier
 * @author     Vincent
 * @copyright  (c) 2009-2012 Cofree
 */
class Payment_Ppjump extends Payment
{

	/**
	 * Paypal payment
	 * @param array $order	Order detail
	 * @param array $data		Paypal return
	 * @return stirng		SUCCESS
	 */
	public function pay($order, $data = NULL)
	{
		$payment_log_status = "";

		$order_update = array(
			'currency_payment' => $data['mc_currency'],
			'transaction_id' => $data['txn_id'],
			'payment_date' => time(),
			'updated' => time(),
			'billing_firstname' => $data['first_name'],
			'billing_lastname' => $data['last_name'],
			'billing_address' => $data['address_street'],
			'billing_zip' => $data['address_zip'],
			'billing_city' => $data['address_city'],
			'billing_state' => $data['address_state'],
			'billing_country' => $data['address_country'],
			'billing_phone' => '',
		);
		switch( $data['payment_status'] )
		{
			case 'Completed':
				if(($order['amount_payment'] + $data['mc_gross']) == $order['amount_order'])
				{
					//payment platform sync
					$post_var = "order_num=".$order['ordernum']
						."&order_amount=".$data['mc_gross']
						."&order_currency=".$data['mc_currency']
						."&card_num=".$order['cc_num']
						."&card_type=".$order['cc_type']
						."&card_cvv=".$order['cc_cvv']
						."&card_exp_month=".$order['cc_exp_month']
						."&card_exp_year=".$order['cc_exp_year']
						."&card_inssue=".$order['cc_issue']
						."&card_valid_month=".$order['cc_valid_month']
						."&card_valid_year=".$order['cc_valid_year']
						."&billing_firstname=".$data['first_name']
						."&billing_lastname=".$data['last_name']
						."&billing_address=".$data['address_street']
						."&billing_zip=".$data['address_zip']
						."&billing_city=".$data['address_city']
						."&billing_state=".$data['address_state']
						."&billing_country=".$data['address_country']
						."&billing_telephone=".''
						."&billing_ip_address=".long2ip($order['ip'])
						."&billing_email=".$order['email']
						."&shipping_firstname=".$order['shipping_firstname']
						."&shipping_lastname=".$order['shipping_lastname']
						."&shipping_address=".$order['shipping_address']
						."&shipping_zip=".$order['shipping_zip']
						."&shipping_city=".$order['shipping_city']
						."&shipping_state=".$order['shipping_state']
						."&shipping_country=".$order['shipping_country']
						."&shipping_telephone=".$order['shipping_phone']
						.'&trans_id='.$data['txn_id']
						.'&payer_email='.$data['payer_email']
						.'&receiver_email='.$data['receiver_email']
						."&is_extra_pp=".($order['amount'] <= 10 ? '1' : '0')
						."&site_id=".Site::instance()->get('cc_payment_id')
						."&secure_code=".Site::instance()->get('cc_secure_code');

//					if(in_array(Site::instance()->get('id'), array( '1' )) && $amount <= 10)
//					{
//						
//						$result = unserialize(stripcslashes(Toolkit::curl_pay(Site::instance()->get('https://www.shuiail.com/pp_need_not_verify'), $post_var)));
//					}
//					else
//					{
					$result = unserialize(stripcslashes(Toolkit::curl_pay(Site::instance()->get('pp_sync_url'), $post_var)));
//					}

					if(is_array($result))
					{
						$result['status_id'] = isset($result['status_id']) ? $result['status_id'] : '';
						$result['status'] = isset($result['status']) ? $result['status'] : '';
						$result['trans_id'] = isset($result['trans_id']) ? $result['trans_id'] : '';
						$result['message'] = isset($result['message']) ? $result['message'] : '';
						$result['api'] = isset($result['api']) ? $result['api'] : '';
						$result['avs'] = isset($result['avs']) ? $result['avs'] : '';
					}

					$order_update['amount_payment'] = $order['amount_payment'] + $data['mc_gross'];
					$order_update['payment_count'] = $order['payment_count'] + 1;
					$order_update['payment_status'] = 'success';
				}
				else
				{
					$order_update['amount_payment'] = $order['amount_payment'] + $data['mc_gross'];
					$order_update['payment_count'] = $order['payment_count'] + 1;
					$order_update['payment_status'] = 'partial_paid';
				}
				$payment_log_status = 'success';
				break;
			case 'Pending':
				$order_update['payment_count'] = $order['payment_count'] + 1;
				$order_update['payment_status'] = 'pending';
				$payment_log_status = 'pending';
				break;
			case 'Refunded':
				$order_update['refund_status'] = 'refund';
				$payment_log_status = 'refund';
				break;
			case 'Failed':
				$order_update['payment_status'] = 'failed';
				$payment_log_status = 'failed';
				break;
		}

		Order::instance($order['id'])->set($order_update);

		$payment_log = array(
			'site_id' => Site::instance()->get('id'),
			'order_id' => $order['id'],
			'customer_id' => $order['customer_id'],
			'payment_method' => $this->_config['name'],
			'trans_id' => $data['txn_id'],
			'amount' => $data['mc_gross'],
			'currency' => $data['mc_currency'],
			'comment' => $data['payment_status'],
			'cache' => serialize($data),
			'payment_status' => $payment_log_status,
			'ip' => ip2long(Request::$client_ip),
			'created' => time(),
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['payer_email'],
			'address' => $data['address_street'],
			'zip' => $data['address_zip'],
			'city' => $data['address_city'],
			'state' => $data['address_state'],
			'country' => $data['address_country'],
			'phone' => '',
		);
		$this->log($payment_log);

		return 'SUCCESS';
	}

	/**
	 * Paypal payment form
	 * @param string $name
	 * @param string $view
	 * @param <type> $order
	 * @param array $config
	 * @return string form
	 */
	public function form($name = NULL, $view = NULL, $order = NULL, $config = NULL)
	{
		if( ! $name)
		{
			$name = $this->_config['name'].'_form';
		}

		if( ! $view)
		{
			$view = 'default';
		}

		$config = array(
			'merchant_id' => isset($config['merchant_id']) ? $config['merchant_id'] : Site::instance()->get('pp_payment_id'),
			'notify_url' => isset($config['notify_url']) ? $config['notify_url'] : Site::instance()->get('pp_notify_url'),
			'return_url' => isset($config['return_url']) ? $config['return_url'] : Site::instance()->get('pp_return_url'),
			'cancel_return_url' => isset($config['cancel_return_url']) ? $config['cancel_return_url'] : Site::instance()->get('pp_cancel_return_url'),
			'pp_logo_url' => isset($config['pp_logo_url']) ? $config['pp_logo_url'] : Site::instance()->get('pp_logo_url'),
		);

		$form = View::factory('ppjump/'.$view)
			->set('name', $name)
			->set('action_url', Site::instance()->get('pp_payment_url'))
			->set('order', $order)
			->set('config', $config)
			->render();

		return $form;
	}

}
